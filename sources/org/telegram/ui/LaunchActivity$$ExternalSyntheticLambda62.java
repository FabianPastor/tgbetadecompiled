package org.telegram.ui;

public final /* synthetic */ class LaunchActivity$$ExternalSyntheticLambda62 implements Runnable {
    public final /* synthetic */ LaunchActivity f$0;
    public final /* synthetic */ ThemePreviewActivity f$1;

    public /* synthetic */ LaunchActivity$$ExternalSyntheticLambda62(LaunchActivity launchActivity, ThemePreviewActivity themePreviewActivity) {
        this.f$0 = launchActivity;
        this.f$1 = themePreviewActivity;
    }

    public final void run() {
        this.f$0.lambda$runLinkRequest$59(this.f$1);
    }
}
