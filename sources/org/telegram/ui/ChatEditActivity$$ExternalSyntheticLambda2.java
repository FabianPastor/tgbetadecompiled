package org.telegram.ui;

import android.content.DialogInterface;

public final /* synthetic */ class ChatEditActivity$$ExternalSyntheticLambda2 implements DialogInterface.OnClickListener {
    public final /* synthetic */ ChatEditActivity f$0;

    public /* synthetic */ ChatEditActivity$$ExternalSyntheticLambda2(ChatEditActivity chatEditActivity) {
        this.f$0 = chatEditActivity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f$0.lambda$checkDiscard$27(dialogInterface, i);
    }
}
