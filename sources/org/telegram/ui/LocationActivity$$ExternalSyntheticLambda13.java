package org.telegram.ui;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;

public final /* synthetic */ class LocationActivity$$ExternalSyntheticLambda13 implements OnMapReadyCallback {
    public final /* synthetic */ LocationActivity f$0;

    public /* synthetic */ LocationActivity$$ExternalSyntheticLambda13(LocationActivity locationActivity) {
        this.f$0 = locationActivity;
    }

    public final void onMapReady(GoogleMap googleMap) {
        this.f$0.lambda$createView$15(googleMap);
    }
}
