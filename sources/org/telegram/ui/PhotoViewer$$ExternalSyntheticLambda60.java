package org.telegram.ui;

public final /* synthetic */ class PhotoViewer$$ExternalSyntheticLambda60 implements Runnable {
    public final /* synthetic */ PhotoViewer f$0;
    public final /* synthetic */ String f$1;
    public final /* synthetic */ boolean f$2;

    public /* synthetic */ PhotoViewer$$ExternalSyntheticLambda60(PhotoViewer photoViewer, String str, boolean z) {
        this.f$0 = photoViewer;
        this.f$1 = str;
        this.f$2 = z;
    }

    public final void run() {
        this.f$0.lambda$detectFaces$54(this.f$1, this.f$2);
    }
}
