package org.telegram.ui;

public final /* synthetic */ class ThemeActivity$$ExternalSyntheticLambda7 implements Runnable {
    public final /* synthetic */ ThemeActivity f$0;
    public final /* synthetic */ String f$1;

    public /* synthetic */ ThemeActivity$$ExternalSyntheticLambda7(ThemeActivity themeActivity, String str) {
        this.f$0 = themeActivity;
        this.f$1 = str;
    }

    public final void run() {
        this.f$0.lambda$updateSunTime$8(this.f$1);
    }
}
