package org.telegram.ui;

import java.util.ArrayList;

public final /* synthetic */ class NotificationsSettingsActivity$$ExternalSyntheticLambda6 implements Runnable {
    public final /* synthetic */ NotificationsSettingsActivity f$0;
    public final /* synthetic */ ArrayList f$1;
    public final /* synthetic */ ArrayList f$2;
    public final /* synthetic */ ArrayList f$3;
    public final /* synthetic */ ArrayList f$4;
    public final /* synthetic */ ArrayList f$5;
    public final /* synthetic */ ArrayList f$6;

    public /* synthetic */ NotificationsSettingsActivity$$ExternalSyntheticLambda6(NotificationsSettingsActivity notificationsSettingsActivity, ArrayList arrayList, ArrayList arrayList2, ArrayList arrayList3, ArrayList arrayList4, ArrayList arrayList5, ArrayList arrayList6) {
        this.f$0 = notificationsSettingsActivity;
        this.f$1 = arrayList;
        this.f$2 = arrayList2;
        this.f$3 = arrayList3;
        this.f$4 = arrayList4;
        this.f$5 = arrayList5;
        this.f$6 = arrayList6;
    }

    public final void run() {
        this.f$0.lambda$loadExceptions$0(this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, this.f$6);
    }
}
