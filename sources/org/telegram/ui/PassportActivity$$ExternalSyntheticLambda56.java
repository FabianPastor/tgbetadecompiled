package org.telegram.ui;

import org.telegram.tgnet.TLObject;

public final /* synthetic */ class PassportActivity$$ExternalSyntheticLambda56 implements Runnable {
    public final /* synthetic */ PassportActivity f$0;
    public final /* synthetic */ TLObject f$1;

    public /* synthetic */ PassportActivity$$ExternalSyntheticLambda56(PassportActivity passportActivity, TLObject tLObject) {
        this.f$0 = passportActivity;
        this.f$1 = tLObject;
    }

    public final void run() {
        this.f$0.lambda$loadPasswordInfo$3(this.f$1);
    }
}
