package org.telegram.ui;

import android.animation.AnimatorSet;

public final /* synthetic */ class ProfileActivity$$ExternalSyntheticLambda18 implements Runnable {
    public final /* synthetic */ AnimatorSet f$0;

    public /* synthetic */ ProfileActivity$$ExternalSyntheticLambda18(AnimatorSet animatorSet) {
        this.f$0 = animatorSet;
    }

    public final void run() {
        this.f$0.start();
    }
}
