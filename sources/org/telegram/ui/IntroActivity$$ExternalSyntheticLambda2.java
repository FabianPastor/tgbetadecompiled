package org.telegram.ui;

import android.view.View;
import org.telegram.ui.Components.RLottieImageView;

public final /* synthetic */ class IntroActivity$$ExternalSyntheticLambda2 implements View.OnClickListener {
    public final /* synthetic */ IntroActivity f$0;
    public final /* synthetic */ RLottieImageView f$1;

    public /* synthetic */ IntroActivity$$ExternalSyntheticLambda2(IntroActivity introActivity, RLottieImageView rLottieImageView) {
        this.f$0 = introActivity;
        this.f$1 = rLottieImageView;
    }

    public final void onClick(View view) {
        this.f$0.lambda$createView$0(this.f$1, view);
    }
}
