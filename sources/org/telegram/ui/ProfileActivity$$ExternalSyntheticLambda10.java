package org.telegram.ui;

import android.content.DialogInterface;

public final /* synthetic */ class ProfileActivity$$ExternalSyntheticLambda10 implements DialogInterface.OnDismissListener {
    public final /* synthetic */ ProfileActivity f$0;

    public /* synthetic */ ProfileActivity$$ExternalSyntheticLambda10(ProfileActivity profileActivity) {
        this.f$0 = profileActivity;
    }

    public final void onDismiss(DialogInterface dialogInterface) {
        this.f$0.lambda$onWriteButtonClick$16(dialogInterface);
    }
}
