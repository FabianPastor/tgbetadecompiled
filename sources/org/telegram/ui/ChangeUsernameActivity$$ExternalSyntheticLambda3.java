package org.telegram.ui;

public final /* synthetic */ class ChangeUsernameActivity$$ExternalSyntheticLambda3 implements Runnable {
    public final /* synthetic */ ChangeUsernameActivity f$0;
    public final /* synthetic */ String f$1;

    public /* synthetic */ ChangeUsernameActivity$$ExternalSyntheticLambda3(ChangeUsernameActivity changeUsernameActivity, String str) {
        this.f$0 = changeUsernameActivity;
        this.f$1 = str;
    }

    public final void run() {
        this.f$0.lambda$checkUserName$4(this.f$1);
    }
}
