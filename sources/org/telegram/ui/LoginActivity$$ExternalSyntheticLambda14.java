package org.telegram.ui;

import org.telegram.ui.LoginActivity;

public final /* synthetic */ class LoginActivity$$ExternalSyntheticLambda14 implements Runnable {
    public final /* synthetic */ LoginActivity.LoginActivityRegisterView f$0;

    public /* synthetic */ LoginActivity$$ExternalSyntheticLambda14(LoginActivity.LoginActivityRegisterView loginActivityRegisterView) {
        this.f$0 = loginActivityRegisterView;
    }

    public final void run() {
        this.f$0.imageUpdater.openGallery();
    }
}
