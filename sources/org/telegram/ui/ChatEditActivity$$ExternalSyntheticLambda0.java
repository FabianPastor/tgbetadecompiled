package org.telegram.ui;

import android.content.DialogInterface;

public final /* synthetic */ class ChatEditActivity$$ExternalSyntheticLambda0 implements DialogInterface.OnCancelListener {
    public final /* synthetic */ ChatEditActivity f$0;

    public /* synthetic */ ChatEditActivity$$ExternalSyntheticLambda0(ChatEditActivity chatEditActivity) {
        this.f$0 = chatEditActivity;
    }

    public final void onCancel(DialogInterface dialogInterface) {
        this.f$0.lambda$processDone$30(dialogInterface);
    }
}
