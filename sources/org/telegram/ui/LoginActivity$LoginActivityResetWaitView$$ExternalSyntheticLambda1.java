package org.telegram.ui;

import android.view.View;
import org.telegram.ui.LoginActivity;

public final /* synthetic */ class LoginActivity$LoginActivityResetWaitView$$ExternalSyntheticLambda1 implements View.OnClickListener {
    public final /* synthetic */ LoginActivity.LoginActivityResetWaitView f$0;

    public /* synthetic */ LoginActivity$LoginActivityResetWaitView$$ExternalSyntheticLambda1(LoginActivity.LoginActivityResetWaitView loginActivityResetWaitView) {
        this.f$0 = loginActivityResetWaitView;
    }

    public final void onClick(View view) {
        this.f$0.lambda$new$3(view);
    }
}
