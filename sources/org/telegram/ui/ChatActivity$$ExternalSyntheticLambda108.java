package org.telegram.ui;

import android.view.MotionEvent;
import android.view.View;

public final /* synthetic */ class ChatActivity$$ExternalSyntheticLambda108 implements View.OnTouchListener {
    public static final /* synthetic */ ChatActivity$$ExternalSyntheticLambda108 INSTANCE = new ChatActivity$$ExternalSyntheticLambda108();

    private /* synthetic */ ChatActivity$$ExternalSyntheticLambda108() {
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return ChatActivity.lambda$createView$56(view, motionEvent);
    }
}
