package org.telegram.ui;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class ChatLinkActivity$$ExternalSyntheticLambda18 implements RecyclerListView.OnItemClickListener {
    public final /* synthetic */ ChatLinkActivity f$0;

    public /* synthetic */ ChatLinkActivity$$ExternalSyntheticLambda18(ChatLinkActivity chatLinkActivity) {
        this.f$0 = chatLinkActivity;
    }

    public final void onItemClick(View view, int i) {
        this.f$0.lambda$createView$6(view, i);
    }
}
