package org.telegram.ui;

public final /* synthetic */ class ChatActivity$$ExternalSyntheticLambda165 implements Runnable {
    public final /* synthetic */ ChatActivity f$0;
    public final /* synthetic */ int f$1;
    public final /* synthetic */ int f$2;
    public final /* synthetic */ boolean f$3;

    public /* synthetic */ ChatActivity$$ExternalSyntheticLambda165(ChatActivity chatActivity, int i, int i2, boolean z) {
        this.f$0 = chatActivity;
        this.f$1 = i;
        this.f$2 = i2;
        this.f$3 = z;
    }

    public final void run() {
        this.f$0.lambda$deleteHistory$3(this.f$1, this.f$2, this.f$3);
    }
}
