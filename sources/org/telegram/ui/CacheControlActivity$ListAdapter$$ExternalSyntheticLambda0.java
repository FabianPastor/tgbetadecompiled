package org.telegram.ui;

import org.telegram.ui.CacheControlActivity;
import org.telegram.ui.Components.SlideChooseView;

public final /* synthetic */ class CacheControlActivity$ListAdapter$$ExternalSyntheticLambda0 implements SlideChooseView.Callback {
    public static final /* synthetic */ CacheControlActivity$ListAdapter$$ExternalSyntheticLambda0 INSTANCE = new CacheControlActivity$ListAdapter$$ExternalSyntheticLambda0();

    private /* synthetic */ CacheControlActivity$ListAdapter$$ExternalSyntheticLambda0() {
    }

    public final void onOptionSelected(int i) {
        CacheControlActivity.ListAdapter.lambda$onCreateViewHolder$0(i);
    }

    public /* synthetic */ void onTouchEnd() {
        SlideChooseView.Callback.CC.$default$onTouchEnd(this);
    }
}
