package org.telegram.ui;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class ChatActivity$$ExternalSyntheticLambda249 implements RecyclerListView.OnItemClickListener {
    public final /* synthetic */ ChatActivity f$0;

    public /* synthetic */ ChatActivity$$ExternalSyntheticLambda249(ChatActivity chatActivity) {
        this.f$0 = chatActivity;
    }

    public final void onItemClick(View view, int i) {
        this.f$0.lambda$initStickers$90(view, i);
    }
}
