package org.telegram.ui;

import android.view.View;
import android.view.ViewTreeObserver;

public final /* synthetic */ class LaunchActivity$$ExternalSyntheticLambda20 implements ViewTreeObserver.OnGlobalLayoutListener {
    public final /* synthetic */ View f$0;

    public /* synthetic */ LaunchActivity$$ExternalSyntheticLambda20(View view) {
        this.f$0 = view;
    }

    public final void onGlobalLayout() {
        LaunchActivity.lambda$onCreate$6(this.f$0);
    }
}
