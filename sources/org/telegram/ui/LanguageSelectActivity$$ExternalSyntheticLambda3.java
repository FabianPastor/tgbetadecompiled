package org.telegram.ui;

import java.util.ArrayList;

public final /* synthetic */ class LanguageSelectActivity$$ExternalSyntheticLambda3 implements Runnable {
    public final /* synthetic */ LanguageSelectActivity f$0;
    public final /* synthetic */ ArrayList f$1;

    public /* synthetic */ LanguageSelectActivity$$ExternalSyntheticLambda3(LanguageSelectActivity languageSelectActivity, ArrayList arrayList) {
        this.f$0 = languageSelectActivity;
        this.f$1 = arrayList;
    }

    public final void run() {
        this.f$0.lambda$updateSearchResults$7(this.f$1);
    }
}
