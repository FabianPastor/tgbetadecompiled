package org.telegram.ui;

import android.view.KeyEvent;
import android.widget.TextView;

public final /* synthetic */ class ChangeBioActivity$$ExternalSyntheticLambda2 implements TextView.OnEditorActionListener {
    public final /* synthetic */ ChangeBioActivity f$0;

    public /* synthetic */ ChangeBioActivity$$ExternalSyntheticLambda2(ChangeBioActivity changeBioActivity) {
        this.f$0 = changeBioActivity;
    }

    public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        return this.f$0.lambda$createView$1(textView, i, keyEvent);
    }
}
