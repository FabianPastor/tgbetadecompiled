package org.telegram.ui;

import android.animation.ValueAnimator;

public final /* synthetic */ class ProfileActivity$$ExternalSyntheticLambda2 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ ProfileActivity f$0;

    public /* synthetic */ ProfileActivity$$ExternalSyntheticLambda2(ProfileActivity profileActivity) {
        this.f$0 = profileActivity;
    }

    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f$0.lambda$dimBehindView$39(valueAnimator);
    }
}
