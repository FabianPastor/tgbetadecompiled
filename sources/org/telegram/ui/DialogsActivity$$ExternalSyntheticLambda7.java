package org.telegram.ui;

import android.content.DialogInterface;

public final /* synthetic */ class DialogsActivity$$ExternalSyntheticLambda7 implements DialogInterface.OnClickListener {
    public final /* synthetic */ DialogsActivity f$0;

    public /* synthetic */ DialogsActivity$$ExternalSyntheticLambda7(DialogsActivity dialogsActivity) {
        this.f$0 = dialogsActivity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f$0.lambda$performSelectedDialogsAction$36(dialogInterface, i);
    }
}
