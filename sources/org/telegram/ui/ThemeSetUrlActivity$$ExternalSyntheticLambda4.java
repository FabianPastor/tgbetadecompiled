package org.telegram.ui;

import android.view.MotionEvent;
import android.view.View;

public final /* synthetic */ class ThemeSetUrlActivity$$ExternalSyntheticLambda4 implements View.OnTouchListener {
    public static final /* synthetic */ ThemeSetUrlActivity$$ExternalSyntheticLambda4 INSTANCE = new ThemeSetUrlActivity$$ExternalSyntheticLambda4();

    private /* synthetic */ ThemeSetUrlActivity$$ExternalSyntheticLambda4() {
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return ThemeSetUrlActivity.lambda$createView$4(view, motionEvent);
    }
}
