package org.telegram.ui;

import android.view.View;
import org.telegram.ui.CallLogActivity;

public final /* synthetic */ class CallLogActivity$EmptyTextProgressView$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ CallLogActivity.EmptyTextProgressView f$0;

    public /* synthetic */ CallLogActivity$EmptyTextProgressView$$ExternalSyntheticLambda0(CallLogActivity.EmptyTextProgressView emptyTextProgressView) {
        this.f$0 = emptyTextProgressView;
    }

    public final void onClick(View view) {
        this.f$0.lambda$new$0(view);
    }
}
