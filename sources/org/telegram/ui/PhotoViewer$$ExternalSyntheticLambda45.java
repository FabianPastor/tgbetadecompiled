package org.telegram.ui;

import android.view.MotionEvent;
import android.view.View;

public final /* synthetic */ class PhotoViewer$$ExternalSyntheticLambda45 implements View.OnTouchListener {
    public static final /* synthetic */ PhotoViewer$$ExternalSyntheticLambda45 INSTANCE = new PhotoViewer$$ExternalSyntheticLambda45();

    private /* synthetic */ PhotoViewer$$ExternalSyntheticLambda45() {
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return PhotoViewer.lambda$setParentActivity$24(view, motionEvent);
    }
}
