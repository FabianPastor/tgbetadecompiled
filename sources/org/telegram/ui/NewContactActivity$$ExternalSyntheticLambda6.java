package org.telegram.ui;

import android.view.KeyEvent;
import android.widget.TextView;

public final /* synthetic */ class NewContactActivity$$ExternalSyntheticLambda6 implements TextView.OnEditorActionListener {
    public final /* synthetic */ NewContactActivity f$0;

    public /* synthetic */ NewContactActivity$$ExternalSyntheticLambda6(NewContactActivity newContactActivity) {
        this.f$0 = newContactActivity;
    }

    public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        return this.f$0.lambda$createView$2(textView, i, keyEvent);
    }
}
