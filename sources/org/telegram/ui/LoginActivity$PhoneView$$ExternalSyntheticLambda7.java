package org.telegram.ui;

import android.content.Context;
import android.view.View;
import android.widget.ViewSwitcher;
import org.telegram.ui.LoginActivity;

public final /* synthetic */ class LoginActivity$PhoneView$$ExternalSyntheticLambda7 implements ViewSwitcher.ViewFactory {
    public final /* synthetic */ Context f$0;

    public /* synthetic */ LoginActivity$PhoneView$$ExternalSyntheticLambda7(Context context) {
        this.f$0 = context;
    }

    public final View makeView() {
        return LoginActivity.PhoneView.lambda$new$0(this.f$0);
    }
}
