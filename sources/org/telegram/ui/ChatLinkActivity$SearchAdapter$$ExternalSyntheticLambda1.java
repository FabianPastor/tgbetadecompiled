package org.telegram.ui;

import org.telegram.ui.ChatLinkActivity;

public final /* synthetic */ class ChatLinkActivity$SearchAdapter$$ExternalSyntheticLambda1 implements Runnable {
    public final /* synthetic */ ChatLinkActivity.SearchAdapter f$0;
    public final /* synthetic */ String f$1;

    public /* synthetic */ ChatLinkActivity$SearchAdapter$$ExternalSyntheticLambda1(ChatLinkActivity.SearchAdapter searchAdapter, String str) {
        this.f$0 = searchAdapter;
        this.f$1 = str;
    }

    public final void run() {
        this.f$0.lambda$processSearch$2(this.f$1);
    }
}
