package org.telegram.ui;

import android.content.DialogInterface;

public final /* synthetic */ class PeopleNearbyActivity$$ExternalSyntheticLambda0 implements DialogInterface.OnCancelListener {
    public final /* synthetic */ PeopleNearbyActivity f$0;

    public /* synthetic */ PeopleNearbyActivity$$ExternalSyntheticLambda0(PeopleNearbyActivity peopleNearbyActivity) {
        this.f$0 = peopleNearbyActivity;
    }

    public final void onCancel(DialogInterface dialogInterface) {
        this.f$0.lambda$createView$0(dialogInterface);
    }
}
