package org.telegram.ui;

import android.content.DialogInterface;
import org.telegram.ui.GroupCallActivity;

public final /* synthetic */ class GroupCallActivity$6$$ExternalSyntheticLambda4 implements DialogInterface.OnClickListener {
    public final /* synthetic */ GroupCallActivity.AnonymousClass6 f$0;
    public final /* synthetic */ boolean f$1;

    public /* synthetic */ GroupCallActivity$6$$ExternalSyntheticLambda4(GroupCallActivity.AnonymousClass6 r1, boolean z) {
        this.f$0 = r1;
        this.f$1 = z;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f$0.lambda$onItemClick$2(this.f$1, dialogInterface, i);
    }
}
