package org.telegram.ui;

import org.telegram.tgnet.TLRPC$TL_error;

public final /* synthetic */ class GroupCallActivity$$ExternalSyntheticLambda44 implements Runnable {
    public final /* synthetic */ GroupCallActivity f$0;
    public final /* synthetic */ TLRPC$TL_error f$1;

    public /* synthetic */ GroupCallActivity$$ExternalSyntheticLambda44(GroupCallActivity groupCallActivity, TLRPC$TL_error tLRPC$TL_error) {
        this.f$0 = groupCallActivity;
        this.f$1 = tLRPC$TL_error;
    }

    public final void run() {
        this.f$0.lambda$new$27(this.f$1);
    }
}
