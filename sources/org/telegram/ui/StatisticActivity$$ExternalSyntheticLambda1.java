package org.telegram.ui;

import java.util.ArrayList;

public final /* synthetic */ class StatisticActivity$$ExternalSyntheticLambda1 implements Runnable {
    public final /* synthetic */ StatisticActivity f$0;
    public final /* synthetic */ ArrayList f$1;

    public /* synthetic */ StatisticActivity$$ExternalSyntheticLambda1(StatisticActivity statisticActivity, ArrayList arrayList) {
        this.f$0 = statisticActivity;
        this.f$1 = arrayList;
    }

    public final void run() {
        this.f$0.lambda$loadMessages$6(this.f$1);
    }
}
