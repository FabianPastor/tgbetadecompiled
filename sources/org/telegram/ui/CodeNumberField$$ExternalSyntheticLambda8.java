package org.telegram.ui;

import org.telegram.ui.Components.SimpleFloatPropertyCompat;

public final /* synthetic */ class CodeNumberField$$ExternalSyntheticLambda8 implements SimpleFloatPropertyCompat.Setter {
    public static final /* synthetic */ CodeNumberField$$ExternalSyntheticLambda8 INSTANCE = new CodeNumberField$$ExternalSyntheticLambda8();

    private /* synthetic */ CodeNumberField$$ExternalSyntheticLambda8() {
    }

    public final void set(Object obj, float f) {
        CodeNumberField.lambda$static$7((CodeNumberField) obj, f);
    }
}
