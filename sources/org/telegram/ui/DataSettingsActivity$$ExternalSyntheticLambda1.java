package org.telegram.ui;

import android.content.DialogInterface;

public final /* synthetic */ class DataSettingsActivity$$ExternalSyntheticLambda1 implements DialogInterface.OnClickListener {
    public final /* synthetic */ DataSettingsActivity f$0;

    public /* synthetic */ DataSettingsActivity$$ExternalSyntheticLambda1(DataSettingsActivity dataSettingsActivity) {
        this.f$0 = dataSettingsActivity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f$0.lambda$createView$5(dialogInterface, i);
    }
}
