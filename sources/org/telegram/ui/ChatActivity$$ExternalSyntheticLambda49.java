package org.telegram.ui;

import android.content.DialogInterface;

public final /* synthetic */ class ChatActivity$$ExternalSyntheticLambda49 implements DialogInterface.OnDismissListener {
    public final /* synthetic */ ChatActivity f$0;

    public /* synthetic */ ChatActivity$$ExternalSyntheticLambda49(ChatActivity chatActivity) {
        this.f$0 = chatActivity;
    }

    public final void onDismiss(DialogInterface dialogInterface) {
        this.f$0.lambda$onResume$148(dialogInterface);
    }
}
