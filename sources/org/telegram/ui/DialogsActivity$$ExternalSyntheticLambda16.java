package org.telegram.ui;

import android.content.DialogInterface;

public final /* synthetic */ class DialogsActivity$$ExternalSyntheticLambda16 implements DialogInterface.OnDismissListener {
    public final /* synthetic */ DialogsActivity f$0;

    public /* synthetic */ DialogsActivity$$ExternalSyntheticLambda16(DialogsActivity dialogsActivity) {
        this.f$0 = dialogsActivity;
    }

    public final void onDismiss(DialogInterface dialogInterface) {
        this.f$0.lambda$showSuggestion$47(dialogInterface);
    }
}
