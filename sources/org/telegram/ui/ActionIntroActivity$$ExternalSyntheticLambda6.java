package org.telegram.ui;

import android.view.MotionEvent;
import android.view.View;

public final /* synthetic */ class ActionIntroActivity$$ExternalSyntheticLambda6 implements View.OnTouchListener {
    public static final /* synthetic */ ActionIntroActivity$$ExternalSyntheticLambda6 INSTANCE = new ActionIntroActivity$$ExternalSyntheticLambda6();

    private /* synthetic */ ActionIntroActivity$$ExternalSyntheticLambda6() {
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return ActionIntroActivity.lambda$createView$0(view, motionEvent);
    }
}
