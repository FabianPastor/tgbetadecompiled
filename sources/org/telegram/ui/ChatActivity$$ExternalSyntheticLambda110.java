package org.telegram.ui;

import android.widget.PopupWindow;

public final /* synthetic */ class ChatActivity$$ExternalSyntheticLambda110 implements PopupWindow.OnDismissListener {
    public final /* synthetic */ ChatActivity f$0;

    public /* synthetic */ ChatActivity$$ExternalSyntheticLambda110(ChatActivity chatActivity) {
        this.f$0 = chatActivity;
    }

    public final void onDismiss() {
        this.f$0.lambda$createView$52();
    }
}
