package org.telegram.ui;

import android.content.DialogInterface;

public final /* synthetic */ class ChatEditActivity$$ExternalSyntheticLambda3 implements DialogInterface.OnDismissListener {
    public final /* synthetic */ ChatEditActivity f$0;

    public /* synthetic */ ChatEditActivity$$ExternalSyntheticLambda3(ChatEditActivity chatEditActivity) {
        this.f$0 = chatEditActivity;
    }

    public final void onDismiss(DialogInterface dialogInterface) {
        this.f$0.lambda$createView$5(dialogInterface);
    }
}
