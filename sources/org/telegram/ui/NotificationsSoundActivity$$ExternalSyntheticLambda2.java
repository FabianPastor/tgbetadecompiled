package org.telegram.ui;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class NotificationsSoundActivity$$ExternalSyntheticLambda2 implements RecyclerListView.OnItemLongClickListener {
    public final /* synthetic */ NotificationsSoundActivity f$0;

    public /* synthetic */ NotificationsSoundActivity$$ExternalSyntheticLambda2(NotificationsSoundActivity notificationsSoundActivity) {
        this.f$0 = notificationsSoundActivity;
    }

    public final boolean onItemClick(View view, int i) {
        return this.f$0.lambda$createView$2(view, i);
    }
}
