package org.telegram.ui;

public final /* synthetic */ class PasscodeActivity$$ExternalSyntheticLambda20 implements Runnable {
    public final /* synthetic */ PasscodeActivity f$0;
    public final /* synthetic */ boolean f$1;
    public final /* synthetic */ boolean f$2;

    public /* synthetic */ PasscodeActivity$$ExternalSyntheticLambda20(PasscodeActivity passcodeActivity, boolean z, boolean z2) {
        this.f$0 = passcodeActivity;
        this.f$1 = z;
        this.f$2 = z2;
    }

    public final void run() {
        this.f$0.lambda$updateFields$17(this.f$1, this.f$2);
    }
}
