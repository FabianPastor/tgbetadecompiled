package org.telegram.ui;

import org.telegram.ui.Components.SimpleFloatPropertyCompat;

public final /* synthetic */ class CodeNumberField$$ExternalSyntheticLambda5 implements SimpleFloatPropertyCompat.Getter {
    public static final /* synthetic */ CodeNumberField$$ExternalSyntheticLambda5 INSTANCE = new CodeNumberField$$ExternalSyntheticLambda5();

    private /* synthetic */ CodeNumberField$$ExternalSyntheticLambda5() {
    }

    public final float get(Object obj) {
        return ((CodeNumberField) obj).successScaleProgress;
    }
}
