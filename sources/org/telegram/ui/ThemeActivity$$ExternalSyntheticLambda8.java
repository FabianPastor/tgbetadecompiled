package org.telegram.ui;

import com.google.android.gms.internal.mlkit_language_id.zzdp$$ExternalSyntheticBackport0;
import java.util.Comparator;
import org.telegram.ui.ActionBar.Theme;

public final /* synthetic */ class ThemeActivity$$ExternalSyntheticLambda8 implements Comparator {
    public static final /* synthetic */ ThemeActivity$$ExternalSyntheticLambda8 INSTANCE = new ThemeActivity$$ExternalSyntheticLambda8();

    private /* synthetic */ ThemeActivity$$ExternalSyntheticLambda8() {
    }

    public final int compare(Object obj, Object obj2) {
        return zzdp$$ExternalSyntheticBackport0.m(((Theme.ThemeInfo) obj).sortIndex, ((Theme.ThemeInfo) obj2).sortIndex);
    }
}
