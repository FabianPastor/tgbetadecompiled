package org.telegram.ui;

import android.view.View;
import org.telegram.ui.ActionBar.BaseFragment;

public final /* synthetic */ class SuggestClearDatabaseBottomSheet$$ExternalSyntheticLambda1 implements View.OnClickListener {
    public final /* synthetic */ SuggestClearDatabaseBottomSheet f$0;
    public final /* synthetic */ BaseFragment f$1;

    public /* synthetic */ SuggestClearDatabaseBottomSheet$$ExternalSyntheticLambda1(SuggestClearDatabaseBottomSheet suggestClearDatabaseBottomSheet, BaseFragment baseFragment) {
        this.f$0 = suggestClearDatabaseBottomSheet;
        this.f$1 = baseFragment;
    }

    public final void onClick(View view) {
        this.f$0.lambda$new$1(this.f$1, view);
    }
}
