package org.telegram.ui;

import org.telegram.ui.ActionBar.AlertDialog;

public final /* synthetic */ class FilterCreateActivity$$ExternalSyntheticLambda6 implements Runnable {
    public final /* synthetic */ FilterCreateActivity f$0;
    public final /* synthetic */ AlertDialog f$1;

    public /* synthetic */ FilterCreateActivity$$ExternalSyntheticLambda6(FilterCreateActivity filterCreateActivity, AlertDialog alertDialog) {
        this.f$0 = filterCreateActivity;
        this.f$1 = alertDialog;
    }

    public final void run() {
        this.f$0.lambda$createView$1(this.f$1);
    }
}
