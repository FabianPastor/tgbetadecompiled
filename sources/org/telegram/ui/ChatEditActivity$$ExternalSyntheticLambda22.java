package org.telegram.ui;

import android.view.MotionEvent;
import android.view.View;

public final /* synthetic */ class ChatEditActivity$$ExternalSyntheticLambda22 implements View.OnTouchListener {
    public static final /* synthetic */ ChatEditActivity$$ExternalSyntheticLambda22 INSTANCE = new ChatEditActivity$$ExternalSyntheticLambda22();

    private /* synthetic */ ChatEditActivity$$ExternalSyntheticLambda22() {
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return ChatEditActivity.lambda$createView$2(view, motionEvent);
    }
}
