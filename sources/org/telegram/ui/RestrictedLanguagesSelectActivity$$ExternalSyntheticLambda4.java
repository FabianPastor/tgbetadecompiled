package org.telegram.ui;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class RestrictedLanguagesSelectActivity$$ExternalSyntheticLambda4 implements RecyclerListView.OnItemClickListener {
    public final /* synthetic */ RestrictedLanguagesSelectActivity f$0;

    public /* synthetic */ RestrictedLanguagesSelectActivity$$ExternalSyntheticLambda4(RestrictedLanguagesSelectActivity restrictedLanguagesSelectActivity) {
        this.f$0 = restrictedLanguagesSelectActivity;
    }

    public final void onItemClick(View view, int i) {
        this.f$0.lambda$createView$1(view, i);
    }
}
