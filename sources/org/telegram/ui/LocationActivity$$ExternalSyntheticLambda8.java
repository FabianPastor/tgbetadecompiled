package org.telegram.ui;

import android.view.MotionEvent;
import android.view.View;

public final /* synthetic */ class LocationActivity$$ExternalSyntheticLambda8 implements View.OnTouchListener {
    public static final /* synthetic */ LocationActivity$$ExternalSyntheticLambda8 INSTANCE = new LocationActivity$$ExternalSyntheticLambda8();

    private /* synthetic */ LocationActivity$$ExternalSyntheticLambda8() {
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return LocationActivity.lambda$createView$7(view, motionEvent);
    }
}
