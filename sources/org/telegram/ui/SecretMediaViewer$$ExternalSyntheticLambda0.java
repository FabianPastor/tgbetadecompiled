package org.telegram.ui;

import android.view.View;
import android.view.WindowInsets;

public final /* synthetic */ class SecretMediaViewer$$ExternalSyntheticLambda0 implements View.OnApplyWindowInsetsListener {
    public final /* synthetic */ SecretMediaViewer f$0;

    public /* synthetic */ SecretMediaViewer$$ExternalSyntheticLambda0(SecretMediaViewer secretMediaViewer) {
        this.f$0 = secretMediaViewer;
    }

    public final WindowInsets onApplyWindowInsets(View view, WindowInsets windowInsets) {
        return this.f$0.lambda$setParentActivity$0(view, windowInsets);
    }
}
