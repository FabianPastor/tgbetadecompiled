package org.telegram.ui;

import android.view.KeyEvent;
import android.widget.TextView;

public final /* synthetic */ class ChangeUsernameActivity$$ExternalSyntheticLambda2 implements TextView.OnEditorActionListener {
    public final /* synthetic */ ChangeUsernameActivity f$0;

    public /* synthetic */ ChangeUsernameActivity$$ExternalSyntheticLambda2(ChangeUsernameActivity changeUsernameActivity) {
        this.f$0 = changeUsernameActivity;
    }

    public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        return this.f$0.lambda$createView$1(textView, i, keyEvent);
    }
}
