package org.telegram.ui;

import android.view.View;

public final /* synthetic */ class ChatActivity$$ExternalSyntheticLambda93 implements View.OnClickListener {
    public final /* synthetic */ boolean[] f$0;

    public /* synthetic */ ChatActivity$$ExternalSyntheticLambda93(boolean[] zArr) {
        this.f$0 = zArr;
    }

    public final void onClick(View view) {
        ChatActivity.lambda$processSelectedOption$195(this.f$0, view);
    }
}
