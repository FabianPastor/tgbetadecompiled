package org.telegram.ui;

import android.view.MotionEvent;
import android.view.View;

public final /* synthetic */ class GroupCreateFinalActivity$$ExternalSyntheticLambda3 implements View.OnTouchListener {
    public static final /* synthetic */ GroupCreateFinalActivity$$ExternalSyntheticLambda3 INSTANCE = new GroupCreateFinalActivity$$ExternalSyntheticLambda3();

    private /* synthetic */ GroupCreateFinalActivity$$ExternalSyntheticLambda3() {
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return GroupCreateFinalActivity.lambda$createView$1(view, motionEvent);
    }
}
