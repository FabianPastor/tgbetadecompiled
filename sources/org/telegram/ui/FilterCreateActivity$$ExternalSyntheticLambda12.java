package org.telegram.ui;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class FilterCreateActivity$$ExternalSyntheticLambda12 implements RecyclerListView.OnItemClickListener {
    public final /* synthetic */ FilterCreateActivity f$0;

    public /* synthetic */ FilterCreateActivity$$ExternalSyntheticLambda12(FilterCreateActivity filterCreateActivity) {
        this.f$0 = filterCreateActivity;
    }

    public final void onItemClick(View view, int i) {
        this.f$0.lambda$createView$4(view, i);
    }
}
