package org.telegram.ui;

import android.content.DialogInterface;

public final /* synthetic */ class LoginActivity$$ExternalSyntheticLambda6 implements DialogInterface.OnClickListener {
    public final /* synthetic */ LoginActivity f$0;

    public /* synthetic */ LoginActivity$$ExternalSyntheticLambda6(LoginActivity loginActivity) {
        this.f$0 = loginActivity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f$0.lambda$onDoneButtonPressed$13(dialogInterface, i);
    }
}
