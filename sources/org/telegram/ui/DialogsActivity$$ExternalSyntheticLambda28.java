package org.telegram.ui;

import android.view.MotionEvent;
import android.view.View;

public final /* synthetic */ class DialogsActivity$$ExternalSyntheticLambda28 implements View.OnTouchListener {
    public static final /* synthetic */ DialogsActivity$$ExternalSyntheticLambda28 INSTANCE = new DialogsActivity$$ExternalSyntheticLambda28();

    private /* synthetic */ DialogsActivity$$ExternalSyntheticLambda28() {
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return DialogsActivity.lambda$createActionMode$15(view, motionEvent);
    }
}
