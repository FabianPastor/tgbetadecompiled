package org.telegram.ui;

import android.view.View;
import org.telegram.ui.ArticleViewer;

public final /* synthetic */ class ArticleViewer$BlockPreformattedCell$$ExternalSyntheticLambda0 implements View.OnScrollChangeListener {
    public final /* synthetic */ ArticleViewer.BlockPreformattedCell f$0;

    public /* synthetic */ ArticleViewer$BlockPreformattedCell$$ExternalSyntheticLambda0(ArticleViewer.BlockPreformattedCell blockPreformattedCell) {
        this.f$0 = blockPreformattedCell;
    }

    public final void onScrollChange(View view, int i, int i2, int i3, int i4) {
        this.f$0.lambda$new$0(view, i, i2, i3, i4);
    }
}
