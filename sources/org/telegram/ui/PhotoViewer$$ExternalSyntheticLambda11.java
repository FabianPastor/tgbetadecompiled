package org.telegram.ui;

import android.content.DialogInterface;

public final /* synthetic */ class PhotoViewer$$ExternalSyntheticLambda11 implements DialogInterface.OnDismissListener {
    public final /* synthetic */ PhotoViewer f$0;

    public /* synthetic */ PhotoViewer$$ExternalSyntheticLambda11(PhotoViewer photoViewer) {
        this.f$0 = photoViewer;
    }

    public final void onDismiss(DialogInterface dialogInterface) {
        this.f$0.lambda$showAlertDialog$52(dialogInterface);
    }
}
