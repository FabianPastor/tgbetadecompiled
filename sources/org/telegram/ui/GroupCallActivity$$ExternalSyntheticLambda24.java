package org.telegram.ui;

import android.content.Context;
import android.view.View;

public final /* synthetic */ class GroupCallActivity$$ExternalSyntheticLambda24 implements View.OnClickListener {
    public final /* synthetic */ GroupCallActivity f$0;
    public final /* synthetic */ Context f$1;

    public /* synthetic */ GroupCallActivity$$ExternalSyntheticLambda24(GroupCallActivity groupCallActivity, Context context) {
        this.f$0 = groupCallActivity;
        this.f$1 = context;
    }

    public final void onClick(View view) {
        this.f$0.lambda$new$34(this.f$1, view);
    }
}
