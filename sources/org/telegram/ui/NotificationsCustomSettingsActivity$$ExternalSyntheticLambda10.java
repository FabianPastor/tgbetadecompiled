package org.telegram.ui;

import java.util.ArrayList;
import org.telegram.ui.DialogsActivity;

public final /* synthetic */ class NotificationsCustomSettingsActivity$$ExternalSyntheticLambda10 implements DialogsActivity.DialogsActivityDelegate {
    public final /* synthetic */ NotificationsCustomSettingsActivity f$0;

    public /* synthetic */ NotificationsCustomSettingsActivity$$ExternalSyntheticLambda10(NotificationsCustomSettingsActivity notificationsCustomSettingsActivity) {
        this.f$0 = notificationsCustomSettingsActivity;
    }

    public final void didSelectDialogs(DialogsActivity dialogsActivity, ArrayList arrayList, CharSequence charSequence, boolean z) {
        this.f$0.lambda$createView$1(dialogsActivity, arrayList, charSequence, z);
    }
}
