package org.telegram.ui;

import android.content.DialogInterface;

public final /* synthetic */ class PrivacyControlActivity$$ExternalSyntheticLambda0 implements DialogInterface.OnClickListener {
    public final /* synthetic */ PrivacyControlActivity f$0;

    public /* synthetic */ PrivacyControlActivity$$ExternalSyntheticLambda0(PrivacyControlActivity privacyControlActivity) {
        this.f$0 = privacyControlActivity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f$0.lambda$checkDiscard$9(dialogInterface, i);
    }
}
