package org.telegram.ui;

public final /* synthetic */ class PassportActivity$$ExternalSyntheticLambda61 implements Runnable {
    public final /* synthetic */ PassportActivity f$0;
    public final /* synthetic */ boolean f$1;
    public final /* synthetic */ String f$2;

    public /* synthetic */ PassportActivity$$ExternalSyntheticLambda61(PassportActivity passportActivity, boolean z, String str) {
        this.f$0 = passportActivity;
        this.f$1 = z;
        this.f$2 = str;
    }

    public final void run() {
        this.f$0.lambda$onPasswordDone$13(this.f$1, this.f$2);
    }
}
