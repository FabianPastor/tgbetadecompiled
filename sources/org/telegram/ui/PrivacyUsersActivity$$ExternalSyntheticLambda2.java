package org.telegram.ui;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class PrivacyUsersActivity$$ExternalSyntheticLambda2 implements RecyclerListView.OnItemClickListener {
    public final /* synthetic */ PrivacyUsersActivity f$0;

    public /* synthetic */ PrivacyUsersActivity$$ExternalSyntheticLambda2(PrivacyUsersActivity privacyUsersActivity) {
        this.f$0 = privacyUsersActivity;
    }

    public final void onItemClick(View view, int i) {
        this.f$0.lambda$createView$1(view, i);
    }
}
