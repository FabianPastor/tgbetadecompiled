package org.telegram.ui;

import org.telegram.ui.ActionBar.ThemeDescription;

public final /* synthetic */ class LocationActivity$$ExternalSyntheticLambda29 implements ThemeDescription.ThemeDescriptionDelegate {
    public final /* synthetic */ LocationActivity f$0;

    public /* synthetic */ LocationActivity$$ExternalSyntheticLambda29(LocationActivity locationActivity) {
        this.f$0 = locationActivity;
    }

    public final void didSetColor() {
        this.f$0.lambda$getThemeDescriptions$37();
    }

    public /* synthetic */ void onAnimationProgress(float f) {
        ThemeDescription.ThemeDescriptionDelegate.CC.$default$onAnimationProgress(this, f);
    }
}
