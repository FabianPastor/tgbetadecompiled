package org.telegram.ui;

import android.content.DialogInterface;

public final /* synthetic */ class GroupCallActivity$$ExternalSyntheticLambda9 implements DialogInterface.OnDismissListener {
    public final /* synthetic */ GroupCallActivity f$0;

    public /* synthetic */ GroupCallActivity$$ExternalSyntheticLambda9(GroupCallActivity groupCallActivity) {
        this.f$0 = groupCallActivity;
    }

    public final void onDismiss(DialogInterface dialogInterface) {
        this.f$0.lambda$didReceivedNotification$3(dialogInterface);
    }
}
