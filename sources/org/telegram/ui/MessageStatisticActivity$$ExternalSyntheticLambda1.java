package org.telegram.ui;

import org.telegram.tgnet.TLObject;

public final /* synthetic */ class MessageStatisticActivity$$ExternalSyntheticLambda1 implements Runnable {
    public final /* synthetic */ MessageStatisticActivity f$0;
    public final /* synthetic */ TLObject f$1;

    public /* synthetic */ MessageStatisticActivity$$ExternalSyntheticLambda1(MessageStatisticActivity messageStatisticActivity, TLObject tLObject) {
        this.f$0 = messageStatisticActivity;
        this.f$1 = tLObject;
    }

    public final void run() {
        this.f$0.lambda$loadStat$4(this.f$1);
    }
}
