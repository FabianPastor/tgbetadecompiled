package org.telegram.ui;

import android.view.MotionEvent;
import android.view.View;

public final /* synthetic */ class PhotoViewer$$ExternalSyntheticLambda44 implements View.OnTouchListener {
    public static final /* synthetic */ PhotoViewer$$ExternalSyntheticLambda44 INSTANCE = new PhotoViewer$$ExternalSyntheticLambda44();

    private /* synthetic */ PhotoViewer$$ExternalSyntheticLambda44() {
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return PhotoViewer.lambda$setParentActivity$25(view, motionEvent);
    }
}
