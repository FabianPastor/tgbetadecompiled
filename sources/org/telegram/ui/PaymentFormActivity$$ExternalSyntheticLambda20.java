package org.telegram.ui;

import android.view.View;
import android.widget.TextView;

public final /* synthetic */ class PaymentFormActivity$$ExternalSyntheticLambda20 implements View.OnClickListener {
    public final /* synthetic */ PaymentFormActivity f$0;
    public final /* synthetic */ TextView f$1;
    public final /* synthetic */ long f$2;

    public /* synthetic */ PaymentFormActivity$$ExternalSyntheticLambda20(PaymentFormActivity paymentFormActivity, TextView textView, long j) {
        this.f$0 = paymentFormActivity;
        this.f$1 = textView;
        this.f$2 = j;
    }

    public final void onClick(View view) {
        this.f$0.lambda$createView$15(this.f$1, this.f$2, view);
    }
}
