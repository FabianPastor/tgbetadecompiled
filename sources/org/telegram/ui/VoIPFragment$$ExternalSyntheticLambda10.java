package org.telegram.ui;

import android.view.View;
import android.view.WindowInsets;

public final /* synthetic */ class VoIPFragment$$ExternalSyntheticLambda10 implements View.OnApplyWindowInsetsListener {
    public final /* synthetic */ VoIPFragment f$0;

    public /* synthetic */ VoIPFragment$$ExternalSyntheticLambda10(VoIPFragment voIPFragment) {
        this.f$0 = voIPFragment;
    }

    public final WindowInsets onApplyWindowInsets(View view, WindowInsets windowInsets) {
        return VoIPFragment.lambda$show$3(this.f$0, view, windowInsets);
    }
}
