package org.telegram.ui;

public final /* synthetic */ class LanguageSelectActivity$$ExternalSyntheticLambda2 implements Runnable {
    public final /* synthetic */ LanguageSelectActivity f$0;
    public final /* synthetic */ String f$1;

    public /* synthetic */ LanguageSelectActivity$$ExternalSyntheticLambda2(LanguageSelectActivity languageSelectActivity, String str) {
        this.f$0 = languageSelectActivity;
        this.f$1 = str;
    }

    public final void run() {
        this.f$0.lambda$processSearch$6(this.f$1);
    }
}
