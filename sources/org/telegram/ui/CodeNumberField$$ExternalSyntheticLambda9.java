package org.telegram.ui;

import org.telegram.ui.Components.SimpleFloatPropertyCompat;

public final /* synthetic */ class CodeNumberField$$ExternalSyntheticLambda9 implements SimpleFloatPropertyCompat.Setter {
    public static final /* synthetic */ CodeNumberField$$ExternalSyntheticLambda9 INSTANCE = new CodeNumberField$$ExternalSyntheticLambda9();

    private /* synthetic */ CodeNumberField$$ExternalSyntheticLambda9() {
    }

    public final void set(Object obj, float f) {
        CodeNumberField.lambda$static$3((CodeNumberField) obj, f);
    }
}
