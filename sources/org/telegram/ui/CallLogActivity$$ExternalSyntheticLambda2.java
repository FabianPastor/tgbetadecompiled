package org.telegram.ui;

import android.view.View;

public final /* synthetic */ class CallLogActivity$$ExternalSyntheticLambda2 implements View.OnClickListener {
    public final /* synthetic */ boolean[] f$0;

    public /* synthetic */ CallLogActivity$$ExternalSyntheticLambda2(boolean[] zArr) {
        this.f$0 = zArr;
    }

    public final void onClick(View view) {
        CallLogActivity.lambda$showDeleteAlert$4(this.f$0, view);
    }
}
