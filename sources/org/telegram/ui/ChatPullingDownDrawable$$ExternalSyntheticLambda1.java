package org.telegram.ui;

import android.animation.ValueAnimator;

public final /* synthetic */ class ChatPullingDownDrawable$$ExternalSyntheticLambda1 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ ChatPullingDownDrawable f$0;

    public /* synthetic */ ChatPullingDownDrawable$$ExternalSyntheticLambda1(ChatPullingDownDrawable chatPullingDownDrawable) {
        this.f$0 = chatPullingDownDrawable;
    }

    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f$0.lambda$runOnAnimationFinish$5(valueAnimator);
    }
}
