package org.telegram.ui;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class PollCreateActivity$$ExternalSyntheticLambda1 implements RecyclerListView.OnItemClickListener {
    public final /* synthetic */ PollCreateActivity f$0;

    public /* synthetic */ PollCreateActivity$$ExternalSyntheticLambda1(PollCreateActivity pollCreateActivity) {
        this.f$0 = pollCreateActivity;
    }

    public final void onItemClick(View view, int i) {
        this.f$0.lambda$createView$0(view, i);
    }
}
