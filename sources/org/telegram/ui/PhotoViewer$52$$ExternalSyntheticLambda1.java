package org.telegram.ui;

import android.content.DialogInterface;
import org.telegram.ui.PhotoViewer;

public final /* synthetic */ class PhotoViewer$52$$ExternalSyntheticLambda1 implements DialogInterface.OnClickListener {
    public final /* synthetic */ PhotoViewer.AnonymousClass52 f$0;

    public /* synthetic */ PhotoViewer$52$$ExternalSyntheticLambda1(PhotoViewer.AnonymousClass52 r1) {
        this.f$0 = r1;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f$0.lambda$onError$0(dialogInterface, i);
    }
}
