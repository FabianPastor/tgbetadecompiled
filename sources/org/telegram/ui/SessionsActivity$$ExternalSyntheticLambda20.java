package org.telegram.ui;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class SessionsActivity$$ExternalSyntheticLambda20 implements RecyclerListView.OnItemClickListener {
    public final /* synthetic */ SessionsActivity f$0;

    public /* synthetic */ SessionsActivity$$ExternalSyntheticLambda20(SessionsActivity sessionsActivity) {
        this.f$0 = sessionsActivity;
    }

    public final void onItemClick(View view, int i) {
        this.f$0.lambda$createView$13(view, i);
    }
}
