package org.telegram.ui;

import android.view.KeyEvent;
import android.widget.TextView;

public final /* synthetic */ class PasscodeActivity$$ExternalSyntheticLambda9 implements TextView.OnEditorActionListener {
    public final /* synthetic */ PasscodeActivity f$0;

    public /* synthetic */ PasscodeActivity$$ExternalSyntheticLambda9(PasscodeActivity passcodeActivity) {
        this.f$0 = passcodeActivity;
    }

    public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        return this.f$0.lambda$createView$10(textView, i, keyEvent);
    }
}
