package org.telegram.ui;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class PasscodeActivity$$ExternalSyntheticLambda22 implements RecyclerListView.OnItemClickListener {
    public final /* synthetic */ PasscodeActivity f$0;

    public /* synthetic */ PasscodeActivity$$ExternalSyntheticLambda22(PasscodeActivity passcodeActivity) {
        this.f$0 = passcodeActivity;
    }

    public final void onItemClick(View view, int i) {
        this.f$0.lambda$createView$5(view, i);
    }
}
