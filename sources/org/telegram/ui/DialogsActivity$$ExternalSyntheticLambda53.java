package org.telegram.ui;

import org.telegram.ui.ActionBar.ThemeDescription;

public final /* synthetic */ class DialogsActivity$$ExternalSyntheticLambda53 implements ThemeDescription.ThemeDescriptionDelegate {
    public final /* synthetic */ DialogsActivity f$0;

    public /* synthetic */ DialogsActivity$$ExternalSyntheticLambda53(DialogsActivity dialogsActivity) {
        this.f$0 = dialogsActivity;
    }

    public final void didSetColor() {
        this.f$0.lambda$getThemeDescriptions$58();
    }

    public /* synthetic */ void onAnimationProgress(float f) {
        ThemeDescription.ThemeDescriptionDelegate.CC.$default$onAnimationProgress(this, f);
    }
}
