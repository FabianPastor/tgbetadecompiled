package org.telegram.ui;

public final /* synthetic */ class TwoStepVerificationSetupActivity$$ExternalSyntheticLambda29 implements Runnable {
    public final /* synthetic */ TwoStepVerificationSetupActivity f$0;
    public final /* synthetic */ String f$1;

    public /* synthetic */ TwoStepVerificationSetupActivity$$ExternalSyntheticLambda29(TwoStepVerificationSetupActivity twoStepVerificationSetupActivity, String str) {
        this.f$0 = twoStepVerificationSetupActivity;
        this.f$1 = str;
    }

    public final void run() {
        this.f$0.lambda$processNext$29(this.f$1);
    }
}
