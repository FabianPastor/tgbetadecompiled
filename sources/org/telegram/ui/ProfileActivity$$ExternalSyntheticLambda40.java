package org.telegram.ui;

import android.text.style.URLSpan;
import org.telegram.ui.Components.TranslateAlert;

public final /* synthetic */ class ProfileActivity$$ExternalSyntheticLambda40 implements TranslateAlert.OnLinkPress {
    public final /* synthetic */ ProfileActivity f$0;

    public /* synthetic */ ProfileActivity$$ExternalSyntheticLambda40(ProfileActivity profileActivity) {
        this.f$0 = profileActivity;
    }

    public final boolean run(URLSpan uRLSpan) {
        return this.f$0.lambda$processOnClickOrPress$20(uRLSpan);
    }
}
