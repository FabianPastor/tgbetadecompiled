package org.telegram.ui;

import org.telegram.messenger.MessagesStorage;

public final /* synthetic */ class ProfileActivity$$ExternalSyntheticLambda32 implements MessagesStorage.BooleanCallback {
    public final /* synthetic */ ProfileActivity f$0;

    public /* synthetic */ ProfileActivity$$ExternalSyntheticLambda32(ProfileActivity profileActivity) {
        this.f$0 = profileActivity;
    }

    public final void run(boolean z) {
        this.f$0.lambda$leaveChatPressed$25(z);
    }
}
