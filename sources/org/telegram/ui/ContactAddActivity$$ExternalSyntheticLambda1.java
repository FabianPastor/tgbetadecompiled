package org.telegram.ui;

import android.view.MotionEvent;
import android.view.View;

public final /* synthetic */ class ContactAddActivity$$ExternalSyntheticLambda1 implements View.OnTouchListener {
    public static final /* synthetic */ ContactAddActivity$$ExternalSyntheticLambda1 INSTANCE = new ContactAddActivity$$ExternalSyntheticLambda1();

    private /* synthetic */ ContactAddActivity$$ExternalSyntheticLambda1() {
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return ContactAddActivity.lambda$createView$0(view, motionEvent);
    }
}
