package org.telegram.ui;

import android.animation.ValueAnimator;
import org.telegram.ui.ProfileActivity;

public final /* synthetic */ class ProfileActivity$OverlaysView$$ExternalSyntheticLambda0 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ ProfileActivity.OverlaysView f$0;

    public /* synthetic */ ProfileActivity$OverlaysView$$ExternalSyntheticLambda0(ProfileActivity.OverlaysView overlaysView) {
        this.f$0 = overlaysView;
    }

    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f$0.lambda$new$0(valueAnimator);
    }
}
