package org.telegram.ui;

import org.telegram.ui.Components.PasscodeView;

public final /* synthetic */ class BubbleActivity$$ExternalSyntheticLambda0 implements PasscodeView.PasscodeViewDelegate {
    public final /* synthetic */ BubbleActivity f$0;

    public /* synthetic */ BubbleActivity$$ExternalSyntheticLambda0(BubbleActivity bubbleActivity) {
        this.f$0 = bubbleActivity;
    }

    public final void didAcceptedPassword() {
        this.f$0.lambda$showPasscodeActivity$0();
    }
}
