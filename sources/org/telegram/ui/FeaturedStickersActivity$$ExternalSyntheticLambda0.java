package org.telegram.ui;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class FeaturedStickersActivity$$ExternalSyntheticLambda0 implements RecyclerListView.OnItemClickListener {
    public final /* synthetic */ FeaturedStickersActivity f$0;

    public /* synthetic */ FeaturedStickersActivity$$ExternalSyntheticLambda0(FeaturedStickersActivity featuredStickersActivity) {
        this.f$0 = featuredStickersActivity;
    }

    public final void onItemClick(View view, int i) {
        this.f$0.lambda$createView$0(view, i);
    }
}
