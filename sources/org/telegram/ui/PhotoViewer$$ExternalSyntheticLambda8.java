package org.telegram.ui;

import android.content.DialogInterface;

public final /* synthetic */ class PhotoViewer$$ExternalSyntheticLambda8 implements DialogInterface.OnClickListener {
    public final /* synthetic */ PhotoViewer f$0;

    public /* synthetic */ PhotoViewer$$ExternalSyntheticLambda8(PhotoViewer photoViewer) {
        this.f$0 = photoViewer;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f$0.lambda$switchToEditMode$59(dialogInterface, i);
    }
}
