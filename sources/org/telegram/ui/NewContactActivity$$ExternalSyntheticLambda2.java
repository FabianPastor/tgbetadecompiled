package org.telegram.ui;

import android.view.MotionEvent;
import android.view.View;

public final /* synthetic */ class NewContactActivity$$ExternalSyntheticLambda2 implements View.OnTouchListener {
    public static final /* synthetic */ NewContactActivity$$ExternalSyntheticLambda2 INSTANCE = new NewContactActivity$$ExternalSyntheticLambda2();

    private /* synthetic */ NewContactActivity$$ExternalSyntheticLambda2() {
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return NewContactActivity.lambda$createView$0(view, motionEvent);
    }
}
