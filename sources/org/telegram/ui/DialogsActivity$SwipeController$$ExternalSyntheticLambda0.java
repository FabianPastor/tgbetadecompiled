package org.telegram.ui;

import android.graphics.drawable.Drawable;
import android.view.View;

public final /* synthetic */ class DialogsActivity$SwipeController$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ View f$0;

    public /* synthetic */ DialogsActivity$SwipeController$$ExternalSyntheticLambda0(View view) {
        this.f$0 = view;
    }

    public final void run() {
        this.f$0.setBackgroundDrawable((Drawable) null);
    }
}
