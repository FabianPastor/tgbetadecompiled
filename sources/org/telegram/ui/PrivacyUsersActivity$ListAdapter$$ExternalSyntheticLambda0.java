package org.telegram.ui;

import org.telegram.ui.Cells.ManageChatUserCell;
import org.telegram.ui.PrivacyUsersActivity;

public final /* synthetic */ class PrivacyUsersActivity$ListAdapter$$ExternalSyntheticLambda0 implements ManageChatUserCell.ManageChatUserCellDelegate {
    public final /* synthetic */ PrivacyUsersActivity.ListAdapter f$0;

    public /* synthetic */ PrivacyUsersActivity$ListAdapter$$ExternalSyntheticLambda0(PrivacyUsersActivity.ListAdapter listAdapter) {
        this.f$0 = listAdapter;
    }

    public final boolean onOptionsButtonCheck(ManageChatUserCell manageChatUserCell, boolean z) {
        return this.f$0.lambda$onCreateViewHolder$0(manageChatUserCell, z);
    }
}
