package org.telegram.ui;

import android.view.View;
import org.telegram.ui.ChatRightsEditActivity;

public final /* synthetic */ class ChatRightsEditActivity$ListAdapter$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ ChatRightsEditActivity.ListAdapter f$0;

    public /* synthetic */ ChatRightsEditActivity$ListAdapter$$ExternalSyntheticLambda0(ChatRightsEditActivity.ListAdapter listAdapter) {
        this.f$0 = listAdapter;
    }

    public final void onClick(View view) {
        this.f$0.lambda$onCreateViewHolder$0(view);
    }
}
