package org.telegram.ui;

import android.animation.ValueAnimator;
import org.telegram.ui.PhotoViewer;

public final /* synthetic */ class PhotoViewer$67$$ExternalSyntheticLambda0 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ PhotoViewer.AnonymousClass67 f$0;

    public /* synthetic */ PhotoViewer$67$$ExternalSyntheticLambda0(PhotoViewer.AnonymousClass67 r1) {
        this.f$0 = r1;
    }

    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f$0.lambda$createAnimator$0(valueAnimator);
    }
}
