package org.telegram.ui;

import android.content.DialogInterface;

public final /* synthetic */ class VoIPFragment$$ExternalSyntheticLambda4 implements DialogInterface.OnClickListener {
    public final /* synthetic */ VoIPFragment f$0;

    public /* synthetic */ VoIPFragment$$ExternalSyntheticLambda4(VoIPFragment voIPFragment) {
        this.f$0 = voIPFragment;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f$0.lambda$requestInlinePermissions$30(dialogInterface, i);
    }
}
