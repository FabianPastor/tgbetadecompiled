package org.telegram.ui;

import org.telegram.ui.ActionBar.ThemeDescription;

public final /* synthetic */ class FilterUsersActivity$$ExternalSyntheticLambda2 implements ThemeDescription.ThemeDescriptionDelegate {
    public final /* synthetic */ FilterUsersActivity f$0;

    public /* synthetic */ FilterUsersActivity$$ExternalSyntheticLambda2(FilterUsersActivity filterUsersActivity) {
        this.f$0 = filterUsersActivity;
    }

    public final void didSetColor() {
        this.f$0.lambda$getThemeDescriptions$3();
    }

    public /* synthetic */ void onAnimationProgress(float f) {
        ThemeDescription.ThemeDescriptionDelegate.CC.$default$onAnimationProgress(this, f);
    }
}
