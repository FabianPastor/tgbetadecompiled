package org.telegram.ui;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class CallLogActivity$$ExternalSyntheticLambda8 implements RecyclerListView.OnItemClickListener {
    public final /* synthetic */ CallLogActivity f$0;

    public /* synthetic */ CallLogActivity$$ExternalSyntheticLambda8(CallLogActivity callLogActivity) {
        this.f$0 = callLogActivity;
    }

    public final void onItemClick(View view, int i) {
        this.f$0.lambda$createView$0(view, i);
    }
}
