package org.telegram.ui;

import android.view.View;

public final /* synthetic */ class ChannelCreateActivity$$ExternalSyntheticLambda5 implements View.OnClickListener {
    public final /* synthetic */ ChannelCreateActivity f$0;

    public /* synthetic */ ChannelCreateActivity$$ExternalSyntheticLambda5(ChannelCreateActivity channelCreateActivity) {
        this.f$0 = channelCreateActivity;
    }

    public final void onClick(View view) {
        this.f$0.lambda$loadAdminedChannels$18(view);
    }
}
