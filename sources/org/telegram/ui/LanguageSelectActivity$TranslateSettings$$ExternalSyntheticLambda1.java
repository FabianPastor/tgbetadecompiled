package org.telegram.ui;

import android.view.View;
import org.telegram.ui.LanguageSelectActivity;

public final /* synthetic */ class LanguageSelectActivity$TranslateSettings$$ExternalSyntheticLambda1 implements View.OnClickListener {
    public final /* synthetic */ LanguageSelectActivity.TranslateSettings f$0;

    public /* synthetic */ LanguageSelectActivity$TranslateSettings$$ExternalSyntheticLambda1(LanguageSelectActivity.TranslateSettings translateSettings) {
        this.f$0 = translateSettings;
    }

    public final void onClick(View view) {
        this.f$0.lambda$new$0(view);
    }
}
