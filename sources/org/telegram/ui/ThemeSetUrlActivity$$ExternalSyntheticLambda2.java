package org.telegram.ui;

import android.content.Context;
import android.view.View;

public final /* synthetic */ class ThemeSetUrlActivity$$ExternalSyntheticLambda2 implements View.OnClickListener {
    public final /* synthetic */ ThemeSetUrlActivity f$0;
    public final /* synthetic */ Context f$1;

    public /* synthetic */ ThemeSetUrlActivity$$ExternalSyntheticLambda2(ThemeSetUrlActivity themeSetUrlActivity, Context context) {
        this.f$0 = themeSetUrlActivity;
        this.f$1 = context;
    }

    public final void onClick(View view) {
        this.f$0.lambda$createView$5(this.f$1, view);
    }
}
