package org.telegram.ui;

import android.view.View;
import java.util.ArrayList;
import org.telegram.ui.ContentPreviewViewer;

public final /* synthetic */ class ContentPreviewViewer$1$$ExternalSyntheticLambda1 implements View.OnClickListener {
    public final /* synthetic */ ContentPreviewViewer.AnonymousClass1 f$0;
    public final /* synthetic */ ArrayList f$1;

    public /* synthetic */ ContentPreviewViewer$1$$ExternalSyntheticLambda1(ContentPreviewViewer.AnonymousClass1 r1, ArrayList arrayList) {
        this.f$0 = r1;
        this.f$1 = arrayList;
    }

    public final void onClick(View view) {
        this.f$0.lambda$run$1(this.f$1, view);
    }
}
