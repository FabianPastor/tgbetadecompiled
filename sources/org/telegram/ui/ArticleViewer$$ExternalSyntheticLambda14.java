package org.telegram.ui;

import android.view.MotionEvent;
import android.view.View;

public final /* synthetic */ class ArticleViewer$$ExternalSyntheticLambda14 implements View.OnTouchListener {
    public static final /* synthetic */ ArticleViewer$$ExternalSyntheticLambda14 INSTANCE = new ArticleViewer$$ExternalSyntheticLambda14();

    private /* synthetic */ ArticleViewer$$ExternalSyntheticLambda14() {
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return ArticleViewer.lambda$setParentActivity$21(view, motionEvent);
    }
}
