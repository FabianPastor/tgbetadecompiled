package org.telegram.ui;

import org.telegram.messenger.MessagesStorage;

public final /* synthetic */ class ChatEditActivity$$ExternalSyntheticLambda28 implements MessagesStorage.LongCallback {
    public final /* synthetic */ ChatEditActivity f$0;

    public /* synthetic */ ChatEditActivity$$ExternalSyntheticLambda28(ChatEditActivity chatEditActivity) {
        this.f$0 = chatEditActivity;
    }

    public final void run(long j) {
        this.f$0.lambda$processDone$29(j);
    }
}
