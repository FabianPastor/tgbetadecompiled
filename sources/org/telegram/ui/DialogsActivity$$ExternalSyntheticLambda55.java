package org.telegram.ui;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class DialogsActivity$$ExternalSyntheticLambda55 implements RecyclerListView.OnItemClickListener {
    public final /* synthetic */ DialogsActivity f$0;

    public /* synthetic */ DialogsActivity$$ExternalSyntheticLambda55(DialogsActivity dialogsActivity) {
        this.f$0 = dialogsActivity;
    }

    public final void onItemClick(View view, int i) {
        this.f$0.lambda$createView$8(view, i);
    }
}
