package org.telegram.ui;

import android.view.KeyEvent;
import android.widget.TextView;

public final /* synthetic */ class ThemeSetUrlActivity$$ExternalSyntheticLambda6 implements TextView.OnEditorActionListener {
    public final /* synthetic */ ThemeSetUrlActivity f$0;

    public /* synthetic */ ThemeSetUrlActivity$$ExternalSyntheticLambda6(ThemeSetUrlActivity themeSetUrlActivity) {
        this.f$0 = themeSetUrlActivity;
    }

    public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        return this.f$0.lambda$createView$1(textView, i, keyEvent);
    }
}
