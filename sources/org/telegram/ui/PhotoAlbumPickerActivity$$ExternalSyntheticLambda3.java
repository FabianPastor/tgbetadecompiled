package org.telegram.ui;

import android.view.MotionEvent;
import android.view.View;

public final /* synthetic */ class PhotoAlbumPickerActivity$$ExternalSyntheticLambda3 implements View.OnTouchListener {
    public static final /* synthetic */ PhotoAlbumPickerActivity$$ExternalSyntheticLambda3 INSTANCE = new PhotoAlbumPickerActivity$$ExternalSyntheticLambda3();

    private /* synthetic */ PhotoAlbumPickerActivity$$ExternalSyntheticLambda3() {
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return PhotoAlbumPickerActivity.lambda$createView$0(view, motionEvent);
    }
}
