package org.telegram.ui;

import android.view.MotionEvent;
import android.view.View;

public final /* synthetic */ class ChatActivity$$ExternalSyntheticLambda107 implements View.OnTouchListener {
    public static final /* synthetic */ ChatActivity$$ExternalSyntheticLambda107 INSTANCE = new ChatActivity$$ExternalSyntheticLambda107();

    private /* synthetic */ ChatActivity$$ExternalSyntheticLambda107() {
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return ChatActivity.lambda$createView$23(view, motionEvent);
    }
}
