package org.telegram.ui;

import android.view.MotionEvent;
import android.view.View;

public final /* synthetic */ class ChannelCreateActivity$$ExternalSyntheticLambda8 implements View.OnTouchListener {
    public static final /* synthetic */ ChannelCreateActivity$$ExternalSyntheticLambda8 INSTANCE = new ChannelCreateActivity$$ExternalSyntheticLambda8();

    private /* synthetic */ ChannelCreateActivity$$ExternalSyntheticLambda8() {
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return ChannelCreateActivity.lambda$createView$4(view, motionEvent);
    }
}
