package org.telegram.ui;

import org.telegram.ui.ActionBar.AlertDialog;

public final /* synthetic */ class ChatLinkActivity$$ExternalSyntheticLambda10 implements Runnable {
    public final /* synthetic */ ChatLinkActivity f$0;
    public final /* synthetic */ AlertDialog[] f$1;
    public final /* synthetic */ int f$2;

    public /* synthetic */ ChatLinkActivity$$ExternalSyntheticLambda10(ChatLinkActivity chatLinkActivity, AlertDialog[] alertDialogArr, int i) {
        this.f$0 = chatLinkActivity;
        this.f$1 = alertDialogArr;
        this.f$2 = i;
    }

    public final void run() {
        this.f$0.lambda$createView$4(this.f$1, this.f$2);
    }
}
