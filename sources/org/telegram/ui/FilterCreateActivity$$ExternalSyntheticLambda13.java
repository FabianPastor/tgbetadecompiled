package org.telegram.ui;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class FilterCreateActivity$$ExternalSyntheticLambda13 implements RecyclerListView.OnItemLongClickListener {
    public final /* synthetic */ FilterCreateActivity f$0;

    public /* synthetic */ FilterCreateActivity$$ExternalSyntheticLambda13(FilterCreateActivity filterCreateActivity) {
        this.f$0 = filterCreateActivity;
    }

    public final boolean onItemClick(View view, int i) {
        return this.f$0.lambda$createView$5(view, i);
    }
}
