package org.telegram.ui;

import android.content.DialogInterface;

public final /* synthetic */ class GroupInviteActivity$$ExternalSyntheticLambda0 implements DialogInterface.OnClickListener {
    public final /* synthetic */ GroupInviteActivity f$0;

    public /* synthetic */ GroupInviteActivity$$ExternalSyntheticLambda0(GroupInviteActivity groupInviteActivity) {
        this.f$0 = groupInviteActivity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f$0.lambda$createView$0(dialogInterface, i);
    }
}
