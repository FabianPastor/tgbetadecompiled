package org.telegram.ui;

import org.telegram.ui.Charts.BaseChartView;
import org.telegram.ui.StatisticActivity;

public final /* synthetic */ class StatisticActivity$BaseChartCell$$ExternalSyntheticLambda5 implements BaseChartView.DateSelectionListener {
    public final /* synthetic */ StatisticActivity.BaseChartCell f$0;

    public /* synthetic */ StatisticActivity$BaseChartCell$$ExternalSyntheticLambda5(StatisticActivity.BaseChartCell baseChartCell) {
        this.f$0 = baseChartCell;
    }

    public final void onDateSelected(long j) {
        this.f$0.lambda$new$1(j);
    }
}
