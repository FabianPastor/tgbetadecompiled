package org.telegram.ui;

import android.content.DialogInterface;
import org.telegram.messenger.MessageObject;

public final /* synthetic */ class StatisticActivity$$ExternalSyntheticLambda0 implements DialogInterface.OnClickListener {
    public final /* synthetic */ StatisticActivity f$0;
    public final /* synthetic */ MessageObject f$1;

    public /* synthetic */ StatisticActivity$$ExternalSyntheticLambda0(StatisticActivity statisticActivity, MessageObject messageObject) {
        this.f$0 = statisticActivity;
        this.f$1 = messageObject;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f$0.lambda$createView$4(this.f$1, dialogInterface, i);
    }
}
