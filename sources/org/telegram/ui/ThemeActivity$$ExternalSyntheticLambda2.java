package org.telegram.ui;

import android.content.DialogInterface;

public final /* synthetic */ class ThemeActivity$$ExternalSyntheticLambda2 implements DialogInterface.OnClickListener {
    public final /* synthetic */ ThemeActivity f$0;

    public /* synthetic */ ThemeActivity$$ExternalSyntheticLambda2(ThemeActivity themeActivity) {
        this.f$0 = themeActivity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f$0.lambda$updateSunTime$7(dialogInterface, i);
    }
}
