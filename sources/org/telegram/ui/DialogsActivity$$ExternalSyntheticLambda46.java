package org.telegram.ui;

import android.app.Activity;

public final /* synthetic */ class DialogsActivity$$ExternalSyntheticLambda46 implements Runnable {
    public final /* synthetic */ DialogsActivity f$0;
    public final /* synthetic */ boolean f$1;
    public final /* synthetic */ boolean f$2;
    public final /* synthetic */ Activity f$3;

    public /* synthetic */ DialogsActivity$$ExternalSyntheticLambda46(DialogsActivity dialogsActivity, boolean z, boolean z2, Activity activity) {
        this.f$0 = dialogsActivity;
        this.f$1 = z;
        this.f$2 = z2;
        this.f$3 = activity;
    }

    public final void run() {
        this.f$0.lambda$onResume$17(this.f$1, this.f$2, this.f$3);
    }
}
