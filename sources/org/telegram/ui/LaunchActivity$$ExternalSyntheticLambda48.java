package org.telegram.ui;

import org.telegram.tgnet.TLObject;
import org.telegram.ui.ActionBar.AlertDialog;

public final /* synthetic */ class LaunchActivity$$ExternalSyntheticLambda48 implements Runnable {
    public final /* synthetic */ LaunchActivity f$0;
    public final /* synthetic */ TLObject f$1;
    public final /* synthetic */ int[] f$2;
    public final /* synthetic */ int f$3;
    public final /* synthetic */ AlertDialog f$4;
    public final /* synthetic */ Integer f$5;
    public final /* synthetic */ Integer f$6;
    public final /* synthetic */ Integer f$7;

    public /* synthetic */ LaunchActivity$$ExternalSyntheticLambda48(LaunchActivity launchActivity, TLObject tLObject, int[] iArr, int i, AlertDialog alertDialog, Integer num, Integer num2, Integer num3) {
        this.f$0 = launchActivity;
        this.f$1 = tLObject;
        this.f$2 = iArr;
        this.f$3 = i;
        this.f$4 = alertDialog;
        this.f$5 = num;
        this.f$6 = num2;
        this.f$7 = num3;
    }

    public final void run() {
        this.f$0.lambda$runLinkRequest$65(this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, this.f$6, this.f$7);
    }
}
