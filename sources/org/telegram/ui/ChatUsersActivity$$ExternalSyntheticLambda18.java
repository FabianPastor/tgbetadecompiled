package org.telegram.ui;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class ChatUsersActivity$$ExternalSyntheticLambda18 implements RecyclerListView.OnItemLongClickListener {
    public final /* synthetic */ ChatUsersActivity f$0;

    public /* synthetic */ ChatUsersActivity$$ExternalSyntheticLambda18(ChatUsersActivity chatUsersActivity) {
        this.f$0 = chatUsersActivity;
    }

    public final boolean onItemClick(View view, int i) {
        return this.f$0.lambda$createView$2(view, i);
    }
}
