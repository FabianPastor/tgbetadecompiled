package org.telegram.ui;

import android.animation.ValueAnimator;
import org.telegram.ui.LanguageSelectActivity;

public final /* synthetic */ class LanguageSelectActivity$TranslateSettings$$ExternalSyntheticLambda0 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ LanguageSelectActivity.TranslateSettings f$0;

    public /* synthetic */ LanguageSelectActivity$TranslateSettings$$ExternalSyntheticLambda0(LanguageSelectActivity.TranslateSettings translateSettings) {
        this.f$0 = translateSettings;
    }

    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f$0.lambda$update$2(valueAnimator);
    }
}
