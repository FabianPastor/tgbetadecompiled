package org.telegram.ui;

import android.view.View;

public final /* synthetic */ class PhotoViewer$$ExternalSyntheticLambda19 implements View.OnClickListener {
    public final /* synthetic */ PhotoViewer f$0;

    public /* synthetic */ PhotoViewer$$ExternalSyntheticLambda19(PhotoViewer photoViewer) {
        this.f$0 = photoViewer;
    }

    public final void onClick(View view) {
        this.f$0.lambda$switchToEditMode$60(view);
    }
}
