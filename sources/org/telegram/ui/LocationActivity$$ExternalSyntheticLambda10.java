package org.telegram.ui;

import com.google.android.gms.maps.GoogleMap;

public final /* synthetic */ class LocationActivity$$ExternalSyntheticLambda10 implements GoogleMap.OnCameraMoveStartedListener {
    public final /* synthetic */ LocationActivity f$0;

    public /* synthetic */ LocationActivity$$ExternalSyntheticLambda10(LocationActivity locationActivity) {
        this.f$0 = locationActivity;
    }

    public final void onCameraMoveStarted(int i) {
        this.f$0.lambda$onMapInit$27(i);
    }
}
