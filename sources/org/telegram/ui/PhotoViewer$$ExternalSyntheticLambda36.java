package org.telegram.ui;

import android.view.View;

public final /* synthetic */ class PhotoViewer$$ExternalSyntheticLambda36 implements View.OnClickListener {
    public final /* synthetic */ PhotoViewer f$0;

    public /* synthetic */ PhotoViewer$$ExternalSyntheticLambda36(PhotoViewer photoViewer) {
        this.f$0 = photoViewer;
    }

    public final void onClick(View view) {
        this.f$0.lambda$switchToEditMode$58(view);
    }
}
