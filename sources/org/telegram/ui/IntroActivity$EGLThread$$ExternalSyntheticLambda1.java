package org.telegram.ui;

import org.telegram.messenger.GenericProvider;
import org.telegram.ui.IntroActivity;

public final /* synthetic */ class IntroActivity$EGLThread$$ExternalSyntheticLambda1 implements GenericProvider {
    public static final /* synthetic */ IntroActivity$EGLThread$$ExternalSyntheticLambda1 INSTANCE = new IntroActivity$EGLThread$$ExternalSyntheticLambda1();

    private /* synthetic */ IntroActivity$EGLThread$$ExternalSyntheticLambda1() {
    }

    public final Object provide(Object obj) {
        return IntroActivity.EGLThread.lambda$initGL$1((Void) obj);
    }
}
