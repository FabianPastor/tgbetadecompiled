package org.telegram.ui;

import android.view.View;
import android.view.WindowInsets;

public final /* synthetic */ class PhotoViewer$$ExternalSyntheticLambda12 implements View.OnApplyWindowInsetsListener {
    public final /* synthetic */ PhotoViewer f$0;

    public /* synthetic */ PhotoViewer$$ExternalSyntheticLambda12(PhotoViewer photoViewer) {
        this.f$0 = photoViewer;
    }

    public final WindowInsets onApplyWindowInsets(View view, WindowInsets windowInsets) {
        return this.f$0.lambda$setParentActivity$4(view, windowInsets);
    }
}
