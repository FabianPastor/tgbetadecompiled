package org.telegram.ui;

import android.widget.ImageView;

public final /* synthetic */ class ChatActivity$$ExternalSyntheticLambda204 implements Runnable {
    public final /* synthetic */ ChatActivity f$0;
    public final /* synthetic */ boolean[] f$1;
    public final /* synthetic */ boolean[] f$2;
    public final /* synthetic */ ImageView f$3;
    public final /* synthetic */ ImageView f$4;

    public /* synthetic */ ChatActivity$$ExternalSyntheticLambda204(ChatActivity chatActivity, boolean[] zArr, boolean[] zArr2, ImageView imageView, ImageView imageView2) {
        this.f$0 = chatActivity;
        this.f$1 = zArr;
        this.f$2 = zArr2;
        this.f$3 = imageView;
        this.f$4 = imageView2;
    }

    public final void run() {
        this.f$0.lambda$createMenu$168(this.f$1, this.f$2, this.f$3, this.f$4);
    }
}
