package org.telegram.ui;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;
import org.telegram.ui.WallpapersListActivity;

public final /* synthetic */ class WallpapersListActivity$SearchAdapter$$ExternalSyntheticLambda5 implements RecyclerListView.OnItemClickListener {
    public final /* synthetic */ WallpapersListActivity.SearchAdapter f$0;

    public /* synthetic */ WallpapersListActivity$SearchAdapter$$ExternalSyntheticLambda5(WallpapersListActivity.SearchAdapter searchAdapter) {
        this.f$0 = searchAdapter;
    }

    public final void onItemClick(View view, int i) {
        this.f$0.lambda$onCreateViewHolder$5(view, i);
    }
}
