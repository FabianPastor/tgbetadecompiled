package org.telegram.ui;

import android.content.Context;
import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class StickersActivity$$ExternalSyntheticLambda4 implements RecyclerListView.OnItemClickListener {
    public final /* synthetic */ StickersActivity f$0;
    public final /* synthetic */ Context f$1;

    public /* synthetic */ StickersActivity$$ExternalSyntheticLambda4(StickersActivity stickersActivity, Context context) {
        this.f$0 = stickersActivity;
        this.f$1 = context;
    }

    public final void onItemClick(View view, int i) {
        this.f$0.lambda$createView$2(this.f$1, view, i);
    }
}
