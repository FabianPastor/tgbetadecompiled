package org.telegram.ui;

import android.view.MotionEvent;
import android.view.View;

public final /* synthetic */ class TwoStepVerificationSetupActivity$$ExternalSyntheticLambda16 implements View.OnTouchListener {
    public static final /* synthetic */ TwoStepVerificationSetupActivity$$ExternalSyntheticLambda16 INSTANCE = new TwoStepVerificationSetupActivity$$ExternalSyntheticLambda16();

    private /* synthetic */ TwoStepVerificationSetupActivity$$ExternalSyntheticLambda16() {
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return TwoStepVerificationSetupActivity.lambda$createView$10(view, motionEvent);
    }
}
