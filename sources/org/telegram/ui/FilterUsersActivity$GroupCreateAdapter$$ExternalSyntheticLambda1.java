package org.telegram.ui;

import org.telegram.ui.FilterUsersActivity;

public final /* synthetic */ class FilterUsersActivity$GroupCreateAdapter$$ExternalSyntheticLambda1 implements Runnable {
    public final /* synthetic */ FilterUsersActivity.GroupCreateAdapter f$0;
    public final /* synthetic */ String f$1;

    public /* synthetic */ FilterUsersActivity$GroupCreateAdapter$$ExternalSyntheticLambda1(FilterUsersActivity.GroupCreateAdapter groupCreateAdapter, String str) {
        this.f$0 = groupCreateAdapter;
        this.f$1 = str;
    }

    public final void run() {
        this.f$0.lambda$searchDialogs$1(this.f$1);
    }
}
