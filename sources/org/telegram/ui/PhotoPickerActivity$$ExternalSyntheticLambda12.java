package org.telegram.ui;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class PhotoPickerActivity$$ExternalSyntheticLambda12 implements RecyclerListView.OnItemLongClickListener {
    public final /* synthetic */ PhotoPickerActivity f$0;

    public /* synthetic */ PhotoPickerActivity$$ExternalSyntheticLambda12(PhotoPickerActivity photoPickerActivity) {
        this.f$0 = photoPickerActivity;
    }

    public final boolean onItemClick(View view, int i) {
        return this.f$0.lambda$createView$2(view, i);
    }
}
