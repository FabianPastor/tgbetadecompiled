package org.telegram.ui;

import android.content.DialogInterface;
import org.telegram.ui.ActionBar.BaseFragment;

public final /* synthetic */ class SuggestClearDatabaseBottomSheet$$ExternalSyntheticLambda0 implements DialogInterface.OnClickListener {
    public final /* synthetic */ SuggestClearDatabaseBottomSheet f$0;
    public final /* synthetic */ BaseFragment f$1;

    public /* synthetic */ SuggestClearDatabaseBottomSheet$$ExternalSyntheticLambda0(SuggestClearDatabaseBottomSheet suggestClearDatabaseBottomSheet, BaseFragment baseFragment) {
        this.f$0 = suggestClearDatabaseBottomSheet;
        this.f$1 = baseFragment;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f$0.lambda$new$0(this.f$1, dialogInterface, i);
    }
}
