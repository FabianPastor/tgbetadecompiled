package org.telegram.ui;

import android.content.Context;
import android.view.View;

public final /* synthetic */ class LinkEditActivity$$ExternalSyntheticLambda4 implements View.OnClickListener {
    public final /* synthetic */ LinkEditActivity f$0;
    public final /* synthetic */ Context f$1;

    public /* synthetic */ LinkEditActivity$$ExternalSyntheticLambda4(LinkEditActivity linkEditActivity, Context context) {
        this.f$0 = linkEditActivity;
        this.f$1 = context;
    }

    public final void onClick(View view) {
        this.f$0.lambda$createView$2(this.f$1, view);
    }
}
