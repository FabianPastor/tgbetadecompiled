package org.telegram.ui;

import org.telegram.ui.Components.voip.VoIPFloatingLayout;

public final /* synthetic */ class VoIPFragment$$ExternalSyntheticLambda30 implements VoIPFloatingLayout.VoIPFloatingLayoutDelegate {
    public final /* synthetic */ VoIPFragment f$0;

    public /* synthetic */ VoIPFragment$$ExternalSyntheticLambda30(VoIPFragment voIPFragment) {
        this.f$0 = voIPFragment;
    }

    public final void onChange(float f, boolean z) {
        this.f$0.lambda$createView$5(f, z);
    }
}
