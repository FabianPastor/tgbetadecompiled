package org.telegram.ui;

import android.view.View;
import org.telegram.ui.CalendarActivity;

public final /* synthetic */ class CalendarActivity$MonthView$2$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ CalendarActivity.MonthView.AnonymousClass2 f$0;

    public /* synthetic */ CalendarActivity$MonthView$2$$ExternalSyntheticLambda0(CalendarActivity.MonthView.AnonymousClass2 r1) {
        this.f$0 = r1;
    }

    public final void onClick(View view) {
        this.f$0.lambda$onLongPress$3(view);
    }
}
