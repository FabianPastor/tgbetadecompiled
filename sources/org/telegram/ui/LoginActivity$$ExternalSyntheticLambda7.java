package org.telegram.ui;

import android.content.DialogInterface;

public final /* synthetic */ class LoginActivity$$ExternalSyntheticLambda7 implements DialogInterface.OnClickListener {
    public final /* synthetic */ LoginActivity f$0;
    public final /* synthetic */ String f$1;
    public final /* synthetic */ String f$2;
    public final /* synthetic */ String f$3;

    public /* synthetic */ LoginActivity$$ExternalSyntheticLambda7(LoginActivity loginActivity, String str, String str2, String str3) {
        this.f$0 = loginActivity;
        this.f$1 = str;
        this.f$2 = str2;
        this.f$3 = str3;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f$0.lambda$tryResetAccount$20(this.f$1, this.f$2, this.f$3, dialogInterface, i);
    }
}
