package org.telegram.ui;

import android.content.DialogInterface;

public final /* synthetic */ class DialogsActivity$$ExternalSyntheticLambda10 implements DialogInterface.OnClickListener {
    public final /* synthetic */ DialogsActivity f$0;

    public /* synthetic */ DialogsActivity$$ExternalSyntheticLambda10(DialogsActivity dialogsActivity) {
        this.f$0 = dialogsActivity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f$0.lambda$showSuggestion$46(dialogInterface, i);
    }
}
