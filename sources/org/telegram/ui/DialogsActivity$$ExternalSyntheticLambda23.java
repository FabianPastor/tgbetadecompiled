package org.telegram.ui;

import android.view.View;

public final /* synthetic */ class DialogsActivity$$ExternalSyntheticLambda23 implements View.OnClickListener {
    public final /* synthetic */ DialogsActivity f$0;
    public final /* synthetic */ long f$1;

    public /* synthetic */ DialogsActivity$$ExternalSyntheticLambda23(DialogsActivity dialogsActivity, long j) {
        this.f$0 = dialogsActivity;
        this.f$1 = j;
    }

    public final void onClick(View view) {
        this.f$0.lambda$showChatPreview$28(this.f$1, view);
    }
}
