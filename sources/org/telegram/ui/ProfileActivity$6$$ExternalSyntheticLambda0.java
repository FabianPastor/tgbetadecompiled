package org.telegram.ui;

import android.view.View;
import java.util.Comparator;
import org.telegram.ui.ProfileActivity;

public final /* synthetic */ class ProfileActivity$6$$ExternalSyntheticLambda0 implements Comparator {
    public static final /* synthetic */ ProfileActivity$6$$ExternalSyntheticLambda0 INSTANCE = new ProfileActivity$6$$ExternalSyntheticLambda0();

    private /* synthetic */ ProfileActivity$6$$ExternalSyntheticLambda0() {
    }

    public final int compare(Object obj, Object obj2) {
        return ProfileActivity.AnonymousClass6.lambda$$0((View) obj, (View) obj2);
    }
}
