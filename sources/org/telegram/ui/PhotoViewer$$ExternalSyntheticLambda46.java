package org.telegram.ui;

import android.view.View;
import android.widget.ViewSwitcher;

public final /* synthetic */ class PhotoViewer$$ExternalSyntheticLambda46 implements ViewSwitcher.ViewFactory {
    public final /* synthetic */ PhotoViewer f$0;

    public /* synthetic */ PhotoViewer$$ExternalSyntheticLambda46(PhotoViewer photoViewer) {
        this.f$0 = photoViewer;
    }

    public final View makeView() {
        return this.f$0.lambda$setParentActivity$6();
    }
}
