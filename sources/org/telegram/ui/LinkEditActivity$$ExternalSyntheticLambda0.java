package org.telegram.ui;

import android.content.DialogInterface;

public final /* synthetic */ class LinkEditActivity$$ExternalSyntheticLambda0 implements DialogInterface.OnClickListener {
    public final /* synthetic */ LinkEditActivity f$0;

    public /* synthetic */ LinkEditActivity$$ExternalSyntheticLambda0(LinkEditActivity linkEditActivity) {
        this.f$0 = linkEditActivity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f$0.lambda$createView$5(dialogInterface, i);
    }
}
