package org.telegram.ui;

import android.view.View;

public final /* synthetic */ class ThemeSetUrlActivity$$ExternalSyntheticLambda3 implements View.OnFocusChangeListener {
    public final /* synthetic */ ThemeSetUrlActivity f$0;

    public /* synthetic */ ThemeSetUrlActivity$$ExternalSyntheticLambda3(ThemeSetUrlActivity themeSetUrlActivity) {
        this.f$0 = themeSetUrlActivity;
    }

    public final void onFocusChange(View view, boolean z) {
        this.f$0.lambda$createView$3(view, z);
    }
}
