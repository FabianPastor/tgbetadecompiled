package org.telegram.ui;

import android.content.DialogInterface;

public final /* synthetic */ class SessionsActivity$$ExternalSyntheticLambda0 implements DialogInterface.OnClickListener {
    public final /* synthetic */ SessionsActivity f$0;

    public /* synthetic */ SessionsActivity$$ExternalSyntheticLambda0(SessionsActivity sessionsActivity) {
        this.f$0 = sessionsActivity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f$0.lambda$onRequestPermissionsResultFragment$20(dialogInterface, i);
    }
}
