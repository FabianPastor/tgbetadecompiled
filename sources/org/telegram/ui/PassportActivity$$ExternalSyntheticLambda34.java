package org.telegram.ui;

import android.view.MotionEvent;
import android.view.View;

public final /* synthetic */ class PassportActivity$$ExternalSyntheticLambda34 implements View.OnTouchListener {
    public final /* synthetic */ PassportActivity f$0;

    public /* synthetic */ PassportActivity$$ExternalSyntheticLambda34(PassportActivity passportActivity) {
        this.f$0 = passportActivity;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return this.f$0.lambda$createAddressInterface$35(view, motionEvent);
    }
}
