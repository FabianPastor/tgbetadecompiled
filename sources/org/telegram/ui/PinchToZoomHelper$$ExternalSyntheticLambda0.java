package org.telegram.ui;

import android.animation.ValueAnimator;

public final /* synthetic */ class PinchToZoomHelper$$ExternalSyntheticLambda0 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ PinchToZoomHelper f$0;

    public /* synthetic */ PinchToZoomHelper$$ExternalSyntheticLambda0(PinchToZoomHelper pinchToZoomHelper) {
        this.f$0 = pinchToZoomHelper;
    }

    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f$0.lambda$finishZoom$0(valueAnimator);
    }
}
