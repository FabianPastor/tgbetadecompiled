package org.telegram.ui;

import android.view.animation.Interpolator;

public final /* synthetic */ class DialogsActivity$$ExternalSyntheticLambda29 implements Interpolator {
    public static final /* synthetic */ DialogsActivity$$ExternalSyntheticLambda29 INSTANCE = new DialogsActivity$$ExternalSyntheticLambda29();

    private /* synthetic */ DialogsActivity$$ExternalSyntheticLambda29() {
    }

    public final float getInterpolation(float f) {
        return DialogsActivity.lambda$static$0(f);
    }
}
