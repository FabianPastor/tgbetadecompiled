package org.telegram.ui;

public final /* synthetic */ class ChatActivity$$ExternalSyntheticLambda203 implements Runnable {
    public final /* synthetic */ ChatActivity f$0;
    public final /* synthetic */ boolean[] f$1;
    public final /* synthetic */ int f$2;
    public final /* synthetic */ ChatActivity f$3;

    public /* synthetic */ ChatActivity$$ExternalSyntheticLambda203(ChatActivity chatActivity, boolean[] zArr, int i, ChatActivity chatActivity2) {
        this.f$0 = chatActivity;
        this.f$1 = zArr;
        this.f$2 = i;
        this.f$3 = chatActivity2;
    }

    public final void run() {
        this.f$0.lambda$processLoadedDiscussionMessage$221(this.f$1, this.f$2, this.f$3);
    }
}
