package org.telegram.ui;

import org.telegram.ui.ActionBar.ThemeDescription;

public final /* synthetic */ class ContactAddActivity$$ExternalSyntheticLambda4 implements ThemeDescription.ThemeDescriptionDelegate {
    public final /* synthetic */ ContactAddActivity f$0;

    public /* synthetic */ ContactAddActivity$$ExternalSyntheticLambda4(ContactAddActivity contactAddActivity) {
        this.f$0 = contactAddActivity;
    }

    public final void didSetColor() {
        this.f$0.lambda$getThemeDescriptions$4();
    }

    public /* synthetic */ void onAnimationProgress(float f) {
        ThemeDescription.ThemeDescriptionDelegate.CC.$default$onAnimationProgress(this, f);
    }
}
