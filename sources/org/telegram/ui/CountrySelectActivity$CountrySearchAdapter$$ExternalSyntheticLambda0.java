package org.telegram.ui;

import org.telegram.ui.CountrySelectActivity;

public final /* synthetic */ class CountrySelectActivity$CountrySearchAdapter$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ CountrySelectActivity.CountrySearchAdapter f$0;
    public final /* synthetic */ String f$1;

    public /* synthetic */ CountrySelectActivity$CountrySearchAdapter$$ExternalSyntheticLambda0(CountrySelectActivity.CountrySearchAdapter countrySearchAdapter, String str) {
        this.f$0 = countrySearchAdapter;
        this.f$1 = str;
    }

    public final void run() {
        this.f$0.lambda$processSearch$0(this.f$1);
    }
}
