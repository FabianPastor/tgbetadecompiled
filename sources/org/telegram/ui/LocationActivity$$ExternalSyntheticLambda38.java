package org.telegram.ui;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class LocationActivity$$ExternalSyntheticLambda38 implements RecyclerListView.OnItemClickListener {
    public final /* synthetic */ LocationActivity f$0;

    public /* synthetic */ LocationActivity$$ExternalSyntheticLambda38(LocationActivity locationActivity) {
        this.f$0 = locationActivity;
    }

    public final void onItemClick(View view, int i) {
        this.f$0.lambda$createView$20(view, i);
    }
}
