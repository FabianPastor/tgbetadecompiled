package org.telegram.ui;

import android.view.View;
import org.telegram.ui.CalendarActivity;

public final /* synthetic */ class CalendarActivity$MonthView$$ExternalSyntheticLambda1 implements View.OnLongClickListener {
    public final /* synthetic */ CalendarActivity.MonthView f$0;

    public /* synthetic */ CalendarActivity$MonthView$$ExternalSyntheticLambda1(CalendarActivity.MonthView monthView) {
        this.f$0 = monthView;
    }

    public final boolean onLongClick(View view) {
        return this.f$0.lambda$new$0(view);
    }
}
