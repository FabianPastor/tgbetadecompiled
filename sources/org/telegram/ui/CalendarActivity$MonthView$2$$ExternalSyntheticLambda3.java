package org.telegram.ui;

import android.view.View;
import org.telegram.ui.CalendarActivity;

public final /* synthetic */ class CalendarActivity$MonthView$2$$ExternalSyntheticLambda3 implements View.OnClickListener {
    public final /* synthetic */ CalendarActivity.MonthView.AnonymousClass2 f$0;
    public final /* synthetic */ CalendarActivity.PeriodDay f$1;

    public /* synthetic */ CalendarActivity$MonthView$2$$ExternalSyntheticLambda3(CalendarActivity.MonthView.AnonymousClass2 r1, CalendarActivity.PeriodDay periodDay) {
        this.f$0 = r1;
        this.f$1 = periodDay;
    }

    public final void onClick(View view) {
        this.f$0.lambda$onLongPress$2(this.f$1, view);
    }
}
