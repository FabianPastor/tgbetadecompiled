package org.telegram.ui;

import android.animation.ValueAnimator;

public final /* synthetic */ class VoIPFragment$$ExternalSyntheticLambda3 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ VoIPFragment f$0;
    public final /* synthetic */ boolean f$1;
    public final /* synthetic */ float f$10;
    public final /* synthetic */ float f$11;
    public final /* synthetic */ float f$12;
    public final /* synthetic */ float f$13;
    public final /* synthetic */ float f$14;
    public final /* synthetic */ float f$15;
    public final /* synthetic */ float f$16;
    public final /* synthetic */ float f$17;
    public final /* synthetic */ float f$2;
    public final /* synthetic */ float f$3;
    public final /* synthetic */ float f$4;
    public final /* synthetic */ float f$5;
    public final /* synthetic */ float f$6;
    public final /* synthetic */ float f$7;
    public final /* synthetic */ float f$8;
    public final /* synthetic */ float f$9;

    public /* synthetic */ VoIPFragment$$ExternalSyntheticLambda3(VoIPFragment voIPFragment, boolean z, float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9, float var_, float var_, float var_, float var_, float var_, float var_, float var_) {
        this.f$0 = voIPFragment;
        this.f$1 = z;
        this.f$2 = f;
        this.f$3 = f2;
        this.f$4 = f3;
        this.f$5 = f4;
        this.f$6 = f5;
        this.f$7 = f6;
        this.f$8 = f7;
        this.f$9 = f8;
        this.f$10 = f9;
        this.f$11 = var_;
        this.f$12 = var_;
        this.f$13 = var_;
        this.f$14 = var_;
        this.f$15 = var_;
        this.f$16 = var_;
        this.f$17 = var_;
    }

    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        ValueAnimator valueAnimator2 = valueAnimator;
        VoIPFragment voIPFragment = this.f$0;
        VoIPFragment voIPFragment2 = voIPFragment;
        voIPFragment2.lambda$createPiPTransition$15(this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, this.f$6, this.f$7, this.f$8, this.f$9, this.f$10, this.f$11, this.f$12, this.f$13, this.f$14, this.f$15, this.f$16, this.f$17, valueAnimator2);
    }
}
