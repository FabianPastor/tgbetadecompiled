package org.telegram.ui;

public final /* synthetic */ class ChatActivity$$ExternalSyntheticLambda169 implements Runnable {
    public final /* synthetic */ ChatActivity f$0;
    public final /* synthetic */ long f$1;

    public /* synthetic */ ChatActivity$$ExternalSyntheticLambda169(ChatActivity chatActivity, long j) {
        this.f$0 = chatActivity;
        this.f$1 = j;
    }

    public final void run() {
        this.f$0.lambda$migrateToNewChat$128(this.f$1);
    }
}
