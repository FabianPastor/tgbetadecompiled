package org.telegram.ui;

import android.view.View;

public final /* synthetic */ class TwoStepVerificationSetupActivity$$ExternalSyntheticLambda15 implements View.OnFocusChangeListener {
    public final /* synthetic */ TwoStepVerificationSetupActivity f$0;

    public /* synthetic */ TwoStepVerificationSetupActivity$$ExternalSyntheticLambda15(TwoStepVerificationSetupActivity twoStepVerificationSetupActivity) {
        this.f$0 = twoStepVerificationSetupActivity;
    }

    public final void onFocusChange(View view, boolean z) {
        this.f$0.lambda$createView$16(view, z);
    }
}
