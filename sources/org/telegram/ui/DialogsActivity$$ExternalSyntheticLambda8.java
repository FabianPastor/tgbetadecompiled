package org.telegram.ui;

import android.content.DialogInterface;

public final /* synthetic */ class DialogsActivity$$ExternalSyntheticLambda8 implements DialogInterface.OnClickListener {
    public final /* synthetic */ DialogsActivity f$0;

    public /* synthetic */ DialogsActivity$$ExternalSyntheticLambda8(DialogsActivity dialogsActivity) {
        this.f$0 = dialogsActivity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f$0.lambda$onArchiveLongPress$24(dialogInterface, i);
    }
}
