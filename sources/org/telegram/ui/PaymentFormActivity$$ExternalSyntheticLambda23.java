package org.telegram.ui;

import android.view.MotionEvent;
import android.view.View;

public final /* synthetic */ class PaymentFormActivity$$ExternalSyntheticLambda23 implements View.OnTouchListener {
    public final /* synthetic */ PaymentFormActivity f$0;

    public /* synthetic */ PaymentFormActivity$$ExternalSyntheticLambda23(PaymentFormActivity paymentFormActivity) {
        this.f$0 = paymentFormActivity;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return this.f$0.lambda$createView$6(view, motionEvent);
    }
}
