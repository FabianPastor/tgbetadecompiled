package org.telegram.ui;

import android.content.DialogInterface;

public final /* synthetic */ class PassportActivity$$ExternalSyntheticLambda4 implements DialogInterface.OnClickListener {
    public final /* synthetic */ PassportActivity f$0;

    public /* synthetic */ PassportActivity$$ExternalSyntheticLambda4(PassportActivity passportActivity) {
        this.f$0 = passportActivity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f$0.lambda$onRequestPermissionsResultFragment$68(dialogInterface, i);
    }
}
