package org.telegram.ui;

import java.util.ArrayList;
import org.telegram.ui.NotificationsCustomSettingsActivity;

public final /* synthetic */ class NotificationsCustomSettingsActivity$SearchAdapter$$ExternalSyntheticLambda3 implements Runnable {
    public final /* synthetic */ NotificationsCustomSettingsActivity.SearchAdapter f$0;
    public final /* synthetic */ ArrayList f$1;
    public final /* synthetic */ ArrayList f$2;
    public final /* synthetic */ ArrayList f$3;

    public /* synthetic */ NotificationsCustomSettingsActivity$SearchAdapter$$ExternalSyntheticLambda3(NotificationsCustomSettingsActivity.SearchAdapter searchAdapter, ArrayList arrayList, ArrayList arrayList2, ArrayList arrayList3) {
        this.f$0 = searchAdapter;
        this.f$1 = arrayList;
        this.f$2 = arrayList2;
        this.f$3 = arrayList3;
    }

    public final void run() {
        this.f$0.lambda$updateSearchResults$4(this.f$1, this.f$2, this.f$3);
    }
}
