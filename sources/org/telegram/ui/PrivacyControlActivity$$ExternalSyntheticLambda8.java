package org.telegram.ui;

import java.util.ArrayList;
import org.telegram.ui.GroupCreateActivity;

public final /* synthetic */ class PrivacyControlActivity$$ExternalSyntheticLambda8 implements GroupCreateActivity.GroupCreateActivityDelegate {
    public final /* synthetic */ PrivacyControlActivity f$0;
    public final /* synthetic */ int f$1;

    public /* synthetic */ PrivacyControlActivity$$ExternalSyntheticLambda8(PrivacyControlActivity privacyControlActivity, int i) {
        this.f$0 = privacyControlActivity;
        this.f$1 = i;
    }

    public final void didSelectUsers(ArrayList arrayList) {
        this.f$0.lambda$createView$0(this.f$1, arrayList);
    }
}
