package org.telegram.ui.Cells;

import android.animation.ValueAnimator;

public final /* synthetic */ class DialogsEmptyCell$$ExternalSyntheticLambda1 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ DialogsEmptyCell f$0;

    public /* synthetic */ DialogsEmptyCell$$ExternalSyntheticLambda1(DialogsEmptyCell dialogsEmptyCell) {
        this.f$0 = dialogsEmptyCell;
    }

    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f$0.lambda$startUtyanCollapseAnimation$4(valueAnimator);
    }
}
