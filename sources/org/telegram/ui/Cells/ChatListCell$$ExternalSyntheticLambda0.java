package org.telegram.ui.Cells;

import android.view.View;

public final /* synthetic */ class ChatListCell$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ ChatListCell f$0;
    public final /* synthetic */ boolean f$1;

    public /* synthetic */ ChatListCell$$ExternalSyntheticLambda0(ChatListCell chatListCell, boolean z) {
        this.f$0 = chatListCell;
        this.f$1 = z;
    }

    public final void onClick(View view) {
        this.f$0.lambda$new$0(this.f$1, view);
    }
}
