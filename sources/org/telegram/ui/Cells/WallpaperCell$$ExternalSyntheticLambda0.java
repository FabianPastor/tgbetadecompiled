package org.telegram.ui.Cells;

import android.view.View;
import org.telegram.ui.Cells.WallpaperCell;

public final /* synthetic */ class WallpaperCell$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ WallpaperCell f$0;
    public final /* synthetic */ WallpaperCell.WallpaperView f$1;
    public final /* synthetic */ int f$2;

    public /* synthetic */ WallpaperCell$$ExternalSyntheticLambda0(WallpaperCell wallpaperCell, WallpaperCell.WallpaperView wallpaperView, int i) {
        this.f$0 = wallpaperCell;
        this.f$1 = wallpaperView;
        this.f$2 = i;
    }

    public final void onClick(View view) {
        this.f$0.lambda$new$0(this.f$1, this.f$2, view);
    }
}
