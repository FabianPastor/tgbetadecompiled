package org.telegram.ui.Cells;

import org.telegram.ui.Components.CheckBoxBase;

public final /* synthetic */ class HintDialogCell$$ExternalSyntheticLambda0 implements CheckBoxBase.ProgressDelegate {
    public final /* synthetic */ HintDialogCell f$0;

    public /* synthetic */ HintDialogCell$$ExternalSyntheticLambda0(HintDialogCell hintDialogCell) {
        this.f$0 = hintDialogCell;
    }

    public final void setProgress(float f) {
        this.f$0.lambda$new$0(f);
    }
}
