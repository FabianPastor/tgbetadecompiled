package org.telegram.ui.Cells;

import android.content.Context;
import android.view.View;
import android.widget.ViewSwitcher;

public final /* synthetic */ class DialogsEmptyCell$$ExternalSyntheticLambda4 implements ViewSwitcher.ViewFactory {
    public final /* synthetic */ Context f$0;

    public /* synthetic */ DialogsEmptyCell$$ExternalSyntheticLambda4(Context context) {
        this.f$0 = context;
    }

    public final View makeView() {
        return DialogsEmptyCell.lambda$new$2(this.f$0);
    }
}
