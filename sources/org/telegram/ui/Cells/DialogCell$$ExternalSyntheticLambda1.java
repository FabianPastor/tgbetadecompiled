package org.telegram.ui.Cells;

import android.animation.ValueAnimator;

public final /* synthetic */ class DialogCell$$ExternalSyntheticLambda1 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ DialogCell f$0;

    public /* synthetic */ DialogCell$$ExternalSyntheticLambda1(DialogCell dialogCell) {
        this.f$0 = dialogCell;
    }

    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f$0.lambda$createStatusDrawableAnimator$2(valueAnimator);
    }
}
