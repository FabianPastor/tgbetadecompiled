package org.telegram.ui.Cells;

import android.view.View;
import org.telegram.ui.Cells.MemberRequestCell;

public final /* synthetic */ class MemberRequestCell$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ MemberRequestCell f$0;
    public final /* synthetic */ MemberRequestCell.OnClickListener f$1;

    public /* synthetic */ MemberRequestCell$$ExternalSyntheticLambda0(MemberRequestCell memberRequestCell, MemberRequestCell.OnClickListener onClickListener) {
        this.f$0 = memberRequestCell;
        this.f$1 = onClickListener;
    }

    public final void onClick(View view) {
        this.f$0.lambda$new$1(this.f$1, view);
    }
}
