package org.telegram.ui.Cells;

import android.animation.ValueAnimator;

public final /* synthetic */ class TextSelectionHelper$$ExternalSyntheticLambda0 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ TextSelectionHelper f$0;

    public /* synthetic */ TextSelectionHelper$$ExternalSyntheticLambda0(TextSelectionHelper textSelectionHelper) {
        this.f$0 = textSelectionHelper;
    }

    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f$0.lambda$showHandleViews$0(valueAnimator);
    }
}
