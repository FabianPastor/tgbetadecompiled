package org.telegram.ui.Cells;

import android.animation.ValueAnimator;
import org.telegram.ui.Cells.AppIconsSelectorCell;

public final /* synthetic */ class AppIconsSelectorCell$IconHolderView$$ExternalSyntheticLambda0 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ AppIconsSelectorCell.IconHolderView f$0;

    public /* synthetic */ AppIconsSelectorCell$IconHolderView$$ExternalSyntheticLambda0(AppIconsSelectorCell.IconHolderView iconHolderView) {
        this.f$0 = iconHolderView;
    }

    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f$0.lambda$setSelected$0(valueAnimator);
    }
}
