package org.telegram.ui.Cells;

import android.content.DialogInterface;
import org.telegram.ui.Cells.AboutLinkCell;

public final /* synthetic */ class AboutLinkCell$3$$ExternalSyntheticLambda1 implements DialogInterface.OnDismissListener {
    public final /* synthetic */ AboutLinkCell.AnonymousClass3 f$0;

    public /* synthetic */ AboutLinkCell$3$$ExternalSyntheticLambda1(AboutLinkCell.AnonymousClass3 r1) {
        this.f$0 = r1;
    }

    public final void onDismiss(DialogInterface dialogInterface) {
        this.f$0.lambda$run$1(dialogInterface);
    }
}
