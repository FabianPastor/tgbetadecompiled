package org.telegram.ui.Cells;

import android.view.View;

public final /* synthetic */ class SharedPhotoVideoCell$$ExternalSyntheticLambda1 implements View.OnLongClickListener {
    public final /* synthetic */ SharedPhotoVideoCell f$0;

    public /* synthetic */ SharedPhotoVideoCell$$ExternalSyntheticLambda1(SharedPhotoVideoCell sharedPhotoVideoCell) {
        this.f$0 = sharedPhotoVideoCell;
    }

    public final boolean onLongClick(View view) {
        return this.f$0.lambda$new$1(view);
    }
}
