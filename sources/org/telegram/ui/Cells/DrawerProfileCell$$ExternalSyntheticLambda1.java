package org.telegram.ui.Cells;

import android.view.View;
import org.telegram.ui.ActionBar.DrawerLayoutContainer;

public final /* synthetic */ class DrawerProfileCell$$ExternalSyntheticLambda1 implements View.OnLongClickListener {
    public final /* synthetic */ DrawerLayoutContainer f$0;

    public /* synthetic */ DrawerProfileCell$$ExternalSyntheticLambda1(DrawerLayoutContainer drawerLayoutContainer) {
        this.f$0 = drawerLayoutContainer;
    }

    public final boolean onLongClick(View view) {
        return DrawerProfileCell.lambda$new$1(this.f$0, view);
    }
}
