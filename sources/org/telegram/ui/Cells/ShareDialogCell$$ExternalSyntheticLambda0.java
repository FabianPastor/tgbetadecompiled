package org.telegram.ui.Cells;

import org.telegram.ui.Components.CheckBoxBase;

public final /* synthetic */ class ShareDialogCell$$ExternalSyntheticLambda0 implements CheckBoxBase.ProgressDelegate {
    public final /* synthetic */ ShareDialogCell f$0;

    public /* synthetic */ ShareDialogCell$$ExternalSyntheticLambda0(ShareDialogCell shareDialogCell) {
        this.f$0 = shareDialogCell;
    }

    public final void setProgress(float f) {
        this.f$0.lambda$new$0(f);
    }
}
