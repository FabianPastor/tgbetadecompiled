package org.telegram.ui.Cells;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class ThemesHorizontalListCell$$ExternalSyntheticLambda3 implements RecyclerListView.OnItemLongClickListener {
    public final /* synthetic */ ThemesHorizontalListCell f$0;

    public /* synthetic */ ThemesHorizontalListCell$$ExternalSyntheticLambda3(ThemesHorizontalListCell themesHorizontalListCell) {
        this.f$0 = themesHorizontalListCell;
    }

    public final boolean onItemClick(View view, int i) {
        return this.f$0.lambda$new$1(view, i);
    }
}
