package org.telegram.ui.Cells;

import android.animation.ValueAnimator;

public final /* synthetic */ class GroupCreateUserCell$$ExternalSyntheticLambda0 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ GroupCreateUserCell f$0;

    public /* synthetic */ GroupCreateUserCell$$ExternalSyntheticLambda0(GroupCreateUserCell groupCreateUserCell) {
        this.f$0 = groupCreateUserCell;
    }

    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f$0.lambda$setChecked$0(valueAnimator);
    }
}
