package org.telegram.ui.Cells;

import android.animation.ValueAnimator;

public final /* synthetic */ class DialogsEmptyCell$$ExternalSyntheticLambda0 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ DialogsEmptyCell f$0;

    public /* synthetic */ DialogsEmptyCell$$ExternalSyntheticLambda0(DialogsEmptyCell dialogsEmptyCell) {
        this.f$0 = dialogsEmptyCell;
    }

    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f$0.lambda$startUtyanExpandAnimation$3(valueAnimator);
    }
}
