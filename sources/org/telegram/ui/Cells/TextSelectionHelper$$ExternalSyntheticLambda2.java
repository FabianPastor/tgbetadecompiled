package org.telegram.ui.Cells;

import android.view.MotionEvent;
import android.view.View;

public final /* synthetic */ class TextSelectionHelper$$ExternalSyntheticLambda2 implements View.OnTouchListener {
    public final /* synthetic */ TextSelectionHelper f$0;

    public /* synthetic */ TextSelectionHelper$$ExternalSyntheticLambda2(TextSelectionHelper textSelectionHelper) {
        this.f$0 = textSelectionHelper;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return this.f$0.lambda$showActions$1(view, motionEvent);
    }
}
