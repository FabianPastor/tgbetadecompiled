package org.telegram.ui.Cells;

import org.telegram.tgnet.TLObject;
import org.telegram.ui.Cells.ThemesHorizontalListCell;

public final /* synthetic */ class ThemesHorizontalListCell$InnerThemeView$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ ThemesHorizontalListCell.InnerThemeView f$0;
    public final /* synthetic */ TLObject f$1;

    public /* synthetic */ ThemesHorizontalListCell$InnerThemeView$$ExternalSyntheticLambda0(ThemesHorizontalListCell.InnerThemeView innerThemeView, TLObject tLObject) {
        this.f$0 = innerThemeView;
        this.f$1 = tLObject;
    }

    public final void run() {
        this.f$0.lambda$parseTheme$0(this.f$1);
    }
}
