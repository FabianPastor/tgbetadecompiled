package org.telegram.ui.Cells;

import android.animation.ValueAnimator;

public final /* synthetic */ class GroupCallUserCell$$ExternalSyntheticLambda0 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ GroupCallUserCell f$0;
    public final /* synthetic */ int f$1;
    public final /* synthetic */ int f$2;

    public /* synthetic */ GroupCallUserCell$$ExternalSyntheticLambda0(GroupCallUserCell groupCallUserCell, int i, int i2) {
        this.f$0 = groupCallUserCell;
        this.f$1 = i;
        this.f$2 = i2;
    }

    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f$0.lambda$applyParticipantChanges$6(this.f$1, this.f$2, valueAnimator);
    }
}
