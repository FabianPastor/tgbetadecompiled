package org.telegram.ui.Cells;

import java.io.File;
import org.telegram.ui.ActionBar.Theme;

public final /* synthetic */ class ThemesHorizontalListCell$$ExternalSyntheticLambda1 implements Runnable {
    public final /* synthetic */ ThemesHorizontalListCell f$0;
    public final /* synthetic */ Theme.ThemeInfo f$1;
    public final /* synthetic */ File f$2;

    public /* synthetic */ ThemesHorizontalListCell$$ExternalSyntheticLambda1(ThemesHorizontalListCell themesHorizontalListCell, Theme.ThemeInfo themeInfo, File file) {
        this.f$0 = themesHorizontalListCell;
        this.f$1 = themeInfo;
        this.f$2 = file;
    }

    public final void run() {
        this.f$0.lambda$didReceivedNotification$3(this.f$1, this.f$2);
    }
}
