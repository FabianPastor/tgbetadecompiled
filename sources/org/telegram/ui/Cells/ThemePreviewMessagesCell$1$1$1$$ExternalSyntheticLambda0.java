package org.telegram.ui.Cells;

import android.animation.ValueAnimator;
import org.telegram.ui.Cells.ThemePreviewMessagesCell;

public final /* synthetic */ class ThemePreviewMessagesCell$1$1$1$$ExternalSyntheticLambda0 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ ThemePreviewMessagesCell.AnonymousClass1.AnonymousClass1.AnonymousClass1 f$0;

    public /* synthetic */ ThemePreviewMessagesCell$1$1$1$$ExternalSyntheticLambda0(ThemePreviewMessagesCell.AnonymousClass1.AnonymousClass1.AnonymousClass1 r1) {
        this.f$0 = r1;
    }

    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f$0.lambda$onPreDraw$0(valueAnimator);
    }
}
