package org.telegram.ui;

import android.view.KeyEvent;
import android.view.View;

public final /* synthetic */ class PassportActivity$$ExternalSyntheticLambda32 implements View.OnKeyListener {
    public final /* synthetic */ PassportActivity f$0;

    public /* synthetic */ PassportActivity$$ExternalSyntheticLambda32(PassportActivity passportActivity) {
        this.f$0 = passportActivity;
    }

    public final boolean onKey(View view, int i, KeyEvent keyEvent) {
        return this.f$0.lambda$createPhoneInterface$31(view, i, keyEvent);
    }
}
