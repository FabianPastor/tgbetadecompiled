package org.telegram.ui;

import android.view.View;

public final /* synthetic */ class PhotoPickerActivity$$ExternalSyntheticLambda3 implements View.OnLongClickListener {
    public final /* synthetic */ PhotoPickerActivity f$0;

    public /* synthetic */ PhotoPickerActivity$$ExternalSyntheticLambda3(PhotoPickerActivity photoPickerActivity) {
        this.f$0 = photoPickerActivity;
    }

    public final boolean onLongClick(View view) {
        return this.f$0.lambda$createView$7(view);
    }
}
