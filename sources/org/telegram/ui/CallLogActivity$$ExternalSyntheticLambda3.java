package org.telegram.ui;

import android.view.MotionEvent;
import android.view.View;

public final /* synthetic */ class CallLogActivity$$ExternalSyntheticLambda3 implements View.OnTouchListener {
    public static final /* synthetic */ CallLogActivity$$ExternalSyntheticLambda3 INSTANCE = new CallLogActivity$$ExternalSyntheticLambda3();

    private /* synthetic */ CallLogActivity$$ExternalSyntheticLambda3() {
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return CallLogActivity.lambda$createActionMode$7(view, motionEvent);
    }
}
