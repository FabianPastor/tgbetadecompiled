package org.telegram.ui;

import android.view.View;
import org.telegram.ui.StickersActivity;

public final /* synthetic */ class StickersActivity$ListAdapter$$ExternalSyntheticLambda2 implements View.OnClickListener {
    public final /* synthetic */ StickersActivity.ListAdapter f$0;

    public /* synthetic */ StickersActivity$ListAdapter$$ExternalSyntheticLambda2(StickersActivity.ListAdapter listAdapter) {
        this.f$0 = listAdapter;
    }

    public final void onClick(View view) {
        this.f$0.lambda$onCreateViewHolder$4(view);
    }
}
