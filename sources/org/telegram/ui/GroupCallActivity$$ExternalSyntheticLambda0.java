package org.telegram.ui;

import android.animation.ValueAnimator;

public final /* synthetic */ class GroupCallActivity$$ExternalSyntheticLambda0 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ GroupCallActivity f$0;

    public /* synthetic */ GroupCallActivity$$ExternalSyntheticLambda0(GroupCallActivity groupCallActivity) {
        this.f$0 = groupCallActivity;
    }

    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f$0.lambda$updateMuteButton$51(valueAnimator);
    }
}
