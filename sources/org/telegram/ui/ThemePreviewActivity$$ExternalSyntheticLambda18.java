package org.telegram.ui;

import org.telegram.ui.ActionBar.Theme;

public final /* synthetic */ class ThemePreviewActivity$$ExternalSyntheticLambda18 implements Runnable {
    public static final /* synthetic */ ThemePreviewActivity$$ExternalSyntheticLambda18 INSTANCE = new ThemePreviewActivity$$ExternalSyntheticLambda18();

    private /* synthetic */ ThemePreviewActivity$$ExternalSyntheticLambda18() {
    }

    public final void run() {
        Theme.setChangingWallpaper(false);
    }
}
