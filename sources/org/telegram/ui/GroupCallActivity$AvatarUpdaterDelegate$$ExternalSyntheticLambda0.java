package org.telegram.ui;

import org.telegram.ui.GroupCallActivity;

public final /* synthetic */ class GroupCallActivity$AvatarUpdaterDelegate$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ GroupCallActivity.AvatarUpdaterDelegate f$0;

    public /* synthetic */ GroupCallActivity$AvatarUpdaterDelegate$$ExternalSyntheticLambda0(GroupCallActivity.AvatarUpdaterDelegate avatarUpdaterDelegate) {
        this.f$0 = avatarUpdaterDelegate;
    }

    public final void run() {
        this.f$0.lambda$didUploadPhoto$2();
    }
}
