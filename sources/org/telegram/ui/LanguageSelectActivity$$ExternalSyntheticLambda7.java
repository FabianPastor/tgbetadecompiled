package org.telegram.ui;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class LanguageSelectActivity$$ExternalSyntheticLambda7 implements RecyclerListView.OnItemLongClickListener {
    public final /* synthetic */ LanguageSelectActivity f$0;

    public /* synthetic */ LanguageSelectActivity$$ExternalSyntheticLambda7(LanguageSelectActivity languageSelectActivity) {
        this.f$0 = languageSelectActivity;
    }

    public final boolean onItemClick(View view, int i) {
        return this.f$0.lambda$createView$3(view, i);
    }
}
