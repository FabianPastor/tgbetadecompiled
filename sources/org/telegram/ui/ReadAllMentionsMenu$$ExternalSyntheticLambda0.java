package org.telegram.ui;

import android.view.View;

public final /* synthetic */ class ReadAllMentionsMenu$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ Runnable f$0;

    public /* synthetic */ ReadAllMentionsMenu$$ExternalSyntheticLambda0(Runnable runnable) {
        this.f$0 = runnable;
    }

    public final void onClick(View view) {
        ReadAllMentionsMenu.lambda$show$0(this.f$0, view);
    }
}
