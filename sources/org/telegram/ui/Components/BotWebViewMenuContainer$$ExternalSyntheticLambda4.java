package org.telegram.ui.Components;

import androidx.core.util.Consumer;

public final /* synthetic */ class BotWebViewMenuContainer$$ExternalSyntheticLambda4 implements Consumer {
    public final /* synthetic */ BotWebViewMenuContainer f$0;

    public /* synthetic */ BotWebViewMenuContainer$$ExternalSyntheticLambda4(BotWebViewMenuContainer botWebViewMenuContainer) {
        this.f$0 = botWebViewMenuContainer;
    }

    public final void accept(Object obj) {
        this.f$0.lambda$onDismiss$18((Float) obj);
    }
}
