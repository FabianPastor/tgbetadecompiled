package org.telegram.ui.Components;

import android.view.MotionEvent;
import android.view.View;

public final /* synthetic */ class AlertsCreator$$ExternalSyntheticLambda78 implements View.OnTouchListener {
    public static final /* synthetic */ AlertsCreator$$ExternalSyntheticLambda78 INSTANCE = new AlertsCreator$$ExternalSyntheticLambda78();

    private /* synthetic */ AlertsCreator$$ExternalSyntheticLambda78() {
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return AlertsCreator.lambda$createCalendarPickerDialog$76(view, motionEvent);
    }
}
