package org.telegram.ui.Components;

import android.animation.ValueAnimator;

public final /* synthetic */ class PhotoViewerCaptionEnterView$$ExternalSyntheticLambda0 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ PhotoViewerCaptionEnterView f$0;

    public /* synthetic */ PhotoViewerCaptionEnterView$$ExternalSyntheticLambda0(PhotoViewerCaptionEnterView photoViewerCaptionEnterView) {
        this.f$0 = photoViewerCaptionEnterView;
    }

    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f$0.lambda$onDraw$7(valueAnimator);
    }
}
