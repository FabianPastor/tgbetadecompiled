package org.telegram.ui.Components;

import android.view.View;
import org.telegram.ui.Components.MediaActionDrawable;

public final /* synthetic */ class RadialProgress2$$ExternalSyntheticLambda0 implements MediaActionDrawable.MediaActionDrawableDelegate {
    public final /* synthetic */ View f$0;

    public /* synthetic */ RadialProgress2$$ExternalSyntheticLambda0(View view) {
        this.f$0 = view;
    }

    public final void invalidate() {
        this.f$0.invalidate();
    }
}
