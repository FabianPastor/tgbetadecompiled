package org.telegram.ui.Components;

public final /* synthetic */ class ChatActivityEnterView$$ExternalSyntheticLambda47 implements Runnable {
    public final /* synthetic */ ChatActivityEnterView f$0;
    public final /* synthetic */ CharSequence f$1;

    public /* synthetic */ ChatActivityEnterView$$ExternalSyntheticLambda47(ChatActivityEnterView chatActivityEnterView, CharSequence charSequence) {
        this.f$0 = chatActivityEnterView;
        this.f$1 = charSequence;
    }

    public final void run() {
        this.f$0.lambda$setEditingMessageObject$43(this.f$1);
    }
}
