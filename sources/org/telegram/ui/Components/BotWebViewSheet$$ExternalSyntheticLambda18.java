package org.telegram.ui.Components;

import org.telegram.ui.Components.SimpleFloatPropertyCompat;

public final /* synthetic */ class BotWebViewSheet$$ExternalSyntheticLambda18 implements SimpleFloatPropertyCompat.Getter {
    public static final /* synthetic */ BotWebViewSheet$$ExternalSyntheticLambda18 INSTANCE = new BotWebViewSheet$$ExternalSyntheticLambda18();

    private /* synthetic */ BotWebViewSheet$$ExternalSyntheticLambda18() {
    }

    public final float get(Object obj) {
        return ((BotWebViewSheet) obj).actionBarTransitionProgress;
    }
}
