package org.telegram.ui.Components;

public final /* synthetic */ class ChatAttachAlert$$ExternalSyntheticLambda23 implements Runnable {
    public final /* synthetic */ ChatAttachAlert f$0;
    public final /* synthetic */ EditTextBoldCursor f$1;
    public final /* synthetic */ boolean f$2;

    public /* synthetic */ ChatAttachAlert$$ExternalSyntheticLambda23(ChatAttachAlert chatAttachAlert, EditTextBoldCursor editTextBoldCursor, boolean z) {
        this.f$0 = chatAttachAlert;
        this.f$1 = editTextBoldCursor;
        this.f$2 = z;
    }

    public final void run() {
        this.f$0.lambda$makeFocusable$31(this.f$1, this.f$2);
    }
}
