package org.telegram.ui.Components;

import android.view.View;
import org.telegram.ui.ActionBar.BaseFragment;

public final /* synthetic */ class LinkActionView$$ExternalSyntheticLambda7 implements View.OnClickListener {
    public final /* synthetic */ LinkActionView f$0;
    public final /* synthetic */ BaseFragment f$1;

    public /* synthetic */ LinkActionView$$ExternalSyntheticLambda7(LinkActionView linkActionView, BaseFragment baseFragment) {
        this.f$0 = linkActionView;
        this.f$1 = baseFragment;
    }

    public final void onClick(View view) {
        this.f$0.lambda$new$2(this.f$1, view);
    }
}
