package org.telegram.ui.Components;

import java.util.ArrayList;

public final /* synthetic */ class StickersAlert$$ExternalSyntheticLambda20 implements Runnable {
    public final /* synthetic */ StickersAlert f$0;
    public final /* synthetic */ ArrayList f$1;
    public final /* synthetic */ Boolean f$2;

    public /* synthetic */ StickersAlert$$ExternalSyntheticLambda20(StickersAlert stickersAlert, ArrayList arrayList, Boolean bool) {
        this.f$0 = stickersAlert;
        this.f$1 = arrayList;
        this.f$2 = bool;
    }

    public final void run() {
        this.f$0.lambda$new$3(this.f$1, this.f$2);
    }
}
