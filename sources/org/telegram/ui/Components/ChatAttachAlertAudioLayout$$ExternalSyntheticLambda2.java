package org.telegram.ui.Components;

import java.util.ArrayList;

public final /* synthetic */ class ChatAttachAlertAudioLayout$$ExternalSyntheticLambda2 implements Runnable {
    public final /* synthetic */ ChatAttachAlertAudioLayout f$0;
    public final /* synthetic */ ArrayList f$1;

    public /* synthetic */ ChatAttachAlertAudioLayout$$ExternalSyntheticLambda2(ChatAttachAlertAudioLayout chatAttachAlertAudioLayout, ArrayList arrayList) {
        this.f$0 = chatAttachAlertAudioLayout;
        this.f$1 = arrayList;
    }

    public final void run() {
        this.f$0.lambda$loadAudio$3(this.f$1);
    }
}
