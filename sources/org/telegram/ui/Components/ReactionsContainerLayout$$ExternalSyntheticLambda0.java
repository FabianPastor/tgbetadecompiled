package org.telegram.ui.Components;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class ReactionsContainerLayout$$ExternalSyntheticLambda0 implements RecyclerListView.OnItemClickListener {
    public final /* synthetic */ ReactionsContainerLayout f$0;

    public /* synthetic */ ReactionsContainerLayout$$ExternalSyntheticLambda0(ReactionsContainerLayout reactionsContainerLayout) {
        this.f$0 = reactionsContainerLayout;
    }

    public final void onItemClick(View view, int i) {
        this.f$0.lambda$new$0(view, i);
    }
}
