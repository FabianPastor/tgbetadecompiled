package org.telegram.ui.Components;

import org.telegram.ui.Components.SenderSelectPopup;

public final /* synthetic */ class ChatActivityEnterView$$ExternalSyntheticLambda49 implements Runnable {
    public final /* synthetic */ ChatActivityEnterView f$0;
    public final /* synthetic */ SimpleAvatarView f$1;
    public final /* synthetic */ int[] f$2;
    public final /* synthetic */ SenderSelectPopup.SenderView f$3;

    public /* synthetic */ ChatActivityEnterView$$ExternalSyntheticLambda49(ChatActivityEnterView chatActivityEnterView, SimpleAvatarView simpleAvatarView, int[] iArr, SenderSelectPopup.SenderView senderView) {
        this.f$0 = chatActivityEnterView;
        this.f$1 = simpleAvatarView;
        this.f$2 = iArr;
        this.f$3 = senderView;
    }

    public final void run() {
        this.f$0.lambda$new$12(this.f$1, this.f$2, this.f$3);
    }
}
