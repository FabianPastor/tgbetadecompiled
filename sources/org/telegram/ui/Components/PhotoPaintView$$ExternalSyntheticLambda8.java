package org.telegram.ui.Components;

import android.view.View;

public final /* synthetic */ class PhotoPaintView$$ExternalSyntheticLambda8 implements View.OnClickListener {
    public final /* synthetic */ PhotoPaintView f$0;
    public final /* synthetic */ int f$1;

    public /* synthetic */ PhotoPaintView$$ExternalSyntheticLambda8(PhotoPaintView photoPaintView, int i) {
        this.f$0 = photoPaintView;
        this.f$1 = i;
    }

    public final void onClick(View view) {
        this.f$0.lambda$buttonForBrush$14(this.f$1, view);
    }
}
