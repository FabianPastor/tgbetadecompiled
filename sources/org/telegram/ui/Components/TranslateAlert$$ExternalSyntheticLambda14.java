package org.telegram.ui.Components;

import org.telegram.ui.Components.TranslateAlert;

public final /* synthetic */ class TranslateAlert$$ExternalSyntheticLambda14 implements TranslateAlert.OnTranslationSuccess {
    public final /* synthetic */ TranslateAlert f$0;

    public /* synthetic */ TranslateAlert$$ExternalSyntheticLambda14(TranslateAlert translateAlert) {
        this.f$0 = translateAlert;
    }

    public final void run(String str, String str2) {
        this.f$0.lambda$fetchNext$7(str, str2);
    }
}
