package org.telegram.ui.Components;

import org.telegram.tgnet.TLRPC$TL_error;

public final /* synthetic */ class BotWebViewMenuContainer$$ExternalSyntheticLambda14 implements Runnable {
    public final /* synthetic */ BotWebViewMenuContainer f$0;
    public final /* synthetic */ TLRPC$TL_error f$1;

    public /* synthetic */ BotWebViewMenuContainer$$ExternalSyntheticLambda14(BotWebViewMenuContainer botWebViewMenuContainer, TLRPC$TL_error tLRPC$TL_error) {
        this.f$0 = botWebViewMenuContainer;
        this.f$1 = tLRPC$TL_error;
    }

    public final void run() {
        this.f$0.lambda$new$2(this.f$1);
    }
}
