package org.telegram.ui.Components;

import android.animation.ValueAnimator;

public final /* synthetic */ class ReplaceableIconDrawable$$ExternalSyntheticLambda0 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ ReplaceableIconDrawable f$0;

    public /* synthetic */ ReplaceableIconDrawable$$ExternalSyntheticLambda0(ReplaceableIconDrawable replaceableIconDrawable) {
        this.f$0 = replaceableIconDrawable;
    }

    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f$0.lambda$setIcon$0(valueAnimator);
    }
}
