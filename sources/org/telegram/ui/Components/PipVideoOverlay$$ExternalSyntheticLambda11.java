package org.telegram.ui.Components;

import org.telegram.ui.Components.SimpleFloatPropertyCompat;

public final /* synthetic */ class PipVideoOverlay$$ExternalSyntheticLambda11 implements SimpleFloatPropertyCompat.Getter {
    public static final /* synthetic */ PipVideoOverlay$$ExternalSyntheticLambda11 INSTANCE = new PipVideoOverlay$$ExternalSyntheticLambda11();

    private /* synthetic */ PipVideoOverlay$$ExternalSyntheticLambda11() {
    }

    public final float get(Object obj) {
        return ((PipVideoOverlay) obj).pipX;
    }
}
