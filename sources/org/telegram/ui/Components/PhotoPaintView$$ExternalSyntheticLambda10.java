package org.telegram.ui.Components;

import android.view.View;
import org.telegram.ui.Components.Paint.Views.EntityView;

public final /* synthetic */ class PhotoPaintView$$ExternalSyntheticLambda10 implements View.OnClickListener {
    public final /* synthetic */ PhotoPaintView f$0;
    public final /* synthetic */ EntityView f$1;

    public /* synthetic */ PhotoPaintView$$ExternalSyntheticLambda10(PhotoPaintView photoPaintView, EntityView entityView) {
        this.f$0 = photoPaintView;
        this.f$1 = entityView;
    }

    public final void onClick(View view) {
        this.f$0.lambda$showMenuForEntity$10(this.f$1, view);
    }
}
