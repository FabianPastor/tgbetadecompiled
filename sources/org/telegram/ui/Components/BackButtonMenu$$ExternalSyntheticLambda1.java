package org.telegram.ui.Components;

import java.util.Comparator;
import org.telegram.ui.Components.BackButtonMenu;

public final /* synthetic */ class BackButtonMenu$$ExternalSyntheticLambda1 implements Comparator {
    public static final /* synthetic */ BackButtonMenu$$ExternalSyntheticLambda1 INSTANCE = new BackButtonMenu$$ExternalSyntheticLambda1();

    private /* synthetic */ BackButtonMenu$$ExternalSyntheticLambda1() {
    }

    public final int compare(Object obj, Object obj2) {
        return BackButtonMenu.lambda$getStackedHistoryDialogs$1((BackButtonMenu.PulledDialog) obj, (BackButtonMenu.PulledDialog) obj2);
    }
}
