package org.telegram.ui.Components;

import android.net.Uri;

public final /* synthetic */ class VideoSeekPreviewImage$$ExternalSyntheticLambda4 implements Runnable {
    public final /* synthetic */ VideoSeekPreviewImage f$0;
    public final /* synthetic */ Uri f$1;

    public /* synthetic */ VideoSeekPreviewImage$$ExternalSyntheticLambda4(VideoSeekPreviewImage videoSeekPreviewImage, Uri uri) {
        this.f$0 = videoSeekPreviewImage;
        this.f$1 = uri;
    }

    public final void run() {
        this.f$0.lambda$open$3(this.f$1);
    }
}
