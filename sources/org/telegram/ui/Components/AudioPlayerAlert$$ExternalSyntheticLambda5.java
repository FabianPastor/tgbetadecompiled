package org.telegram.ui.Components;

import android.view.MotionEvent;
import android.view.View;

public final /* synthetic */ class AudioPlayerAlert$$ExternalSyntheticLambda5 implements View.OnTouchListener {
    public static final /* synthetic */ AudioPlayerAlert$$ExternalSyntheticLambda5 INSTANCE = new AudioPlayerAlert$$ExternalSyntheticLambda5();

    private /* synthetic */ AudioPlayerAlert$$ExternalSyntheticLambda5() {
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return AudioPlayerAlert.lambda$new$7(view, motionEvent);
    }
}
