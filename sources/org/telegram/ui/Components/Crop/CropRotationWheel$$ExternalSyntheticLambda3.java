package org.telegram.ui.Components.Crop;

import android.view.View;

public final /* synthetic */ class CropRotationWheel$$ExternalSyntheticLambda3 implements View.OnLongClickListener {
    public final /* synthetic */ CropRotationWheel f$0;

    public /* synthetic */ CropRotationWheel$$ExternalSyntheticLambda3(CropRotationWheel cropRotationWheel) {
        this.f$0 = cropRotationWheel;
    }

    public final boolean onLongClick(View view) {
        return this.f$0.lambda$new$1(view);
    }
}
