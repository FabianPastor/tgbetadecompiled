package org.telegram.ui.Components;

import androidx.recyclerview.widget.RecyclerView;

public final /* synthetic */ class SideMenultItemAnimator$$ExternalSyntheticLambda0 implements RecyclerView.ChildDrawingOrderCallback {
    public static final /* synthetic */ SideMenultItemAnimator$$ExternalSyntheticLambda0 INSTANCE = new SideMenultItemAnimator$$ExternalSyntheticLambda0();

    private /* synthetic */ SideMenultItemAnimator$$ExternalSyntheticLambda0() {
    }

    public final int onGetChildDrawingOrder(int i, int i2) {
        return SideMenultItemAnimator.lambda$new$0(i, i2);
    }
}
