package org.telegram.ui.Components;

import android.animation.ValueAnimator;
import org.telegram.ui.Components.ShareAlert;

public final /* synthetic */ class ShareAlert$11$$ExternalSyntheticLambda0 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ ShareAlert.AnonymousClass11 f$0;

    public /* synthetic */ ShareAlert$11$$ExternalSyntheticLambda0(ShareAlert.AnonymousClass11 r1) {
        this.f$0 = r1;
    }

    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f$0.lambda$onDraw$0(valueAnimator);
    }
}
