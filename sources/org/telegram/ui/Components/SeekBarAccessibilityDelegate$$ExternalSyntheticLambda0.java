package org.telegram.ui.Components;

import android.view.View;

public final /* synthetic */ class SeekBarAccessibilityDelegate$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ SeekBarAccessibilityDelegate f$0;
    public final /* synthetic */ View f$1;

    public /* synthetic */ SeekBarAccessibilityDelegate$$ExternalSyntheticLambda0(SeekBarAccessibilityDelegate seekBarAccessibilityDelegate, View view) {
        this.f$0 = seekBarAccessibilityDelegate;
        this.f$1 = view;
    }

    public final void run() {
        this.f$0.lambda$postAccessibilityEventRunnable$0(this.f$1);
    }
}
