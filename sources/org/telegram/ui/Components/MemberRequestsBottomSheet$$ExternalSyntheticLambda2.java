package org.telegram.ui.Components;

public final /* synthetic */ class MemberRequestsBottomSheet$$ExternalSyntheticLambda2 implements Runnable {
    public final /* synthetic */ MemberRequestsBottomSheet f$0;
    public final /* synthetic */ EditTextBoldCursor f$1;

    public /* synthetic */ MemberRequestsBottomSheet$$ExternalSyntheticLambda2(MemberRequestsBottomSheet memberRequestsBottomSheet, EditTextBoldCursor editTextBoldCursor) {
        this.f$0 = memberRequestsBottomSheet;
        this.f$1 = editTextBoldCursor;
    }

    public final void run() {
        this.f$0.lambda$onSearchViewTouched$1(this.f$1);
    }
}
