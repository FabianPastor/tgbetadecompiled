package org.telegram.ui.Components;

import org.telegram.ui.ActionBar.ActionBarMenuItem;

public final /* synthetic */ class ColorPicker$$ExternalSyntheticLambda6 implements ActionBarMenuItem.ActionBarMenuItemDelegate {
    public final /* synthetic */ ColorPicker f$0;

    public /* synthetic */ ColorPicker$$ExternalSyntheticLambda6(ColorPicker colorPicker) {
        this.f$0 = colorPicker;
    }

    public final void onItemClick(int i) {
        this.f$0.lambda$new$5(i);
    }
}
