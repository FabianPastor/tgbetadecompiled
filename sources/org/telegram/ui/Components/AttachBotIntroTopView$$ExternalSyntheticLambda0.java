package org.telegram.ui.Components;

import android.animation.ValueAnimator;

public final /* synthetic */ class AttachBotIntroTopView$$ExternalSyntheticLambda0 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ AttachBotIntroTopView f$0;

    public /* synthetic */ AttachBotIntroTopView$$ExternalSyntheticLambda0(AttachBotIntroTopView attachBotIntroTopView) {
        this.f$0 = attachBotIntroTopView;
    }

    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f$0.lambda$new$0(valueAnimator);
    }
}
