package org.telegram.ui.Components;

import android.view.View;
import org.telegram.ui.Components.AutoDeletePopupWrapper;

public final /* synthetic */ class AutoDeletePopupWrapper$$ExternalSyntheticLambda2 implements View.OnClickListener {
    public final /* synthetic */ AutoDeletePopupWrapper f$0;
    public final /* synthetic */ AutoDeletePopupWrapper.Callback f$1;

    public /* synthetic */ AutoDeletePopupWrapper$$ExternalSyntheticLambda2(AutoDeletePopupWrapper autoDeletePopupWrapper, AutoDeletePopupWrapper.Callback callback) {
        this.f$0 = autoDeletePopupWrapper;
        this.f$1 = callback;
    }

    public final void onClick(View view) {
        this.f$0.lambda$new$2(this.f$1, view);
    }
}
