package org.telegram.ui.Components;

import android.view.View;

public final /* synthetic */ class PhonebookShareAlert$$ExternalSyntheticLambda1 implements View.OnClickListener {
    public final /* synthetic */ PhonebookShareAlert f$0;
    public final /* synthetic */ int f$1;
    public final /* synthetic */ View f$2;

    public /* synthetic */ PhonebookShareAlert$$ExternalSyntheticLambda1(PhonebookShareAlert phonebookShareAlert, int i, View view) {
        this.f$0 = phonebookShareAlert;
        this.f$1 = i;
        this.f$2 = view;
    }

    public final void onClick(View view) {
        this.f$0.lambda$new$2(this.f$1, this.f$2, view);
    }
}
