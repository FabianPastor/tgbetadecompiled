package org.telegram.ui.Components;

import android.animation.ValueAnimator;
import org.telegram.ui.Components.SizeNotifierFrameLayout;

public final /* synthetic */ class SizeNotifierFrameLayout$BlurBackgroundTask$$ExternalSyntheticLambda0 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ SizeNotifierFrameLayout.BlurBackgroundTask f$0;

    public /* synthetic */ SizeNotifierFrameLayout$BlurBackgroundTask$$ExternalSyntheticLambda0(SizeNotifierFrameLayout.BlurBackgroundTask blurBackgroundTask) {
        this.f$0 = blurBackgroundTask;
    }

    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f$0.lambda$run$0(valueAnimator);
    }
}
