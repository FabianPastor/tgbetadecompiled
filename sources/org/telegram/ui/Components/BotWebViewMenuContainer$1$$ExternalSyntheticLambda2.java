package org.telegram.ui.Components;

import android.view.View;
import org.telegram.ui.Components.BotWebViewMenuContainer;

public final /* synthetic */ class BotWebViewMenuContainer$1$$ExternalSyntheticLambda2 implements View.OnClickListener {
    public final /* synthetic */ BotWebViewMenuContainer.AnonymousClass1 f$0;

    public /* synthetic */ BotWebViewMenuContainer$1$$ExternalSyntheticLambda2(BotWebViewMenuContainer.AnonymousClass1 r1) {
        this.f$0 = r1;
    }

    public final void onClick(View view) {
        this.f$0.lambda$onSetupMainButton$3(view);
    }
}
