package org.telegram.ui.Components;

import android.view.MotionEvent;
import android.view.View;

public final /* synthetic */ class EmbedBottomSheet$$ExternalSyntheticLambda5 implements View.OnTouchListener {
    public static final /* synthetic */ EmbedBottomSheet$$ExternalSyntheticLambda5 INSTANCE = new EmbedBottomSheet$$ExternalSyntheticLambda5();

    private /* synthetic */ EmbedBottomSheet$$ExternalSyntheticLambda5() {
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return EmbedBottomSheet.lambda$new$1(view, motionEvent);
    }
}
