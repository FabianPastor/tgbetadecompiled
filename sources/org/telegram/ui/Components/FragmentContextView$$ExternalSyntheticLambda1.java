package org.telegram.ui.Components;

import android.content.DialogInterface;

public final /* synthetic */ class FragmentContextView$$ExternalSyntheticLambda1 implements DialogInterface.OnDismissListener {
    public final /* synthetic */ FragmentContextView f$0;

    public /* synthetic */ FragmentContextView$$ExternalSyntheticLambda1(FragmentContextView fragmentContextView) {
        this.f$0 = fragmentContextView;
    }

    public final void onDismiss(DialogInterface dialogInterface) {
        this.f$0.lambda$new$9(dialogInterface);
    }
}
