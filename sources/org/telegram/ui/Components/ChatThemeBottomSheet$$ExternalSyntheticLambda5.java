package org.telegram.ui.Components;

public final /* synthetic */ class ChatThemeBottomSheet$$ExternalSyntheticLambda5 implements Runnable {
    public final /* synthetic */ ChatThemeBottomSheet f$0;
    public final /* synthetic */ boolean f$1;

    public /* synthetic */ ChatThemeBottomSheet$$ExternalSyntheticLambda5(ChatThemeBottomSheet chatThemeBottomSheet, boolean z) {
        this.f$0 = chatThemeBottomSheet;
        this.f$1 = z;
    }

    public final void run() {
        this.f$0.lambda$setupLightDarkTheme$6(this.f$1);
    }
}
