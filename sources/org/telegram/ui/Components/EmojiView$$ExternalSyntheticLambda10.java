package org.telegram.ui.Components;

import org.telegram.ui.Components.ScrollSlidingTabStrip;

public final /* synthetic */ class EmojiView$$ExternalSyntheticLambda10 implements ScrollSlidingTabStrip.ScrollSlidingTabStripDelegate {
    public final /* synthetic */ EmojiView f$0;

    public /* synthetic */ EmojiView$$ExternalSyntheticLambda10(EmojiView emojiView) {
        this.f$0 = emojiView;
    }

    public final void onPageSelected(int i) {
        this.f$0.lambda$new$6(i);
    }
}
