package org.telegram.ui.Components;

import android.view.View;
import org.telegram.messenger.ForwardingMessagesParams;

public final /* synthetic */ class ForwardingPreviewView$$ExternalSyntheticLambda3 implements View.OnClickListener {
    public final /* synthetic */ ForwardingPreviewView f$0;
    public final /* synthetic */ ForwardingMessagesParams f$1;

    public /* synthetic */ ForwardingPreviewView$$ExternalSyntheticLambda3(ForwardingPreviewView forwardingPreviewView, ForwardingMessagesParams forwardingMessagesParams) {
        this.f$0 = forwardingPreviewView;
        this.f$1 = forwardingMessagesParams;
    }

    public final void onClick(View view) {
        this.f$0.lambda$new$3(this.f$1, view);
    }
}
