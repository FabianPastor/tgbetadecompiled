package org.telegram.ui.Components;

import android.view.KeyEvent;
import android.widget.TextView;

public final /* synthetic */ class ReportAlert$$ExternalSyntheticLambda1 implements TextView.OnEditorActionListener {
    public final /* synthetic */ ReportAlert f$0;

    public /* synthetic */ ReportAlert$$ExternalSyntheticLambda1(ReportAlert reportAlert) {
        this.f$0 = reportAlert;
    }

    public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        return this.f$0.lambda$new$0(textView, i, keyEvent);
    }
}
