package org.telegram.ui.Components;

import android.view.MotionEvent;
import android.view.View;

public final /* synthetic */ class ShareAlert$$ExternalSyntheticLambda8 implements View.OnTouchListener {
    public static final /* synthetic */ ShareAlert$$ExternalSyntheticLambda8 INSTANCE = new ShareAlert$$ExternalSyntheticLambda8();

    private /* synthetic */ ShareAlert$$ExternalSyntheticLambda8() {
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return ShareAlert.lambda$new$6(view, motionEvent);
    }
}
