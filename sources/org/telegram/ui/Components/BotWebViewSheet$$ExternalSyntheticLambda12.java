package org.telegram.ui.Components;

import org.telegram.messenger.GenericProvider;

public final /* synthetic */ class BotWebViewSheet$$ExternalSyntheticLambda12 implements GenericProvider {
    public final /* synthetic */ BotWebViewSheet f$0;

    public /* synthetic */ BotWebViewSheet$$ExternalSyntheticLambda12(BotWebViewSheet botWebViewSheet) {
        this.f$0 = botWebViewSheet;
    }

    public final Object provide(Object obj) {
        return this.f$0.lambda$new$11((Void) obj);
    }
}
