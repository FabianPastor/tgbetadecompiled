package org.telegram.ui.Components;

import android.view.View;

public final /* synthetic */ class AlertsCreator$$ExternalSyntheticLambda72 implements View.OnClickListener {
    public final /* synthetic */ boolean[] f$0;
    public final /* synthetic */ int f$1;

    public /* synthetic */ AlertsCreator$$ExternalSyntheticLambda72(boolean[] zArr, int i) {
        this.f$0 = zArr;
        this.f$1 = i;
    }

    public final void onClick(View view) {
        AlertsCreator.lambda$createBlockDialogAlert$38(this.f$0, this.f$1, view);
    }
}
