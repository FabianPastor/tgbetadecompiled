package org.telegram.ui.Components;

import android.app.Activity;
import android.content.DialogInterface;

public final /* synthetic */ class AlertsCreator$$ExternalSyntheticLambda7 implements DialogInterface.OnClickListener {
    public final /* synthetic */ Activity f$0;

    public /* synthetic */ AlertsCreator$$ExternalSyntheticLambda7(Activity activity) {
        this.f$0 = activity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        AlertsCreator.lambda$createBackgroundLocationPermissionDialog$95(this.f$0, dialogInterface, i);
    }
}
