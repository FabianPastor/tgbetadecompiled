package org.telegram.ui.Components;

import org.telegram.ui.Components.InviteMembersBottomSheet;

public final /* synthetic */ class InviteMembersBottomSheet$SpansContainer$$ExternalSyntheticLambda1 implements Runnable {
    public final /* synthetic */ InviteMembersBottomSheet.SpansContainer f$0;
    public final /* synthetic */ int f$1;

    public /* synthetic */ InviteMembersBottomSheet$SpansContainer$$ExternalSyntheticLambda1(InviteMembersBottomSheet.SpansContainer spansContainer, int i) {
        this.f$0 = spansContainer;
        this.f$1 = i;
    }

    public final void run() {
        this.f$0.lambda$onMeasure$1(this.f$1);
    }
}
