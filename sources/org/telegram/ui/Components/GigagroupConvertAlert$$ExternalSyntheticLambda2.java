package org.telegram.ui.Components;

import android.content.Context;
import android.view.View;
import org.telegram.ui.ActionBar.BaseFragment;

public final /* synthetic */ class GigagroupConvertAlert$$ExternalSyntheticLambda2 implements View.OnClickListener {
    public final /* synthetic */ GigagroupConvertAlert f$0;
    public final /* synthetic */ Context f$1;
    public final /* synthetic */ BaseFragment f$2;

    public /* synthetic */ GigagroupConvertAlert$$ExternalSyntheticLambda2(GigagroupConvertAlert gigagroupConvertAlert, Context context, BaseFragment baseFragment) {
        this.f$0 = gigagroupConvertAlert;
        this.f$1 = context;
        this.f$2 = baseFragment;
    }

    public final void onClick(View view) {
        this.f$0.lambda$new$1(this.f$1, this.f$2, view);
    }
}
