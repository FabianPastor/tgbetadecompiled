package org.telegram.ui.Components;

import org.telegram.ui.Components.SimpleFloatPropertyCompat;

public final /* synthetic */ class BotWebViewMenuContainer$$ExternalSyntheticLambda20 implements SimpleFloatPropertyCompat.Getter {
    public static final /* synthetic */ BotWebViewMenuContainer$$ExternalSyntheticLambda20 INSTANCE = new BotWebViewMenuContainer$$ExternalSyntheticLambda20();

    private /* synthetic */ BotWebViewMenuContainer$$ExternalSyntheticLambda20() {
    }

    public final float get(Object obj) {
        return ((BotWebViewMenuContainer) obj).actionBarTransitionProgress;
    }
}
