package org.telegram.ui.Components;

import android.view.View;
import org.telegram.ui.Components.EmojiView;

public final /* synthetic */ class EmojiView$StickersGridAdapter$$ExternalSyntheticLambda1 implements View.OnClickListener {
    public final /* synthetic */ EmojiView.StickersGridAdapter f$0;

    public /* synthetic */ EmojiView$StickersGridAdapter$$ExternalSyntheticLambda1(EmojiView.StickersGridAdapter stickersGridAdapter) {
        this.f$0 = stickersGridAdapter;
    }

    public final void onClick(View view) {
        this.f$0.lambda$onCreateViewHolder$3(view);
    }
}
