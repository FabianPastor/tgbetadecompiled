package org.telegram.ui.Components;

import android.content.DialogInterface;

public final /* synthetic */ class ChatActivityEnterView$$ExternalSyntheticLambda11 implements DialogInterface.OnDismissListener {
    public final /* synthetic */ ChatActivityEnterView f$0;

    public /* synthetic */ ChatActivityEnterView$$ExternalSyntheticLambda11(ChatActivityEnterView chatActivityEnterView) {
        this.f$0 = chatActivityEnterView;
    }

    public final void onDismiss(DialogInterface dialogInterface) {
        this.f$0.lambda$openWebViewMenu$33(dialogInterface);
    }
}
