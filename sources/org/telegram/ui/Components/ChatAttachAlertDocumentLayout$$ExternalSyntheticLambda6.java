package org.telegram.ui.Components;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class ChatAttachAlertDocumentLayout$$ExternalSyntheticLambda6 implements RecyclerListView.OnItemLongClickListener {
    public final /* synthetic */ ChatAttachAlertDocumentLayout f$0;

    public /* synthetic */ ChatAttachAlertDocumentLayout$$ExternalSyntheticLambda6(ChatAttachAlertDocumentLayout chatAttachAlertDocumentLayout) {
        this.f$0 = chatAttachAlertDocumentLayout;
    }

    public final boolean onItemClick(View view, int i) {
        return this.f$0.lambda$new$2(view, i);
    }
}
