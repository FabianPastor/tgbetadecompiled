package org.telegram.ui.Components;

import java.util.ArrayList;
import org.telegram.ui.Components.GroupVoipInviteAlert;

public final /* synthetic */ class GroupVoipInviteAlert$SearchAdapter$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ GroupVoipInviteAlert.SearchAdapter f$0;
    public final /* synthetic */ int f$1;
    public final /* synthetic */ ArrayList f$2;

    public /* synthetic */ GroupVoipInviteAlert$SearchAdapter$$ExternalSyntheticLambda0(GroupVoipInviteAlert.SearchAdapter searchAdapter, int i, ArrayList arrayList) {
        this.f$0 = searchAdapter;
        this.f$1 = i;
        this.f$2 = arrayList;
    }

    public final void run() {
        this.f$0.lambda$updateSearchResults$3(this.f$1, this.f$2);
    }
}
