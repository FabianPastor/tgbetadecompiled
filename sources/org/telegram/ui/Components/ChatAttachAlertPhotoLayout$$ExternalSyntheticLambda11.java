package org.telegram.ui.Components;

import org.telegram.ui.Components.ChatAttachAlert;

public final /* synthetic */ class ChatAttachAlertPhotoLayout$$ExternalSyntheticLambda11 implements Runnable {
    public final /* synthetic */ ChatAttachAlertPhotoLayout f$0;
    public final /* synthetic */ ChatAttachAlert.AttachAlertLayout f$1;

    public /* synthetic */ ChatAttachAlertPhotoLayout$$ExternalSyntheticLambda11(ChatAttachAlertPhotoLayout chatAttachAlertPhotoLayout, ChatAttachAlert.AttachAlertLayout attachAlertLayout) {
        this.f$0 = chatAttachAlertPhotoLayout;
        this.f$1 = attachAlertLayout;
    }

    public final void run() {
        this.f$0.lambda$onShow$16(this.f$1);
    }
}
