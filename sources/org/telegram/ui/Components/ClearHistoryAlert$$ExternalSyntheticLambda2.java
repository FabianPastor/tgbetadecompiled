package org.telegram.ui.Components;

import android.view.View;

public final /* synthetic */ class ClearHistoryAlert$$ExternalSyntheticLambda2 implements View.OnClickListener {
    public final /* synthetic */ boolean[] f$0;

    public /* synthetic */ ClearHistoryAlert$$ExternalSyntheticLambda2(boolean[] zArr) {
        this.f$0 = zArr;
    }

    public final void onClick(View view) {
        ClearHistoryAlert.lambda$new$0(this.f$0, view);
    }
}
