package org.telegram.ui.Components;

import android.animation.ValueAnimator;

public final /* synthetic */ class ThemeSmallPreviewView$$ExternalSyntheticLambda0 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ ThemeSmallPreviewView f$0;

    public /* synthetic */ ThemeSmallPreviewView$$ExternalSyntheticLambda0(ThemeSmallPreviewView themeSmallPreviewView) {
        this.f$0 = themeSmallPreviewView;
    }

    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f$0.lambda$setSelected$4(valueAnimator);
    }
}
