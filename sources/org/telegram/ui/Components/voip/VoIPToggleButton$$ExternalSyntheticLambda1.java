package org.telegram.ui.Components.voip;

import android.animation.ValueAnimator;

public final /* synthetic */ class VoIPToggleButton$$ExternalSyntheticLambda1 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ VoIPToggleButton f$0;
    public final /* synthetic */ boolean f$1;

    public /* synthetic */ VoIPToggleButton$$ExternalSyntheticLambda1(VoIPToggleButton voIPToggleButton, boolean z) {
        this.f$0 = voIPToggleButton;
        this.f$1 = z;
    }

    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f$0.lambda$setData$0(this.f$1, valueAnimator);
    }
}
