package org.telegram.ui.Components.voip;

import android.animation.ValueAnimator;

public final /* synthetic */ class GroupCallMiniTextureView$$ExternalSyntheticLambda0 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ GroupCallMiniTextureView f$0;

    public /* synthetic */ GroupCallMiniTextureView$$ExternalSyntheticLambda0(GroupCallMiniTextureView groupCallMiniTextureView) {
        this.f$0 = groupCallMiniTextureView;
    }

    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f$0.lambda$startFlipAnimation$7(valueAnimator);
    }
}
