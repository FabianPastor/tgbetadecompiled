package org.telegram.ui.Components.voip;

import android.content.Context;
import android.view.View;

public final /* synthetic */ class RTMPStreamPipOverlay$$ExternalSyntheticLambda1 implements View.OnClickListener {
    public final /* synthetic */ Context f$0;

    public /* synthetic */ RTMPStreamPipOverlay$$ExternalSyntheticLambda1(Context context) {
        this.f$0 = context;
    }

    public final void onClick(View view) {
        RTMPStreamPipOverlay.lambda$showInternal$8(this.f$0, view);
    }
}
