package org.telegram.ui.Components.voip;

import org.telegram.ui.Components.SimpleFloatPropertyCompat;

public final /* synthetic */ class RTMPStreamPipOverlay$$ExternalSyntheticLambda8 implements SimpleFloatPropertyCompat.Setter {
    public static final /* synthetic */ RTMPStreamPipOverlay$$ExternalSyntheticLambda8 INSTANCE = new RTMPStreamPipOverlay$$ExternalSyntheticLambda8();

    private /* synthetic */ RTMPStreamPipOverlay$$ExternalSyntheticLambda8() {
    }

    public final void set(Object obj, float f) {
        RTMPStreamPipOverlay.lambda$static$1((RTMPStreamPipOverlay) obj, f);
    }
}
