package org.telegram.ui.Components.voip;

import android.animation.ValueAnimator;

public final /* synthetic */ class VoIPToggleButton$$ExternalSyntheticLambda0 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ VoIPToggleButton f$0;

    public /* synthetic */ VoIPToggleButton$$ExternalSyntheticLambda0(VoIPToggleButton voIPToggleButton) {
        this.f$0 = voIPToggleButton;
    }

    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f$0.lambda$setChecked$1(valueAnimator);
    }
}
