package org.telegram.ui.Components.voip;

import android.animation.ValueAnimator;

public final /* synthetic */ class VoIPFloatingLayout$$ExternalSyntheticLambda0 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ VoIPFloatingLayout f$0;

    public /* synthetic */ VoIPFloatingLayout$$ExternalSyntheticLambda0(VoIPFloatingLayout voIPFloatingLayout) {
        this.f$0 = voIPFloatingLayout;
    }

    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f$0.lambda$new$0(valueAnimator);
    }
}
