package org.telegram.ui.Components.voip;

import org.telegram.ui.Components.SimpleFloatPropertyCompat;

public final /* synthetic */ class RTMPStreamPipOverlay$$ExternalSyntheticLambda5 implements SimpleFloatPropertyCompat.Getter {
    public static final /* synthetic */ RTMPStreamPipOverlay$$ExternalSyntheticLambda5 INSTANCE = new RTMPStreamPipOverlay$$ExternalSyntheticLambda5();

    private /* synthetic */ RTMPStreamPipOverlay$$ExternalSyntheticLambda5() {
    }

    public final float get(Object obj) {
        return ((RTMPStreamPipOverlay) obj).pipY;
    }
}
