package org.telegram.ui.Components.voip;

import android.view.View;
import org.telegram.ui.GroupCallActivity;

public final /* synthetic */ class GroupCallRenderersContainer$$ExternalSyntheticLambda6 implements View.OnClickListener {
    public final /* synthetic */ GroupCallRenderersContainer f$0;
    public final /* synthetic */ GroupCallActivity f$1;

    public /* synthetic */ GroupCallRenderersContainer$$ExternalSyntheticLambda6(GroupCallRenderersContainer groupCallRenderersContainer, GroupCallActivity groupCallActivity) {
        this.f$0 = groupCallRenderersContainer;
        this.f$1 = groupCallActivity;
    }

    public final void onClick(View view) {
        this.f$0.lambda$new$2(this.f$1, view);
    }
}
