package org.telegram.ui.Components.voip;

import android.animation.ValueAnimator;

public final /* synthetic */ class AcceptDeclineView$$ExternalSyntheticLambda1 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ AcceptDeclineView f$0;

    public /* synthetic */ AcceptDeclineView$$ExternalSyntheticLambda1(AcceptDeclineView acceptDeclineView) {
        this.f$0 = acceptDeclineView;
    }

    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f$0.lambda$onTouchEvent$1(valueAnimator);
    }
}
