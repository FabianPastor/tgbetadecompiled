package org.telegram.ui.Components.voip;

import android.view.View;

public final /* synthetic */ class PrivateVideoPreviewDialog$$ExternalSyntheticLambda1 implements View.OnClickListener {
    public final /* synthetic */ PrivateVideoPreviewDialog f$0;
    public final /* synthetic */ int f$1;

    public /* synthetic */ PrivateVideoPreviewDialog$$ExternalSyntheticLambda1(PrivateVideoPreviewDialog privateVideoPreviewDialog, int i) {
        this.f$0 = privateVideoPreviewDialog;
        this.f$1 = i;
    }

    public final void onClick(View view) {
        this.f$0.lambda$new$1(this.f$1, view);
    }
}
