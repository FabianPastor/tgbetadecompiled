package org.telegram.ui.Components.voip;

import org.telegram.ui.Components.SimpleFloatPropertyCompat;

public final /* synthetic */ class RTMPStreamPipOverlay$$ExternalSyntheticLambda6 implements SimpleFloatPropertyCompat.Getter {
    public static final /* synthetic */ RTMPStreamPipOverlay$$ExternalSyntheticLambda6 INSTANCE = new RTMPStreamPipOverlay$$ExternalSyntheticLambda6();

    private /* synthetic */ RTMPStreamPipOverlay$$ExternalSyntheticLambda6() {
    }

    public final float get(Object obj) {
        return ((RTMPStreamPipOverlay) obj).pipX;
    }
}
