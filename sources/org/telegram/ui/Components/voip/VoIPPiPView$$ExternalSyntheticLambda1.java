package org.telegram.ui.Components.voip;

import android.content.Context;
import android.view.View;

public final /* synthetic */ class VoIPPiPView$$ExternalSyntheticLambda1 implements View.OnClickListener {
    public final /* synthetic */ VoIPPiPView f$0;
    public final /* synthetic */ Context f$1;

    public /* synthetic */ VoIPPiPView$$ExternalSyntheticLambda1(VoIPPiPView voIPPiPView, Context context) {
        this.f$0 = voIPPiPView;
        this.f$1 = context;
    }

    public final void onClick(View view) {
        this.f$0.lambda$new$2(this.f$1, view);
    }
}
