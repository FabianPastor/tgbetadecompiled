package org.telegram.ui.Components.voip;

import android.view.View;
import org.telegram.ui.Components.RLottieDrawable;

public final /* synthetic */ class PrivateVideoPreviewDialog$$ExternalSyntheticLambda2 implements View.OnClickListener {
    public final /* synthetic */ PrivateVideoPreviewDialog f$0;
    public final /* synthetic */ RLottieDrawable f$1;

    public /* synthetic */ PrivateVideoPreviewDialog$$ExternalSyntheticLambda2(PrivateVideoPreviewDialog privateVideoPreviewDialog, RLottieDrawable rLottieDrawable) {
        this.f$0 = privateVideoPreviewDialog;
        this.f$1 = rLottieDrawable;
    }

    public final void onClick(View view) {
        this.f$0.lambda$new$2(this.f$1, view);
    }
}
