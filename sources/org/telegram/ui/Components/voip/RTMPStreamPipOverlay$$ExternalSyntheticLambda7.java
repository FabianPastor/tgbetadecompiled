package org.telegram.ui.Components.voip;

import org.telegram.ui.Components.SimpleFloatPropertyCompat;

public final /* synthetic */ class RTMPStreamPipOverlay$$ExternalSyntheticLambda7 implements SimpleFloatPropertyCompat.Setter {
    public static final /* synthetic */ RTMPStreamPipOverlay$$ExternalSyntheticLambda7 INSTANCE = new RTMPStreamPipOverlay$$ExternalSyntheticLambda7();

    private /* synthetic */ RTMPStreamPipOverlay$$ExternalSyntheticLambda7() {
    }

    public final void set(Object obj, float f) {
        RTMPStreamPipOverlay.lambda$static$3((RTMPStreamPipOverlay) obj, f);
    }
}
