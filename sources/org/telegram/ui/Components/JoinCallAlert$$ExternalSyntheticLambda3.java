package org.telegram.ui.Components;

import android.view.View;
import org.telegram.ui.Components.JoinCallAlert;

public final /* synthetic */ class JoinCallAlert$$ExternalSyntheticLambda3 implements View.OnClickListener {
    public final /* synthetic */ JoinCallAlert f$0;
    public final /* synthetic */ JoinCallAlert.JoinCallAlertDelegate f$1;

    public /* synthetic */ JoinCallAlert$$ExternalSyntheticLambda3(JoinCallAlert joinCallAlert, JoinCallAlert.JoinCallAlertDelegate joinCallAlertDelegate) {
        this.f$0 = joinCallAlert;
        this.f$1 = joinCallAlertDelegate;
    }

    public final void onClick(View view) {
        this.f$0.lambda$new$7(this.f$1, view);
    }
}
