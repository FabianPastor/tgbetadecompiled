package org.telegram.ui.Components;

import java.util.List;
import org.telegram.tgnet.TLObject;

public final /* synthetic */ class ReactedHeaderView$$ExternalSyntheticLambda3 implements Runnable {
    public final /* synthetic */ ReactedHeaderView f$0;
    public final /* synthetic */ TLObject f$1;
    public final /* synthetic */ List f$2;
    public final /* synthetic */ List f$3;
    public final /* synthetic */ Runnable f$4;

    public /* synthetic */ ReactedHeaderView$$ExternalSyntheticLambda3(ReactedHeaderView reactedHeaderView, TLObject tLObject, List list, List list2, Runnable runnable) {
        this.f$0 = reactedHeaderView;
        this.f$1 = tLObject;
        this.f$2 = list;
        this.f$3 = list2;
        this.f$4 = runnable;
    }

    public final void run() {
        this.f$0.lambda$onAttachedToWindow$3(this.f$1, this.f$2, this.f$3, this.f$4);
    }
}
