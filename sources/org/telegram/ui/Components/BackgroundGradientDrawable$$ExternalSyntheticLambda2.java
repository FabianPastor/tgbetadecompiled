package org.telegram.ui.Components;

import android.view.View;
import org.telegram.ui.Components.BackgroundGradientDrawable;

public final /* synthetic */ class BackgroundGradientDrawable$$ExternalSyntheticLambda2 implements BackgroundGradientDrawable.Disposable {
    public final /* synthetic */ BackgroundGradientDrawable f$0;
    public final /* synthetic */ View f$1;
    public final /* synthetic */ BackgroundGradientDrawable.Disposable f$2;

    public /* synthetic */ BackgroundGradientDrawable$$ExternalSyntheticLambda2(BackgroundGradientDrawable backgroundGradientDrawable, View view, BackgroundGradientDrawable.Disposable disposable) {
        this.f$0 = backgroundGradientDrawable;
        this.f$1 = view;
        this.f$2 = disposable;
    }

    public final void dispose() {
        this.f$0.lambda$drawExactBoundsSize$0(this.f$1, this.f$2);
    }
}
