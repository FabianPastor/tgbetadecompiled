package org.telegram.ui.Components;

import android.view.View;

public final /* synthetic */ class AlertsCreator$$ExternalSyntheticLambda70 implements View.OnClickListener {
    public final /* synthetic */ boolean[] f$0;

    public /* synthetic */ AlertsCreator$$ExternalSyntheticLambda70(boolean[] zArr) {
        this.f$0 = zArr;
    }

    public final void onClick(View view) {
        AlertsCreator.lambda$createClearDaysDialogAlert$26(this.f$0, view);
    }
}
