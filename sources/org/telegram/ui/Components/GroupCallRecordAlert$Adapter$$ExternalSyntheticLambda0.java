package org.telegram.ui.Components;

import android.view.View;
import org.telegram.ui.Components.GroupCallRecordAlert;

public final /* synthetic */ class GroupCallRecordAlert$Adapter$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ GroupCallRecordAlert.Adapter f$0;
    public final /* synthetic */ int f$1;

    public /* synthetic */ GroupCallRecordAlert$Adapter$$ExternalSyntheticLambda0(GroupCallRecordAlert.Adapter adapter, int i) {
        this.f$0 = adapter;
        this.f$1 = i;
    }

    public final void onClick(View view) {
        this.f$0.lambda$instantiateItem$0(this.f$1, view);
    }
}
