package org.telegram.ui.Components;

import androidx.arch.core.util.Function;

public final /* synthetic */ class EmbedBottomSheet$$ExternalSyntheticLambda6 implements Function {
    public static final /* synthetic */ EmbedBottomSheet$$ExternalSyntheticLambda6 INSTANCE = new EmbedBottomSheet$$ExternalSyntheticLambda6();

    private /* synthetic */ EmbedBottomSheet$$ExternalSyntheticLambda6() {
    }

    public final Object apply(Object obj) {
        return ((BulletinFactory) obj).createCopyLinkBulletin();
    }
}
