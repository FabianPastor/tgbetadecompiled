package org.telegram.ui.Components;

public final /* synthetic */ class InviteMembersBottomSheet$$ExternalSyntheticLambda6 implements Runnable {
    public final /* synthetic */ InviteMembersBottomSheet f$0;
    public final /* synthetic */ EditTextBoldCursor f$1;

    public /* synthetic */ InviteMembersBottomSheet$$ExternalSyntheticLambda6(InviteMembersBottomSheet inviteMembersBottomSheet, EditTextBoldCursor editTextBoldCursor) {
        this.f$0 = inviteMembersBottomSheet;
        this.f$1 = editTextBoldCursor;
    }

    public final void run() {
        this.f$0.lambda$onSearchViewTouched$5(this.f$1);
    }
}
