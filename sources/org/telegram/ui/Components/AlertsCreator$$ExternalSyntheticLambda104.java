package org.telegram.ui.Components;

import org.telegram.ui.Components.NumberPicker;

public final /* synthetic */ class AlertsCreator$$ExternalSyntheticLambda104 implements NumberPicker.Formatter {
    public final /* synthetic */ int[] f$0;

    public /* synthetic */ AlertsCreator$$ExternalSyntheticLambda104(int[] iArr) {
        this.f$0 = iArr;
    }

    public final String format(int i) {
        return AlertsCreator.lambda$createAutoDeleteDatePickerDialog$62(this.f$0, i);
    }
}
