package org.telegram.ui.Components;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class ShareAlert$$ExternalSyntheticLambda15 implements RecyclerListView.OnItemClickListener {
    public final /* synthetic */ ShareAlert f$0;

    public /* synthetic */ ShareAlert$$ExternalSyntheticLambda15(ShareAlert shareAlert) {
        this.f$0 = shareAlert;
    }

    public final void onItemClick(View view, int i) {
        this.f$0.lambda$new$3(view, i);
    }
}
