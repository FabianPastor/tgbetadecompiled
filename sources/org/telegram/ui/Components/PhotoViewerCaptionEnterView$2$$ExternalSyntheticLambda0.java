package org.telegram.ui.Components;

import android.animation.ValueAnimator;
import org.telegram.ui.Components.PhotoViewerCaptionEnterView;

public final /* synthetic */ class PhotoViewerCaptionEnterView$2$$ExternalSyntheticLambda0 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ PhotoViewerCaptionEnterView.AnonymousClass2 f$0;

    public /* synthetic */ PhotoViewerCaptionEnterView$2$$ExternalSyntheticLambda0(PhotoViewerCaptionEnterView.AnonymousClass2 r1) {
        this.f$0 = r1;
    }

    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f$0.lambda$afterTextChanged$0(valueAnimator);
    }
}
