package org.telegram.ui.Components;

import com.google.android.gms.maps.MapView;

public final /* synthetic */ class ChatAttachAlertLocationLayout$$ExternalSyntheticLambda16 implements Runnable {
    public final /* synthetic */ ChatAttachAlertLocationLayout f$0;
    public final /* synthetic */ MapView f$1;

    public /* synthetic */ ChatAttachAlertLocationLayout$$ExternalSyntheticLambda16(ChatAttachAlertLocationLayout chatAttachAlertLocationLayout, MapView mapView) {
        this.f$0 = chatAttachAlertLocationLayout;
        this.f$1 = mapView;
    }

    public final void run() {
        this.f$0.lambda$new$11(this.f$1);
    }
}
