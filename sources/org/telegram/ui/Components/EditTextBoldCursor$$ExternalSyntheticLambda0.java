package org.telegram.ui.Components;

import android.view.ViewTreeObserver;

public final /* synthetic */ class EditTextBoldCursor$$ExternalSyntheticLambda0 implements ViewTreeObserver.OnPreDrawListener {
    public final /* synthetic */ EditTextBoldCursor f$0;

    public /* synthetic */ EditTextBoldCursor$$ExternalSyntheticLambda0(EditTextBoldCursor editTextBoldCursor) {
        this.f$0 = editTextBoldCursor;
    }

    public final boolean onPreDraw() {
        return this.f$0.lambda$startActionMode$0();
    }
}
