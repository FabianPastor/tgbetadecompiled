package org.telegram.ui.Components;

import org.telegram.ui.Components.AlertsCreator;

public final /* synthetic */ class PhonebookShareAlert$$ExternalSyntheticLambda5 implements AlertsCreator.ScheduleDatePickerDelegate {
    public final /* synthetic */ PhonebookShareAlert f$0;

    public /* synthetic */ PhonebookShareAlert$$ExternalSyntheticLambda5(PhonebookShareAlert phonebookShareAlert) {
        this.f$0 = phonebookShareAlert;
    }

    public final void didSelectDate(boolean z, int i) {
        this.f$0.lambda$new$4(z, i);
    }
}
