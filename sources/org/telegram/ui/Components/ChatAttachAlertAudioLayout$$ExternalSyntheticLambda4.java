package org.telegram.ui.Components;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class ChatAttachAlertAudioLayout$$ExternalSyntheticLambda4 implements RecyclerListView.OnItemLongClickListener {
    public final /* synthetic */ ChatAttachAlertAudioLayout f$0;

    public /* synthetic */ ChatAttachAlertAudioLayout$$ExternalSyntheticLambda4(ChatAttachAlertAudioLayout chatAttachAlertAudioLayout) {
        this.f$0 = chatAttachAlertAudioLayout;
    }

    public final boolean onItemClick(View view, int i) {
        return this.f$0.lambda$new$2(view, i);
    }
}
