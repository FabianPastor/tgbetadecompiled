package org.telegram.ui.Components;

import org.telegram.ui.Components.SimpleFloatPropertyCompat;

public final /* synthetic */ class OutlineTextContainerView$$ExternalSyntheticLambda0 implements SimpleFloatPropertyCompat.Getter {
    public static final /* synthetic */ OutlineTextContainerView$$ExternalSyntheticLambda0 INSTANCE = new OutlineTextContainerView$$ExternalSyntheticLambda0();

    private /* synthetic */ OutlineTextContainerView$$ExternalSyntheticLambda0() {
    }

    public final float get(Object obj) {
        return ((OutlineTextContainerView) obj).selectionProgress;
    }
}
