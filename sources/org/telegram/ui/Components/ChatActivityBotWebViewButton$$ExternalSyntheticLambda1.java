package org.telegram.ui.Components;

import org.telegram.ui.Components.SimpleFloatPropertyCompat;

public final /* synthetic */ class ChatActivityBotWebViewButton$$ExternalSyntheticLambda1 implements SimpleFloatPropertyCompat.Setter {
    public static final /* synthetic */ ChatActivityBotWebViewButton$$ExternalSyntheticLambda1 INSTANCE = new ChatActivityBotWebViewButton$$ExternalSyntheticLambda1();

    private /* synthetic */ ChatActivityBotWebViewButton$$ExternalSyntheticLambda1() {
    }

    public final void set(Object obj, float f) {
        ((ChatActivityBotWebViewButton) obj).setProgress(f);
    }
}
