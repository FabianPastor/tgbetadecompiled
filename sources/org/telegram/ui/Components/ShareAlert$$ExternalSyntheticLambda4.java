package org.telegram.ui.Components;

import android.view.View;
import org.telegram.messenger.MessageObject;

public final /* synthetic */ class ShareAlert$$ExternalSyntheticLambda4 implements View.OnClickListener {
    public final /* synthetic */ ShareAlert f$0;
    public final /* synthetic */ MessageObject f$1;

    public /* synthetic */ ShareAlert$$ExternalSyntheticLambda4(ShareAlert shareAlert, MessageObject messageObject) {
        this.f$0 = shareAlert;
        this.f$1 = messageObject;
    }

    public final void onClick(View view) {
        this.f$0.lambda$new$5(this.f$1, view);
    }
}
