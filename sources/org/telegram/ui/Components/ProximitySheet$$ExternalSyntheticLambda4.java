package org.telegram.ui.Components;

import org.telegram.ui.Components.NumberPicker;

public final /* synthetic */ class ProximitySheet$$ExternalSyntheticLambda4 implements NumberPicker.OnValueChangeListener {
    public final /* synthetic */ ProximitySheet f$0;

    public /* synthetic */ ProximitySheet$$ExternalSyntheticLambda4(ProximitySheet proximitySheet) {
        this.f$0 = proximitySheet;
    }

    public final void onValueChange(NumberPicker numberPicker, int i, int i2) {
        this.f$0.lambda$new$2(numberPicker, i, i2);
    }
}
