package org.telegram.ui.Components;

import android.view.MotionEvent;
import android.view.View;

public final /* synthetic */ class ChatAttachAlert$$ExternalSyntheticLambda14 implements View.OnTouchListener {
    public static final /* synthetic */ ChatAttachAlert$$ExternalSyntheticLambda14 INSTANCE = new ChatAttachAlert$$ExternalSyntheticLambda14();

    private /* synthetic */ ChatAttachAlert$$ExternalSyntheticLambda14() {
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return ChatAttachAlert.lambda$new$10(view, motionEvent);
    }
}
