package org.telegram.ui.Components;

import java.util.ArrayList;

public final /* synthetic */ class StickersAlert$$ExternalSyntheticLambda21 implements Runnable {
    public final /* synthetic */ StickersAlert f$0;
    public final /* synthetic */ ArrayList f$1;
    public final /* synthetic */ ArrayList f$2;

    public /* synthetic */ StickersAlert$$ExternalSyntheticLambda21(StickersAlert stickersAlert, ArrayList arrayList, ArrayList arrayList2) {
        this.f$0 = stickersAlert;
        this.f$1 = arrayList;
        this.f$2 = arrayList2;
    }

    public final void run() {
        this.f$0.lambda$new$4(this.f$1, this.f$2);
    }
}
