package org.telegram.ui.Components;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;

public final /* synthetic */ class ChatAttachAlertLocationLayout$$ExternalSyntheticLambda10 implements OnMapReadyCallback {
    public final /* synthetic */ ChatAttachAlertLocationLayout f$0;

    public /* synthetic */ ChatAttachAlertLocationLayout$$ExternalSyntheticLambda10(ChatAttachAlertLocationLayout chatAttachAlertLocationLayout) {
        this.f$0 = chatAttachAlertLocationLayout;
    }

    public final void onMapReady(GoogleMap googleMap) {
        this.f$0.lambda$new$10(googleMap);
    }
}
