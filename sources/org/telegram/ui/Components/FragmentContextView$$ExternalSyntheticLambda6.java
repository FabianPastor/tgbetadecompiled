package org.telegram.ui.Components;

import android.view.View;
import org.telegram.ui.ActionBar.Theme;

public final /* synthetic */ class FragmentContextView$$ExternalSyntheticLambda6 implements View.OnClickListener {
    public final /* synthetic */ FragmentContextView f$0;
    public final /* synthetic */ Theme.ResourcesProvider f$1;

    public /* synthetic */ FragmentContextView$$ExternalSyntheticLambda6(FragmentContextView fragmentContextView, Theme.ResourcesProvider resourcesProvider) {
        this.f$0 = fragmentContextView;
        this.f$1 = resourcesProvider;
    }

    public final void onClick(View view) {
        this.f$0.lambda$new$8(this.f$1, view);
    }
}
