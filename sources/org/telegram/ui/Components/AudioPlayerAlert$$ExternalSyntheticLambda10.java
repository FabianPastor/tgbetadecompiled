package org.telegram.ui.Components;

import org.telegram.ui.ActionBar.ThemeDescription;

public final /* synthetic */ class AudioPlayerAlert$$ExternalSyntheticLambda10 implements ThemeDescription.ThemeDescriptionDelegate {
    public final /* synthetic */ AudioPlayerAlert f$0;

    public /* synthetic */ AudioPlayerAlert$$ExternalSyntheticLambda10(AudioPlayerAlert audioPlayerAlert) {
        this.f$0 = audioPlayerAlert;
    }

    public final void didSetColor() {
        this.f$0.lambda$getThemeDescriptions$11();
    }

    public /* synthetic */ void onAnimationProgress(float f) {
        ThemeDescription.ThemeDescriptionDelegate.CC.$default$onAnimationProgress(this, f);
    }
}
