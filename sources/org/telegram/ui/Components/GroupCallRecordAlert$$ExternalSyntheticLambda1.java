package org.telegram.ui.Components;

import android.view.View;

public final /* synthetic */ class GroupCallRecordAlert$$ExternalSyntheticLambda1 implements View.OnClickListener {
    public final /* synthetic */ GroupCallRecordAlert f$0;
    public final /* synthetic */ int f$1;

    public /* synthetic */ GroupCallRecordAlert$$ExternalSyntheticLambda1(GroupCallRecordAlert groupCallRecordAlert, int i) {
        this.f$0 = groupCallRecordAlert;
        this.f$1 = i;
    }

    public final void onClick(View view) {
        this.f$0.lambda$new$1(this.f$1, view);
    }
}
