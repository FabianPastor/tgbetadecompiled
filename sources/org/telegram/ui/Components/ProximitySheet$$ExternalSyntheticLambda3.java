package org.telegram.ui.Components;

import org.telegram.ui.Components.NumberPicker;

public final /* synthetic */ class ProximitySheet$$ExternalSyntheticLambda3 implements NumberPicker.Formatter {
    public final /* synthetic */ ProximitySheet f$0;

    public /* synthetic */ ProximitySheet$$ExternalSyntheticLambda3(ProximitySheet proximitySheet) {
        this.f$0 = proximitySheet;
    }

    public final String format(int i) {
        return this.f$0.lambda$new$3(i);
    }
}
