package org.telegram.ui.Components;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class ChatAttachAlert$$ExternalSyntheticLambda36 implements RecyclerListView.OnItemLongClickListener {
    public final /* synthetic */ ChatAttachAlert f$0;

    public /* synthetic */ ChatAttachAlert$$ExternalSyntheticLambda36(ChatAttachAlert chatAttachAlert) {
        this.f$0 = chatAttachAlert;
    }

    public final boolean onItemClick(View view, int i) {
        return this.f$0.lambda$new$8(view, i);
    }
}
