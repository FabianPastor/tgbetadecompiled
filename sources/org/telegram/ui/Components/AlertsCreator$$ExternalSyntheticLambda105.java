package org.telegram.ui.Components;

import org.telegram.ui.Components.NumberPicker;

public final /* synthetic */ class AlertsCreator$$ExternalSyntheticLambda105 implements NumberPicker.Formatter {
    public final /* synthetic */ int[] f$0;

    public /* synthetic */ AlertsCreator$$ExternalSyntheticLambda105(int[] iArr) {
        this.f$0 = iArr;
    }

    public final String format(int i) {
        return AlertsCreator.lambda$createMuteForPickerDialog$72(this.f$0, i);
    }
}
