package org.telegram.ui.Components;

import org.telegram.tgnet.TLObject;
import org.telegram.ui.Components.EmojiView;

public final /* synthetic */ class EmojiView$GifSearchPreloader$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ EmojiView.GifSearchPreloader f$0;
    public final /* synthetic */ String f$1;
    public final /* synthetic */ String f$2;
    public final /* synthetic */ boolean f$3;
    public final /* synthetic */ String f$4;
    public final /* synthetic */ TLObject f$5;

    public /* synthetic */ EmojiView$GifSearchPreloader$$ExternalSyntheticLambda0(EmojiView.GifSearchPreloader gifSearchPreloader, String str, String str2, boolean z, String str3, TLObject tLObject) {
        this.f$0 = gifSearchPreloader;
        this.f$1 = str;
        this.f$2 = str2;
        this.f$3 = z;
        this.f$4 = str3;
        this.f$5 = tLObject;
    }

    public final void run() {
        this.f$0.lambda$preload$0(this.f$1, this.f$2, this.f$3, this.f$4, this.f$5);
    }
}
