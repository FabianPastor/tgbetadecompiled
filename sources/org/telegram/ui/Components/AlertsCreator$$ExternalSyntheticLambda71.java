package org.telegram.ui.Components;

import android.view.View;

public final /* synthetic */ class AlertsCreator$$ExternalSyntheticLambda71 implements View.OnClickListener {
    public final /* synthetic */ boolean[] f$0;

    public /* synthetic */ AlertsCreator$$ExternalSyntheticLambda71(boolean[] zArr) {
        this.f$0 = zArr;
    }

    public final void onClick(View view) {
        AlertsCreator.lambda$createDeleteMessagesAlert$116(this.f$0, view);
    }
}
