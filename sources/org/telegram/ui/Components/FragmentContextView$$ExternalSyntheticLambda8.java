package org.telegram.ui.Components;

import android.view.View;

public final /* synthetic */ class FragmentContextView$$ExternalSyntheticLambda8 implements View.OnLongClickListener {
    public final /* synthetic */ FragmentContextView f$0;

    public /* synthetic */ FragmentContextView$$ExternalSyntheticLambda8(FragmentContextView fragmentContextView) {
        this.f$0 = fragmentContextView;
    }

    public final boolean onLongClick(View view) {
        return this.f$0.lambda$new$4(view);
    }
}
