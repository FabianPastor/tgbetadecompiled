package org.telegram.ui.Components;

import com.google.android.gms.maps.GoogleMap;

public final /* synthetic */ class ChatAttachAlertLocationLayout$$ExternalSyntheticLambda6 implements GoogleMap.OnCameraMoveStartedListener {
    public final /* synthetic */ ChatAttachAlertLocationLayout f$0;

    public /* synthetic */ ChatAttachAlertLocationLayout$$ExternalSyntheticLambda6(ChatAttachAlertLocationLayout chatAttachAlertLocationLayout) {
        this.f$0 = chatAttachAlertLocationLayout;
    }

    public final void onCameraMoveStarted(int i) {
        this.f$0.lambda$onMapInit$17(i);
    }
}
