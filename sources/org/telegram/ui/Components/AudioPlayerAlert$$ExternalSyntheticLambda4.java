package org.telegram.ui.Components;

import android.view.View;

public final /* synthetic */ class AudioPlayerAlert$$ExternalSyntheticLambda4 implements View.OnLongClickListener {
    public final /* synthetic */ AudioPlayerAlert f$0;

    public /* synthetic */ AudioPlayerAlert$$ExternalSyntheticLambda4(AudioPlayerAlert audioPlayerAlert) {
        this.f$0 = audioPlayerAlert;
    }

    public final boolean onLongClick(View view) {
        return this.f$0.lambda$new$2(view);
    }
}
