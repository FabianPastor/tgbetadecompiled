package org.telegram.ui.Components;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class ChatAttachAlertDocumentLayout$$ExternalSyntheticLambda5 implements RecyclerListView.OnItemClickListener {
    public final /* synthetic */ ChatAttachAlertDocumentLayout f$0;

    public /* synthetic */ ChatAttachAlertDocumentLayout$$ExternalSyntheticLambda5(ChatAttachAlertDocumentLayout chatAttachAlertDocumentLayout) {
        this.f$0 = chatAttachAlertDocumentLayout;
    }

    public final void onItemClick(View view, int i) {
        this.f$0.lambda$new$3(view, i);
    }
}
