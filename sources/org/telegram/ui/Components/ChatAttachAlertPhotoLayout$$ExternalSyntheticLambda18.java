package org.telegram.ui.Components;

import org.telegram.ui.Components.ZoomControlView;

public final /* synthetic */ class ChatAttachAlertPhotoLayout$$ExternalSyntheticLambda18 implements ZoomControlView.ZoomControlViewDelegate {
    public final /* synthetic */ ChatAttachAlertPhotoLayout f$0;

    public /* synthetic */ ChatAttachAlertPhotoLayout$$ExternalSyntheticLambda18(ChatAttachAlertPhotoLayout chatAttachAlertPhotoLayout) {
        this.f$0 = chatAttachAlertPhotoLayout;
    }

    public final void didSetZoom(float f) {
        this.f$0.lambda$new$4(f);
    }
}
