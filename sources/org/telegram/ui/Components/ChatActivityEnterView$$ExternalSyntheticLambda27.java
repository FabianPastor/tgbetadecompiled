package org.telegram.ui.Components;

import android.view.View;

public final /* synthetic */ class ChatActivityEnterView$$ExternalSyntheticLambda27 implements View.OnLongClickListener {
    public final /* synthetic */ ChatActivityEnterView f$0;

    public /* synthetic */ ChatActivityEnterView$$ExternalSyntheticLambda27(ChatActivityEnterView chatActivityEnterView) {
        this.f$0 = chatActivityEnterView;
    }

    public final boolean onLongClick(View view) {
        return this.f$0.onSendLongClick(view);
    }
}
