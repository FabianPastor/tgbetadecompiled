package org.telegram.ui.Components;

import android.view.View;

public final /* synthetic */ class PasscodeView$$ExternalSyntheticLambda4 implements View.OnLongClickListener {
    public final /* synthetic */ PasscodeView f$0;

    public /* synthetic */ PasscodeView$$ExternalSyntheticLambda4(PasscodeView passcodeView) {
        this.f$0 = passcodeView;
    }

    public final boolean onLongClick(View view) {
        return this.f$0.lambda$new$3(view);
    }
}
