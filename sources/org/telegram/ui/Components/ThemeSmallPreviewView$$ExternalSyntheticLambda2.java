package org.telegram.ui.Components;

import org.telegram.ui.Components.ChatThemeBottomSheet;

public final /* synthetic */ class ThemeSmallPreviewView$$ExternalSyntheticLambda2 implements Runnable {
    public final /* synthetic */ ThemeSmallPreviewView f$0;
    public final /* synthetic */ ChatThemeBottomSheet.ChatThemeItem f$1;

    public /* synthetic */ ThemeSmallPreviewView$$ExternalSyntheticLambda2(ThemeSmallPreviewView themeSmallPreviewView, ChatThemeBottomSheet.ChatThemeItem chatThemeItem) {
        this.f$0 = themeSmallPreviewView;
        this.f$1 = chatThemeItem;
    }

    public final void run() {
        this.f$0.lambda$setItem$3(this.f$1);
    }
}
