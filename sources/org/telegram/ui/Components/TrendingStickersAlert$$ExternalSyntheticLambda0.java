package org.telegram.ui.Components;

import org.telegram.ui.ActionBar.ThemeDescription;

public final /* synthetic */ class TrendingStickersAlert$$ExternalSyntheticLambda0 implements ThemeDescription.ThemeDescriptionDelegate {
    public final /* synthetic */ TrendingStickersLayout f$0;

    public /* synthetic */ TrendingStickersAlert$$ExternalSyntheticLambda0(TrendingStickersLayout trendingStickersLayout) {
        this.f$0 = trendingStickersLayout;
    }

    public final void didSetColor() {
        this.f$0.updateColors();
    }

    public /* synthetic */ void onAnimationProgress(float f) {
        ThemeDescription.ThemeDescriptionDelegate.CC.$default$onAnimationProgress(this, f);
    }
}
