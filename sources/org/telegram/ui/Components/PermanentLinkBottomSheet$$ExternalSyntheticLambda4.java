package org.telegram.ui.Components;

import org.telegram.ui.ActionBar.ThemeDescription;

public final /* synthetic */ class PermanentLinkBottomSheet$$ExternalSyntheticLambda4 implements ThemeDescription.ThemeDescriptionDelegate {
    public final /* synthetic */ PermanentLinkBottomSheet f$0;

    public /* synthetic */ PermanentLinkBottomSheet$$ExternalSyntheticLambda4(PermanentLinkBottomSheet permanentLinkBottomSheet) {
        this.f$0 = permanentLinkBottomSheet;
    }

    public final void didSetColor() {
        this.f$0.updateColors();
    }

    public /* synthetic */ void onAnimationProgress(float f) {
        ThemeDescription.ThemeDescriptionDelegate.CC.$default$onAnimationProgress(this, f);
    }
}
