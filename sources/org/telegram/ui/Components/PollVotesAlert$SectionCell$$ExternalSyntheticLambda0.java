package org.telegram.ui.Components;

import android.view.View;
import org.telegram.ui.Components.PollVotesAlert;

public final /* synthetic */ class PollVotesAlert$SectionCell$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ PollVotesAlert.SectionCell f$0;

    public /* synthetic */ PollVotesAlert$SectionCell$$ExternalSyntheticLambda0(PollVotesAlert.SectionCell sectionCell) {
        this.f$0 = sectionCell;
    }

    public final void onClick(View view) {
        this.f$0.lambda$new$0(view);
    }
}
