package org.telegram.ui.Components;

import org.telegram.ui.Components.SimpleFloatPropertyCompat;

public final /* synthetic */ class OutlineTextContainerView$$ExternalSyntheticLambda1 implements SimpleFloatPropertyCompat.Getter {
    public static final /* synthetic */ OutlineTextContainerView$$ExternalSyntheticLambda1 INSTANCE = new OutlineTextContainerView$$ExternalSyntheticLambda1();

    private /* synthetic */ OutlineTextContainerView$$ExternalSyntheticLambda1() {
    }

    public final float get(Object obj) {
        return ((OutlineTextContainerView) obj).errorProgress;
    }
}
