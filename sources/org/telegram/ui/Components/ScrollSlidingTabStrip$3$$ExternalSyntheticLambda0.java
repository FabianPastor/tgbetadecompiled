package org.telegram.ui.Components;

import android.animation.ValueAnimator;
import org.telegram.ui.Components.ScrollSlidingTabStrip;

public final /* synthetic */ class ScrollSlidingTabStrip$3$$ExternalSyntheticLambda0 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ ScrollSlidingTabStrip.AnonymousClass3 f$0;

    public /* synthetic */ ScrollSlidingTabStrip$3$$ExternalSyntheticLambda0(ScrollSlidingTabStrip.AnonymousClass3 r1) {
        this.f$0 = r1;
    }

    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f$0.lambda$createAnimator$0(valueAnimator);
    }
}
