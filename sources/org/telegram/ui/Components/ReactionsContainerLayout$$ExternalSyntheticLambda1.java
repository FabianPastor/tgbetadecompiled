package org.telegram.ui.Components;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class ReactionsContainerLayout$$ExternalSyntheticLambda1 implements RecyclerListView.OnItemLongClickListener {
    public final /* synthetic */ ReactionsContainerLayout f$0;

    public /* synthetic */ ReactionsContainerLayout$$ExternalSyntheticLambda1(ReactionsContainerLayout reactionsContainerLayout) {
        this.f$0 = reactionsContainerLayout;
    }

    public final boolean onItemClick(View view, int i) {
        return this.f$0.lambda$new$1(view, i);
    }
}
