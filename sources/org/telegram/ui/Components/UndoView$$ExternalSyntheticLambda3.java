package org.telegram.ui.Components;

import android.view.MotionEvent;
import android.view.View;

public final /* synthetic */ class UndoView$$ExternalSyntheticLambda3 implements View.OnTouchListener {
    public static final /* synthetic */ UndoView$$ExternalSyntheticLambda3 INSTANCE = new UndoView$$ExternalSyntheticLambda3();

    private /* synthetic */ UndoView$$ExternalSyntheticLambda3() {
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return UndoView.lambda$new$1(view, motionEvent);
    }
}
