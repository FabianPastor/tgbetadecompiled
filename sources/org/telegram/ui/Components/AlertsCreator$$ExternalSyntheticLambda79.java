package org.telegram.ui.Components;

import android.view.MotionEvent;
import android.view.View;

public final /* synthetic */ class AlertsCreator$$ExternalSyntheticLambda79 implements View.OnTouchListener {
    public static final /* synthetic */ AlertsCreator$$ExternalSyntheticLambda79 INSTANCE = new AlertsCreator$$ExternalSyntheticLambda79();

    private /* synthetic */ AlertsCreator$$ExternalSyntheticLambda79() {
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return AlertsCreator.lambda$createAutoDeleteDatePickerDialog$63(view, motionEvent);
    }
}
