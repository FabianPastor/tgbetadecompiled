package org.telegram.ui.Components;

import android.view.MotionEvent;
import android.view.View;

public final /* synthetic */ class AlertsCreator$$ExternalSyntheticLambda77 implements View.OnTouchListener {
    public static final /* synthetic */ AlertsCreator$$ExternalSyntheticLambda77 INSTANCE = new AlertsCreator$$ExternalSyntheticLambda77();

    private /* synthetic */ AlertsCreator$$ExternalSyntheticLambda77() {
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return AlertsCreator.lambda$createScheduleDatePickerDialog$47(view, motionEvent);
    }
}
