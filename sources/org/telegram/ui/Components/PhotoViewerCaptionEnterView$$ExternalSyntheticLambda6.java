package org.telegram.ui.Components;

import android.view.View;

public final /* synthetic */ class PhotoViewerCaptionEnterView$$ExternalSyntheticLambda6 implements View.OnFocusChangeListener {
    public final /* synthetic */ PhotoViewerCaptionEnterView f$0;

    public /* synthetic */ PhotoViewerCaptionEnterView$$ExternalSyntheticLambda6(PhotoViewerCaptionEnterView photoViewerCaptionEnterView) {
        this.f$0 = photoViewerCaptionEnterView;
    }

    public final void onFocusChange(View view, boolean z) {
        this.f$0.lambda$new$1(view, z);
    }
}
