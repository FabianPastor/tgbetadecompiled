package org.telegram.ui.Components;

import android.view.View;

public final /* synthetic */ class PipVideoOverlay$$ExternalSyntheticLambda2 implements View.OnClickListener {
    public final /* synthetic */ PipVideoOverlay f$0;
    public final /* synthetic */ boolean f$1;

    public /* synthetic */ PipVideoOverlay$$ExternalSyntheticLambda2(PipVideoOverlay pipVideoOverlay, boolean z) {
        this.f$0 = pipVideoOverlay;
        this.f$1 = z;
    }

    public final void onClick(View view) {
        this.f$0.lambda$showInternal$10(this.f$1, view);
    }
}
