package org.telegram.ui.Components;

import org.telegram.ui.Components.SimpleFloatPropertyCompat;

public final /* synthetic */ class ChatActivityBotWebViewButton$$ExternalSyntheticLambda0 implements SimpleFloatPropertyCompat.Getter {
    public static final /* synthetic */ ChatActivityBotWebViewButton$$ExternalSyntheticLambda0 INSTANCE = new ChatActivityBotWebViewButton$$ExternalSyntheticLambda0();

    private /* synthetic */ ChatActivityBotWebViewButton$$ExternalSyntheticLambda0() {
    }

    public final float get(Object obj) {
        return ((ChatActivityBotWebViewButton) obj).progress;
    }
}
