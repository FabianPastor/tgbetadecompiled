package org.telegram.ui.Components;

import android.content.DialogInterface;

public final /* synthetic */ class ChatAttachAlert$$ExternalSyntheticLambda5 implements DialogInterface.OnDismissListener {
    public final /* synthetic */ ChatAttachAlert f$0;

    public /* synthetic */ ChatAttachAlert$$ExternalSyntheticLambda5(ChatAttachAlert chatAttachAlert) {
        this.f$0 = chatAttachAlert;
    }

    public final void onDismiss(DialogInterface dialogInterface) {
        this.f$0.lambda$dismiss$34(dialogInterface);
    }
}
