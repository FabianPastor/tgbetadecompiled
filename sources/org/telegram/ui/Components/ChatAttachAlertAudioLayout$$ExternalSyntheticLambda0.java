package org.telegram.ui.Components;

import android.view.MotionEvent;
import android.view.View;

public final /* synthetic */ class ChatAttachAlertAudioLayout$$ExternalSyntheticLambda0 implements View.OnTouchListener {
    public static final /* synthetic */ ChatAttachAlertAudioLayout$$ExternalSyntheticLambda0 INSTANCE = new ChatAttachAlertAudioLayout$$ExternalSyntheticLambda0();

    private /* synthetic */ ChatAttachAlertAudioLayout$$ExternalSyntheticLambda0() {
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return ChatAttachAlertAudioLayout.lambda$new$0(view, motionEvent);
    }
}
