package org.telegram.ui.Components;

import android.view.View;

public final /* synthetic */ class ChatAttachAlert$$ExternalSyntheticLambda12 implements View.OnClickListener {
    public final /* synthetic */ ChatAttachAlert f$0;
    public final /* synthetic */ boolean f$1;

    public /* synthetic */ ChatAttachAlert$$ExternalSyntheticLambda12(ChatAttachAlert chatAttachAlert, boolean z) {
        this.f$0 = chatAttachAlert;
        this.f$1 = z;
    }

    public final void onClick(View view) {
        this.f$0.lambda$new$3(this.f$1, view);
    }
}
