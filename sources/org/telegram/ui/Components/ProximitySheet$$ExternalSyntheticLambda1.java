package org.telegram.ui.Components;

import android.view.MotionEvent;
import android.view.View;

public final /* synthetic */ class ProximitySheet$$ExternalSyntheticLambda1 implements View.OnTouchListener {
    public static final /* synthetic */ ProximitySheet$$ExternalSyntheticLambda1 INSTANCE = new ProximitySheet$$ExternalSyntheticLambda1();

    private /* synthetic */ ProximitySheet$$ExternalSyntheticLambda1() {
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return ProximitySheet.lambda$new$0(view, motionEvent);
    }
}
