package org.telegram.ui.Components;

import android.content.DialogInterface;

public final /* synthetic */ class ChatThemeBottomSheet$$ExternalSyntheticLambda1 implements DialogInterface.OnClickListener {
    public final /* synthetic */ ChatThemeBottomSheet f$0;

    public /* synthetic */ ChatThemeBottomSheet$$ExternalSyntheticLambda1(ChatThemeBottomSheet chatThemeBottomSheet) {
        this.f$0 = chatThemeBottomSheet;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f$0.lambda$close$5(dialogInterface, i);
    }
}
