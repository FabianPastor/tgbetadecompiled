package org.telegram.ui.Components;

import android.widget.FrameLayout;

public final /* synthetic */ class ChatAttachAlertPhotoLayout$$ExternalSyntheticLambda9 implements Runnable {
    public final /* synthetic */ ChatAttachAlertPhotoLayout f$0;
    public final /* synthetic */ FrameLayout.LayoutParams f$1;

    public /* synthetic */ ChatAttachAlertPhotoLayout$$ExternalSyntheticLambda9(ChatAttachAlertPhotoLayout chatAttachAlertPhotoLayout, FrameLayout.LayoutParams layoutParams) {
        this.f$0 = chatAttachAlertPhotoLayout;
        this.f$1 = layoutParams;
    }

    public final void run() {
        this.f$0.lambda$applyCameraViewPosition$13(this.f$1);
    }
}
