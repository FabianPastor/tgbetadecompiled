package org.telegram.ui.Components;

import android.content.DialogInterface;

public final /* synthetic */ class PasscodeView$$ExternalSyntheticLambda0 implements DialogInterface.OnDismissListener {
    public final /* synthetic */ PasscodeView f$0;

    public /* synthetic */ PasscodeView$$ExternalSyntheticLambda0(PasscodeView passcodeView) {
        this.f$0 = passcodeView;
    }

    public final void onDismiss(DialogInterface dialogInterface) {
        this.f$0.lambda$checkFingerprint$7(dialogInterface);
    }
}
