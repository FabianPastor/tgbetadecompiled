package org.telegram.ui.Components.spoilers;

import android.animation.TimeInterpolator;

public final /* synthetic */ class SpoilerEffect$$ExternalSyntheticLambda0 implements TimeInterpolator {
    public static final /* synthetic */ SpoilerEffect$$ExternalSyntheticLambda0 INSTANCE = new SpoilerEffect$$ExternalSyntheticLambda0();

    private /* synthetic */ SpoilerEffect$$ExternalSyntheticLambda0() {
    }

    public final float getInterpolation(float f) {
        return SpoilerEffect.lambda$new$0(f);
    }
}
