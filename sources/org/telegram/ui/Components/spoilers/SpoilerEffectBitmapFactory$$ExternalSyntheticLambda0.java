package org.telegram.ui.Components.spoilers;

import android.graphics.Bitmap;

public final /* synthetic */ class SpoilerEffectBitmapFactory$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ SpoilerEffectBitmapFactory f$0;
    public final /* synthetic */ Bitmap f$1;

    public /* synthetic */ SpoilerEffectBitmapFactory$$ExternalSyntheticLambda0(SpoilerEffectBitmapFactory spoilerEffectBitmapFactory, Bitmap bitmap) {
        this.f$0 = spoilerEffectBitmapFactory;
        this.f$1 = bitmap;
    }

    public final void run() {
        this.f$0.lambda$checkUpdate$1(this.f$1);
    }
}
