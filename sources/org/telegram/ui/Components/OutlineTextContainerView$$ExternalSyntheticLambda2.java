package org.telegram.ui.Components;

import org.telegram.ui.Components.SimpleFloatPropertyCompat;

public final /* synthetic */ class OutlineTextContainerView$$ExternalSyntheticLambda2 implements SimpleFloatPropertyCompat.Setter {
    public static final /* synthetic */ OutlineTextContainerView$$ExternalSyntheticLambda2 INSTANCE = new OutlineTextContainerView$$ExternalSyntheticLambda2();

    private /* synthetic */ OutlineTextContainerView$$ExternalSyntheticLambda2() {
    }

    public final void set(Object obj, float f) {
        OutlineTextContainerView.lambda$static$1((OutlineTextContainerView) obj, f);
    }
}
