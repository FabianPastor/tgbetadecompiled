package org.telegram.ui.Components;

import org.telegram.ui.Components.NumberPicker;

public final /* synthetic */ class SwipeGestureSettingsView$$ExternalSyntheticLambda1 implements NumberPicker.Formatter {
    public final /* synthetic */ SwipeGestureSettingsView f$0;

    public /* synthetic */ SwipeGestureSettingsView$$ExternalSyntheticLambda1(SwipeGestureSettingsView swipeGestureSettingsView) {
        this.f$0 = swipeGestureSettingsView;
    }

    public final String format(int i) {
        return this.f$0.lambda$new$0(i);
    }
}
