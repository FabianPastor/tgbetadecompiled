package org.telegram.ui.Components;

import android.view.MotionEvent;
import android.view.View;

public final /* synthetic */ class PhotoPaintView$$ExternalSyntheticLambda11 implements View.OnTouchListener {
    public final /* synthetic */ PhotoPaintView f$0;

    public /* synthetic */ PhotoPaintView$$ExternalSyntheticLambda11(PhotoPaintView photoPaintView) {
        this.f$0 = photoPaintView;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return this.f$0.lambda$showPopup$18(view, motionEvent);
    }
}
