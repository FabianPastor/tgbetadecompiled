package org.telegram.ui.Components;

import android.view.View;

public final /* synthetic */ class JoinGroupAlert$$ExternalSyntheticLambda5 implements View.OnClickListener {
    public final /* synthetic */ JoinGroupAlert f$0;
    public final /* synthetic */ boolean f$1;

    public /* synthetic */ JoinGroupAlert$$ExternalSyntheticLambda5(JoinGroupAlert joinGroupAlert, boolean z) {
        this.f$0 = joinGroupAlert;
        this.f$1 = z;
    }

    public final void onClick(View view) {
        this.f$0.lambda$new$7(this.f$1, view);
    }
}
