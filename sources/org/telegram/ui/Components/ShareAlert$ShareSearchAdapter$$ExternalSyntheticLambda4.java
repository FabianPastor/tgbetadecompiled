package org.telegram.ui.Components;

import java.util.Comparator;
import org.telegram.ui.Components.ShareAlert;

public final /* synthetic */ class ShareAlert$ShareSearchAdapter$$ExternalSyntheticLambda4 implements Comparator {
    public static final /* synthetic */ ShareAlert$ShareSearchAdapter$$ExternalSyntheticLambda4 INSTANCE = new ShareAlert$ShareSearchAdapter$$ExternalSyntheticLambda4();

    private /* synthetic */ ShareAlert$ShareSearchAdapter$$ExternalSyntheticLambda4() {
    }

    public final int compare(Object obj, Object obj2) {
        return ShareAlert.ShareSearchAdapter.lambda$searchDialogsInternal$0(obj, obj2);
    }
}
