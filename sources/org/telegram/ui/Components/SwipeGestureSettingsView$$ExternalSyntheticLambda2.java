package org.telegram.ui.Components;

import org.telegram.ui.Components.NumberPicker;

public final /* synthetic */ class SwipeGestureSettingsView$$ExternalSyntheticLambda2 implements NumberPicker.OnValueChangeListener {
    public final /* synthetic */ SwipeGestureSettingsView f$0;

    public /* synthetic */ SwipeGestureSettingsView$$ExternalSyntheticLambda2(SwipeGestureSettingsView swipeGestureSettingsView) {
        this.f$0 = swipeGestureSettingsView;
    }

    public final void onValueChange(NumberPicker numberPicker, int i, int i2) {
        this.f$0.lambda$new$1(numberPicker, i, i2);
    }
}
