package org.telegram.ui.Components;

import android.view.MotionEvent;
import android.view.View;

public final /* synthetic */ class ForwardingPreviewView$$ExternalSyntheticLambda7 implements View.OnTouchListener {
    public final /* synthetic */ ForwardingPreviewView f$0;

    public /* synthetic */ ForwardingPreviewView$$ExternalSyntheticLambda7(ForwardingPreviewView forwardingPreviewView) {
        this.f$0 = forwardingPreviewView;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return this.f$0.lambda$new$7(view, motionEvent);
    }
}
