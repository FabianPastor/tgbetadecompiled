package org.telegram.ui.Components;

import android.content.Context;
import android.view.View;

public final /* synthetic */ class QRCodeBottomSheet$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ QRCodeBottomSheet f$0;
    public final /* synthetic */ Context f$1;

    public /* synthetic */ QRCodeBottomSheet$$ExternalSyntheticLambda0(QRCodeBottomSheet qRCodeBottomSheet, Context context) {
        this.f$0 = qRCodeBottomSheet;
        this.f$1 = context;
    }

    public final void onClick(View view) {
        this.f$0.lambda$new$0(this.f$1, view);
    }
}
