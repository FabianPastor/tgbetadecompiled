package org.telegram.ui.Components.Paint;

import org.telegram.ui.Components.Paint.RenderView;

public final /* synthetic */ class RenderView$CanvasInternal$$ExternalSyntheticLambda2 implements Runnable {
    public final /* synthetic */ RenderView.CanvasInternal f$0;

    public /* synthetic */ RenderView$CanvasInternal$$ExternalSyntheticLambda2(RenderView.CanvasInternal canvasInternal) {
        this.f$0 = canvasInternal;
    }

    public final void run() {
        this.f$0.lambda$scheduleRedraw$1();
    }
}
