package org.telegram.ui.Components;

import org.telegram.ui.Components.SimpleFloatPropertyCompat;

public final /* synthetic */ class OutlineTextContainerView$$ExternalSyntheticLambda3 implements SimpleFloatPropertyCompat.Setter {
    public static final /* synthetic */ OutlineTextContainerView$$ExternalSyntheticLambda3 INSTANCE = new OutlineTextContainerView$$ExternalSyntheticLambda3();

    private /* synthetic */ OutlineTextContainerView$$ExternalSyntheticLambda3() {
    }

    public final void set(Object obj, float f) {
        OutlineTextContainerView.lambda$static$3((OutlineTextContainerView) obj, f);
    }
}
