package org.telegram.ui.Components;

import org.telegram.ui.Components.ChatAttachAlertBotWebViewLayout;
import org.telegram.ui.Components.SimpleFloatPropertyCompat;

public final /* synthetic */ class ChatAttachAlertBotWebViewLayout$WebProgressView$$ExternalSyntheticLambda1 implements SimpleFloatPropertyCompat.Setter {
    public static final /* synthetic */ ChatAttachAlertBotWebViewLayout$WebProgressView$$ExternalSyntheticLambda1 INSTANCE = new ChatAttachAlertBotWebViewLayout$WebProgressView$$ExternalSyntheticLambda1();

    private /* synthetic */ ChatAttachAlertBotWebViewLayout$WebProgressView$$ExternalSyntheticLambda1() {
    }

    public final void set(Object obj, float f) {
        ((ChatAttachAlertBotWebViewLayout.WebProgressView) obj).setLoadProgress(f);
    }
}
