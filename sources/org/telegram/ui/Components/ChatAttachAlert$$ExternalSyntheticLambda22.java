package org.telegram.ui.Components;

import org.telegram.ui.Components.ChatAttachAlert;

public final /* synthetic */ class ChatAttachAlert$$ExternalSyntheticLambda22 implements Runnable {
    public final /* synthetic */ ChatAttachAlert f$0;
    public final /* synthetic */ ChatAttachAlert.AttachAlertLayout f$1;
    public final /* synthetic */ Runnable f$2;

    public /* synthetic */ ChatAttachAlert$$ExternalSyntheticLambda22(ChatAttachAlert chatAttachAlert, ChatAttachAlert.AttachAlertLayout attachAlertLayout, Runnable runnable) {
        this.f$0 = chatAttachAlert;
        this.f$1 = attachAlertLayout;
        this.f$2 = runnable;
    }

    public final void run() {
        this.f$0.lambda$showLayout$23(this.f$1, this.f$2);
    }
}
