package org.telegram.ui.Components;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class FilterTabsView$$ExternalSyntheticLambda2 implements RecyclerListView.OnItemLongClickListener {
    public final /* synthetic */ FilterTabsView f$0;

    public /* synthetic */ FilterTabsView$$ExternalSyntheticLambda2(FilterTabsView filterTabsView) {
        this.f$0 = filterTabsView;
    }

    public final boolean onItemClick(View view, int i) {
        return this.f$0.lambda$new$1(view, i);
    }
}
