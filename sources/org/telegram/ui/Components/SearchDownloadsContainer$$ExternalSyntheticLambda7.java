package org.telegram.ui.Components;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class SearchDownloadsContainer$$ExternalSyntheticLambda7 implements RecyclerListView.OnItemLongClickListener {
    public final /* synthetic */ SearchDownloadsContainer f$0;

    public /* synthetic */ SearchDownloadsContainer$$ExternalSyntheticLambda7(SearchDownloadsContainer searchDownloadsContainer) {
        this.f$0 = searchDownloadsContainer;
    }

    public final boolean onItemClick(View view, int i) {
        return this.f$0.lambda$new$1(view, i);
    }
}
