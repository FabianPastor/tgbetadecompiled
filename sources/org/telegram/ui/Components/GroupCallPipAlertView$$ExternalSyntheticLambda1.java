package org.telegram.ui.Components;

import android.content.Context;
import android.view.View;

public final /* synthetic */ class GroupCallPipAlertView$$ExternalSyntheticLambda1 implements View.OnClickListener {
    public final /* synthetic */ GroupCallPipAlertView f$0;
    public final /* synthetic */ Context f$1;

    public /* synthetic */ GroupCallPipAlertView$$ExternalSyntheticLambda1(GroupCallPipAlertView groupCallPipAlertView, Context context) {
        this.f$0 = groupCallPipAlertView;
        this.f$1 = context;
    }

    public final void onClick(View view) {
        this.f$0.lambda$new$1(this.f$1, view);
    }
}
