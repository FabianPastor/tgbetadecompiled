package org.telegram.ui.Components;

import android.animation.ValueAnimator;
import org.telegram.ui.Components.BotWebViewContainer;

public final /* synthetic */ class BotWebViewContainer$1$1$$ExternalSyntheticLambda0 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ BotWebViewContainer.AnonymousClass1.AnonymousClass1 f$0;

    public /* synthetic */ BotWebViewContainer$1$1$$ExternalSyntheticLambda0(BotWebViewContainer.AnonymousClass1.AnonymousClass1 r1) {
        this.f$0 = r1;
    }

    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f$0.lambda$setImageBitmapByKey$0(valueAnimator);
    }
}
