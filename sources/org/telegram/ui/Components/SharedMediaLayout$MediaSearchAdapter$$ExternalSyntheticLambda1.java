package org.telegram.ui.Components;

import org.telegram.ui.Components.SharedMediaLayout;

public final /* synthetic */ class SharedMediaLayout$MediaSearchAdapter$$ExternalSyntheticLambda1 implements Runnable {
    public final /* synthetic */ SharedMediaLayout.MediaSearchAdapter f$0;
    public final /* synthetic */ String f$1;

    public /* synthetic */ SharedMediaLayout$MediaSearchAdapter$$ExternalSyntheticLambda1(SharedMediaLayout.MediaSearchAdapter mediaSearchAdapter, String str) {
        this.f$0 = mediaSearchAdapter;
        this.f$1 = str;
    }

    public final void run() {
        this.f$0.lambda$search$3(this.f$1);
    }
}
