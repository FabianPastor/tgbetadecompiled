package org.telegram.ui.Components;

import org.telegram.ui.Components.PhotoFilterBlurControl;

public final /* synthetic */ class PhotoFilterView$$ExternalSyntheticLambda7 implements PhotoFilterBlurControl.PhotoFilterLinearBlurControlDelegate {
    public final /* synthetic */ PhotoFilterView f$0;

    public /* synthetic */ PhotoFilterView$$ExternalSyntheticLambda7(PhotoFilterView photoFilterView) {
        this.f$0 = photoFilterView;
    }

    public final void valueChanged(Point point, float f, float f2, float f3) {
        this.f$0.lambda$new$1(point, f, f2, f3);
    }
}
