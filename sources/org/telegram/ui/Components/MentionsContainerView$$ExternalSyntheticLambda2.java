package org.telegram.ui.Components;

import androidx.dynamicanimation.animation.DynamicAnimation;

public final /* synthetic */ class MentionsContainerView$$ExternalSyntheticLambda2 implements DynamicAnimation.OnAnimationUpdateListener {
    public final /* synthetic */ MentionsContainerView f$0;
    public final /* synthetic */ float f$1;
    public final /* synthetic */ float f$2;
    public final /* synthetic */ float f$3;
    public final /* synthetic */ float f$4;

    public /* synthetic */ MentionsContainerView$$ExternalSyntheticLambda2(MentionsContainerView mentionsContainerView, float f, float f2, float f3, float f4) {
        this.f$0 = mentionsContainerView;
        this.f$1 = f;
        this.f$2 = f2;
        this.f$3 = f3;
        this.f$4 = f4;
    }

    public final void onAnimationUpdate(DynamicAnimation dynamicAnimation, float f, float f2) {
        this.f$0.lambda$updateListViewTranslation$1(this.f$1, this.f$2, this.f$3, this.f$4, dynamicAnimation, f, f2);
    }
}
