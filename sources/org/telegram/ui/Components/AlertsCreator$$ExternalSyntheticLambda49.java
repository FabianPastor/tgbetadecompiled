package org.telegram.ui.Components;

import android.view.View;
import android.widget.LinearLayout;

public final /* synthetic */ class AlertsCreator$$ExternalSyntheticLambda49 implements View.OnClickListener {
    public final /* synthetic */ LinearLayout f$0;
    public final /* synthetic */ int[] f$1;

    public /* synthetic */ AlertsCreator$$ExternalSyntheticLambda49(LinearLayout linearLayout, int[] iArr) {
        this.f$0 = linearLayout;
        this.f$1 = iArr;
    }

    public final void onClick(View view) {
        AlertsCreator.lambda$createColorSelectDialog$88(this.f$0, this.f$1, view);
    }
}
