package org.telegram.ui.Components;

import android.animation.ValueAnimator;

public final /* synthetic */ class SubstringLayoutAnimator$$ExternalSyntheticLambda0 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ SubstringLayoutAnimator f$0;

    public /* synthetic */ SubstringLayoutAnimator$$ExternalSyntheticLambda0(SubstringLayoutAnimator substringLayoutAnimator) {
        this.f$0 = substringLayoutAnimator;
    }

    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f$0.lambda$create$0(valueAnimator);
    }
}
