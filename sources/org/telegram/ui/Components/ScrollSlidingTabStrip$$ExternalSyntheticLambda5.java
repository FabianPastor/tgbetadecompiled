package org.telegram.ui.Components;

import android.view.View;

public final /* synthetic */ class ScrollSlidingTabStrip$$ExternalSyntheticLambda5 implements View.OnClickListener {
    public final /* synthetic */ ScrollSlidingTabStrip f$0;

    public /* synthetic */ ScrollSlidingTabStrip$$ExternalSyntheticLambda5(ScrollSlidingTabStrip scrollSlidingTabStrip) {
        this.f$0 = scrollSlidingTabStrip;
    }

    public final void onClick(View view) {
        this.f$0.lambda$addEmojiTab$4(view);
    }
}
