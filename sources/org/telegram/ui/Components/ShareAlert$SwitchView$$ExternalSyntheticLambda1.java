package org.telegram.ui.Components;

import android.view.View;
import org.telegram.ui.Components.ShareAlert;

public final /* synthetic */ class ShareAlert$SwitchView$$ExternalSyntheticLambda1 implements View.OnClickListener {
    public final /* synthetic */ ShareAlert.SwitchView f$0;

    public /* synthetic */ ShareAlert$SwitchView$$ExternalSyntheticLambda1(ShareAlert.SwitchView switchView) {
        this.f$0 = switchView;
    }

    public final void onClick(View view) {
        this.f$0.lambda$new$0(view);
    }
}
