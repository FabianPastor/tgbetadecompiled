package org.telegram.ui.Components;

import org.telegram.tgnet.TLRPC$TL_error;

public final /* synthetic */ class BotWebViewSheet$$ExternalSyntheticLambda11 implements Runnable {
    public final /* synthetic */ BotWebViewSheet f$0;
    public final /* synthetic */ TLRPC$TL_error f$1;

    public /* synthetic */ BotWebViewSheet$$ExternalSyntheticLambda11(BotWebViewSheet botWebViewSheet, TLRPC$TL_error tLRPC$TL_error) {
        this.f$0 = botWebViewSheet;
        this.f$1 = tLRPC$TL_error;
    }

    public final void run() {
        this.f$0.lambda$new$2(this.f$1);
    }
}
