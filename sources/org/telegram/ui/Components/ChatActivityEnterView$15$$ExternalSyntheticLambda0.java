package org.telegram.ui.Components;

import android.animation.ValueAnimator;
import org.telegram.ui.Components.ChatActivityEnterView;

public final /* synthetic */ class ChatActivityEnterView$15$$ExternalSyntheticLambda0 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ ChatActivityEnterView.AnonymousClass15 f$0;

    public /* synthetic */ ChatActivityEnterView$15$$ExternalSyntheticLambda0(ChatActivityEnterView.AnonymousClass15 r1) {
        this.f$0 = r1;
    }

    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f$0.lambda$afterTextChanged$0(valueAnimator);
    }
}
