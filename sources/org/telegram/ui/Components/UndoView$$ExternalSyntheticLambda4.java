package org.telegram.ui.Components;

import android.view.MotionEvent;
import android.view.View;

public final /* synthetic */ class UndoView$$ExternalSyntheticLambda4 implements View.OnTouchListener {
    public static final /* synthetic */ UndoView$$ExternalSyntheticLambda4 INSTANCE = new UndoView$$ExternalSyntheticLambda4();

    private /* synthetic */ UndoView$$ExternalSyntheticLambda4() {
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return UndoView.lambda$showWithAction$3(view, motionEvent);
    }
}
