package org.telegram.ui.Components;

import android.content.DialogInterface;

public final /* synthetic */ class BotWebViewContainer$$ExternalSyntheticLambda1 implements DialogInterface.OnDismissListener {
    public final /* synthetic */ BotWebViewContainer f$0;

    public /* synthetic */ BotWebViewContainer$$ExternalSyntheticLambda1(BotWebViewContainer botWebViewContainer) {
        this.f$0 = botWebViewContainer;
    }

    public final void onDismiss(DialogInterface dialogInterface) {
        this.f$0.lambda$onOpenUri$2(dialogInterface);
    }
}
