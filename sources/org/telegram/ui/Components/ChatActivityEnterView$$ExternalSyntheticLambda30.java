package org.telegram.ui.Components;

import android.view.MotionEvent;
import android.view.View;

public final /* synthetic */ class ChatActivityEnterView$$ExternalSyntheticLambda30 implements View.OnTouchListener {
    public static final /* synthetic */ ChatActivityEnterView$$ExternalSyntheticLambda30 INSTANCE = new ChatActivityEnterView$$ExternalSyntheticLambda30();

    private /* synthetic */ ChatActivityEnterView$$ExternalSyntheticLambda30() {
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return ChatActivityEnterView.lambda$new$17(view, motionEvent);
    }
}
