package org.telegram.ui.Components;

import android.view.MotionEvent;
import android.view.View;

public final /* synthetic */ class StickersAlert$$ExternalSyntheticLambda14 implements View.OnTouchListener {
    public static final /* synthetic */ StickersAlert$$ExternalSyntheticLambda14 INSTANCE = new StickersAlert$$ExternalSyntheticLambda14();

    private /* synthetic */ StickersAlert$$ExternalSyntheticLambda14() {
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return StickersAlert.lambda$init$9(view, motionEvent);
    }
}
