package org.telegram.ui.Components;

import org.telegram.ui.Components.ChatAttachAlertBotWebViewLayout;
import org.telegram.ui.Components.SimpleFloatPropertyCompat;

public final /* synthetic */ class ChatAttachAlertBotWebViewLayout$WebProgressView$$ExternalSyntheticLambda0 implements SimpleFloatPropertyCompat.Getter {
    public static final /* synthetic */ ChatAttachAlertBotWebViewLayout$WebProgressView$$ExternalSyntheticLambda0 INSTANCE = new ChatAttachAlertBotWebViewLayout$WebProgressView$$ExternalSyntheticLambda0();

    private /* synthetic */ ChatAttachAlertBotWebViewLayout$WebProgressView$$ExternalSyntheticLambda0() {
    }

    public final float get(Object obj) {
        return ((ChatAttachAlertBotWebViewLayout.WebProgressView) obj).loadProgress;
    }
}
