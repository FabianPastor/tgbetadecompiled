package org.telegram.ui.Components;

import android.view.View;
import org.telegram.ui.Components.ProximitySheet;

public final /* synthetic */ class ProximitySheet$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ ProximitySheet f$0;
    public final /* synthetic */ ProximitySheet.onRadiusPickerChange f$1;

    public /* synthetic */ ProximitySheet$$ExternalSyntheticLambda0(ProximitySheet proximitySheet, ProximitySheet.onRadiusPickerChange onradiuspickerchange) {
        this.f$0 = proximitySheet;
        this.f$1 = onradiuspickerchange;
    }

    public final void onClick(View view) {
        this.f$0.lambda$new$4(this.f$1, view);
    }
}
