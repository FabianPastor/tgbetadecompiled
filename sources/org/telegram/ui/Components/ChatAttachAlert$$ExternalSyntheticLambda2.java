package org.telegram.ui.Components;

import android.content.DialogInterface;

public final /* synthetic */ class ChatAttachAlert$$ExternalSyntheticLambda2 implements DialogInterface.OnCancelListener {
    public final /* synthetic */ ChatAttachAlert f$0;

    public /* synthetic */ ChatAttachAlert$$ExternalSyntheticLambda2(ChatAttachAlert chatAttachAlert) {
        this.f$0 = chatAttachAlert;
    }

    public final void onCancel(DialogInterface dialogInterface) {
        this.f$0.lambda$dismiss$33(dialogInterface);
    }
}
