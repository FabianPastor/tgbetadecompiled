package org.telegram.ui.Components;

import android.view.KeyEvent;
import android.view.View;

public final /* synthetic */ class EmojiView$$ExternalSyntheticLambda1 implements View.OnKeyListener {
    public final /* synthetic */ EmojiView f$0;

    public /* synthetic */ EmojiView$$ExternalSyntheticLambda1(EmojiView emojiView) {
        this.f$0 = emojiView;
    }

    public final boolean onKey(View view, int i, KeyEvent keyEvent) {
        return this.f$0.lambda$new$7(view, i, keyEvent);
    }
}
