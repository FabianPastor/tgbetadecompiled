package org.telegram.ui.Components;

import android.animation.ValueAnimator;

public final /* synthetic */ class FadingTextViewLayout$$ExternalSyntheticLambda0 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ FadingTextViewLayout f$0;

    public /* synthetic */ FadingTextViewLayout$$ExternalSyntheticLambda0(FadingTextViewLayout fadingTextViewLayout) {
        this.f$0 = fadingTextViewLayout;
    }

    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f$0.lambda$new$0(valueAnimator);
    }
}
