package org.telegram.ui.Components;

import android.animation.ValueAnimator;

public final /* synthetic */ class TranslateAlert$$ExternalSyntheticLambda0 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ TranslateAlert f$0;

    public /* synthetic */ TranslateAlert$$ExternalSyntheticLambda0(TranslateAlert translateAlert) {
        this.f$0 = translateAlert;
    }

    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f$0.lambda$openAnimationTo$0(valueAnimator);
    }
}
