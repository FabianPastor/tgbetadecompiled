package org.telegram.ui.Components;

import org.telegram.ui.Components.SizeNotifierFrameLayout;

public final /* synthetic */ class BotWebViewSheet$$ExternalSyntheticLambda20 implements SizeNotifierFrameLayout.SizeNotifierFrameLayoutDelegate {
    public final /* synthetic */ BotWebViewSheet f$0;

    public /* synthetic */ BotWebViewSheet$$ExternalSyntheticLambda20(BotWebViewSheet botWebViewSheet) {
        this.f$0 = botWebViewSheet;
    }

    public final void onSizeChanged(int i, boolean z) {
        this.f$0.lambda$new$5(i, z);
    }
}
