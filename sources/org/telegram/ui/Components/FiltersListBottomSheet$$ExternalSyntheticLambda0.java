package org.telegram.ui.Components;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class FiltersListBottomSheet$$ExternalSyntheticLambda0 implements RecyclerListView.OnItemClickListener {
    public final /* synthetic */ FiltersListBottomSheet f$0;

    public /* synthetic */ FiltersListBottomSheet$$ExternalSyntheticLambda0(FiltersListBottomSheet filtersListBottomSheet) {
        this.f$0 = filtersListBottomSheet;
    }

    public final void onItemClick(View view, int i) {
        this.f$0.lambda$new$0(view, i);
    }
}
