package org.telegram.ui.Components;

import org.telegram.ui.Components.NumberPicker;

public final /* synthetic */ class AlertsCreator$$ExternalSyntheticLambda114 implements NumberPicker.Formatter {
    public static final /* synthetic */ AlertsCreator$$ExternalSyntheticLambda114 INSTANCE = new AlertsCreator$$ExternalSyntheticLambda114();

    private /* synthetic */ AlertsCreator$$ExternalSyntheticLambda114() {
    }

    public final String format(int i) {
        return AlertsCreator.lambda$createCalendarPickerDialog$79(i);
    }
}
