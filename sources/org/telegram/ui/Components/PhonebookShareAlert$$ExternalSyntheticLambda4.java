package org.telegram.ui.Components;

import androidx.core.widget.NestedScrollView;

public final /* synthetic */ class PhonebookShareAlert$$ExternalSyntheticLambda4 implements NestedScrollView.OnScrollChangeListener {
    public final /* synthetic */ PhonebookShareAlert f$0;

    public /* synthetic */ PhonebookShareAlert$$ExternalSyntheticLambda4(PhonebookShareAlert phonebookShareAlert) {
        this.f$0 = phonebookShareAlert;
    }

    public final void onScrollChange(NestedScrollView nestedScrollView, int i, int i2, int i3, int i4) {
        this.f$0.lambda$new$0(nestedScrollView, i, i2, i3, i4);
    }
}
