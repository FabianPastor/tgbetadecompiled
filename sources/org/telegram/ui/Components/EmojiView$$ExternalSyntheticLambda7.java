package org.telegram.ui.Components;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class EmojiView$$ExternalSyntheticLambda7 implements RecyclerListView.OnItemClickListener {
    public final /* synthetic */ EmojiView f$0;

    public /* synthetic */ EmojiView$$ExternalSyntheticLambda7(EmojiView emojiView) {
        this.f$0 = emojiView;
    }

    public final void onItemClick(View view, int i) {
        this.f$0.lambda$new$2(view, i);
    }
}
