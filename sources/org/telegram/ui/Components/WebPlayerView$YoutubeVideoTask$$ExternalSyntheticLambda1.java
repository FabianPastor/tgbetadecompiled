package org.telegram.ui.Components;

import org.telegram.ui.Components.WebPlayerView;

public final /* synthetic */ class WebPlayerView$YoutubeVideoTask$$ExternalSyntheticLambda1 implements Runnable {
    public final /* synthetic */ WebPlayerView.YoutubeVideoTask f$0;
    public final /* synthetic */ String f$1;

    public /* synthetic */ WebPlayerView$YoutubeVideoTask$$ExternalSyntheticLambda1(WebPlayerView.YoutubeVideoTask youtubeVideoTask, String str) {
        this.f$0 = youtubeVideoTask;
        this.f$1 = str;
    }

    public final void run() {
        this.f$0.lambda$doInBackground$1(this.f$1);
    }
}
