package org.telegram.ui.Components;

import android.content.Context;
import android.view.View;

public final /* synthetic */ class InviteMembersBottomSheet$$ExternalSyntheticLambda2 implements View.OnClickListener {
    public final /* synthetic */ InviteMembersBottomSheet f$0;
    public final /* synthetic */ Context f$1;
    public final /* synthetic */ long f$2;

    public /* synthetic */ InviteMembersBottomSheet$$ExternalSyntheticLambda2(InviteMembersBottomSheet inviteMembersBottomSheet, Context context, long j) {
        this.f$0 = inviteMembersBottomSheet;
        this.f$1 = context;
        this.f$2 = j;
    }

    public final void onClick(View view) {
        this.f$0.lambda$new$2(this.f$1, this.f$2, view);
    }
}
