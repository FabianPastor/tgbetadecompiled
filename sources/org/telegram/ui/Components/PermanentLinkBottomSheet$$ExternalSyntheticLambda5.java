package org.telegram.ui.Components;

import org.telegram.ui.Components.LinkActionView;

public final /* synthetic */ class PermanentLinkBottomSheet$$ExternalSyntheticLambda5 implements LinkActionView.Delegate {
    public final /* synthetic */ PermanentLinkBottomSheet f$0;

    public /* synthetic */ PermanentLinkBottomSheet$$ExternalSyntheticLambda5(PermanentLinkBottomSheet permanentLinkBottomSheet) {
        this.f$0 = permanentLinkBottomSheet;
    }

    public /* synthetic */ void editLink() {
        LinkActionView.Delegate.CC.$default$editLink(this);
    }

    public /* synthetic */ void removeLink() {
        LinkActionView.Delegate.CC.$default$removeLink(this);
    }

    public final void revokeLink() {
        this.f$0.lambda$new$0();
    }

    public /* synthetic */ void showUsersForPermanentLink() {
        LinkActionView.Delegate.CC.$default$showUsersForPermanentLink(this);
    }
}
