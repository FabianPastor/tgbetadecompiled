package org.telegram.ui.Components;

import android.view.MotionEvent;
import android.view.View;

public final /* synthetic */ class AlertsCreator$$ExternalSyntheticLambda76 implements View.OnTouchListener {
    public static final /* synthetic */ AlertsCreator$$ExternalSyntheticLambda76 INSTANCE = new AlertsCreator$$ExternalSyntheticLambda76();

    private /* synthetic */ AlertsCreator$$ExternalSyntheticLambda76() {
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return AlertsCreator.lambda$createDatePickerDialog$56(view, motionEvent);
    }
}
