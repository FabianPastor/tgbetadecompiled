package org.telegram.ui.Components;

import android.view.View;

public final /* synthetic */ class ScrollSlidingTabStrip$$ExternalSyntheticLambda3 implements View.OnClickListener {
    public final /* synthetic */ ScrollSlidingTabStrip f$0;

    public /* synthetic */ ScrollSlidingTabStrip$$ExternalSyntheticLambda3(ScrollSlidingTabStrip scrollSlidingTabStrip) {
        this.f$0 = scrollSlidingTabStrip;
    }

    public final void onClick(View view) {
        this.f$0.lambda$addIconTab$1(view);
    }
}
