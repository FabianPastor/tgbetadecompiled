package org.telegram.ui.Components;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class SearchDownloadsContainer$$ExternalSyntheticLambda6 implements RecyclerListView.OnItemClickListener {
    public final /* synthetic */ SearchDownloadsContainer f$0;

    public /* synthetic */ SearchDownloadsContainer$$ExternalSyntheticLambda6(SearchDownloadsContainer searchDownloadsContainer) {
        this.f$0 = searchDownloadsContainer;
    }

    public final void onItemClick(View view, int i) {
        this.f$0.lambda$new$0(view, i);
    }
}
