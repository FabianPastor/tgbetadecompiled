package org.telegram.ui.Components;

import android.view.MotionEvent;
import android.view.View;

public final /* synthetic */ class StickersAlert$$ExternalSyntheticLambda13 implements View.OnTouchListener {
    public final /* synthetic */ StickersAlert f$0;

    public /* synthetic */ StickersAlert$$ExternalSyntheticLambda13(StickersAlert stickersAlert) {
        this.f$0 = stickersAlert;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return this.f$0.lambda$init$7(view, motionEvent);
    }
}
