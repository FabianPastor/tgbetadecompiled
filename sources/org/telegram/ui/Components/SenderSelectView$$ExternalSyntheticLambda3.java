package org.telegram.ui.Components;

import org.telegram.ui.Components.SimpleFloatPropertyCompat;

public final /* synthetic */ class SenderSelectView$$ExternalSyntheticLambda3 implements SimpleFloatPropertyCompat.Getter {
    public static final /* synthetic */ SenderSelectView$$ExternalSyntheticLambda3 INSTANCE = new SenderSelectView$$ExternalSyntheticLambda3();

    private /* synthetic */ SenderSelectView$$ExternalSyntheticLambda3() {
    }

    public final float get(Object obj) {
        return ((SenderSelectView) obj).menuProgress;
    }
}
