package org.telegram.ui.Components;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class ChatAttachAlertPhotoLayout$$ExternalSyntheticLambda17 implements RecyclerListView.OnItemLongClickListener {
    public final /* synthetic */ ChatAttachAlertPhotoLayout f$0;

    public /* synthetic */ ChatAttachAlertPhotoLayout$$ExternalSyntheticLambda17(ChatAttachAlertPhotoLayout chatAttachAlertPhotoLayout) {
        this.f$0 = chatAttachAlertPhotoLayout;
    }

    public final boolean onItemClick(View view, int i) {
        return this.f$0.lambda$new$2(view, i);
    }
}
