package org.telegram.ui.Components;

import androidx.dynamicanimation.animation.SpringAnimation;

public final /* synthetic */ class AnimatedPhoneNumberEditText$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ SpringAnimation f$0;

    public /* synthetic */ AnimatedPhoneNumberEditText$$ExternalSyntheticLambda0(SpringAnimation springAnimation) {
        this.f$0 = springAnimation;
    }

    public final void run() {
        this.f$0.start();
    }
}
