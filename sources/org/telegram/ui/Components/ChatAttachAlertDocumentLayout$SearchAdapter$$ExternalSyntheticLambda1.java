package org.telegram.ui.Components;

import org.telegram.messenger.AccountInstance;
import org.telegram.ui.Components.ChatAttachAlertDocumentLayout;

public final /* synthetic */ class ChatAttachAlertDocumentLayout$SearchAdapter$$ExternalSyntheticLambda1 implements Runnable {
    public final /* synthetic */ ChatAttachAlertDocumentLayout.SearchAdapter f$0;
    public final /* synthetic */ long f$1;
    public final /* synthetic */ String f$2;
    public final /* synthetic */ AccountInstance f$3;
    public final /* synthetic */ long f$4;
    public final /* synthetic */ long f$5;
    public final /* synthetic */ boolean f$6;
    public final /* synthetic */ String f$7;
    public final /* synthetic */ int f$8;

    public /* synthetic */ ChatAttachAlertDocumentLayout$SearchAdapter$$ExternalSyntheticLambda1(ChatAttachAlertDocumentLayout.SearchAdapter searchAdapter, long j, String str, AccountInstance accountInstance, long j2, long j3, boolean z, String str2, int i) {
        this.f$0 = searchAdapter;
        this.f$1 = j;
        this.f$2 = str;
        this.f$3 = accountInstance;
        this.f$4 = j2;
        this.f$5 = j3;
        this.f$6 = z;
        this.f$7 = str2;
        this.f$8 = i;
    }

    public final void run() {
        this.f$0.lambda$searchGlobal$4(this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, this.f$6, this.f$7, this.f$8);
    }
}
