package org.telegram.ui.Components;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class GroupVoipInviteAlert$$ExternalSyntheticLambda4 implements RecyclerListView.OnItemClickListener {
    public final /* synthetic */ GroupVoipInviteAlert f$0;

    public /* synthetic */ GroupVoipInviteAlert$$ExternalSyntheticLambda4(GroupVoipInviteAlert groupVoipInviteAlert) {
        this.f$0 = groupVoipInviteAlert;
    }

    public final void onItemClick(View view, int i) {
        this.f$0.lambda$new$0(view, i);
    }
}
