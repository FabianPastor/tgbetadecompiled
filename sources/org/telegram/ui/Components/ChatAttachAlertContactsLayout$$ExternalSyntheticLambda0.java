package org.telegram.ui.Components;

import org.telegram.ui.ActionBar.ThemeDescription;

public final /* synthetic */ class ChatAttachAlertContactsLayout$$ExternalSyntheticLambda0 implements ThemeDescription.ThemeDescriptionDelegate {
    public final /* synthetic */ ChatAttachAlertContactsLayout f$0;

    public /* synthetic */ ChatAttachAlertContactsLayout$$ExternalSyntheticLambda0(ChatAttachAlertContactsLayout chatAttachAlertContactsLayout) {
        this.f$0 = chatAttachAlertContactsLayout;
    }

    public final void didSetColor() {
        this.f$0.lambda$getThemeDescriptions$2();
    }

    public /* synthetic */ void onAnimationProgress(float f) {
        ThemeDescription.ThemeDescriptionDelegate.CC.$default$onAnimationProgress(this, f);
    }
}
