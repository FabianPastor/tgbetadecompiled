package org.telegram.ui.Components;

import android.animation.ValueAnimator;
import org.telegram.ui.Components.FilterTabsView;

public final /* synthetic */ class FilterTabsView$4$$ExternalSyntheticLambda0 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ FilterTabsView.AnonymousClass4 f$0;

    public /* synthetic */ FilterTabsView$4$$ExternalSyntheticLambda0(FilterTabsView.AnonymousClass4 r1) {
        this.f$0 = r1;
    }

    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f$0.lambda$runPendingAnimations$0(valueAnimator);
    }
}
