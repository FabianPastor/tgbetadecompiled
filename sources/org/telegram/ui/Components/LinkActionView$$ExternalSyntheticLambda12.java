package org.telegram.ui.Components;

import android.view.KeyEvent;
import org.telegram.ui.ActionBar.ActionBarPopupWindow;

public final /* synthetic */ class LinkActionView$$ExternalSyntheticLambda12 implements ActionBarPopupWindow.OnDispatchKeyEventListener {
    public final /* synthetic */ LinkActionView f$0;

    public /* synthetic */ LinkActionView$$ExternalSyntheticLambda12(LinkActionView linkActionView) {
        this.f$0 = linkActionView;
    }

    public final void onDispatchKeyEvent(KeyEvent keyEvent) {
        this.f$0.lambda$new$8(keyEvent);
    }
}
