package org.telegram.ui.Components;

import android.view.MotionEvent;
import android.view.View;

public final /* synthetic */ class SharedMediaLayout$$ExternalSyntheticLambda4 implements View.OnTouchListener {
    public static final /* synthetic */ SharedMediaLayout$$ExternalSyntheticLambda4 INSTANCE = new SharedMediaLayout$$ExternalSyntheticLambda4();

    private /* synthetic */ SharedMediaLayout$$ExternalSyntheticLambda4() {
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return SharedMediaLayout.lambda$new$8(view, motionEvent);
    }
}
