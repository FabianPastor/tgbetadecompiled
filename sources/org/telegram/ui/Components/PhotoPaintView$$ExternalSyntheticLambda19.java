package org.telegram.ui.Components;

import android.view.KeyEvent;
import org.telegram.ui.ActionBar.ActionBarPopupWindow;

public final /* synthetic */ class PhotoPaintView$$ExternalSyntheticLambda19 implements ActionBarPopupWindow.OnDispatchKeyEventListener {
    public final /* synthetic */ PhotoPaintView f$0;

    public /* synthetic */ PhotoPaintView$$ExternalSyntheticLambda19(PhotoPaintView photoPaintView) {
        this.f$0 = photoPaintView;
    }

    public final void onDispatchKeyEvent(KeyEvent keyEvent) {
        this.f$0.lambda$showPopup$19(keyEvent);
    }
}
