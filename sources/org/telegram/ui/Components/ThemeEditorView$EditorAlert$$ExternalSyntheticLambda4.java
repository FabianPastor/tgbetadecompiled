package org.telegram.ui.Components;

import android.view.View;
import org.telegram.ui.Components.ThemeEditorView;

public final /* synthetic */ class ThemeEditorView$EditorAlert$$ExternalSyntheticLambda4 implements View.OnClickListener {
    public final /* synthetic */ ThemeEditorView.EditorAlert f$0;

    public /* synthetic */ ThemeEditorView$EditorAlert$$ExternalSyntheticLambda4(ThemeEditorView.EditorAlert editorAlert) {
        this.f$0 = editorAlert;
    }

    public final void onClick(View view) {
        this.f$0.lambda$new$3(view);
    }
}
