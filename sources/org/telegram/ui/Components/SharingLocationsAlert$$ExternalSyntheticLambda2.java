package org.telegram.ui.Components;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class SharingLocationsAlert$$ExternalSyntheticLambda2 implements RecyclerListView.OnItemClickListener {
    public final /* synthetic */ SharingLocationsAlert f$0;

    public /* synthetic */ SharingLocationsAlert$$ExternalSyntheticLambda2(SharingLocationsAlert sharingLocationsAlert) {
        this.f$0 = sharingLocationsAlert;
    }

    public final void onItemClick(View view, int i) {
        this.f$0.lambda$new$0(view, i);
    }
}
