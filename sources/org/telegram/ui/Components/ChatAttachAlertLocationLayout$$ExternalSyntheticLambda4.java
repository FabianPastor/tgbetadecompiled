package org.telegram.ui.Components;

import android.view.MotionEvent;
import android.view.View;

public final /* synthetic */ class ChatAttachAlertLocationLayout$$ExternalSyntheticLambda4 implements View.OnTouchListener {
    public static final /* synthetic */ ChatAttachAlertLocationLayout$$ExternalSyntheticLambda4 INSTANCE = new ChatAttachAlertLocationLayout$$ExternalSyntheticLambda4();

    private /* synthetic */ ChatAttachAlertLocationLayout$$ExternalSyntheticLambda4() {
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return ChatAttachAlertLocationLayout.lambda$new$4(view, motionEvent);
    }
}
