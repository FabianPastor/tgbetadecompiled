package org.telegram.ui.Components;

import org.telegram.ui.Components.LinkSpanDrawable;

public final /* synthetic */ class LinkSpanDrawable$LinkCollector$$ExternalSyntheticLambda1 implements Runnable {
    public final /* synthetic */ LinkSpanDrawable.LinkCollector f$0;
    public final /* synthetic */ LinkSpanDrawable f$1;

    public /* synthetic */ LinkSpanDrawable$LinkCollector$$ExternalSyntheticLambda1(LinkSpanDrawable.LinkCollector linkCollector, LinkSpanDrawable linkSpanDrawable) {
        this.f$0 = linkCollector;
        this.f$1 = linkSpanDrawable;
    }

    public final void run() {
        this.f$0.lambda$removeLink$1(this.f$1);
    }
}
