package org.telegram.ui.Components;

import android.view.View;
import android.widget.LinearLayout;

public final /* synthetic */ class AlertsCreator$$ExternalSyntheticLambda61 implements View.OnClickListener {
    public final /* synthetic */ int[] f$0;
    public final /* synthetic */ LinearLayout f$1;

    public /* synthetic */ AlertsCreator$$ExternalSyntheticLambda61(int[] iArr, LinearLayout linearLayout) {
        this.f$0 = iArr;
        this.f$1 = linearLayout;
    }

    public final void onClick(View view) {
        AlertsCreator.lambda$createLocationUpdateDialog$93(this.f$0, this.f$1, view);
    }
}
