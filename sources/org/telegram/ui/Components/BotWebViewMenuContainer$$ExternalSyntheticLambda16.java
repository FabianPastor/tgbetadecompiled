package org.telegram.ui.Components;

import org.telegram.messenger.GenericProvider;

public final /* synthetic */ class BotWebViewMenuContainer$$ExternalSyntheticLambda16 implements GenericProvider {
    public final /* synthetic */ ChatActivityEnterView f$0;

    public /* synthetic */ BotWebViewMenuContainer$$ExternalSyntheticLambda16(ChatActivityEnterView chatActivityEnterView) {
        this.f$0 = chatActivityEnterView;
    }

    public final Object provide(Object obj) {
        return BotWebViewMenuContainer.lambda$new$7(this.f$0, (Void) obj);
    }
}
