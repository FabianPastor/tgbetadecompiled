package org.telegram.ui.Components;

import android.content.Context;
import org.telegram.tgnet.TLObject;

public final /* synthetic */ class ShareAlert$$ExternalSyntheticLambda10 implements Runnable {
    public final /* synthetic */ ShareAlert f$0;
    public final /* synthetic */ TLObject f$1;
    public final /* synthetic */ Context f$2;

    public /* synthetic */ ShareAlert$$ExternalSyntheticLambda10(ShareAlert shareAlert, TLObject tLObject, Context context) {
        this.f$0 = shareAlert;
        this.f$1 = tLObject;
        this.f$2 = context;
    }

    public final void run() {
        this.f$0.lambda$new$0(this.f$1, this.f$2);
    }
}
