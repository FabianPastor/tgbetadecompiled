package org.telegram.ui.Components;

import android.view.animation.Interpolator;

public final /* synthetic */ class ViewPagerFixed$$ExternalSyntheticLambda0 implements Interpolator {
    public static final /* synthetic */ ViewPagerFixed$$ExternalSyntheticLambda0 INSTANCE = new ViewPagerFixed$$ExternalSyntheticLambda0();

    private /* synthetic */ ViewPagerFixed$$ExternalSyntheticLambda0() {
    }

    public final float getInterpolation(float f) {
        return ViewPagerFixed.lambda$static$0(f);
    }
}
