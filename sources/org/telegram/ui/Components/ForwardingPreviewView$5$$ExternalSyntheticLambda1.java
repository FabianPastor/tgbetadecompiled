package org.telegram.ui.Components;

import org.telegram.ui.Components.ForwardingPreviewView;

public final /* synthetic */ class ForwardingPreviewView$5$$ExternalSyntheticLambda1 implements Runnable {
    public final /* synthetic */ ForwardingPreviewView.AnonymousClass5 f$0;
    public final /* synthetic */ int f$1;

    public /* synthetic */ ForwardingPreviewView$5$$ExternalSyntheticLambda1(ForwardingPreviewView.AnonymousClass5 r1, int i) {
        this.f$0 = r1;
        this.f$1 = i;
    }

    public final void run() {
        this.f$0.lambda$endAnimations$2(this.f$1);
    }
}
