package org.telegram.ui.Components;

import android.view.KeyEvent;
import android.widget.TextView;

public final /* synthetic */ class PasscodeView$$ExternalSyntheticLambda6 implements TextView.OnEditorActionListener {
    public final /* synthetic */ PasscodeView f$0;

    public /* synthetic */ PasscodeView$$ExternalSyntheticLambda6(PasscodeView passcodeView) {
        this.f$0 = passcodeView;
    }

    public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        return this.f$0.lambda$new$0(textView, i, keyEvent);
    }
}
