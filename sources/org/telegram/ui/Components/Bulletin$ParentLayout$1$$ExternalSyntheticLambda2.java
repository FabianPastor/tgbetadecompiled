package org.telegram.ui.Components;

import androidx.dynamicanimation.animation.DynamicAnimation;
import org.telegram.ui.Components.Bulletin;

public final /* synthetic */ class Bulletin$ParentLayout$1$$ExternalSyntheticLambda2 implements DynamicAnimation.OnAnimationUpdateListener {
    public final /* synthetic */ Bulletin.Layout f$0;

    public /* synthetic */ Bulletin$ParentLayout$1$$ExternalSyntheticLambda2(Bulletin.Layout layout) {
        this.f$0 = layout;
    }

    public final void onAnimationUpdate(DynamicAnimation dynamicAnimation, float f, float f2) {
        Bulletin.ParentLayout.AnonymousClass1.lambda$onFling$1(this.f$0, dynamicAnimation, f, f2);
    }
}
