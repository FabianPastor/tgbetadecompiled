package org.telegram.ui.Components;

import android.view.KeyEvent;
import android.view.View;

public final /* synthetic */ class PhotoViewerCaptionEnterView$$ExternalSyntheticLambda7 implements View.OnKeyListener {
    public final /* synthetic */ PhotoViewerCaptionEnterView f$0;

    public /* synthetic */ PhotoViewerCaptionEnterView$$ExternalSyntheticLambda7(PhotoViewerCaptionEnterView photoViewerCaptionEnterView) {
        this.f$0 = photoViewerCaptionEnterView;
    }

    public final boolean onKey(View view, int i, KeyEvent keyEvent) {
        return this.f$0.lambda$new$3(view, i, keyEvent);
    }
}
