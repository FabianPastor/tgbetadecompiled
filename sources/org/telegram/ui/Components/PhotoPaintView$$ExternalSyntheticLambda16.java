package org.telegram.ui.Components;

import org.telegram.ui.Components.Paint.Views.EntityView;

public final /* synthetic */ class PhotoPaintView$$ExternalSyntheticLambda16 implements Runnable {
    public final /* synthetic */ PhotoPaintView f$0;
    public final /* synthetic */ EntityView f$1;

    public /* synthetic */ PhotoPaintView$$ExternalSyntheticLambda16(PhotoPaintView photoPaintView, EntityView entityView) {
        this.f$0 = photoPaintView;
        this.f$1 = entityView;
    }

    public final void run() {
        this.f$0.lambda$showMenuForEntity$13(this.f$1);
    }
}
