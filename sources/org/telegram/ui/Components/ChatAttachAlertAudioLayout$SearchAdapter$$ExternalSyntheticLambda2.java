package org.telegram.ui.Components;

import java.util.ArrayList;
import org.telegram.ui.Components.ChatAttachAlertAudioLayout;

public final /* synthetic */ class ChatAttachAlertAudioLayout$SearchAdapter$$ExternalSyntheticLambda2 implements Runnable {
    public final /* synthetic */ ChatAttachAlertAudioLayout.SearchAdapter f$0;
    public final /* synthetic */ String f$1;
    public final /* synthetic */ ArrayList f$2;
    public final /* synthetic */ int f$3;

    public /* synthetic */ ChatAttachAlertAudioLayout$SearchAdapter$$ExternalSyntheticLambda2(ChatAttachAlertAudioLayout.SearchAdapter searchAdapter, String str, ArrayList arrayList, int i) {
        this.f$0 = searchAdapter;
        this.f$1 = str;
        this.f$2 = arrayList;
        this.f$3 = i;
    }

    public final void run() {
        this.f$0.lambda$search$0(this.f$1, this.f$2, this.f$3);
    }
}
