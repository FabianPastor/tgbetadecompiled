package org.telegram.ui.Components;

import org.telegram.tgnet.TLObject;

public final /* synthetic */ class ChatAttachAlertBotWebViewLayout$$ExternalSyntheticLambda8 implements Runnable {
    public final /* synthetic */ ChatAttachAlertBotWebViewLayout f$0;
    public final /* synthetic */ TLObject f$1;
    public final /* synthetic */ int f$2;

    public /* synthetic */ ChatAttachAlertBotWebViewLayout$$ExternalSyntheticLambda8(ChatAttachAlertBotWebViewLayout chatAttachAlertBotWebViewLayout, TLObject tLObject, int i) {
        this.f$0 = chatAttachAlertBotWebViewLayout;
        this.f$1 = tLObject;
        this.f$2 = i;
    }

    public final void run() {
        this.f$0.lambda$requestWebView$11(this.f$1, this.f$2);
    }
}
