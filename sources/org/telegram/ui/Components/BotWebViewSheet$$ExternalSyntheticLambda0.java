package org.telegram.ui.Components;

import android.animation.ValueAnimator;

public final /* synthetic */ class BotWebViewSheet$$ExternalSyntheticLambda0 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ BotWebViewSheet f$0;

    public /* synthetic */ BotWebViewSheet$$ExternalSyntheticLambda0(BotWebViewSheet botWebViewSheet) {
        this.f$0 = botWebViewSheet;
    }

    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f$0.lambda$new$7(valueAnimator);
    }
}
