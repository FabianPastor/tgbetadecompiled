package org.telegram.ui.Components;

import android.view.View;
import android.widget.ImageView;

public final /* synthetic */ class ShareAlert$$ExternalSyntheticLambda7 implements View.OnLongClickListener {
    public final /* synthetic */ ShareAlert f$0;
    public final /* synthetic */ ImageView f$1;

    public /* synthetic */ ShareAlert$$ExternalSyntheticLambda7(ShareAlert shareAlert, ImageView imageView) {
        this.f$0 = shareAlert;
        this.f$1 = imageView;
    }

    public final boolean onLongClick(View view) {
        return this.f$0.lambda$new$8(this.f$1, view);
    }
}
