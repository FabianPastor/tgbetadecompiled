package org.telegram.ui.Components;

import android.view.KeyEvent;
import android.widget.TextView;

public final /* synthetic */ class ColorPicker$$ExternalSyntheticLambda5 implements TextView.OnEditorActionListener {
    public static final /* synthetic */ ColorPicker$$ExternalSyntheticLambda5 INSTANCE = new ColorPicker$$ExternalSyntheticLambda5();

    private /* synthetic */ ColorPicker$$ExternalSyntheticLambda5() {
    }

    public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        return ColorPicker.lambda$new$1(textView, i, keyEvent);
    }
}
