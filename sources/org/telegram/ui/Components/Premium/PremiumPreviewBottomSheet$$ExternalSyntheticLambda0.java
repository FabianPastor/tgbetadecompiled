package org.telegram.ui.Components.Premium;

import android.content.DialogInterface;

public final /* synthetic */ class PremiumPreviewBottomSheet$$ExternalSyntheticLambda0 implements DialogInterface.OnDismissListener {
    public final /* synthetic */ PremiumPreviewBottomSheet f$0;

    public /* synthetic */ PremiumPreviewBottomSheet$$ExternalSyntheticLambda0(PremiumPreviewBottomSheet premiumPreviewBottomSheet) {
        this.f$0 = premiumPreviewBottomSheet;
    }

    public final void onDismiss(DialogInterface dialogInterface) {
        this.f$0.lambda$showDialog$0(dialogInterface);
    }
}
