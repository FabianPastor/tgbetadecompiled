package org.telegram.ui.Components.Premium;

import j$.util.function.ToIntFunction;
import org.telegram.ui.Components.Premium.CarouselView;

public final /* synthetic */ class CarouselView$$ExternalSyntheticLambda2 implements ToIntFunction {
    public static final /* synthetic */ CarouselView$$ExternalSyntheticLambda2 INSTANCE = new CarouselView$$ExternalSyntheticLambda2();

    private /* synthetic */ CarouselView$$ExternalSyntheticLambda2() {
    }

    public final int applyAsInt(Object obj) {
        return CarouselView.lambda$new$1((CarouselView.DrawingObject) obj);
    }
}
