package org.telegram.ui.Components.Premium;

import android.view.animation.Interpolator;

public final /* synthetic */ class CarouselView$$ExternalSyntheticLambda1 implements Interpolator {
    public static final /* synthetic */ CarouselView$$ExternalSyntheticLambda1 INSTANCE = new CarouselView$$ExternalSyntheticLambda1();

    private /* synthetic */ CarouselView$$ExternalSyntheticLambda1() {
    }

    public final float getInterpolation(float f) {
        return CarouselView.lambda$static$0(f);
    }
}
