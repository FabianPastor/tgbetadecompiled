package org.telegram.ui.Components.Premium;

import android.view.View;
import org.telegram.ui.ActionBar.BaseFragment;

public final /* synthetic */ class DoubledLimitsBottomSheet$$ExternalSyntheticLambda1 implements View.OnClickListener {
    public final /* synthetic */ DoubledLimitsBottomSheet f$0;
    public final /* synthetic */ int f$1;
    public final /* synthetic */ BaseFragment f$2;

    public /* synthetic */ DoubledLimitsBottomSheet$$ExternalSyntheticLambda1(DoubledLimitsBottomSheet doubledLimitsBottomSheet, int i, BaseFragment baseFragment) {
        this.f$0 = doubledLimitsBottomSheet;
        this.f$1 = i;
        this.f$2 = baseFragment;
    }

    public final void onClick(View view) {
        this.f$0.lambda$new$0(this.f$1, this.f$2, view);
    }
}
