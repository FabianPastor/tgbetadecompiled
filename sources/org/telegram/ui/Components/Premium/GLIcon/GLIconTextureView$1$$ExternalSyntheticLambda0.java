package org.telegram.ui.Components.Premium.GLIcon;

import org.telegram.ui.Components.Premium.GLIcon.GLIconTextureView;

public final /* synthetic */ class GLIconTextureView$1$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ GLIconTextureView.AnonymousClass1 f$0;
    public final /* synthetic */ float f$1;
    public final /* synthetic */ float f$2;

    public /* synthetic */ GLIconTextureView$1$$ExternalSyntheticLambda0(GLIconTextureView.AnonymousClass1 r1, float f, float f2) {
        this.f$0 = r1;
        this.f$1 = f;
        this.f$2 = f2;
    }

    public final void run() {
        this.f$0.lambda$onSingleTapUp$0(this.f$1, this.f$2);
    }
}
