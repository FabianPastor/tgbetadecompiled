package org.telegram.ui.Components.Premium;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class LimitReachedBottomSheet$$ExternalSyntheticLambda10 implements RecyclerListView.OnItemLongClickListener {
    public final /* synthetic */ LimitReachedBottomSheet f$0;

    public /* synthetic */ LimitReachedBottomSheet$$ExternalSyntheticLambda10(LimitReachedBottomSheet limitReachedBottomSheet) {
        this.f$0 = limitReachedBottomSheet;
    }

    public final boolean onItemClick(View view, int i) {
        return this.f$0.lambda$onViewCreated$1(view, i);
    }
}
