package org.telegram.ui.Components.Premium;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class LimitReachedBottomSheet$$ExternalSyntheticLambda9 implements RecyclerListView.OnItemClickListener {
    public final /* synthetic */ LimitReachedBottomSheet f$0;

    public /* synthetic */ LimitReachedBottomSheet$$ExternalSyntheticLambda9(LimitReachedBottomSheet limitReachedBottomSheet) {
        this.f$0 = limitReachedBottomSheet;
    }

    public final void onItemClick(View view, int i) {
        this.f$0.lambda$onViewCreated$0(view, i);
    }
}
