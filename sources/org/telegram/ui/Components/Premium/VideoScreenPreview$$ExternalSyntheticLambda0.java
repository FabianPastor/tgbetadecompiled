package org.telegram.ui.Components.Premium;

import java.io.File;

public final /* synthetic */ class VideoScreenPreview$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ VideoScreenPreview f$0;
    public final /* synthetic */ File f$1;

    public /* synthetic */ VideoScreenPreview$$ExternalSyntheticLambda0(VideoScreenPreview videoScreenPreview, File file) {
        this.f$0 = videoScreenPreview;
        this.f$1 = file;
    }

    public final void run() {
        this.f$0.lambda$setVideo$0(this.f$1);
    }
}
