package org.telegram.ui.Components.Premium;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class PremiumStickersPreviewRecycler$$ExternalSyntheticLambda2 implements RecyclerListView.OnItemClickListener {
    public final /* synthetic */ PremiumStickersPreviewRecycler f$0;

    public /* synthetic */ PremiumStickersPreviewRecycler$$ExternalSyntheticLambda2(PremiumStickersPreviewRecycler premiumStickersPreviewRecycler) {
        this.f$0 = premiumStickersPreviewRecycler;
    }

    public final void onItemClick(View view, int i) {
        this.f$0.lambda$new$1(view, i);
    }
}
