package org.telegram.ui.Components.Premium;

import java.util.Comparator;
import java.util.HashMap;

public final /* synthetic */ class PremiumFeatureBottomSheet$$ExternalSyntheticLambda3 implements Comparator {
    public final /* synthetic */ HashMap f$0;

    public /* synthetic */ PremiumFeatureBottomSheet$$ExternalSyntheticLambda3(HashMap hashMap) {
        this.f$0 = hashMap;
    }

    public final int compare(Object obj, Object obj2) {
        return PremiumFeatureBottomSheet.lambda$getViewForPosition$3(this.f$0, (ReactionDrawingObject) obj, (ReactionDrawingObject) obj2);
    }
}
