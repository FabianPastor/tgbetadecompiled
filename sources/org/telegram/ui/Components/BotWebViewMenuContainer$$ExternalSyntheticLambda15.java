package org.telegram.ui.Components;

import org.telegram.ui.ActionBar.ActionBar;

public final /* synthetic */ class BotWebViewMenuContainer$$ExternalSyntheticLambda15 implements Runnable {
    public final /* synthetic */ BotWebViewMenuContainer f$0;
    public final /* synthetic */ ActionBar f$1;

    public /* synthetic */ BotWebViewMenuContainer$$ExternalSyntheticLambda15(BotWebViewMenuContainer botWebViewMenuContainer, ActionBar actionBar) {
        this.f$0 = botWebViewMenuContainer;
        this.f$1 = actionBar;
    }

    public final void run() {
        this.f$0.lambda$new$5(this.f$1);
    }
}
