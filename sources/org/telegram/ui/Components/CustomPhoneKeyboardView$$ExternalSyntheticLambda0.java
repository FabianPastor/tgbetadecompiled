package org.telegram.ui.Components;

import android.view.View;

public final /* synthetic */ class CustomPhoneKeyboardView$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ CustomPhoneKeyboardView f$0;
    public final /* synthetic */ String f$1;

    public /* synthetic */ CustomPhoneKeyboardView$$ExternalSyntheticLambda0(CustomPhoneKeyboardView customPhoneKeyboardView, String str) {
        this.f$0 = customPhoneKeyboardView;
        this.f$1 = str;
    }

    public final void onClick(View view) {
        this.f$0.lambda$new$2(this.f$1, view);
    }
}
