package org.telegram.ui.Components;

import java.util.ArrayList;
import org.telegram.ui.Components.ChatAttachAlertContactsLayout;

public final /* synthetic */ class ChatAttachAlertContactsLayout$ShareSearchAdapter$$ExternalSyntheticLambda3 implements Runnable {
    public final /* synthetic */ ChatAttachAlertContactsLayout.ShareSearchAdapter f$0;
    public final /* synthetic */ String f$1;
    public final /* synthetic */ ArrayList f$2;
    public final /* synthetic */ ArrayList f$3;
    public final /* synthetic */ int f$4;
    public final /* synthetic */ int f$5;

    public /* synthetic */ ChatAttachAlertContactsLayout$ShareSearchAdapter$$ExternalSyntheticLambda3(ChatAttachAlertContactsLayout.ShareSearchAdapter shareSearchAdapter, String str, ArrayList arrayList, ArrayList arrayList2, int i, int i2) {
        this.f$0 = shareSearchAdapter;
        this.f$1 = str;
        this.f$2 = arrayList;
        this.f$3 = arrayList2;
        this.f$4 = i;
        this.f$5 = i2;
    }

    public final void run() {
        this.f$0.lambda$processSearch$1(this.f$1, this.f$2, this.f$3, this.f$4, this.f$5);
    }
}
