package org.telegram.ui.Components;

import org.telegram.messenger.GenericProvider;

public final /* synthetic */ class ChatAttachAlertBotWebViewLayout$$ExternalSyntheticLambda10 implements GenericProvider {
    public final /* synthetic */ ChatAttachAlertBotWebViewLayout f$0;

    public /* synthetic */ ChatAttachAlertBotWebViewLayout$$ExternalSyntheticLambda10(ChatAttachAlertBotWebViewLayout chatAttachAlertBotWebViewLayout) {
        this.f$0 = chatAttachAlertBotWebViewLayout;
    }

    public final Object provide(Object obj) {
        return this.f$0.lambda$new$6((Void) obj);
    }
}
