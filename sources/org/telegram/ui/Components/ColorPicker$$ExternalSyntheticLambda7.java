package org.telegram.ui.Components;

import org.telegram.ui.ActionBar.ThemeDescription;

public final /* synthetic */ class ColorPicker$$ExternalSyntheticLambda7 implements ThemeDescription.ThemeDescriptionDelegate {
    public final /* synthetic */ ColorPicker f$0;

    public /* synthetic */ ColorPicker$$ExternalSyntheticLambda7(ColorPicker colorPicker) {
        this.f$0 = colorPicker;
    }

    public final void didSetColor() {
        this.f$0.lambda$provideThemeDescriptions$7();
    }

    public /* synthetic */ void onAnimationProgress(float f) {
        ThemeDescription.ThemeDescriptionDelegate.CC.$default$onAnimationProgress(this, f);
    }
}
