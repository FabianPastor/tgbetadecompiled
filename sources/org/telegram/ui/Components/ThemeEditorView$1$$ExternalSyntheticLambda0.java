package org.telegram.ui.Components;

import android.content.DialogInterface;
import org.telegram.ui.Components.ThemeEditorView;

public final /* synthetic */ class ThemeEditorView$1$$ExternalSyntheticLambda0 implements DialogInterface.OnDismissListener {
    public final /* synthetic */ ThemeEditorView.AnonymousClass1 f$0;

    public /* synthetic */ ThemeEditorView$1$$ExternalSyntheticLambda0(ThemeEditorView.AnonymousClass1 r1) {
        this.f$0 = r1;
    }

    public final void onDismiss(DialogInterface dialogInterface) {
        this.f$0.lambda$onTouchEvent$1(dialogInterface);
    }
}
