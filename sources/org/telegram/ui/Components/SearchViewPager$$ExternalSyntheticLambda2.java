package org.telegram.ui.Components;

import android.view.MotionEvent;
import android.view.View;

public final /* synthetic */ class SearchViewPager$$ExternalSyntheticLambda2 implements View.OnTouchListener {
    public static final /* synthetic */ SearchViewPager$$ExternalSyntheticLambda2 INSTANCE = new SearchViewPager$$ExternalSyntheticLambda2();

    private /* synthetic */ SearchViewPager$$ExternalSyntheticLambda2() {
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return SearchViewPager.lambda$showActionMode$0(view, motionEvent);
    }
}
