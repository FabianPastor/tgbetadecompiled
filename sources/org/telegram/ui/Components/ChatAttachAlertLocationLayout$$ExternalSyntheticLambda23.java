package org.telegram.ui.Components;

import org.telegram.ui.Components.AlertsCreator;

public final /* synthetic */ class ChatAttachAlertLocationLayout$$ExternalSyntheticLambda23 implements AlertsCreator.ScheduleDatePickerDelegate {
    public final /* synthetic */ ChatAttachAlertLocationLayout f$0;
    public final /* synthetic */ Object f$1;

    public /* synthetic */ ChatAttachAlertLocationLayout$$ExternalSyntheticLambda23(ChatAttachAlertLocationLayout chatAttachAlertLocationLayout, Object obj) {
        this.f$0 = chatAttachAlertLocationLayout;
        this.f$1 = obj;
    }

    public final void didSelectDate(boolean z, int i) {
        this.f$0.lambda$new$6(this.f$1, z, i);
    }
}
