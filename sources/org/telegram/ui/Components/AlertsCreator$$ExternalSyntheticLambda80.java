package org.telegram.ui.Components;

import android.view.MotionEvent;
import android.view.View;

public final /* synthetic */ class AlertsCreator$$ExternalSyntheticLambda80 implements View.OnTouchListener {
    public static final /* synthetic */ AlertsCreator$$ExternalSyntheticLambda80 INSTANCE = new AlertsCreator$$ExternalSyntheticLambda80();

    private /* synthetic */ AlertsCreator$$ExternalSyntheticLambda80() {
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return AlertsCreator.lambda$createMuteForPickerDialog$73(view, motionEvent);
    }
}
