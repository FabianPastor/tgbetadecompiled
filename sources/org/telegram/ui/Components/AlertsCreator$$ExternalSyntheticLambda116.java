package org.telegram.ui.Components;

import org.telegram.ui.Components.NumberPicker;

public final /* synthetic */ class AlertsCreator$$ExternalSyntheticLambda116 implements NumberPicker.Formatter {
    public static final /* synthetic */ AlertsCreator$$ExternalSyntheticLambda116 INSTANCE = new AlertsCreator$$ExternalSyntheticLambda116();

    private /* synthetic */ AlertsCreator$$ExternalSyntheticLambda116() {
    }

    public final String format(int i) {
        return AlertsCreator.lambda$createTTLAlert$107(i);
    }
}
