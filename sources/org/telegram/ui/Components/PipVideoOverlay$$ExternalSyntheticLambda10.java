package org.telegram.ui.Components;

import org.telegram.ui.Components.SimpleFloatPropertyCompat;

public final /* synthetic */ class PipVideoOverlay$$ExternalSyntheticLambda10 implements SimpleFloatPropertyCompat.Getter {
    public static final /* synthetic */ PipVideoOverlay$$ExternalSyntheticLambda10 INSTANCE = new PipVideoOverlay$$ExternalSyntheticLambda10();

    private /* synthetic */ PipVideoOverlay$$ExternalSyntheticLambda10() {
    }

    public final float get(Object obj) {
        return ((PipVideoOverlay) obj).pipY;
    }
}
