package org.telegram.ui.Components;

import java.util.Comparator;
import org.telegram.tgnet.TLObject;

public final /* synthetic */ class GroupVoipInviteAlert$$ExternalSyntheticLambda2 implements Comparator {
    public final /* synthetic */ GroupVoipInviteAlert f$0;
    public final /* synthetic */ int f$1;

    public /* synthetic */ GroupVoipInviteAlert$$ExternalSyntheticLambda2(GroupVoipInviteAlert groupVoipInviteAlert, int i) {
        this.f$0 = groupVoipInviteAlert;
        this.f$1 = i;
    }

    public final int compare(Object obj, Object obj2) {
        return this.f$0.lambda$loadChatParticipants$2(this.f$1, (TLObject) obj, (TLObject) obj2);
    }
}
