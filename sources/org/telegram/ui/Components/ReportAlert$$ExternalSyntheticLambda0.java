package org.telegram.ui.Components;

import android.view.View;

public final /* synthetic */ class ReportAlert$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ ReportAlert f$0;
    public final /* synthetic */ int f$1;

    public /* synthetic */ ReportAlert$$ExternalSyntheticLambda0(ReportAlert reportAlert, int i) {
        this.f$0 = reportAlert;
        this.f$1 = i;
    }

    public final void onClick(View view) {
        this.f$0.lambda$new$1(this.f$1, view);
    }
}
