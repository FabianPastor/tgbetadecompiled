package org.telegram.ui.Components;

import android.animation.ValueAnimator;

public final /* synthetic */ class ChatAttachAlertBotWebViewLayout$$ExternalSyntheticLambda0 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ ChatAttachAlertBotWebViewLayout f$0;

    public /* synthetic */ ChatAttachAlertBotWebViewLayout$$ExternalSyntheticLambda0(ChatAttachAlertBotWebViewLayout chatAttachAlertBotWebViewLayout) {
        this.f$0 = chatAttachAlertBotWebViewLayout;
    }

    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f$0.lambda$onPanTransitionStart$9(valueAnimator);
    }
}
