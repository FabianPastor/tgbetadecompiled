package org.telegram.ui;

import java.util.Comparator;
import org.telegram.ui.PassportActivity;

public final /* synthetic */ class PassportActivity$3$$ExternalSyntheticLambda5 implements Comparator {
    public final /* synthetic */ PassportActivity.AnonymousClass3 f$0;

    public /* synthetic */ PassportActivity$3$$ExternalSyntheticLambda5(PassportActivity.AnonymousClass3 r1) {
        this.f$0 = r1;
    }

    public final int compare(Object obj, Object obj2) {
        return this.f$0.lambda$onIdentityDone$3((String) obj, (String) obj2);
    }
}
