package org.telegram.ui;

import android.view.MotionEvent;
import android.view.View;

public final /* synthetic */ class ChangeUsernameActivity$$ExternalSyntheticLambda1 implements View.OnTouchListener {
    public static final /* synthetic */ ChangeUsernameActivity$$ExternalSyntheticLambda1 INSTANCE = new ChangeUsernameActivity$$ExternalSyntheticLambda1();

    private /* synthetic */ ChangeUsernameActivity$$ExternalSyntheticLambda1() {
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return ChangeUsernameActivity.lambda$createView$0(view, motionEvent);
    }
}
