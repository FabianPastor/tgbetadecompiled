package org.telegram.ui;

public final /* synthetic */ class PhotoViewer$$ExternalSyntheticLambda67 implements Runnable {
    public final /* synthetic */ PhotoViewer f$0;
    public final /* synthetic */ boolean f$1;

    public /* synthetic */ PhotoViewer$$ExternalSyntheticLambda67(PhotoViewer photoViewer, boolean z) {
        this.f$0 = photoViewer;
        this.f$1 = z;
    }

    public final void run() {
        this.f$0.lambda$updateResetButtonVisibility$53(this.f$1);
    }
}
