package org.telegram.ui;

import android.view.View;
import org.telegram.ui.FiltersSetupActivity;

public final /* synthetic */ class FiltersSetupActivity$ListAdapter$$ExternalSyntheticLambda2 implements View.OnClickListener {
    public final /* synthetic */ FiltersSetupActivity.ListAdapter f$0;

    public /* synthetic */ FiltersSetupActivity$ListAdapter$$ExternalSyntheticLambda2(FiltersSetupActivity.ListAdapter listAdapter) {
        this.f$0 = listAdapter;
    }

    public final void onClick(View view) {
        this.f$0.lambda$onCreateViewHolder$5(view);
    }
}
