package org.telegram.ui;

import java.util.ArrayList;

public final /* synthetic */ class DialogsActivity$$ExternalSyntheticLambda41 implements Runnable {
    public final /* synthetic */ DialogsActivity f$0;
    public final /* synthetic */ ArrayList f$1;

    public /* synthetic */ DialogsActivity$$ExternalSyntheticLambda41(DialogsActivity dialogsActivity, ArrayList arrayList) {
        this.f$0 = dialogsActivity;
        this.f$1 = arrayList;
    }

    public final void run() {
        this.f$0.lambda$performSelectedDialogsAction$32(this.f$1);
    }
}
