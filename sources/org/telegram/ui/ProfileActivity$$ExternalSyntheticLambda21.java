package org.telegram.ui;

import org.telegram.tgnet.TLObject;

public final /* synthetic */ class ProfileActivity$$ExternalSyntheticLambda21 implements Runnable {
    public final /* synthetic */ ProfileActivity f$0;
    public final /* synthetic */ TLObject f$1;

    public /* synthetic */ ProfileActivity$$ExternalSyntheticLambda21(ProfileActivity profileActivity, TLObject tLObject) {
        this.f$0 = profileActivity;
        this.f$1 = tLObject;
    }

    public final void run() {
        this.f$0.lambda$createView$8(this.f$1);
    }
}
