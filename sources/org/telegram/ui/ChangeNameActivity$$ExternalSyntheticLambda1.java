package org.telegram.ui;

import android.view.KeyEvent;
import android.widget.TextView;

public final /* synthetic */ class ChangeNameActivity$$ExternalSyntheticLambda1 implements TextView.OnEditorActionListener {
    public final /* synthetic */ ChangeNameActivity f$0;

    public /* synthetic */ ChangeNameActivity$$ExternalSyntheticLambda1(ChangeNameActivity changeNameActivity) {
        this.f$0 = changeNameActivity;
    }

    public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        return this.f$0.lambda$createView$1(textView, i, keyEvent);
    }
}
