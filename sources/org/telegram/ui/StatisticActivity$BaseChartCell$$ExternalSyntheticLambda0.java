package org.telegram.ui;

import android.animation.ValueAnimator;
import org.telegram.ui.StatisticActivity;

public final /* synthetic */ class StatisticActivity$BaseChartCell$$ExternalSyntheticLambda0 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ StatisticActivity.BaseChartCell f$0;

    public /* synthetic */ StatisticActivity$BaseChartCell$$ExternalSyntheticLambda0(StatisticActivity.BaseChartCell baseChartCell) {
        this.f$0 = baseChartCell;
    }

    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f$0.lambda$updateData$5(valueAnimator);
    }
}
