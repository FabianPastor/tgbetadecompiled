package org.telegram.ui;

import android.view.animation.Interpolator;

public final /* synthetic */ class DataUsageActivity$$ExternalSyntheticLambda1 implements Interpolator {
    public static final /* synthetic */ DataUsageActivity$$ExternalSyntheticLambda1 INSTANCE = new DataUsageActivity$$ExternalSyntheticLambda1();

    private /* synthetic */ DataUsageActivity$$ExternalSyntheticLambda1() {
    }

    public final float getInterpolation(float f) {
        return DataUsageActivity.lambda$static$0(f);
    }
}
