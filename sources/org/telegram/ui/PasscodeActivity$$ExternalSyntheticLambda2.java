package org.telegram.ui;

import android.content.DialogInterface;

public final /* synthetic */ class PasscodeActivity$$ExternalSyntheticLambda2 implements DialogInterface.OnClickListener {
    public final /* synthetic */ PasscodeActivity f$0;

    public /* synthetic */ PasscodeActivity$$ExternalSyntheticLambda2(PasscodeActivity passcodeActivity) {
        this.f$0 = passcodeActivity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f$0.lambda$createView$2(dialogInterface, i);
    }
}
