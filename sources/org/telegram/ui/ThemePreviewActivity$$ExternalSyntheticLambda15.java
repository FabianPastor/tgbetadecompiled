package org.telegram.ui;

import android.content.SharedPreferences;

public final /* synthetic */ class ThemePreviewActivity$$ExternalSyntheticLambda15 implements Runnable {
    public final /* synthetic */ ThemePreviewActivity f$0;
    public final /* synthetic */ SharedPreferences f$1;

    public /* synthetic */ ThemePreviewActivity$$ExternalSyntheticLambda15(ThemePreviewActivity themePreviewActivity, SharedPreferences sharedPreferences) {
        this.f$0 = themePreviewActivity;
        this.f$1 = sharedPreferences;
    }

    public final void run() {
        this.f$0.lambda$showAnimationHint$25(this.f$1);
    }
}
