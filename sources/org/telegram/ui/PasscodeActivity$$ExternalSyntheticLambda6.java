package org.telegram.ui;

import android.view.View;
import java.util.concurrent.atomic.AtomicBoolean;

public final /* synthetic */ class PasscodeActivity$$ExternalSyntheticLambda6 implements View.OnClickListener {
    public final /* synthetic */ PasscodeActivity f$0;
    public final /* synthetic */ AtomicBoolean f$1;

    public /* synthetic */ PasscodeActivity$$ExternalSyntheticLambda6(PasscodeActivity passcodeActivity, AtomicBoolean atomicBoolean) {
        this.f$0 = passcodeActivity;
        this.f$1 = atomicBoolean;
    }

    public final void onClick(View view) {
        this.f$0.lambda$createView$9(this.f$1, view);
    }
}
