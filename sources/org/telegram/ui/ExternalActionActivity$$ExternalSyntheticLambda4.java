package org.telegram.ui;

import android.view.MotionEvent;
import android.view.View;

public final /* synthetic */ class ExternalActionActivity$$ExternalSyntheticLambda4 implements View.OnTouchListener {
    public final /* synthetic */ ExternalActionActivity f$0;

    public /* synthetic */ ExternalActionActivity$$ExternalSyntheticLambda4(ExternalActionActivity externalActionActivity) {
        this.f$0 = externalActionActivity;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return this.f$0.lambda$onCreate$0(view, motionEvent);
    }
}
