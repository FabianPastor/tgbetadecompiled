package org.telegram.ui;

import android.view.View;
import android.view.WindowInsets;

public final /* synthetic */ class ContentPreviewViewer$$ExternalSyntheticLambda0 implements View.OnApplyWindowInsetsListener {
    public final /* synthetic */ ContentPreviewViewer f$0;

    public /* synthetic */ ContentPreviewViewer$$ExternalSyntheticLambda0(ContentPreviewViewer contentPreviewViewer) {
        this.f$0 = contentPreviewViewer;
    }

    public final WindowInsets onApplyWindowInsets(View view, WindowInsets windowInsets) {
        return this.f$0.lambda$setParentActivity$4(view, windowInsets);
    }
}
