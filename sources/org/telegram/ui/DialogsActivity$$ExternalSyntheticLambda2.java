package org.telegram.ui;

import android.animation.ValueAnimator;

public final /* synthetic */ class DialogsActivity$$ExternalSyntheticLambda2 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ DialogsActivity f$0;

    public /* synthetic */ DialogsActivity$$ExternalSyntheticLambda2(DialogsActivity dialogsActivity) {
        this.f$0 = dialogsActivity;
    }

    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f$0.lambda$animateContactsAlpha$51(valueAnimator);
    }
}
