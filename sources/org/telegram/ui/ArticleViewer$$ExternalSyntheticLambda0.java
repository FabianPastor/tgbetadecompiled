package org.telegram.ui;

import android.animation.ValueAnimator;

public final /* synthetic */ class ArticleViewer$$ExternalSyntheticLambda0 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ ArticleViewer f$0;

    public /* synthetic */ ArticleViewer$$ExternalSyntheticLambda0(ArticleViewer articleViewer) {
        this.f$0 = articleViewer;
    }

    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f$0.lambda$checkScrollAnimated$29(valueAnimator);
    }
}
