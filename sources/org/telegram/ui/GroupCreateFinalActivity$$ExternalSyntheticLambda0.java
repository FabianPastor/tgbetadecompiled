package org.telegram.ui;

import android.content.DialogInterface;

public final /* synthetic */ class GroupCreateFinalActivity$$ExternalSyntheticLambda0 implements DialogInterface.OnDismissListener {
    public final /* synthetic */ GroupCreateFinalActivity f$0;

    public /* synthetic */ GroupCreateFinalActivity$$ExternalSyntheticLambda0(GroupCreateFinalActivity groupCreateFinalActivity) {
        this.f$0 = groupCreateFinalActivity;
    }

    public final void onDismiss(DialogInterface dialogInterface) {
        this.f$0.lambda$createView$3(dialogInterface);
    }
}
