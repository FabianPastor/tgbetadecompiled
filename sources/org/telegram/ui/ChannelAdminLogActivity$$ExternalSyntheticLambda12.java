package org.telegram.ui;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class ChannelAdminLogActivity$$ExternalSyntheticLambda12 implements RecyclerListView.OnItemClickListener {
    public final /* synthetic */ ChannelAdminLogActivity f$0;

    public /* synthetic */ ChannelAdminLogActivity$$ExternalSyntheticLambda12(ChannelAdminLogActivity channelAdminLogActivity) {
        this.f$0 = channelAdminLogActivity;
    }

    public final void onItemClick(View view, int i) {
        this.f$0.lambda$createView$3(view, i);
    }
}
