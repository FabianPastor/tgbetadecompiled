package org.telegram.ui;

import android.view.View;

public final /* synthetic */ class ProfileActivity$$ExternalSyntheticLambda15 implements View.OnLongClickListener {
    public final /* synthetic */ ProfileActivity f$0;

    public /* synthetic */ ProfileActivity$$ExternalSyntheticLambda15(ProfileActivity profileActivity) {
        this.f$0 = profileActivity;
    }

    public final boolean onLongClick(View view) {
        return this.f$0.lambda$createView$12(view);
    }
}
