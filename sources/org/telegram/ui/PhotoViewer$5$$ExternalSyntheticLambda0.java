package org.telegram.ui;

import org.telegram.ui.PhotoViewer;

public final /* synthetic */ class PhotoViewer$5$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ PhotoViewer.AnonymousClass5 f$0;
    public final /* synthetic */ float f$1;

    public /* synthetic */ PhotoViewer$5$$ExternalSyntheticLambda0(PhotoViewer.AnonymousClass5 r1, float f) {
        this.f$0 = r1;
        this.f$1 = f;
    }

    public final void run() {
        this.f$0.lambda$run$0(this.f$1);
    }
}
