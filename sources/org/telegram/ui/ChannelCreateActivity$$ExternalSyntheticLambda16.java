package org.telegram.ui;

import org.telegram.tgnet.TLObject;

public final /* synthetic */ class ChannelCreateActivity$$ExternalSyntheticLambda16 implements Runnable {
    public final /* synthetic */ ChannelCreateActivity f$0;
    public final /* synthetic */ TLObject f$1;

    public /* synthetic */ ChannelCreateActivity$$ExternalSyntheticLambda16(ChannelCreateActivity channelCreateActivity, TLObject tLObject) {
        this.f$0 = channelCreateActivity;
        this.f$1 = tLObject;
    }

    public final void run() {
        this.f$0.lambda$loadAdminedChannels$19(this.f$1);
    }
}
