package org.telegram.ui;

import android.view.animation.Interpolator;

public final /* synthetic */ class DialogOrContactPickerActivity$$ExternalSyntheticLambda1 implements Interpolator {
    public static final /* synthetic */ DialogOrContactPickerActivity$$ExternalSyntheticLambda1 INSTANCE = new DialogOrContactPickerActivity$$ExternalSyntheticLambda1();

    private /* synthetic */ DialogOrContactPickerActivity$$ExternalSyntheticLambda1() {
    }

    public final float getInterpolation(float f) {
        return DialogOrContactPickerActivity.lambda$static$0(f);
    }
}
