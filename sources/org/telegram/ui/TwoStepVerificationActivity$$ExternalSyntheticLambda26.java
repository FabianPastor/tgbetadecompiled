package org.telegram.ui;

public final /* synthetic */ class TwoStepVerificationActivity$$ExternalSyntheticLambda26 implements Runnable {
    public final /* synthetic */ TwoStepVerificationActivity f$0;
    public final /* synthetic */ byte[] f$1;

    public /* synthetic */ TwoStepVerificationActivity$$ExternalSyntheticLambda26(TwoStepVerificationActivity twoStepVerificationActivity, byte[] bArr) {
        this.f$0 = twoStepVerificationActivity;
        this.f$1 = bArr;
    }

    public final void run() {
        this.f$0.lambda$processDone$35(this.f$1);
    }
}
