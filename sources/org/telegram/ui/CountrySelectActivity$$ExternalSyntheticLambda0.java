package org.telegram.ui;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class CountrySelectActivity$$ExternalSyntheticLambda0 implements RecyclerListView.OnItemClickListener {
    public final /* synthetic */ CountrySelectActivity f$0;

    public /* synthetic */ CountrySelectActivity$$ExternalSyntheticLambda0(CountrySelectActivity countrySelectActivity) {
        this.f$0 = countrySelectActivity;
    }

    public final void onItemClick(View view, int i) {
        this.f$0.lambda$createView$0(view, i);
    }
}
