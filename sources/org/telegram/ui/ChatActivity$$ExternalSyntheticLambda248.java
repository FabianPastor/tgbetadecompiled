package org.telegram.ui;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class ChatActivity$$ExternalSyntheticLambda248 implements RecyclerListView.OnItemClickListener {
    public final /* synthetic */ ChatActivity f$0;

    public /* synthetic */ ChatActivity$$ExternalSyntheticLambda248(ChatActivity chatActivity) {
        this.f$0 = chatActivity;
    }

    public final void onItemClick(View view, int i) {
        this.f$0.lambda$createView$46(view, i);
    }
}
