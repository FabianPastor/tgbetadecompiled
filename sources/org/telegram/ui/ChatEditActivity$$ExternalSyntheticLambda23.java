package org.telegram.ui;

import android.view.KeyEvent;
import android.widget.TextView;

public final /* synthetic */ class ChatEditActivity$$ExternalSyntheticLambda23 implements TextView.OnEditorActionListener {
    public final /* synthetic */ ChatEditActivity f$0;

    public /* synthetic */ ChatEditActivity$$ExternalSyntheticLambda23(ChatEditActivity chatEditActivity) {
        this.f$0 = chatEditActivity;
    }

    public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        return this.f$0.lambda$createView$7(textView, i, keyEvent);
    }
}
