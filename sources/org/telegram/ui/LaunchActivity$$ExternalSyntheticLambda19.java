package org.telegram.ui;

import android.view.MotionEvent;
import android.view.View;

public final /* synthetic */ class LaunchActivity$$ExternalSyntheticLambda19 implements View.OnTouchListener {
    public final /* synthetic */ LaunchActivity f$0;

    public /* synthetic */ LaunchActivity$$ExternalSyntheticLambda19(LaunchActivity launchActivity) {
        this.f$0 = launchActivity;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return this.f$0.lambda$onCreate$0(view, motionEvent);
    }
}
