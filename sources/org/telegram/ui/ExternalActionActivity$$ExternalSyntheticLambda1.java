package org.telegram.ui;

import android.content.DialogInterface;

public final /* synthetic */ class ExternalActionActivity$$ExternalSyntheticLambda1 implements DialogInterface.OnDismissListener {
    public final /* synthetic */ ExternalActionActivity f$0;

    public /* synthetic */ ExternalActionActivity$$ExternalSyntheticLambda1(ExternalActionActivity externalActionActivity) {
        this.f$0 = externalActionActivity;
    }

    public final void onDismiss(DialogInterface dialogInterface) {
        this.f$0.lambda$handleIntent$4(dialogInterface);
    }
}
