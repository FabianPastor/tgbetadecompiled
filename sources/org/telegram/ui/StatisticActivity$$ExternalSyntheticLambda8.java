package org.telegram.ui;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class StatisticActivity$$ExternalSyntheticLambda8 implements RecyclerListView.OnItemLongClickListener {
    public final /* synthetic */ StatisticActivity f$0;

    public /* synthetic */ StatisticActivity$$ExternalSyntheticLambda8(StatisticActivity statisticActivity) {
        this.f$0 = statisticActivity;
    }

    public final boolean onItemClick(View view, int i) {
        return this.f$0.lambda$createView$5(view, i);
    }
}
