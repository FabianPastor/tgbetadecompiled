package org.telegram.ui;

import org.telegram.ui.ActionBar.ThemeDescription;

public final /* synthetic */ class ProfileActivity$$ExternalSyntheticLambda36 implements ThemeDescription.ThemeDescriptionDelegate {
    public final /* synthetic */ ProfileActivity f$0;

    public /* synthetic */ ProfileActivity$$ExternalSyntheticLambda36(ProfileActivity profileActivity) {
        this.f$0 = profileActivity;
    }

    public final void didSetColor() {
        this.f$0.lambda$getThemeDescriptions$40();
    }

    public /* synthetic */ void onAnimationProgress(float f) {
        ThemeDescription.ThemeDescriptionDelegate.CC.$default$onAnimationProgress(this, f);
    }
}
