package org.telegram.ui;

import android.view.MotionEvent;
import android.view.View;

public final /* synthetic */ class ChangeNameActivity$$ExternalSyntheticLambda0 implements View.OnTouchListener {
    public static final /* synthetic */ ChangeNameActivity$$ExternalSyntheticLambda0 INSTANCE = new ChangeNameActivity$$ExternalSyntheticLambda0();

    private /* synthetic */ ChangeNameActivity$$ExternalSyntheticLambda0() {
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return ChangeNameActivity.lambda$createView$0(view, motionEvent);
    }
}
