package org.telegram.ui;

import android.view.animation.Interpolator;

public final /* synthetic */ class PhotoPickerSearchActivity$$ExternalSyntheticLambda0 implements Interpolator {
    public static final /* synthetic */ PhotoPickerSearchActivity$$ExternalSyntheticLambda0 INSTANCE = new PhotoPickerSearchActivity$$ExternalSyntheticLambda0();

    private /* synthetic */ PhotoPickerSearchActivity$$ExternalSyntheticLambda0() {
    }

    public final float getInterpolation(float f) {
        return PhotoPickerSearchActivity.lambda$static$0(f);
    }
}
