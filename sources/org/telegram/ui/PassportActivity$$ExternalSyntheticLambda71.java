package org.telegram.ui;

import org.telegram.ui.CountrySelectActivity;

public final /* synthetic */ class PassportActivity$$ExternalSyntheticLambda71 implements CountrySelectActivity.CountrySelectActivityDelegate {
    public final /* synthetic */ PassportActivity f$0;

    public /* synthetic */ PassportActivity$$ExternalSyntheticLambda71(PassportActivity passportActivity) {
        this.f$0 = passportActivity;
    }

    public final void didSelectCountry(CountrySelectActivity.Country country) {
        this.f$0.lambda$createPhoneInterface$28(country);
    }
}
