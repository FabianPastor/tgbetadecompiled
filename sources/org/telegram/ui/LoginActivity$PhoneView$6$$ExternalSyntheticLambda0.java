package org.telegram.ui;

import org.telegram.ui.LoginActivity;

public final /* synthetic */ class LoginActivity$PhoneView$6$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ LoginActivity.PhoneView.AnonymousClass6 f$0;
    public final /* synthetic */ String f$1;
    public final /* synthetic */ LoginActivity.PhoneNumberConfirmView f$2;

    public /* synthetic */ LoginActivity$PhoneView$6$$ExternalSyntheticLambda0(LoginActivity.PhoneView.AnonymousClass6 r1, String str, LoginActivity.PhoneNumberConfirmView phoneNumberConfirmView) {
        this.f$0 = r1;
        this.f$1 = str;
        this.f$2 = phoneNumberConfirmView;
    }

    public final void run() {
        this.f$0.lambda$onConfirm$0(this.f$1, this.f$2);
    }
}
