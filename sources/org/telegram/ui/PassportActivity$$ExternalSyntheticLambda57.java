package org.telegram.ui;

import org.telegram.tgnet.TLRPC$TL_error;

public final /* synthetic */ class PassportActivity$$ExternalSyntheticLambda57 implements Runnable {
    public final /* synthetic */ PassportActivity f$0;
    public final /* synthetic */ TLRPC$TL_error f$1;

    public /* synthetic */ PassportActivity$$ExternalSyntheticLambda57(PassportActivity passportActivity, TLRPC$TL_error tLRPC$TL_error) {
        this.f$0 = passportActivity;
        this.f$1 = tLRPC$TL_error;
    }

    public final void run() {
        this.f$0.lambda$createRequestInterface$14(this.f$1);
    }
}
