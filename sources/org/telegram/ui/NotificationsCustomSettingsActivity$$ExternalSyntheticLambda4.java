package org.telegram.ui;

public final /* synthetic */ class NotificationsCustomSettingsActivity$$ExternalSyntheticLambda4 implements Runnable {
    public final /* synthetic */ NotificationsCustomSettingsActivity f$0;
    public final /* synthetic */ int f$1;

    public /* synthetic */ NotificationsCustomSettingsActivity$$ExternalSyntheticLambda4(NotificationsCustomSettingsActivity notificationsCustomSettingsActivity, int i) {
        this.f$0 = notificationsCustomSettingsActivity;
        this.f$1 = i;
    }

    public final void run() {
        this.f$0.lambda$createView$4(this.f$1);
    }
}
