package org.telegram.ui;

import android.view.KeyEvent;
import org.telegram.ui.ActionBar.ActionBarPopupWindow;

public final /* synthetic */ class PhotoAlbumPickerActivity$$ExternalSyntheticLambda5 implements ActionBarPopupWindow.OnDispatchKeyEventListener {
    public final /* synthetic */ PhotoAlbumPickerActivity f$0;

    public /* synthetic */ PhotoAlbumPickerActivity$$ExternalSyntheticLambda5(PhotoAlbumPickerActivity photoAlbumPickerActivity) {
        this.f$0 = photoAlbumPickerActivity;
    }

    public final void onDispatchKeyEvent(KeyEvent keyEvent) {
        this.f$0.lambda$createView$4(keyEvent);
    }
}
