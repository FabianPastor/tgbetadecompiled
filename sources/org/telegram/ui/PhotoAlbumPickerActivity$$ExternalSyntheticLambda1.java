package org.telegram.ui;

import android.view.View;

public final /* synthetic */ class PhotoAlbumPickerActivity$$ExternalSyntheticLambda1 implements View.OnClickListener {
    public final /* synthetic */ PhotoAlbumPickerActivity f$0;
    public final /* synthetic */ int f$1;

    public /* synthetic */ PhotoAlbumPickerActivity$$ExternalSyntheticLambda1(PhotoAlbumPickerActivity photoAlbumPickerActivity, int i) {
        this.f$0 = photoAlbumPickerActivity;
        this.f$1 = i;
    }

    public final void onClick(View view) {
        this.f$0.lambda$createView$6(this.f$1, view);
    }
}
