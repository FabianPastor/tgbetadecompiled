package org.telegram.ui;

import org.telegram.ui.TooManyCommunitiesActivity;

public final /* synthetic */ class TooManyCommunitiesActivity$SearchAdapter$$ExternalSyntheticLambda1 implements Runnable {
    public final /* synthetic */ TooManyCommunitiesActivity.SearchAdapter f$0;
    public final /* synthetic */ String f$1;
    public final /* synthetic */ int f$2;

    public /* synthetic */ TooManyCommunitiesActivity$SearchAdapter$$ExternalSyntheticLambda1(TooManyCommunitiesActivity.SearchAdapter searchAdapter, String str, int i) {
        this.f$0 = searchAdapter;
        this.f$1 = str;
        this.f$2 = i;
    }

    public final void run() {
        this.f$0.lambda$search$0(this.f$1, this.f$2);
    }
}
