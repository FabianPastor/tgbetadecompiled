package org.telegram.ui;

import android.content.Context;
import android.view.View;

public final /* synthetic */ class ChatActivity$$ExternalSyntheticLambda86 implements View.OnClickListener {
    public final /* synthetic */ ChatActivity f$0;
    public final /* synthetic */ Context f$1;

    public /* synthetic */ ChatActivity$$ExternalSyntheticLambda86(ChatActivity chatActivity, Context context) {
        this.f$0 = chatActivity;
        this.f$1 = context;
    }

    public final void onClick(View view) {
        this.f$0.lambda$createView$72(this.f$1, view);
    }
}
