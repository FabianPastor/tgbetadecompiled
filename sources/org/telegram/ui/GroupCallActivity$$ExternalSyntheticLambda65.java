package org.telegram.ui;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class GroupCallActivity$$ExternalSyntheticLambda65 implements RecyclerListView.OnItemLongClickListener {
    public final /* synthetic */ GroupCallActivity f$0;

    public /* synthetic */ GroupCallActivity$$ExternalSyntheticLambda65(GroupCallActivity groupCallActivity) {
        this.f$0 = groupCallActivity;
    }

    public final boolean onItemClick(View view, int i) {
        return this.f$0.lambda$new$24(view, i);
    }
}
