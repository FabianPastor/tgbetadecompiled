package org.telegram.ui;

import android.content.DialogInterface;

public final /* synthetic */ class CacheControlActivity$$ExternalSyntheticLambda0 implements DialogInterface.OnClickListener {
    public final /* synthetic */ CacheControlActivity f$0;

    public /* synthetic */ CacheControlActivity$$ExternalSyntheticLambda0(CacheControlActivity cacheControlActivity) {
        this.f$0 = cacheControlActivity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f$0.lambda$clearDatabase$7(dialogInterface, i);
    }
}
