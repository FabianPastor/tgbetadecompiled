package org.telegram.ui;

import android.content.DialogInterface;
import org.telegram.ui.ProfileActivity;

public final /* synthetic */ class ProfileActivity$13$$ExternalSyntheticLambda0 implements DialogInterface.OnClickListener {
    public final /* synthetic */ ProfileActivity.AnonymousClass13 f$0;

    public /* synthetic */ ProfileActivity$13$$ExternalSyntheticLambda0(ProfileActivity.AnonymousClass13 r1) {
        this.f$0 = r1;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f$0.lambda$onItemClick$0(dialogInterface, i);
    }
}
