package org.telegram.ui;

import org.telegram.tgnet.TLObject;

public final /* synthetic */ class ChatLinkActivity$$ExternalSyntheticLambda8 implements Runnable {
    public final /* synthetic */ ChatLinkActivity f$0;
    public final /* synthetic */ TLObject f$1;

    public /* synthetic */ ChatLinkActivity$$ExternalSyntheticLambda8(ChatLinkActivity chatLinkActivity, TLObject tLObject) {
        this.f$0 = chatLinkActivity;
        this.f$1 = tLObject;
    }

    public final void run() {
        this.f$0.lambda$loadChats$16(this.f$1);
    }
}
