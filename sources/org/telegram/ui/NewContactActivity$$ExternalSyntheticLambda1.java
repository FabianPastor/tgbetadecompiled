package org.telegram.ui;

import android.view.KeyEvent;
import android.view.View;

public final /* synthetic */ class NewContactActivity$$ExternalSyntheticLambda1 implements View.OnKeyListener {
    public final /* synthetic */ NewContactActivity f$0;

    public /* synthetic */ NewContactActivity$$ExternalSyntheticLambda1(NewContactActivity newContactActivity) {
        this.f$0 = newContactActivity;
    }

    public final boolean onKey(View view, int i, KeyEvent keyEvent) {
        return this.f$0.lambda$createView$7(view, i, keyEvent);
    }
}
