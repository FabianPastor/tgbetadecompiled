package org.telegram.ui;

import android.content.DialogInterface;
import org.telegram.ui.ChannelAdminLogActivity;

public final /* synthetic */ class ChannelAdminLogActivity$ChatActivityAdapter$3$$ExternalSyntheticLambda0 implements DialogInterface.OnCancelListener {
    public final /* synthetic */ ChannelAdminLogActivity.ChatActivityAdapter.AnonymousClass3 f$0;
    public final /* synthetic */ boolean[] f$1;

    public /* synthetic */ ChannelAdminLogActivity$ChatActivityAdapter$3$$ExternalSyntheticLambda0(ChannelAdminLogActivity.ChatActivityAdapter.AnonymousClass3 r1, boolean[] zArr) {
        this.f$0 = r1;
        this.f$1 = zArr;
    }

    public final void onCancel(DialogInterface dialogInterface) {
        this.f$0.lambda$needOpenInviteLink$0(this.f$1, dialogInterface);
    }
}
