package org.telegram.ui;

import android.content.DialogInterface;

public final /* synthetic */ class ProfileActivity$$ExternalSyntheticLambda4 implements DialogInterface.OnClickListener {
    public final /* synthetic */ ProfileActivity f$0;

    public /* synthetic */ ProfileActivity$$ExternalSyntheticLambda4(ProfileActivity profileActivity) {
        this.f$0 = profileActivity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f$0.lambda$createView$3(dialogInterface, i);
    }
}
