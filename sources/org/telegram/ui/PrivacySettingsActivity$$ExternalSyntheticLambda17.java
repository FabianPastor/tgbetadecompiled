package org.telegram.ui;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class PrivacySettingsActivity$$ExternalSyntheticLambda17 implements RecyclerListView.OnItemClickListener {
    public final /* synthetic */ PrivacySettingsActivity f$0;

    public /* synthetic */ PrivacySettingsActivity$$ExternalSyntheticLambda17(PrivacySettingsActivity privacySettingsActivity) {
        this.f$0 = privacySettingsActivity;
    }

    public final void onItemClick(View view, int i) {
        this.f$0.lambda$createView$15(view, i);
    }
}
