package org.telegram.ui;

import org.telegram.ui.PinchToZoomHelper;

public final /* synthetic */ class ChatActivity$$ExternalSyntheticLambda256 implements PinchToZoomHelper.ClipBoundsListener {
    public final /* synthetic */ ChatActivity f$0;

    public /* synthetic */ ChatActivity$$ExternalSyntheticLambda256(ChatActivity chatActivity) {
        this.f$0 = chatActivity;
    }

    public final void getClipTopBottom(float[] fArr) {
        this.f$0.lambda$createView$76(fArr);
    }
}
