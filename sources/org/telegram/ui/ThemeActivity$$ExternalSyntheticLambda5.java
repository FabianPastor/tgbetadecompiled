package org.telegram.ui;

import android.content.DialogInterface;

public final /* synthetic */ class ThemeActivity$$ExternalSyntheticLambda5 implements DialogInterface.OnDismissListener {
    public final /* synthetic */ ThemeActivity f$0;

    public /* synthetic */ ThemeActivity$$ExternalSyntheticLambda5(ThemeActivity themeActivity) {
        this.f$0 = themeActivity;
    }

    public final void onDismiss(DialogInterface dialogInterface) {
        this.f$0.lambda$didReceivedNotification$1(dialogInterface);
    }
}
