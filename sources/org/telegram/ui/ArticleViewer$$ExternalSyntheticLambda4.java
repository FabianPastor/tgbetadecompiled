package org.telegram.ui;

import android.view.View;
import android.view.WindowInsets;

public final /* synthetic */ class ArticleViewer$$ExternalSyntheticLambda4 implements View.OnApplyWindowInsetsListener {
    public static final /* synthetic */ ArticleViewer$$ExternalSyntheticLambda4 INSTANCE = new ArticleViewer$$ExternalSyntheticLambda4();

    private /* synthetic */ ArticleViewer$$ExternalSyntheticLambda4() {
    }

    public final WindowInsets onApplyWindowInsets(View view, WindowInsets windowInsets) {
        return ArticleViewer.lambda$setParentActivity$8(view, windowInsets);
    }
}
