package org.telegram.ui;

import android.content.DialogInterface;
import org.telegram.ui.Components.EditTextBoldCursor;

public final /* synthetic */ class PassportActivity$$ExternalSyntheticLambda10 implements DialogInterface.OnClickListener {
    public final /* synthetic */ PassportActivity f$0;
    public final /* synthetic */ EditTextBoldCursor f$1;

    public /* synthetic */ PassportActivity$$ExternalSyntheticLambda10(PassportActivity passportActivity, EditTextBoldCursor editTextBoldCursor) {
        this.f$0 = passportActivity;
        this.f$1 = editTextBoldCursor;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f$0.lambda$createIdentityInterface$48(this.f$1, dialogInterface, i);
    }
}
