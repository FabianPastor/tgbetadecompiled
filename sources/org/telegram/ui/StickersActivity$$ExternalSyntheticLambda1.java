package org.telegram.ui;

import android.view.MotionEvent;
import android.view.View;

public final /* synthetic */ class StickersActivity$$ExternalSyntheticLambda1 implements View.OnTouchListener {
    public static final /* synthetic */ StickersActivity$$ExternalSyntheticLambda1 INSTANCE = new StickersActivity$$ExternalSyntheticLambda1();

    private /* synthetic */ StickersActivity$$ExternalSyntheticLambda1() {
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return StickersActivity.lambda$createView$0(view, motionEvent);
    }
}
