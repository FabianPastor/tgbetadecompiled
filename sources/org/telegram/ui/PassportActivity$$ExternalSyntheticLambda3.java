package org.telegram.ui;

import android.content.DialogInterface;

public final /* synthetic */ class PassportActivity$$ExternalSyntheticLambda3 implements DialogInterface.OnClickListener {
    public final /* synthetic */ PassportActivity f$0;

    public /* synthetic */ PassportActivity$$ExternalSyntheticLambda3(PassportActivity passportActivity) {
        this.f$0 = passportActivity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f$0.lambda$createIdentityInterface$50(dialogInterface, i);
    }
}
