package org.telegram.ui;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class GroupCallActivity$$ExternalSyntheticLambda64 implements RecyclerListView.OnItemLongClickListener {
    public final /* synthetic */ GroupCallActivity f$0;

    public /* synthetic */ GroupCallActivity$$ExternalSyntheticLambda64(GroupCallActivity groupCallActivity) {
        this.f$0 = groupCallActivity;
    }

    public final boolean onItemClick(View view, int i) {
        return this.f$0.lambda$new$13(view, i);
    }
}
