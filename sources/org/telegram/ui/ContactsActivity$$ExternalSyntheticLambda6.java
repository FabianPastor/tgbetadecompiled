package org.telegram.ui;

import org.telegram.messenger.MessagesStorage;

public final /* synthetic */ class ContactsActivity$$ExternalSyntheticLambda6 implements MessagesStorage.IntCallback {
    public final /* synthetic */ ContactsActivity f$0;

    public /* synthetic */ ContactsActivity$$ExternalSyntheticLambda6(ContactsActivity contactsActivity) {
        this.f$0 = contactsActivity;
    }

    public final void run(int i) {
        this.f$0.lambda$askForPermissons$6(i);
    }
}
