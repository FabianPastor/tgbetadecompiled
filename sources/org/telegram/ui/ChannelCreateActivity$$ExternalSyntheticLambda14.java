package org.telegram.ui;

public final /* synthetic */ class ChannelCreateActivity$$ExternalSyntheticLambda14 implements Runnable {
    public final /* synthetic */ ChannelCreateActivity f$0;
    public final /* synthetic */ String f$1;

    public /* synthetic */ ChannelCreateActivity$$ExternalSyntheticLambda14(ChannelCreateActivity channelCreateActivity, String str) {
        this.f$0 = channelCreateActivity;
        this.f$1 = str;
    }

    public final void run() {
        this.f$0.lambda$checkUserName$23(this.f$1);
    }
}
