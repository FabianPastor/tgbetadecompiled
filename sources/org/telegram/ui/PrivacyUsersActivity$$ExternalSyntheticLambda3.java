package org.telegram.ui;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class PrivacyUsersActivity$$ExternalSyntheticLambda3 implements RecyclerListView.OnItemLongClickListener {
    public final /* synthetic */ PrivacyUsersActivity f$0;

    public /* synthetic */ PrivacyUsersActivity$$ExternalSyntheticLambda3(PrivacyUsersActivity privacyUsersActivity) {
        this.f$0 = privacyUsersActivity;
    }

    public final boolean onItemClick(View view, int i) {
        return this.f$0.lambda$createView$2(view, i);
    }
}
