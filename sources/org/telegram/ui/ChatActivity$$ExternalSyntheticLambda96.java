package org.telegram.ui;

import android.view.View;

public final /* synthetic */ class ChatActivity$$ExternalSyntheticLambda96 implements View.OnClickListener {
    public final /* synthetic */ boolean[] f$0;
    public final /* synthetic */ Runnable f$1;

    public /* synthetic */ ChatActivity$$ExternalSyntheticLambda96(boolean[] zArr, Runnable runnable) {
        this.f$0 = zArr;
        this.f$1 = runnable;
    }

    public final void onClick(View view) {
        ChatActivity.lambda$createMenu$170(this.f$0, this.f$1, view);
    }
}
