package org.telegram.ui;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class FilteredSearchView$$ExternalSyntheticLambda4 implements RecyclerListView.OnItemClickListener {
    public final /* synthetic */ FilteredSearchView f$0;

    public /* synthetic */ FilteredSearchView$$ExternalSyntheticLambda4(FilteredSearchView filteredSearchView) {
        this.f$0 = filteredSearchView;
    }

    public final void onItemClick(View view, int i) {
        this.f$0.lambda$new$1(view, i);
    }
}
