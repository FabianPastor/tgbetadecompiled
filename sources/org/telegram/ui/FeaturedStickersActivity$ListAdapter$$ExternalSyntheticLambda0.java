package org.telegram.ui;

import android.view.View;
import org.telegram.ui.FeaturedStickersActivity;

public final /* synthetic */ class FeaturedStickersActivity$ListAdapter$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ FeaturedStickersActivity.ListAdapter f$0;

    public /* synthetic */ FeaturedStickersActivity$ListAdapter$$ExternalSyntheticLambda0(FeaturedStickersActivity.ListAdapter listAdapter) {
        this.f$0 = listAdapter;
    }

    public final void onClick(View view) {
        this.f$0.lambda$onCreateViewHolder$0(view);
    }
}
