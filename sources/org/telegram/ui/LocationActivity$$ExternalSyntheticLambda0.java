package org.telegram.ui;

import android.content.DialogInterface;

public final /* synthetic */ class LocationActivity$$ExternalSyntheticLambda0 implements DialogInterface.OnCancelListener {
    public final /* synthetic */ LocationActivity f$0;
    public final /* synthetic */ int f$1;

    public /* synthetic */ LocationActivity$$ExternalSyntheticLambda0(LocationActivity locationActivity, int i) {
        this.f$0 = locationActivity;
        this.f$1 = i;
    }

    public final void onCancel(DialogInterface dialogInterface) {
        this.f$0.lambda$createView$11(this.f$1, dialogInterface);
    }
}
