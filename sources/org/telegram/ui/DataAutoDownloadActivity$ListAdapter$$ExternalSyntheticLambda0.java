package org.telegram.ui;

import org.telegram.ui.Components.SlideChooseView;
import org.telegram.ui.DataAutoDownloadActivity;

public final /* synthetic */ class DataAutoDownloadActivity$ListAdapter$$ExternalSyntheticLambda0 implements SlideChooseView.Callback {
    public final /* synthetic */ DataAutoDownloadActivity.ListAdapter f$0;

    public /* synthetic */ DataAutoDownloadActivity$ListAdapter$$ExternalSyntheticLambda0(DataAutoDownloadActivity.ListAdapter listAdapter) {
        this.f$0 = listAdapter;
    }

    public final void onOptionSelected(int i) {
        this.f$0.lambda$onCreateViewHolder$0(i);
    }

    public /* synthetic */ void onTouchEnd() {
        SlideChooseView.Callback.CC.$default$onTouchEnd(this);
    }
}
