package org.telegram.ui;

import android.content.DialogInterface;

public final /* synthetic */ class LaunchActivity$$ExternalSyntheticLambda4 implements DialogInterface.OnClickListener {
    public final /* synthetic */ int f$0;

    public /* synthetic */ LaunchActivity$$ExternalSyntheticLambda4(int i) {
        this.f$0 = i;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        LaunchActivity.lambda$didReceivedNotification$81(this.f$0, dialogInterface, i);
    }
}
