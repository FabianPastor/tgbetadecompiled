package org.telegram.ui;

import android.content.DialogInterface;

public final /* synthetic */ class DataSettingsActivity$$ExternalSyntheticLambda0 implements DialogInterface.OnClickListener {
    public final /* synthetic */ DataSettingsActivity f$0;

    public /* synthetic */ DataSettingsActivity$$ExternalSyntheticLambda0(DataSettingsActivity dataSettingsActivity) {
        this.f$0 = dataSettingsActivity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f$0.lambda$createView$0(dialogInterface, i);
    }
}
