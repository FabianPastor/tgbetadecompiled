package org.telegram.ui;

import android.content.DialogInterface;

public final /* synthetic */ class ThemeActivity$$ExternalSyntheticLambda3 implements DialogInterface.OnClickListener {
    public final /* synthetic */ ThemeActivity f$0;

    public /* synthetic */ ThemeActivity$$ExternalSyntheticLambda3(ThemeActivity themeActivity) {
        this.f$0 = themeActivity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f$0.lambda$createNewTheme$6(dialogInterface, i);
    }
}
