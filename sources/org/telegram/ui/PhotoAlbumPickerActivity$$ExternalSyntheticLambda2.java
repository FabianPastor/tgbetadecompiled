package org.telegram.ui;

import android.view.View;

public final /* synthetic */ class PhotoAlbumPickerActivity$$ExternalSyntheticLambda2 implements View.OnLongClickListener {
    public final /* synthetic */ PhotoAlbumPickerActivity f$0;

    public /* synthetic */ PhotoAlbumPickerActivity$$ExternalSyntheticLambda2(PhotoAlbumPickerActivity photoAlbumPickerActivity) {
        this.f$0 = photoAlbumPickerActivity;
    }

    public final boolean onLongClick(View view) {
        return this.f$0.lambda$createView$7(view);
    }
}
