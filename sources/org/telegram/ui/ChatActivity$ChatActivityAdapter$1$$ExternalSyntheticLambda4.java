package org.telegram.ui;

import org.telegram.ui.ChatActivity;
import org.telegram.ui.Components.ReactedUsersListView;

public final /* synthetic */ class ChatActivity$ChatActivityAdapter$1$$ExternalSyntheticLambda4 implements ReactedUsersListView.OnProfileSelectedListener {
    public final /* synthetic */ ChatActivity.ChatActivityAdapter.AnonymousClass1 f$0;

    public /* synthetic */ ChatActivity$ChatActivityAdapter$1$$ExternalSyntheticLambda4(ChatActivity.ChatActivityAdapter.AnonymousClass1 r1) {
        this.f$0 = r1;
    }

    public final void onProfileSelected(ReactedUsersListView reactedUsersListView, long j) {
        this.f$0.lambda$didPressReaction$3(reactedUsersListView, j);
    }
}
