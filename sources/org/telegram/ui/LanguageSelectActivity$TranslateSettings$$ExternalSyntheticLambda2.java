package org.telegram.ui;

import android.view.View;
import org.telegram.ui.LanguageSelectActivity;

public final /* synthetic */ class LanguageSelectActivity$TranslateSettings$$ExternalSyntheticLambda2 implements View.OnClickListener {
    public final /* synthetic */ LanguageSelectActivity.TranslateSettings f$0;

    public /* synthetic */ LanguageSelectActivity$TranslateSettings$$ExternalSyntheticLambda2(LanguageSelectActivity.TranslateSettings translateSettings) {
        this.f$0 = translateSettings;
    }

    public final void onClick(View view) {
        this.f$0.lambda$new$1(view);
    }
}
