package org.telegram.ui;

import android.content.DialogInterface;

public final /* synthetic */ class PollCreateActivity$$ExternalSyntheticLambda0 implements DialogInterface.OnClickListener {
    public final /* synthetic */ PollCreateActivity f$0;

    public /* synthetic */ PollCreateActivity$$ExternalSyntheticLambda0(PollCreateActivity pollCreateActivity) {
        this.f$0 = pollCreateActivity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f$0.lambda$checkDiscard$1(dialogInterface, i);
    }
}
