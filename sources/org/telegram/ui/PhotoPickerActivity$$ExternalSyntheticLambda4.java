package org.telegram.ui;

import android.view.MotionEvent;
import android.view.View;

public final /* synthetic */ class PhotoPickerActivity$$ExternalSyntheticLambda4 implements View.OnTouchListener {
    public static final /* synthetic */ PhotoPickerActivity$$ExternalSyntheticLambda4 INSTANCE = new PhotoPickerActivity$$ExternalSyntheticLambda4();

    private /* synthetic */ PhotoPickerActivity$$ExternalSyntheticLambda4() {
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return PhotoPickerActivity.lambda$createView$3(view, motionEvent);
    }
}
