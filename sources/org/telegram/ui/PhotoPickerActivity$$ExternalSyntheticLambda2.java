package org.telegram.ui;

import android.view.View;

public final /* synthetic */ class PhotoPickerActivity$$ExternalSyntheticLambda2 implements View.OnClickListener {
    public final /* synthetic */ PhotoPickerActivity f$0;
    public final /* synthetic */ int f$1;

    public /* synthetic */ PhotoPickerActivity$$ExternalSyntheticLambda2(PhotoPickerActivity photoPickerActivity, int i) {
        this.f$0 = photoPickerActivity;
        this.f$1 = i;
    }

    public final void onClick(View view) {
        this.f$0.lambda$createView$6(this.f$1, view);
    }
}
