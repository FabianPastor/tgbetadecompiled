package org.telegram.ui;

import java.util.ArrayList;
import org.telegram.ui.ProfileActivity;

public final /* synthetic */ class ProfileActivity$SearchAdapter$$ExternalSyntheticLambda85 implements Runnable {
    public final /* synthetic */ ProfileActivity.SearchAdapter f$0;
    public final /* synthetic */ ArrayList f$1;

    public /* synthetic */ ProfileActivity$SearchAdapter$$ExternalSyntheticLambda85(ProfileActivity.SearchAdapter searchAdapter, ArrayList arrayList) {
        this.f$0 = searchAdapter;
        this.f$1 = arrayList;
    }

    public final void run() {
        this.f$0.lambda$loadFaqWebPage$84(this.f$1);
    }
}
