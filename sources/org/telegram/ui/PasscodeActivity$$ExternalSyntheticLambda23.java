package org.telegram.ui;

import org.telegram.ui.Components.SizeNotifierFrameLayout;

public final /* synthetic */ class PasscodeActivity$$ExternalSyntheticLambda23 implements SizeNotifierFrameLayout.SizeNotifierFrameLayoutDelegate {
    public final /* synthetic */ PasscodeActivity f$0;

    public /* synthetic */ PasscodeActivity$$ExternalSyntheticLambda23(PasscodeActivity passcodeActivity) {
        this.f$0 = passcodeActivity;
    }

    public final void onSizeChanged(int i, boolean z) {
        this.f$0.lambda$createView$1(i, z);
    }
}
