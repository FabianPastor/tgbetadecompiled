package org.telegram.ui;

import org.telegram.tgnet.TLObject;
import org.telegram.tgnet.TLRPC$TL_error;
import org.telegram.ui.ActionBar.AlertDialog;

public final /* synthetic */ class LaunchActivity$$ExternalSyntheticLambda45 implements Runnable {
    public final /* synthetic */ LaunchActivity f$0;
    public final /* synthetic */ TLObject f$1;
    public final /* synthetic */ Integer f$10;
    public final /* synthetic */ Integer f$11;
    public final /* synthetic */ Integer f$12;
    public final /* synthetic */ int[] f$13;
    public final /* synthetic */ AlertDialog f$14;
    public final /* synthetic */ String f$15;
    public final /* synthetic */ String f$16;
    public final /* synthetic */ String f$17;
    public final /* synthetic */ String f$18;
    public final /* synthetic */ int f$19;
    public final /* synthetic */ TLRPC$TL_error f$2;
    public final /* synthetic */ String f$20;
    public final /* synthetic */ String f$3;
    public final /* synthetic */ String f$4;
    public final /* synthetic */ String f$5;
    public final /* synthetic */ int f$6;
    public final /* synthetic */ String f$7;
    public final /* synthetic */ String f$8;
    public final /* synthetic */ String f$9;

    public /* synthetic */ LaunchActivity$$ExternalSyntheticLambda45(LaunchActivity launchActivity, TLObject tLObject, TLRPC$TL_error tLRPC$TL_error, String str, String str2, String str3, int i, String str4, String str5, String str6, Integer num, Integer num2, Integer num3, int[] iArr, AlertDialog alertDialog, String str7, String str8, String str9, String str10, int i2, String str11) {
        this.f$0 = launchActivity;
        this.f$1 = tLObject;
        this.f$2 = tLRPC$TL_error;
        this.f$3 = str;
        this.f$4 = str2;
        this.f$5 = str3;
        this.f$6 = i;
        this.f$7 = str4;
        this.f$8 = str5;
        this.f$9 = str6;
        this.f$10 = num;
        this.f$11 = num2;
        this.f$12 = num3;
        this.f$13 = iArr;
        this.f$14 = alertDialog;
        this.f$15 = str7;
        this.f$16 = str8;
        this.f$17 = str9;
        this.f$18 = str10;
        this.f$19 = i2;
        this.f$20 = str11;
    }

    public final void run() {
        LaunchActivity launchActivity = this.f$0;
        LaunchActivity launchActivity2 = launchActivity;
        launchActivity2.lambda$runLinkRequest$43(this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, this.f$6, this.f$7, this.f$8, this.f$9, this.f$10, this.f$11, this.f$12, this.f$13, this.f$14, this.f$15, this.f$16, this.f$17, this.f$18, this.f$19, this.f$20);
    }
}
