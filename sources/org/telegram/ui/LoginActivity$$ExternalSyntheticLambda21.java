package org.telegram.ui;

import org.telegram.ui.Components.SizeNotifierFrameLayout;

public final /* synthetic */ class LoginActivity$$ExternalSyntheticLambda21 implements SizeNotifierFrameLayout.SizeNotifierFrameLayoutDelegate {
    public final /* synthetic */ LoginActivity f$0;

    public /* synthetic */ LoginActivity$$ExternalSyntheticLambda21(LoginActivity loginActivity) {
        this.f$0 = loginActivity;
    }

    public final void onSizeChanged(int i, boolean z) {
        this.f$0.lambda$createView$0(i, z);
    }
}
