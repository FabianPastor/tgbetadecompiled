package org.telegram.ui;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class ChatActivity$$ExternalSyntheticLambda251 implements RecyclerListView.OnItemLongClickListener {
    public final /* synthetic */ ChatActivity f$0;

    public /* synthetic */ ChatActivity$$ExternalSyntheticLambda251(ChatActivity chatActivity) {
        this.f$0 = chatActivity;
    }

    public final boolean onItemClick(View view, int i) {
        return this.f$0.lambda$createView$48(view, i);
    }
}
