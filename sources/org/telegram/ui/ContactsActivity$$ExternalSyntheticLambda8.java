package org.telegram.ui;

import org.telegram.ui.ActionBar.ThemeDescription;

public final /* synthetic */ class ContactsActivity$$ExternalSyntheticLambda8 implements ThemeDescription.ThemeDescriptionDelegate {
    public final /* synthetic */ ContactsActivity f$0;

    public /* synthetic */ ContactsActivity$$ExternalSyntheticLambda8(ContactsActivity contactsActivity) {
        this.f$0 = contactsActivity;
    }

    public final void didSetColor() {
        this.f$0.lambda$getThemeDescriptions$9();
    }

    public /* synthetic */ void onAnimationProgress(float f) {
        ThemeDescription.ThemeDescriptionDelegate.CC.$default$onAnimationProgress(this, f);
    }
}
