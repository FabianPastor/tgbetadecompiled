package org.telegram.ui;

import android.animation.Animator;

public final /* synthetic */ class VoIPFragment$$ExternalSyntheticLambda28 implements Runnable {
    public final /* synthetic */ VoIPFragment f$0;
    public final /* synthetic */ Animator f$1;

    public /* synthetic */ VoIPFragment$$ExternalSyntheticLambda28(VoIPFragment voIPFragment, Animator animator) {
        this.f$0 = voIPFragment;
        this.f$1 = animator;
    }

    public final void run() {
        this.f$0.lambda$startTransitionFromPiP$13(this.f$1);
    }
}
