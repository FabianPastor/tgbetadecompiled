package org.telegram.ui;

import android.content.DialogInterface;

public final /* synthetic */ class ChatActivity$$ExternalSyntheticLambda55 implements DialogInterface.OnShowListener {
    public final /* synthetic */ ChatActivity f$0;

    public /* synthetic */ ChatActivity$$ExternalSyntheticLambda55(ChatActivity chatActivity) {
        this.f$0 = chatActivity;
    }

    public final void onShow(DialogInterface dialogInterface) {
        this.f$0.lambda$scrollToMessageId$108(dialogInterface);
    }
}
