package org.telegram.ui;

import android.content.DialogInterface;
import org.telegram.ui.LoginActivity;

public final /* synthetic */ class LoginActivity$LoginActivityResetWaitView$$ExternalSyntheticLambda0 implements DialogInterface.OnClickListener {
    public final /* synthetic */ LoginActivity.LoginActivityResetWaitView f$0;

    public /* synthetic */ LoginActivity$LoginActivityResetWaitView$$ExternalSyntheticLambda0(LoginActivity.LoginActivityResetWaitView loginActivityResetWaitView) {
        this.f$0 = loginActivityResetWaitView;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f$0.lambda$new$2(dialogInterface, i);
    }
}
