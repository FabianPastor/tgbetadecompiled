package org.telegram.ui;

import android.view.KeyEvent;
import android.widget.TextView;
import org.telegram.ui.LoginActivity;

public final /* synthetic */ class LoginActivity$LoginActivityRegisterView$$ExternalSyntheticLambda10 implements TextView.OnEditorActionListener {
    public final /* synthetic */ LoginActivity.LoginActivityRegisterView f$0;

    public /* synthetic */ LoginActivity$LoginActivityRegisterView$$ExternalSyntheticLambda10(LoginActivity.LoginActivityRegisterView loginActivityRegisterView) {
        this.f$0 = loginActivityRegisterView;
    }

    public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        return this.f$0.lambda$new$9(textView, i, keyEvent);
    }
}
