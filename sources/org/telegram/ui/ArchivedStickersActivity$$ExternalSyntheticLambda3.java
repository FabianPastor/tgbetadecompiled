package org.telegram.ui;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class ArchivedStickersActivity$$ExternalSyntheticLambda3 implements RecyclerListView.OnItemClickListener {
    public final /* synthetic */ ArchivedStickersActivity f$0;

    public /* synthetic */ ArchivedStickersActivity$$ExternalSyntheticLambda3(ArchivedStickersActivity archivedStickersActivity) {
        this.f$0 = archivedStickersActivity;
    }

    public final void onItemClick(View view, int i) {
        this.f$0.lambda$createView$0(view, i);
    }
}
