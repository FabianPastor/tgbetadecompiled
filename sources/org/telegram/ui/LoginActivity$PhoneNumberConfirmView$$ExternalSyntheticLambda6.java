package org.telegram.ui;

import android.view.View;
import org.telegram.ui.LoginActivity;

public final /* synthetic */ class LoginActivity$PhoneNumberConfirmView$$ExternalSyntheticLambda6 implements View.OnClickListener {
    public final /* synthetic */ LoginActivity.PhoneNumberConfirmView f$0;
    public final /* synthetic */ LoginActivity.PhoneNumberConfirmView.IConfirmDialogCallback f$1;

    public /* synthetic */ LoginActivity$PhoneNumberConfirmView$$ExternalSyntheticLambda6(LoginActivity.PhoneNumberConfirmView phoneNumberConfirmView, LoginActivity.PhoneNumberConfirmView.IConfirmDialogCallback iConfirmDialogCallback) {
        this.f$0 = phoneNumberConfirmView;
        this.f$1 = iConfirmDialogCallback;
    }

    public final void onClick(View view) {
        this.f$0.lambda$new$1(this.f$1, view);
    }
}
