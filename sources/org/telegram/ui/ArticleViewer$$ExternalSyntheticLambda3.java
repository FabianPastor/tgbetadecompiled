package org.telegram.ui;

import android.content.DialogInterface;

public final /* synthetic */ class ArticleViewer$$ExternalSyntheticLambda3 implements DialogInterface.OnDismissListener {
    public final /* synthetic */ ArticleViewer f$0;

    public /* synthetic */ ArticleViewer$$ExternalSyntheticLambda3(ArticleViewer articleViewer) {
        this.f$0 = articleViewer;
    }

    public final void onDismiss(DialogInterface dialogInterface) {
        this.f$0.lambda$showDialog$44(dialogInterface);
    }
}
