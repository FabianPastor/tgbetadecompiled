package org.telegram.ui;

import android.view.View;

public final /* synthetic */ class PaymentFormActivity$$ExternalSyntheticLambda35 implements Runnable {
    public final /* synthetic */ PaymentFormActivity f$0;
    public final /* synthetic */ View f$1;

    public /* synthetic */ PaymentFormActivity$$ExternalSyntheticLambda35(PaymentFormActivity paymentFormActivity, View view) {
        this.f$0 = paymentFormActivity;
        this.f$1 = view;
    }

    public final void run() {
        this.f$0.lambda$createView$22(this.f$1);
    }
}
