package org.telegram.ui;

import org.telegram.ui.ProfileActivity;

public final /* synthetic */ class ProfileActivity$5$$ExternalSyntheticLambda7 implements Runnable {
    public final /* synthetic */ ProfileActivity.AnonymousClass5 f$0;
    public final /* synthetic */ boolean f$1;

    public /* synthetic */ ProfileActivity$5$$ExternalSyntheticLambda7(ProfileActivity.AnonymousClass5 r1, boolean z) {
        this.f$0 = r1;
        this.f$1 = z;
    }

    public final void run() {
        this.f$0.lambda$onItemClick$8(this.f$1);
    }
}
