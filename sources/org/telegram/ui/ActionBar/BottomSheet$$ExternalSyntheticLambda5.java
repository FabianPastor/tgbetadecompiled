package org.telegram.ui.ActionBar;

import android.view.MotionEvent;
import android.view.View;

public final /* synthetic */ class BottomSheet$$ExternalSyntheticLambda5 implements View.OnTouchListener {
    public static final /* synthetic */ BottomSheet$$ExternalSyntheticLambda5 INSTANCE = new BottomSheet$$ExternalSyntheticLambda5();

    private /* synthetic */ BottomSheet$$ExternalSyntheticLambda5() {
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return BottomSheet.lambda$onCreate$2(view, motionEvent);
    }
}
