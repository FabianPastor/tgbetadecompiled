package org.telegram.ui.ActionBar;

import java.util.ArrayList;
import org.telegram.ui.ActionBar.Theme;

public final /* synthetic */ class Theme$PatternsLoader$$ExternalSyntheticLambda1 implements Runnable {
    public final /* synthetic */ Theme.PatternsLoader f$0;
    public final /* synthetic */ ArrayList f$1;
    public final /* synthetic */ boolean f$2;

    public /* synthetic */ Theme$PatternsLoader$$ExternalSyntheticLambda1(Theme.PatternsLoader patternsLoader, ArrayList arrayList, boolean z) {
        this.f$0 = patternsLoader;
        this.f$1 = arrayList;
        this.f$2 = z;
    }

    public final void run() {
        this.f$0.lambda$checkCurrentWallpaper$2(this.f$1, this.f$2);
    }
}
