package org.telegram.ui.ActionBar;

import android.animation.ValueAnimator;
import org.telegram.ui.ActionBar.BottomSheet;

public final /* synthetic */ class BottomSheet$ContainerView$$ExternalSyntheticLambda1 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ BottomSheet.ContainerView f$0;

    public /* synthetic */ BottomSheet$ContainerView$$ExternalSyntheticLambda1(BottomSheet.ContainerView containerView) {
        this.f$0 = containerView;
    }

    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f$0.lambda$checkDismiss$0(valueAnimator);
    }
}
