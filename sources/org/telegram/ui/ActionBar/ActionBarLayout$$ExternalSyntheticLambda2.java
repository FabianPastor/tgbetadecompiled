package org.telegram.ui.ActionBar;

public final /* synthetic */ class ActionBarLayout$$ExternalSyntheticLambda2 implements Runnable {
    public final /* synthetic */ ActionBarLayout f$0;
    public final /* synthetic */ BaseFragment f$1;

    public /* synthetic */ ActionBarLayout$$ExternalSyntheticLambda2(ActionBarLayout actionBarLayout, BaseFragment baseFragment) {
        this.f$0 = actionBarLayout;
        this.f$1 = baseFragment;
    }

    public final void run() {
        this.f$0.lambda$closeLastFragment$5(this.f$1);
    }
}
