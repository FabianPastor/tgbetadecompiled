package org.telegram.ui.ActionBar;

import android.view.MenuItem;
import java.util.Comparator;

public final /* synthetic */ class FloatingToolbar$$ExternalSyntheticLambda1 implements Comparator {
    public static final /* synthetic */ FloatingToolbar$$ExternalSyntheticLambda1 INSTANCE = new FloatingToolbar$$ExternalSyntheticLambda1();

    private /* synthetic */ FloatingToolbar$$ExternalSyntheticLambda1() {
    }

    public final int compare(Object obj, Object obj2) {
        return FloatingToolbar.lambda$new$1((MenuItem) obj, (MenuItem) obj2);
    }
}
