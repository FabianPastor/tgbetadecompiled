package org.telegram.ui.ActionBar;

import android.view.MenuItem;
import android.widget.PopupMenu;

public final /* synthetic */ class FloatingActionMode$$ExternalSyntheticLambda1 implements PopupMenu.OnMenuItemClickListener {
    public final /* synthetic */ FloatingActionMode f$0;

    public /* synthetic */ FloatingActionMode$$ExternalSyntheticLambda1(FloatingActionMode floatingActionMode) {
        this.f$0 = floatingActionMode;
    }

    public final boolean onMenuItemClick(MenuItem menuItem) {
        return this.f$0.lambda$new$0(menuItem);
    }
}
