package org.telegram.ui.ActionBar;

import android.animation.ValueAnimator;
import org.telegram.ui.ActionBar.ActionBarMenuItem;

public final /* synthetic */ class ActionBarMenuItem$SearchFilterView$$ExternalSyntheticLambda0 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ ActionBarMenuItem.SearchFilterView f$0;

    public /* synthetic */ ActionBarMenuItem$SearchFilterView$$ExternalSyntheticLambda0(ActionBarMenuItem.SearchFilterView searchFilterView) {
        this.f$0 = searchFilterView;
    }

    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f$0.lambda$setSelectedForDelete$0(valueAnimator);
    }
}
