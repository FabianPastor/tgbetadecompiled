package org.telegram.ui.ActionBar;

import android.view.ViewTreeObserver;
import org.telegram.ui.ActionBar.AlertDialog;

public final /* synthetic */ class AlertDialog$1$$ExternalSyntheticLambda0 implements ViewTreeObserver.OnScrollChangedListener {
    public final /* synthetic */ AlertDialog.AnonymousClass1 f$0;

    public /* synthetic */ AlertDialog$1$$ExternalSyntheticLambda0(AlertDialog.AnonymousClass1 r1) {
        this.f$0 = r1;
    }

    public final void onScrollChanged() {
        this.f$0.lambda$onLayout$1();
    }
}
