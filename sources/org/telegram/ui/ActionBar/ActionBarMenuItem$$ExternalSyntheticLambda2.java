package org.telegram.ui.ActionBar;

import android.view.View;

public final /* synthetic */ class ActionBarMenuItem$$ExternalSyntheticLambda2 implements View.OnClickListener {
    public final /* synthetic */ ActionBarMenuItem f$0;

    public /* synthetic */ ActionBarMenuItem$$ExternalSyntheticLambda2(ActionBarMenuItem actionBarMenuItem) {
        this.f$0 = actionBarMenuItem;
    }

    public final void onClick(View view) {
        this.f$0.lambda$setIsSearchField$13(view);
    }
}
