package org.telegram.ui.ActionBar;

import android.graphics.drawable.Drawable;

public final /* synthetic */ class Theme$$ExternalSyntheticLambda1 implements Runnable {
    public final /* synthetic */ Drawable f$0;

    public /* synthetic */ Theme$$ExternalSyntheticLambda1(Drawable drawable) {
        this.f$0 = drawable;
    }

    public final void run() {
        Theme.lambda$loadWallpaper$7(this.f$0);
    }
}
