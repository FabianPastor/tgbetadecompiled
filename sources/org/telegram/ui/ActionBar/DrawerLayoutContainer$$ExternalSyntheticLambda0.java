package org.telegram.ui.ActionBar;

import android.view.View;
import android.view.WindowInsets;

public final /* synthetic */ class DrawerLayoutContainer$$ExternalSyntheticLambda0 implements View.OnApplyWindowInsetsListener {
    public final /* synthetic */ DrawerLayoutContainer f$0;

    public /* synthetic */ DrawerLayoutContainer$$ExternalSyntheticLambda0(DrawerLayoutContainer drawerLayoutContainer) {
        this.f$0 = drawerLayoutContainer;
    }

    public final WindowInsets onApplyWindowInsets(View view, WindowInsets windowInsets) {
        return this.f$0.lambda$new$0(view, windowInsets);
    }
}
