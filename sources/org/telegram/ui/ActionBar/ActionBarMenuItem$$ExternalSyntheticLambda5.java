package org.telegram.ui.ActionBar;

import android.view.View;

public final /* synthetic */ class ActionBarMenuItem$$ExternalSyntheticLambda5 implements View.OnClickListener {
    public final /* synthetic */ ActionBarMenuItem f$0;
    public final /* synthetic */ boolean f$1;

    public /* synthetic */ ActionBarMenuItem$$ExternalSyntheticLambda5(ActionBarMenuItem actionBarMenuItem, boolean z) {
        this.f$0 = actionBarMenuItem;
        this.f$1 = z;
    }

    public final void onClick(View view) {
        this.f$0.lambda$addSubItem$6(this.f$1, view);
    }
}
