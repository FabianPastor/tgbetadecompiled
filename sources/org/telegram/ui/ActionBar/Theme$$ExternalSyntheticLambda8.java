package org.telegram.ui.ActionBar;

import java.util.Comparator;
import org.telegram.ui.ActionBar.Theme;

public final /* synthetic */ class Theme$$ExternalSyntheticLambda8 implements Comparator {
    public static final /* synthetic */ Theme$$ExternalSyntheticLambda8 INSTANCE = new Theme$$ExternalSyntheticLambda8();

    private /* synthetic */ Theme$$ExternalSyntheticLambda8() {
    }

    public final int compare(Object obj, Object obj2) {
        return Theme.lambda$sortThemes$1((Theme.ThemeInfo) obj, (Theme.ThemeInfo) obj2);
    }
}
