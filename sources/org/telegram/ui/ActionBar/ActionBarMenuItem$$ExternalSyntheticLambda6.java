package org.telegram.ui.ActionBar;

import android.view.View;

public final /* synthetic */ class ActionBarMenuItem$$ExternalSyntheticLambda6 implements View.OnClickListener {
    public final /* synthetic */ ActionBarMenuSubItem f$0;

    public /* synthetic */ ActionBarMenuItem$$ExternalSyntheticLambda6(ActionBarMenuSubItem actionBarMenuSubItem) {
        this.f$0 = actionBarMenuSubItem;
    }

    public final void onClick(View view) {
        this.f$0.openSwipeBack();
    }
}
