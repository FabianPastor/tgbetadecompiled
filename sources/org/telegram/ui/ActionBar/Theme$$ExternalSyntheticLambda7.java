package org.telegram.ui.ActionBar;

import java.util.Comparator;
import org.telegram.ui.ActionBar.Theme;

public final /* synthetic */ class Theme$$ExternalSyntheticLambda7 implements Comparator {
    public static final /* synthetic */ Theme$$ExternalSyntheticLambda7 INSTANCE = new Theme$$ExternalSyntheticLambda7();

    private /* synthetic */ Theme$$ExternalSyntheticLambda7() {
    }

    public final int compare(Object obj, Object obj2) {
        return Theme.lambda$sortAccents$0((Theme.ThemeAccent) obj, (Theme.ThemeAccent) obj2);
    }
}
