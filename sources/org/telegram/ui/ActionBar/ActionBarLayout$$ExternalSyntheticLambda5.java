package org.telegram.ui.ActionBar;

public final /* synthetic */ class ActionBarLayout$$ExternalSyntheticLambda5 implements Runnable {
    public final /* synthetic */ BaseFragment f$0;
    public final /* synthetic */ BaseFragment f$1;

    public /* synthetic */ ActionBarLayout$$ExternalSyntheticLambda5(BaseFragment baseFragment, BaseFragment baseFragment2) {
        this.f$0 = baseFragment;
        this.f$1 = baseFragment2;
    }

    public final void run() {
        ActionBarLayout.lambda$presentFragment$0(this.f$0, this.f$1);
    }
}
