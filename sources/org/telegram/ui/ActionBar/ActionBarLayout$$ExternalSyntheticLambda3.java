package org.telegram.ui.ActionBar;

public final /* synthetic */ class ActionBarLayout$$ExternalSyntheticLambda3 implements Runnable {
    public final /* synthetic */ ActionBarLayout f$0;
    public final /* synthetic */ BaseFragment f$1;
    public final /* synthetic */ BaseFragment f$2;

    public /* synthetic */ ActionBarLayout$$ExternalSyntheticLambda3(ActionBarLayout actionBarLayout, BaseFragment baseFragment, BaseFragment baseFragment2) {
        this.f$0 = actionBarLayout;
        this.f$1 = baseFragment;
        this.f$2 = baseFragment2;
    }

    public final void run() {
        this.f$0.lambda$closeLastFragment$3(this.f$1, this.f$2);
    }
}
