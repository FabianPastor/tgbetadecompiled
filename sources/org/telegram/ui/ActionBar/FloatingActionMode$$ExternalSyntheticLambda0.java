package org.telegram.ui.ActionBar;

import android.view.MenuItem;

public final /* synthetic */ class FloatingActionMode$$ExternalSyntheticLambda0 implements MenuItem.OnMenuItemClickListener {
    public final /* synthetic */ FloatingActionMode f$0;

    public /* synthetic */ FloatingActionMode$$ExternalSyntheticLambda0(FloatingActionMode floatingActionMode) {
        this.f$0 = floatingActionMode;
    }

    public final boolean onMenuItemClick(MenuItem menuItem) {
        return this.f$0.lambda$setFloatingToolbar$1(menuItem);
    }
}
