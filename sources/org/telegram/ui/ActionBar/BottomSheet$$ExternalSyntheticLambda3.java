package org.telegram.ui.ActionBar;

import android.view.View;
import android.view.WindowInsets;

public final /* synthetic */ class BottomSheet$$ExternalSyntheticLambda3 implements View.OnApplyWindowInsetsListener {
    public final /* synthetic */ BottomSheet f$0;

    public /* synthetic */ BottomSheet$$ExternalSyntheticLambda3(BottomSheet bottomSheet) {
        this.f$0 = bottomSheet;
    }

    public final WindowInsets onApplyWindowInsets(View view, WindowInsets windowInsets) {
        return this.f$0.lambda$new$1(view, windowInsets);
    }
}
