package org.telegram.ui.ActionBar;

import android.view.KeyEvent;
import android.view.View;

public final /* synthetic */ class ActionBarMenuItem$$ExternalSyntheticLambda7 implements View.OnKeyListener {
    public final /* synthetic */ ActionBarMenuItem f$0;

    public /* synthetic */ ActionBarMenuItem$$ExternalSyntheticLambda7(ActionBarMenuItem actionBarMenuItem) {
        this.f$0 = actionBarMenuItem;
    }

    public final boolean onKey(View view, int i, KeyEvent keyEvent) {
        return this.f$0.lambda$toggleSubMenu$9(view, i, keyEvent);
    }
}
