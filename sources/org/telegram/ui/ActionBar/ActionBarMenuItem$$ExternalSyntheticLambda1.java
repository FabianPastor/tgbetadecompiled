package org.telegram.ui.ActionBar;

import android.view.View;

public final /* synthetic */ class ActionBarMenuItem$$ExternalSyntheticLambda1 implements View.OnClickListener {
    public final /* synthetic */ ActionBarMenuItem f$0;

    public /* synthetic */ ActionBarMenuItem$$ExternalSyntheticLambda1(ActionBarMenuItem actionBarMenuItem) {
        this.f$0 = actionBarMenuItem;
    }

    public final void onClick(View view) {
        this.f$0.lambda$createPopupLayout$3(view);
    }
}
