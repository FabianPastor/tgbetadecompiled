package org.telegram.ui;

import android.view.View;
import org.telegram.ui.ActionBar.AlertDialog;

public final /* synthetic */ class StickersActivity$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ StickersActivity f$0;
    public final /* synthetic */ AlertDialog.Builder f$1;

    public /* synthetic */ StickersActivity$$ExternalSyntheticLambda0(StickersActivity stickersActivity, AlertDialog.Builder builder) {
        this.f$0 = stickersActivity;
        this.f$1 = builder;
    }

    public final void onClick(View view) {
        this.f$0.lambda$createView$1(this.f$1, view);
    }
}
