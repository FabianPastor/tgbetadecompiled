package org.telegram.ui;

public final /* synthetic */ class ChatEditTypeActivity$$ExternalSyntheticLambda8 implements Runnable {
    public final /* synthetic */ ChatEditTypeActivity f$0;
    public final /* synthetic */ String f$1;

    public /* synthetic */ ChatEditTypeActivity$$ExternalSyntheticLambda8(ChatEditTypeActivity chatEditTypeActivity, String str) {
        this.f$0 = chatEditTypeActivity;
        this.f$1 = str;
    }

    public final void run() {
        this.f$0.lambda$checkUserName$17(this.f$1);
    }
}
