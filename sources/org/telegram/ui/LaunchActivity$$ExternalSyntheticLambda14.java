package org.telegram.ui;

import android.content.DialogInterface;

public final /* synthetic */ class LaunchActivity$$ExternalSyntheticLambda14 implements DialogInterface.OnDismissListener {
    public final /* synthetic */ LaunchActivity f$0;

    public /* synthetic */ LaunchActivity$$ExternalSyntheticLambda14(LaunchActivity launchActivity) {
        this.f$0 = launchActivity;
    }

    public final void onDismiss(DialogInterface dialogInterface) {
        this.f$0.lambda$showAlertDialog$75(dialogInterface);
    }
}
