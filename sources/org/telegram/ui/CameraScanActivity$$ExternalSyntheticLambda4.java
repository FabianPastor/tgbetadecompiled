package org.telegram.ui;

import android.view.MotionEvent;
import android.view.View;

public final /* synthetic */ class CameraScanActivity$$ExternalSyntheticLambda4 implements View.OnTouchListener {
    public static final /* synthetic */ CameraScanActivity$$ExternalSyntheticLambda4 INSTANCE = new CameraScanActivity$$ExternalSyntheticLambda4();

    private /* synthetic */ CameraScanActivity$$ExternalSyntheticLambda4() {
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return CameraScanActivity.lambda$createView$1(view, motionEvent);
    }
}
