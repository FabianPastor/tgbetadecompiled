package org.telegram.ui;

import android.content.DialogInterface;

public final /* synthetic */ class ChatEditActivity$$ExternalSyntheticLambda1 implements DialogInterface.OnClickListener {
    public final /* synthetic */ ChatEditActivity f$0;

    public /* synthetic */ ChatEditActivity$$ExternalSyntheticLambda1(ChatEditActivity chatEditActivity) {
        this.f$0 = chatEditActivity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f$0.lambda$checkDiscard$28(dialogInterface, i);
    }
}
