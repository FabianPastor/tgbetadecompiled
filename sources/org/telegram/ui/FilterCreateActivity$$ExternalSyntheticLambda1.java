package org.telegram.ui;

import android.content.DialogInterface;

public final /* synthetic */ class FilterCreateActivity$$ExternalSyntheticLambda1 implements DialogInterface.OnClickListener {
    public final /* synthetic */ FilterCreateActivity f$0;

    public /* synthetic */ FilterCreateActivity$$ExternalSyntheticLambda1(FilterCreateActivity filterCreateActivity) {
        this.f$0 = filterCreateActivity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f$0.lambda$checkDiscard$8(dialogInterface, i);
    }
}
