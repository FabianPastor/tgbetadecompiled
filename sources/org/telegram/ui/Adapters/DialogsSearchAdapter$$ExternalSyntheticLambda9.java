package org.telegram.ui.Adapters;

public final /* synthetic */ class DialogsSearchAdapter$$ExternalSyntheticLambda9 implements Runnable {
    public final /* synthetic */ DialogsSearchAdapter f$0;
    public final /* synthetic */ int f$1;
    public final /* synthetic */ String f$2;
    public final /* synthetic */ String f$3;

    public /* synthetic */ DialogsSearchAdapter$$ExternalSyntheticLambda9(DialogsSearchAdapter dialogsSearchAdapter, int i, String str, String str2) {
        this.f$0 = dialogsSearchAdapter;
        this.f$1 = i;
        this.f$2 = str;
        this.f$3 = str2;
    }

    public final void run() {
        this.f$0.lambda$searchDialogs$13(this.f$1, this.f$2, this.f$3);
    }
}
