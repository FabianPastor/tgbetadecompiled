package org.telegram.ui.Adapters;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class DialogsSearchAdapter$$ExternalSyntheticLambda24 implements RecyclerListView.OnItemLongClickListener {
    public final /* synthetic */ DialogsSearchAdapter f$0;

    public /* synthetic */ DialogsSearchAdapter$$ExternalSyntheticLambda24(DialogsSearchAdapter dialogsSearchAdapter) {
        this.f$0 = dialogsSearchAdapter;
    }

    public final boolean onItemClick(View view, int i) {
        return this.f$0.lambda$onCreateViewHolder$16(view, i);
    }
}
