package org.telegram.ui.Adapters;

import org.telegram.ui.Cells.ContextLinkCell;

public final /* synthetic */ class MentionsAdapter$$ExternalSyntheticLambda9 implements ContextLinkCell.ContextLinkCellDelegate {
    public final /* synthetic */ MentionsAdapter f$0;

    public /* synthetic */ MentionsAdapter$$ExternalSyntheticLambda9(MentionsAdapter mentionsAdapter) {
        this.f$0 = mentionsAdapter;
    }

    public final void didPressedImage(ContextLinkCell contextLinkCell) {
        this.f$0.lambda$onCreateViewHolder$9(contextLinkCell);
    }
}
