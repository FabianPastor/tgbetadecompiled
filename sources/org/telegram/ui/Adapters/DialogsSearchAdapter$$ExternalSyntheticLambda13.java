package org.telegram.ui.Adapters;

import android.view.View;

public final /* synthetic */ class DialogsSearchAdapter$$ExternalSyntheticLambda13 implements Runnable {
    public final /* synthetic */ DialogsSearchAdapter f$0;
    public final /* synthetic */ View f$1;

    public /* synthetic */ DialogsSearchAdapter$$ExternalSyntheticLambda13(DialogsSearchAdapter dialogsSearchAdapter, View view) {
        this.f$0 = dialogsSearchAdapter;
        this.f$1 = view;
    }

    public final void run() {
        this.f$0.lambda$onBindViewHolder$22(this.f$1);
    }
}
