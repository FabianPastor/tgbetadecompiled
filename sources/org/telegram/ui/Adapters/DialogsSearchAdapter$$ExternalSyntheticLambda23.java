package org.telegram.ui.Adapters;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class DialogsSearchAdapter$$ExternalSyntheticLambda23 implements RecyclerListView.OnItemClickListener {
    public final /* synthetic */ DialogsSearchAdapter f$0;

    public /* synthetic */ DialogsSearchAdapter$$ExternalSyntheticLambda23(DialogsSearchAdapter dialogsSearchAdapter) {
        this.f$0 = dialogsSearchAdapter;
    }

    public final void onItemClick(View view, int i) {
        this.f$0.lambda$onCreateViewHolder$15(view, i);
    }
}
