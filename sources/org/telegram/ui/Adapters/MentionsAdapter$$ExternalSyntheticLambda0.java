package org.telegram.ui.Adapters;

import android.content.DialogInterface;

public final /* synthetic */ class MentionsAdapter$$ExternalSyntheticLambda0 implements DialogInterface.OnClickListener {
    public final /* synthetic */ MentionsAdapter f$0;
    public final /* synthetic */ boolean[] f$1;

    public /* synthetic */ MentionsAdapter$$ExternalSyntheticLambda0(MentionsAdapter mentionsAdapter, boolean[] zArr) {
        this.f$0 = mentionsAdapter;
        this.f$1 = zArr;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f$0.lambda$processFoundUser$3(this.f$1, dialogInterface, i);
    }
}
