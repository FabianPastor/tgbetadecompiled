package org.telegram.ui.Adapters;

import org.telegram.ui.Cells.GraySectionCell;

public final /* synthetic */ class DialogsSearchAdapter$$ExternalSyntheticLambda18 implements Runnable {
    public final /* synthetic */ DialogsSearchAdapter f$0;
    public final /* synthetic */ GraySectionCell f$1;

    public /* synthetic */ DialogsSearchAdapter$$ExternalSyntheticLambda18(DialogsSearchAdapter dialogsSearchAdapter, GraySectionCell graySectionCell) {
        this.f$0 = dialogsSearchAdapter;
        this.f$1 = graySectionCell;
    }

    public final void run() {
        this.f$0.lambda$onBindViewHolder$20(this.f$1);
    }
}
