package org.telegram.ui;

import android.content.DialogInterface;

public final /* synthetic */ class ActionIntroActivity$$ExternalSyntheticLambda0 implements DialogInterface.OnClickListener {
    public final /* synthetic */ ActionIntroActivity f$0;

    public /* synthetic */ ActionIntroActivity$$ExternalSyntheticLambda0(ActionIntroActivity actionIntroActivity) {
        this.f$0 = actionIntroActivity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f$0.lambda$onRequestPermissionsResultFragment$7(dialogInterface, i);
    }
}
