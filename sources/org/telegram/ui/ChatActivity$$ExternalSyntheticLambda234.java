package org.telegram.ui;

import org.telegram.ui.ActionBar.ThemeDescription;

public final /* synthetic */ class ChatActivity$$ExternalSyntheticLambda234 implements ThemeDescription.ThemeDescriptionDelegate {
    public final /* synthetic */ ChatActivity f$0;

    public /* synthetic */ ChatActivity$$ExternalSyntheticLambda234(ChatActivity chatActivity) {
        this.f$0 = chatActivity;
    }

    public final void didSetColor() {
        this.f$0.lambda$getThemeDescriptions$246();
    }

    public /* synthetic */ void onAnimationProgress(float f) {
        ThemeDescription.ThemeDescriptionDelegate.CC.$default$onAnimationProgress(this, f);
    }
}
