package org.telegram.ui;

import android.view.KeyEvent;
import android.widget.TextView;

public final /* synthetic */ class PaymentFormActivity$$ExternalSyntheticLambda30 implements TextView.OnEditorActionListener {
    public static final /* synthetic */ PaymentFormActivity$$ExternalSyntheticLambda30 INSTANCE = new PaymentFormActivity$$ExternalSyntheticLambda30();

    private /* synthetic */ PaymentFormActivity$$ExternalSyntheticLambda30() {
    }

    public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        return PaymentFormActivity.lambda$createView$14(textView, i, keyEvent);
    }
}
