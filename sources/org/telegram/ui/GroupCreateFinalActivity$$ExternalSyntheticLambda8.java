package org.telegram.ui;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class GroupCreateFinalActivity$$ExternalSyntheticLambda8 implements RecyclerListView.OnItemClickListener {
    public final /* synthetic */ GroupCreateFinalActivity f$0;

    public /* synthetic */ GroupCreateFinalActivity$$ExternalSyntheticLambda8(GroupCreateFinalActivity groupCreateFinalActivity) {
        this.f$0 = groupCreateFinalActivity;
    }

    public final void onItemClick(View view, int i) {
        this.f$0.lambda$createView$6(view, i);
    }
}
