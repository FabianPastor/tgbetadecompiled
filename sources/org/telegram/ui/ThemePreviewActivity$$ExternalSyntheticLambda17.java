package org.telegram.ui;

import org.telegram.tgnet.TLObject;

public final /* synthetic */ class ThemePreviewActivity$$ExternalSyntheticLambda17 implements Runnable {
    public final /* synthetic */ ThemePreviewActivity f$0;
    public final /* synthetic */ TLObject f$1;

    public /* synthetic */ ThemePreviewActivity$$ExternalSyntheticLambda17(ThemePreviewActivity themePreviewActivity, TLObject tLObject) {
        this.f$0 = themePreviewActivity;
        this.f$1 = tLObject;
    }

    public final void run() {
        this.f$0.lambda$didReceivedNotification$21(this.f$1);
    }
}
