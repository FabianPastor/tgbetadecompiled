package org.telegram.ui;

import java.util.ArrayList;

public final /* synthetic */ class ChatActivity$$ExternalSyntheticLambda172 implements Runnable {
    public final /* synthetic */ ChatActivity f$0;
    public final /* synthetic */ ArrayList f$1;
    public final /* synthetic */ ArrayList f$2;
    public final /* synthetic */ int f$3;

    public /* synthetic */ ChatActivity$$ExternalSyntheticLambda172(ChatActivity chatActivity, ArrayList arrayList, ArrayList arrayList2, int i) {
        this.f$0 = chatActivity;
        this.f$1 = arrayList;
        this.f$2 = arrayList2;
        this.f$3 = i;
    }

    public final void run() {
        this.f$0.lambda$unpinMessage$187(this.f$1, this.f$2, this.f$3);
    }
}
