package org.telegram.ui;

public final /* synthetic */ class DialogsActivity$$ExternalSyntheticLambda40 implements Runnable {
    public final /* synthetic */ DialogsActivity f$0;
    public final /* synthetic */ long f$1;
    public final /* synthetic */ boolean f$2;

    public /* synthetic */ DialogsActivity$$ExternalSyntheticLambda40(DialogsActivity dialogsActivity, long j, boolean z) {
        this.f$0 = dialogsActivity;
        this.f$1 = j;
        this.f$2 = z;
    }

    public final void run() {
        this.f$0.lambda$didSelectResult$52(this.f$1, this.f$2);
    }
}
