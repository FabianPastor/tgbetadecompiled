package org.telegram.ui;

import org.telegram.ui.ActionBar.AlertDialog;

public final /* synthetic */ class ProfileActivity$$ExternalSyntheticLambda26 implements Runnable {
    public final /* synthetic */ ProfileActivity f$0;
    public final /* synthetic */ boolean f$1;
    public final /* synthetic */ AlertDialog f$2;

    public /* synthetic */ ProfileActivity$$ExternalSyntheticLambda26(ProfileActivity profileActivity, boolean z, AlertDialog alertDialog) {
        this.f$0 = profileActivity;
        this.f$1 = z;
        this.f$2 = alertDialog;
    }

    public final void run() {
        this.f$0.lambda$sendLogs$38(this.f$1, this.f$2);
    }
}
