package org.telegram.ui;

import android.view.MotionEvent;
import android.view.View;

public final /* synthetic */ class ChatActivity$$ExternalSyntheticLambda106 implements View.OnTouchListener {
    public static final /* synthetic */ ChatActivity$$ExternalSyntheticLambda106 INSTANCE = new ChatActivity$$ExternalSyntheticLambda106();

    private /* synthetic */ ChatActivity$$ExternalSyntheticLambda106() {
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return ChatActivity.lambda$createView$24(view, motionEvent);
    }
}
