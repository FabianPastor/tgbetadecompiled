package org.telegram.ui;

import android.view.MotionEvent;
import android.view.View;

public final /* synthetic */ class PaymentFormActivity$$ExternalSyntheticLambda24 implements View.OnTouchListener {
    public static final /* synthetic */ PaymentFormActivity$$ExternalSyntheticLambda24 INSTANCE = new PaymentFormActivity$$ExternalSyntheticLambda24();

    private /* synthetic */ PaymentFormActivity$$ExternalSyntheticLambda24() {
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return PaymentFormActivity.lambda$createView$10(view, motionEvent);
    }
}
