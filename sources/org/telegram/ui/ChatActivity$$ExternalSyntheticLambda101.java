package org.telegram.ui;

import android.view.MotionEvent;
import android.view.View;

public final /* synthetic */ class ChatActivity$$ExternalSyntheticLambda101 implements View.OnTouchListener {
    public final /* synthetic */ ChatActivity f$0;

    public /* synthetic */ ChatActivity$$ExternalSyntheticLambda101(ChatActivity chatActivity) {
        this.f$0 = chatActivity;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return this.f$0.lambda$createView$55(view, motionEvent);
    }
}
