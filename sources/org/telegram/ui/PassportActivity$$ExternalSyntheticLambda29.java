package org.telegram.ui;

import android.view.View;

public final /* synthetic */ class PassportActivity$$ExternalSyntheticLambda29 implements View.OnClickListener {
    public final /* synthetic */ PassportActivity f$0;
    public final /* synthetic */ int f$1;

    public /* synthetic */ PassportActivity$$ExternalSyntheticLambda29(PassportActivity passportActivity, int i) {
        this.f$0 = passportActivity;
        this.f$1 = i;
    }

    public final void onClick(View view) {
        this.f$0.lambda$addDocumentView$55(this.f$1, view);
    }
}
