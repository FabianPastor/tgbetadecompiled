package org.telegram.ui;

import java.util.HashMap;
import org.telegram.tgnet.TLRPC$TL_wallPaper;
import org.telegram.ui.Components.AlertsCreator;

public final /* synthetic */ class LaunchActivity$$ExternalSyntheticLambda91 implements AlertsCreator.AccountSelectDelegate {
    public final /* synthetic */ LaunchActivity f$0;
    public final /* synthetic */ int f$1;
    public final /* synthetic */ boolean f$10;
    public final /* synthetic */ Integer f$11;
    public final /* synthetic */ Long f$12;
    public final /* synthetic */ Integer f$13;
    public final /* synthetic */ Integer f$14;
    public final /* synthetic */ String f$15;
    public final /* synthetic */ HashMap f$16;
    public final /* synthetic */ String f$17;
    public final /* synthetic */ String f$18;
    public final /* synthetic */ String f$19;
    public final /* synthetic */ String f$2;
    public final /* synthetic */ String f$20;
    public final /* synthetic */ TLRPC$TL_wallPaper f$21;
    public final /* synthetic */ String f$22;
    public final /* synthetic */ String f$23;
    public final /* synthetic */ String f$24;
    public final /* synthetic */ String f$25;
    public final /* synthetic */ int f$26;
    public final /* synthetic */ String f$27;
    public final /* synthetic */ String f$28;
    public final /* synthetic */ String f$29;
    public final /* synthetic */ String f$3;
    public final /* synthetic */ String f$4;
    public final /* synthetic */ String f$5;
    public final /* synthetic */ String f$6;
    public final /* synthetic */ String f$7;
    public final /* synthetic */ String f$8;
    public final /* synthetic */ String f$9;

    public /* synthetic */ LaunchActivity$$ExternalSyntheticLambda91(LaunchActivity launchActivity, int i, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, boolean z, Integer num, Long l, Integer num2, Integer num3, String str9, HashMap hashMap, String str10, String str11, String str12, String str13, TLRPC$TL_wallPaper tLRPC$TL_wallPaper, String str14, String str15, String str16, String str17, int i2, String str18, String str19, String str20) {
        this.f$0 = launchActivity;
        this.f$1 = i;
        this.f$2 = str;
        this.f$3 = str2;
        this.f$4 = str3;
        this.f$5 = str4;
        this.f$6 = str5;
        this.f$7 = str6;
        this.f$8 = str7;
        this.f$9 = str8;
        this.f$10 = z;
        this.f$11 = num;
        this.f$12 = l;
        this.f$13 = num2;
        this.f$14 = num3;
        this.f$15 = str9;
        this.f$16 = hashMap;
        this.f$17 = str10;
        this.f$18 = str11;
        this.f$19 = str12;
        this.f$20 = str13;
        this.f$21 = tLRPC$TL_wallPaper;
        this.f$22 = str14;
        this.f$23 = str15;
        this.f$24 = str16;
        this.f$25 = str17;
        this.f$26 = i2;
        this.f$27 = str18;
        this.f$28 = str19;
        this.f$29 = str20;
    }

    public final void didSelectAccount(int i) {
        int i2 = i;
        LaunchActivity launchActivity = this.f$0;
        LaunchActivity launchActivity2 = launchActivity;
        launchActivity2.lambda$runLinkRequest$28(this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, this.f$6, this.f$7, this.f$8, this.f$9, this.f$10, this.f$11, this.f$12, this.f$13, this.f$14, this.f$15, this.f$16, this.f$17, this.f$18, this.f$19, this.f$20, this.f$21, this.f$22, this.f$23, this.f$24, this.f$25, this.f$26, this.f$27, this.f$28, this.f$29, i2);
    }
}
