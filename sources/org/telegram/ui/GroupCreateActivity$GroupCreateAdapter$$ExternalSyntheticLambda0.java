package org.telegram.ui;

import org.telegram.ui.GroupCreateActivity;

public final /* synthetic */ class GroupCreateActivity$GroupCreateAdapter$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ GroupCreateActivity.GroupCreateAdapter f$0;
    public final /* synthetic */ String f$1;

    public /* synthetic */ GroupCreateActivity$GroupCreateAdapter$$ExternalSyntheticLambda0(GroupCreateActivity.GroupCreateAdapter groupCreateAdapter, String str) {
        this.f$0 = groupCreateAdapter;
        this.f$1 = str;
    }

    public final void run() {
        this.f$0.lambda$searchDialogs$2(this.f$1);
    }
}
