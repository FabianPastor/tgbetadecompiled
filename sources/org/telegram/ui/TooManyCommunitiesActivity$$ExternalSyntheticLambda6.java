package org.telegram.ui;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class TooManyCommunitiesActivity$$ExternalSyntheticLambda6 implements RecyclerListView.OnItemLongClickListener {
    public final /* synthetic */ TooManyCommunitiesActivity f$0;

    public /* synthetic */ TooManyCommunitiesActivity$$ExternalSyntheticLambda6(TooManyCommunitiesActivity tooManyCommunitiesActivity) {
        this.f$0 = tooManyCommunitiesActivity;
    }

    public final boolean onItemClick(View view, int i) {
        return this.f$0.lambda$new$1(view, i);
    }
}
