package org.telegram.ui;

import android.view.View;
import org.telegram.ui.Cells.CheckBoxCell;

public final /* synthetic */ class GroupCallActivity$$ExternalSyntheticLambda26 implements View.OnClickListener {
    public final /* synthetic */ CheckBoxCell[] f$0;

    public /* synthetic */ GroupCallActivity$$ExternalSyntheticLambda26(CheckBoxCell[] checkBoxCellArr) {
        this.f$0 = checkBoxCellArr;
    }

    public final void onClick(View view) {
        GroupCallActivity.lambda$onLeaveClick$52(this.f$0, view);
    }
}
