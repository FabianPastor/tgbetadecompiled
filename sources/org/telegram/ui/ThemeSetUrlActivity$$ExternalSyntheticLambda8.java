package org.telegram.ui;

public final /* synthetic */ class ThemeSetUrlActivity$$ExternalSyntheticLambda8 implements Runnable {
    public final /* synthetic */ ThemeSetUrlActivity f$0;
    public final /* synthetic */ String f$1;

    public /* synthetic */ ThemeSetUrlActivity$$ExternalSyntheticLambda8(ThemeSetUrlActivity themeSetUrlActivity, String str) {
        this.f$0 = themeSetUrlActivity;
        this.f$1 = str;
    }

    public final void run() {
        this.f$0.lambda$checkUrl$8(this.f$1);
    }
}
