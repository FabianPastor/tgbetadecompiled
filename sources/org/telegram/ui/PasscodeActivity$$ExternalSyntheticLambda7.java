package org.telegram.ui;

import android.view.View;

public final /* synthetic */ class PasscodeActivity$$ExternalSyntheticLambda7 implements View.OnFocusChangeListener {
    public final /* synthetic */ PasscodeActivity f$0;

    public /* synthetic */ PasscodeActivity$$ExternalSyntheticLambda7(PasscodeActivity passcodeActivity) {
        this.f$0 = passcodeActivity;
    }

    public final void onFocusChange(View view, boolean z) {
        this.f$0.lambda$createView$8(view, z);
    }
}
