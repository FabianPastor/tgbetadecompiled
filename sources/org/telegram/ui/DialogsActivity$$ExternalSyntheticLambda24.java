package org.telegram.ui;

import android.view.View;
import java.util.ArrayList;

public final /* synthetic */ class DialogsActivity$$ExternalSyntheticLambda24 implements View.OnClickListener {
    public final /* synthetic */ DialogsActivity f$0;
    public final /* synthetic */ ArrayList f$1;

    public /* synthetic */ DialogsActivity$$ExternalSyntheticLambda24(DialogsActivity dialogsActivity, ArrayList arrayList) {
        this.f$0 = dialogsActivity;
        this.f$1 = arrayList;
    }

    public final void onClick(View view) {
        this.f$0.lambda$showChatPreview$29(this.f$1, view);
    }
}
