package org.telegram.ui;

import org.telegram.ui.Components.NumberPicker;

public final /* synthetic */ class PhotoViewer$$ExternalSyntheticLambda75 implements NumberPicker.Formatter {
    public static final /* synthetic */ PhotoViewer$$ExternalSyntheticLambda75 INSTANCE = new PhotoViewer$$ExternalSyntheticLambda75();

    private /* synthetic */ PhotoViewer$$ExternalSyntheticLambda75() {
    }

    public final String format(int i) {
        return PhotoViewer.lambda$setParentActivity$26(i);
    }
}
