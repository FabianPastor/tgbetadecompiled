package org.telegram.ui;

import org.telegram.ui.QrActivity;

public final /* synthetic */ class QrActivity$ThemeListViewController$$ExternalSyntheticLambda2 implements Runnable {
    public final /* synthetic */ QrActivity.ThemeListViewController f$0;
    public final /* synthetic */ int f$1;

    public /* synthetic */ QrActivity$ThemeListViewController$$ExternalSyntheticLambda2(QrActivity.ThemeListViewController themeListViewController, int i) {
        this.f$0 = themeListViewController;
        this.f$1 = i;
    }

    public final void run() {
        this.f$0.lambda$onItemClicked$1(this.f$1);
    }
}
