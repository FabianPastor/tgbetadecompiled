package org.telegram.ui;

import android.view.View;
import org.telegram.ui.ChatActivity;

public final /* synthetic */ class ChatActivity$21$$ExternalSyntheticLambda0 implements View.OnLongClickListener {
    public final /* synthetic */ ChatActivity.AnonymousClass21 f$0;

    public /* synthetic */ ChatActivity$21$$ExternalSyntheticLambda0(ChatActivity.AnonymousClass21 r1) {
        this.f$0 = r1;
    }

    public final boolean onLongClick(View view) {
        return this.f$0.lambda$new$0(view);
    }
}
