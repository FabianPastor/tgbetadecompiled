package org.telegram.ui;

import android.content.DialogInterface;

public final /* synthetic */ class GroupCallActivity$$ExternalSyntheticLambda13 implements DialogInterface.OnDismissListener {
    public static final /* synthetic */ GroupCallActivity$$ExternalSyntheticLambda13 INSTANCE = new GroupCallActivity$$ExternalSyntheticLambda13();

    private /* synthetic */ GroupCallActivity$$ExternalSyntheticLambda13() {
    }

    public final void onDismiss(DialogInterface dialogInterface) {
        GroupCallActivity.lambda$processSelectedOption$58(dialogInterface);
    }
}
