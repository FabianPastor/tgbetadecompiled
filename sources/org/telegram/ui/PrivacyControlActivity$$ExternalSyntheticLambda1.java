package org.telegram.ui;

import android.content.DialogInterface;

public final /* synthetic */ class PrivacyControlActivity$$ExternalSyntheticLambda1 implements DialogInterface.OnClickListener {
    public final /* synthetic */ PrivacyControlActivity f$0;

    public /* synthetic */ PrivacyControlActivity$$ExternalSyntheticLambda1(PrivacyControlActivity privacyControlActivity) {
        this.f$0 = privacyControlActivity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f$0.lambda$checkDiscard$8(dialogInterface, i);
    }
}
