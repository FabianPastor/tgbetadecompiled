package org.telegram.ui;

import android.content.Context;
import android.view.View;

public final /* synthetic */ class ChatActivity$$ExternalSyntheticLambda87 implements View.OnClickListener {
    public final /* synthetic */ ChatActivity f$0;
    public final /* synthetic */ Context f$1;

    public /* synthetic */ ChatActivity$$ExternalSyntheticLambda87(ChatActivity chatActivity, Context context) {
        this.f$0 = chatActivity;
        this.f$1 = context;
    }

    public final void onClick(View view) {
        this.f$0.lambda$createView$36(this.f$1, view);
    }
}
