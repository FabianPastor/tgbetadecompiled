package org.telegram.ui;

import android.view.View;

public final /* synthetic */ class PaymentFormActivity$$ExternalSyntheticLambda21 implements View.OnClickListener {
    public final /* synthetic */ PaymentFormActivity f$0;
    public final /* synthetic */ String f$1;

    public /* synthetic */ PaymentFormActivity$$ExternalSyntheticLambda21(PaymentFormActivity paymentFormActivity, String str) {
        this.f$0 = paymentFormActivity;
        this.f$1 = str;
    }

    public final void onClick(View view) {
        this.f$0.lambda$createView$24(this.f$1, view);
    }
}
