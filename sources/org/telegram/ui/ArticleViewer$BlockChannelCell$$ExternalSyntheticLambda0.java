package org.telegram.ui;

import android.view.View;
import org.telegram.ui.ArticleViewer;

public final /* synthetic */ class ArticleViewer$BlockChannelCell$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ ArticleViewer.BlockChannelCell f$0;

    public /* synthetic */ ArticleViewer$BlockChannelCell$$ExternalSyntheticLambda0(ArticleViewer.BlockChannelCell blockChannelCell) {
        this.f$0 = blockChannelCell;
    }

    public final void onClick(View view) {
        this.f$0.lambda$new$0(view);
    }
}
