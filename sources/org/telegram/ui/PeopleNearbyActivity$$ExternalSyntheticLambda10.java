package org.telegram.ui;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class PeopleNearbyActivity$$ExternalSyntheticLambda10 implements RecyclerListView.OnItemClickListener {
    public final /* synthetic */ PeopleNearbyActivity f$0;

    public /* synthetic */ PeopleNearbyActivity$$ExternalSyntheticLambda10(PeopleNearbyActivity peopleNearbyActivity) {
        this.f$0 = peopleNearbyActivity;
    }

    public final void onItemClick(View view, int i) {
        this.f$0.lambda$createView$2(view, i);
    }
}
