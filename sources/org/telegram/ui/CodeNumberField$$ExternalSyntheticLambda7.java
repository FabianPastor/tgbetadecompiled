package org.telegram.ui;

import org.telegram.ui.Components.SimpleFloatPropertyCompat;

public final /* synthetic */ class CodeNumberField$$ExternalSyntheticLambda7 implements SimpleFloatPropertyCompat.Setter {
    public static final /* synthetic */ CodeNumberField$$ExternalSyntheticLambda7 INSTANCE = new CodeNumberField$$ExternalSyntheticLambda7();

    private /* synthetic */ CodeNumberField$$ExternalSyntheticLambda7() {
    }

    public final void set(Object obj, float f) {
        CodeNumberField.lambda$static$5((CodeNumberField) obj, f);
    }
}
