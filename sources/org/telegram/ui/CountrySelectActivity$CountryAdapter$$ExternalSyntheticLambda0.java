package org.telegram.ui;

import java.util.Comparator;

public final /* synthetic */ class CountrySelectActivity$CountryAdapter$$ExternalSyntheticLambda0 implements Comparator {
    public static final /* synthetic */ CountrySelectActivity$CountryAdapter$$ExternalSyntheticLambda0 INSTANCE = new CountrySelectActivity$CountryAdapter$$ExternalSyntheticLambda0();

    private /* synthetic */ CountrySelectActivity$CountryAdapter$$ExternalSyntheticLambda0() {
    }

    public final int compare(Object obj, Object obj2) {
        return ((String) obj).compareTo((String) obj2);
    }
}
