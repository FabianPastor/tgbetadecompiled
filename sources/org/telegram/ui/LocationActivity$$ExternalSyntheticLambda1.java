package org.telegram.ui;

import android.content.DialogInterface;

public final /* synthetic */ class LocationActivity$$ExternalSyntheticLambda1 implements DialogInterface.OnClickListener {
    public final /* synthetic */ LocationActivity f$0;

    public /* synthetic */ LocationActivity$$ExternalSyntheticLambda1(LocationActivity locationActivity) {
        this.f$0 = locationActivity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f$0.lambda$showPermissionAlert$32(dialogInterface, i);
    }
}
