package org.telegram.ui;

import org.telegram.ui.PaymentFormActivity;

public final /* synthetic */ class PaymentFormActivity$TelegramWebviewProxy$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ PaymentFormActivity.TelegramWebviewProxy f$0;
    public final /* synthetic */ String f$1;
    public final /* synthetic */ String f$2;

    public /* synthetic */ PaymentFormActivity$TelegramWebviewProxy$$ExternalSyntheticLambda0(PaymentFormActivity.TelegramWebviewProxy telegramWebviewProxy, String str, String str2) {
        this.f$0 = telegramWebviewProxy;
        this.f$1 = str;
        this.f$2 = str2;
    }

    public final void run() {
        this.f$0.lambda$postEvent$0(this.f$1, this.f$2);
    }
}
