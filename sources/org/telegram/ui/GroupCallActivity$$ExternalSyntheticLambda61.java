package org.telegram.ui;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class GroupCallActivity$$ExternalSyntheticLambda61 implements RecyclerListView.OnItemClickListener {
    public final /* synthetic */ GroupCallActivity f$0;

    public /* synthetic */ GroupCallActivity$$ExternalSyntheticLambda61(GroupCallActivity groupCallActivity) {
        this.f$0 = groupCallActivity;
    }

    public final void onItemClick(View view, int i) {
        this.f$0.lambda$new$23(view, i);
    }
}
