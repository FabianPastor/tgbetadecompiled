package org.telegram.ui;

import android.view.KeyEvent;
import android.widget.TextView;

public final /* synthetic */ class ContactAddActivity$$ExternalSyntheticLambda3 implements TextView.OnEditorActionListener {
    public final /* synthetic */ ContactAddActivity f$0;

    public /* synthetic */ ContactAddActivity$$ExternalSyntheticLambda3(ContactAddActivity contactAddActivity) {
        this.f$0 = contactAddActivity;
    }

    public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        return this.f$0.lambda$createView$2(textView, i, keyEvent);
    }
}
