package org.telegram.ui;

import android.view.View;

public final /* synthetic */ class PopupNotificationActivity$$ExternalSyntheticLambda4 implements View.OnClickListener {
    public final /* synthetic */ PopupNotificationActivity f$0;

    public /* synthetic */ PopupNotificationActivity$$ExternalSyntheticLambda4(PopupNotificationActivity popupNotificationActivity) {
        this.f$0 = popupNotificationActivity;
    }

    public final void onClick(View view) {
        this.f$0.lambda$getViewForMessage$7(view);
    }
}
