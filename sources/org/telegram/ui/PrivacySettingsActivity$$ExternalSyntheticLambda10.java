package org.telegram.ui;

import org.telegram.ui.Cells.TextCheckCell;

public final /* synthetic */ class PrivacySettingsActivity$$ExternalSyntheticLambda10 implements Runnable {
    public final /* synthetic */ PrivacySettingsActivity f$0;
    public final /* synthetic */ TextCheckCell f$1;

    public /* synthetic */ PrivacySettingsActivity$$ExternalSyntheticLambda10(PrivacySettingsActivity privacySettingsActivity, TextCheckCell textCheckCell) {
        this.f$0 = privacySettingsActivity;
        this.f$1 = textCheckCell;
    }

    public final void run() {
        this.f$0.lambda$createView$7(this.f$1);
    }
}
