package org.telegram.ui;

import org.telegram.tgnet.TLRPC$TL_error;

public final /* synthetic */ class ManageLinksActivity$$ExternalSyntheticLambda3 implements Runnable {
    public final /* synthetic */ ManageLinksActivity f$0;
    public final /* synthetic */ TLRPC$TL_error f$1;

    public /* synthetic */ ManageLinksActivity$$ExternalSyntheticLambda3(ManageLinksActivity manageLinksActivity, TLRPC$TL_error tLRPC$TL_error) {
        this.f$0 = manageLinksActivity;
        this.f$1 = tLRPC$TL_error;
    }

    public final void run() {
        this.f$0.lambda$createView$6(this.f$1);
    }
}
