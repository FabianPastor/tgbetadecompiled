package org.telegram.ui;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class ProxyListActivity$$ExternalSyntheticLambda4 implements RecyclerListView.OnItemLongClickListener {
    public final /* synthetic */ ProxyListActivity f$0;

    public /* synthetic */ ProxyListActivity$$ExternalSyntheticLambda4(ProxyListActivity proxyListActivity) {
        this.f$0 = proxyListActivity;
    }

    public final boolean onItemClick(View view, int i) {
        return this.f$0.lambda$createView$2(view, i);
    }
}
