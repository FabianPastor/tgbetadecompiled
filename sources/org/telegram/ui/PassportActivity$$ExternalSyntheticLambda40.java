package org.telegram.ui;

import android.view.KeyEvent;
import android.widget.TextView;

public final /* synthetic */ class PassportActivity$$ExternalSyntheticLambda40 implements TextView.OnEditorActionListener {
    public final /* synthetic */ PassportActivity f$0;

    public /* synthetic */ PassportActivity$$ExternalSyntheticLambda40(PassportActivity passportActivity) {
        this.f$0 = passportActivity;
    }

    public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        return this.f$0.lambda$createPasswordInterface$7(textView, i, keyEvent);
    }
}
