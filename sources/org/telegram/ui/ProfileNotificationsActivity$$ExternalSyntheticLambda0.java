package org.telegram.ui;

import android.content.DialogInterface;

public final /* synthetic */ class ProfileNotificationsActivity$$ExternalSyntheticLambda0 implements DialogInterface.OnClickListener {
    public final /* synthetic */ ProfileNotificationsActivity f$0;

    public /* synthetic */ ProfileNotificationsActivity$$ExternalSyntheticLambda0(ProfileNotificationsActivity profileNotificationsActivity) {
        this.f$0 = profileNotificationsActivity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f$0.lambda$createView$0(dialogInterface, i);
    }
}
