package org.telegram.ui;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class ThemePreviewActivity$$ExternalSyntheticLambda23 implements RecyclerListView.OnItemClickListener {
    public final /* synthetic */ ThemePreviewActivity f$0;

    public /* synthetic */ ThemePreviewActivity$$ExternalSyntheticLambda23(ThemePreviewActivity themePreviewActivity) {
        this.f$0 = themePreviewActivity;
    }

    public final void onItemClick(View view, int i) {
        this.f$0.lambda$createView$11(view, i);
    }
}
