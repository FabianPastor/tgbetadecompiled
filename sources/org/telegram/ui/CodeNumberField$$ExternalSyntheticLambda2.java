package org.telegram.ui;

import org.telegram.ui.Components.SimpleFloatPropertyCompat;

public final /* synthetic */ class CodeNumberField$$ExternalSyntheticLambda2 implements SimpleFloatPropertyCompat.Getter {
    public static final /* synthetic */ CodeNumberField$$ExternalSyntheticLambda2 INSTANCE = new CodeNumberField$$ExternalSyntheticLambda2();

    private /* synthetic */ CodeNumberField$$ExternalSyntheticLambda2() {
    }

    public final float get(Object obj) {
        return ((CodeNumberField) obj).successProgress;
    }
}
