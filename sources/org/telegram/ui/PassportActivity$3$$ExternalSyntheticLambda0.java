package org.telegram.ui;

import android.content.DialogInterface;
import org.telegram.ui.PassportActivity;

public final /* synthetic */ class PassportActivity$3$$ExternalSyntheticLambda0 implements DialogInterface.OnClickListener {
    public final /* synthetic */ PassportActivity.AnonymousClass3 f$0;
    public final /* synthetic */ int f$1;

    public /* synthetic */ PassportActivity$3$$ExternalSyntheticLambda0(PassportActivity.AnonymousClass3 r1, int i) {
        this.f$0 = r1;
        this.f$1 = i;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f$0.lambda$onIdentityDone$1(this.f$1, dialogInterface, i);
    }
}
