package org.telegram.ui;

import android.widget.ImageView;

public final /* synthetic */ class ChatActivity$$ExternalSyntheticLambda205 implements Runnable {
    public final /* synthetic */ long[] f$0;
    public final /* synthetic */ boolean[] f$1;
    public final /* synthetic */ ImageView f$2;
    public final /* synthetic */ ImageView f$3;

    public /* synthetic */ ChatActivity$$ExternalSyntheticLambda205(long[] jArr, boolean[] zArr, ImageView imageView, ImageView imageView2) {
        this.f$0 = jArr;
        this.f$1 = zArr;
        this.f$2 = imageView;
        this.f$3 = imageView2;
    }

    public final void run() {
        ChatActivity.lambda$createMenu$165(this.f$0, this.f$1, this.f$2, this.f$3);
    }
}
