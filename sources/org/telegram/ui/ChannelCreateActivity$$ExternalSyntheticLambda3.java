package org.telegram.ui;

import android.content.DialogInterface;

public final /* synthetic */ class ChannelCreateActivity$$ExternalSyntheticLambda3 implements DialogInterface.OnDismissListener {
    public final /* synthetic */ ChannelCreateActivity f$0;

    public /* synthetic */ ChannelCreateActivity$$ExternalSyntheticLambda3(ChannelCreateActivity channelCreateActivity) {
        this.f$0 = channelCreateActivity;
    }

    public final void onDismiss(DialogInterface dialogInterface) {
        this.f$0.lambda$createView$6(dialogInterface);
    }
}
