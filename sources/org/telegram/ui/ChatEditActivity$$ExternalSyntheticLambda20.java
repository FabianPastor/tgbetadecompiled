package org.telegram.ui;

import android.content.Context;
import android.view.View;

public final /* synthetic */ class ChatEditActivity$$ExternalSyntheticLambda20 implements View.OnClickListener {
    public final /* synthetic */ ChatEditActivity f$0;
    public final /* synthetic */ Context f$1;

    public /* synthetic */ ChatEditActivity$$ExternalSyntheticLambda20(ChatEditActivity chatEditActivity, Context context) {
        this.f$0 = chatEditActivity;
        this.f$1 = context;
    }

    public final void onClick(View view) {
        this.f$0.lambda$createView$13(this.f$1, view);
    }
}
