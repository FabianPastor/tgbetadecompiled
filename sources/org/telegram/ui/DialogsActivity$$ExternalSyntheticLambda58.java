package org.telegram.ui;

import java.util.ArrayList;
import org.telegram.ui.FilteredSearchView;

public final /* synthetic */ class DialogsActivity$$ExternalSyntheticLambda58 implements FilteredSearchView.Delegate {
    public final /* synthetic */ DialogsActivity f$0;

    public /* synthetic */ DialogsActivity$$ExternalSyntheticLambda58(DialogsActivity dialogsActivity) {
        this.f$0 = dialogsActivity;
    }

    public final void updateFiltersView(boolean z, ArrayList arrayList, ArrayList arrayList2, boolean z2) {
        this.f$0.lambda$createView$7(z, arrayList, arrayList2, z2);
    }
}
