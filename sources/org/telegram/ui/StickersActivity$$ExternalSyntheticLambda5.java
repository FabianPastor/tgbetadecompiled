package org.telegram.ui;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class StickersActivity$$ExternalSyntheticLambda5 implements RecyclerListView.OnItemLongClickListener {
    public final /* synthetic */ StickersActivity f$0;

    public /* synthetic */ StickersActivity$$ExternalSyntheticLambda5(StickersActivity stickersActivity) {
        this.f$0 = stickersActivity;
    }

    public final boolean onItemClick(View view, int i) {
        return this.f$0.lambda$createView$3(view, i);
    }
}
