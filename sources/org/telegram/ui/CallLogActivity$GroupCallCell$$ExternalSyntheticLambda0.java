package org.telegram.ui;

import android.view.View;
import org.telegram.ui.CallLogActivity;

public final /* synthetic */ class CallLogActivity$GroupCallCell$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ CallLogActivity.GroupCallCell f$0;

    public /* synthetic */ CallLogActivity$GroupCallCell$$ExternalSyntheticLambda0(CallLogActivity.GroupCallCell groupCallCell) {
        this.f$0 = groupCallCell;
    }

    public final void onClick(View view) {
        this.f$0.lambda$new$0(view);
    }
}
