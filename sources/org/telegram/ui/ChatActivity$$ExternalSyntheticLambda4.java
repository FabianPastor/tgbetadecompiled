package org.telegram.ui;

import android.animation.ValueAnimator;

public final /* synthetic */ class ChatActivity$$ExternalSyntheticLambda4 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ ChatActivity f$0;

    public /* synthetic */ ChatActivity$$ExternalSyntheticLambda4(ChatActivity chatActivity) {
        this.f$0 = chatActivity;
    }

    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f$0.lambda$closeMenu$183(valueAnimator);
    }
}
