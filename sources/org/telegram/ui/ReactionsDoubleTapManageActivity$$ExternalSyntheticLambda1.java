package org.telegram.ui;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class ReactionsDoubleTapManageActivity$$ExternalSyntheticLambda1 implements RecyclerListView.OnItemClickListener {
    public final /* synthetic */ ReactionsDoubleTapManageActivity f$0;

    public /* synthetic */ ReactionsDoubleTapManageActivity$$ExternalSyntheticLambda1(ReactionsDoubleTapManageActivity reactionsDoubleTapManageActivity) {
        this.f$0 = reactionsDoubleTapManageActivity;
    }

    public final void onItemClick(View view, int i) {
        this.f$0.lambda$createView$0(view, i);
    }
}
