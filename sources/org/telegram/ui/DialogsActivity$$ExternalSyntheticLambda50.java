package org.telegram.ui;

import org.telegram.messenger.MessagesStorage;

public final /* synthetic */ class DialogsActivity$$ExternalSyntheticLambda50 implements MessagesStorage.IntCallback {
    public final /* synthetic */ DialogsActivity f$0;

    public /* synthetic */ DialogsActivity$$ExternalSyntheticLambda50(DialogsActivity dialogsActivity) {
        this.f$0 = dialogsActivity;
    }

    public final void run(int i) {
        this.f$0.lambda$askForPermissons$42(i);
    }
}
