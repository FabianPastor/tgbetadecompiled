package org.telegram.ui.Delegates;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class MemberRequestsDelegate$$ExternalSyntheticLambda9 implements RecyclerListView.OnItemClickListener {
    public final /* synthetic */ MemberRequestsDelegate f$0;

    public /* synthetic */ MemberRequestsDelegate$$ExternalSyntheticLambda9(MemberRequestsDelegate memberRequestsDelegate) {
        this.f$0 = memberRequestsDelegate;
    }

    public final void onItemClick(View view, int i) {
        this.f$0.onItemClick(view, i);
    }
}
