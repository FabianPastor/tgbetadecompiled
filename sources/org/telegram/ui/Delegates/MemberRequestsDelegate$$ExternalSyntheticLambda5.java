package org.telegram.ui.Delegates;

public final /* synthetic */ class MemberRequestsDelegate$$ExternalSyntheticLambda5 implements Runnable {
    public final /* synthetic */ MemberRequestsDelegate f$0;
    public final /* synthetic */ boolean f$1;

    public /* synthetic */ MemberRequestsDelegate$$ExternalSyntheticLambda5(MemberRequestsDelegate memberRequestsDelegate, boolean z) {
        this.f$0 = memberRequestsDelegate;
        this.f$1 = z;
    }

    public final void run() {
        this.f$0.lambda$loadMembers$5(this.f$1);
    }
}
