package org.telegram.ui;

import android.content.Context;
import android.view.MotionEvent;
import android.view.View;

public final /* synthetic */ class PassportActivity$$ExternalSyntheticLambda38 implements View.OnTouchListener {
    public final /* synthetic */ PassportActivity f$0;
    public final /* synthetic */ Context f$1;

    public /* synthetic */ PassportActivity$$ExternalSyntheticLambda38(PassportActivity passportActivity, Context context) {
        this.f$0 = passportActivity;
        this.f$1 = context;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return this.f$0.lambda$createIdentityInterface$49(this.f$1, view, motionEvent);
    }
}
