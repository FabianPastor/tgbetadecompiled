package org.telegram.ui.Charts.view_data;

import android.view.View;

public final /* synthetic */ class ChartHeaderView$$ExternalSyntheticLambda0 implements View.OnLayoutChangeListener {
    public final /* synthetic */ ChartHeaderView f$0;

    public /* synthetic */ ChartHeaderView$$ExternalSyntheticLambda0(ChartHeaderView chartHeaderView) {
        this.f$0 = chartHeaderView;
    }

    public final void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        this.f$0.lambda$new$0(view, i, i2, i3, i4, i5, i6, i7, i8);
    }
}
