package org.telegram.ui.Charts;

import android.animation.ValueAnimator;

public final /* synthetic */ class BaseChartView$$ExternalSyntheticLambda0 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ BaseChartView f$0;

    public /* synthetic */ BaseChartView$$ExternalSyntheticLambda0(BaseChartView baseChartView) {
        this.f$0 = baseChartView;
    }

    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f$0.lambda$new$1(valueAnimator);
    }
}
