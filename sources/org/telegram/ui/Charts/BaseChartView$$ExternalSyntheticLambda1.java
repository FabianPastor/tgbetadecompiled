package org.telegram.ui.Charts;

import android.animation.ValueAnimator;

public final /* synthetic */ class BaseChartView$$ExternalSyntheticLambda1 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ BaseChartView f$0;

    public /* synthetic */ BaseChartView$$ExternalSyntheticLambda1(BaseChartView baseChartView) {
        this.f$0 = baseChartView;
    }

    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f$0.lambda$new$0(valueAnimator);
    }
}
