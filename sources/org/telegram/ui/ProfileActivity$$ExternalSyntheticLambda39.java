package org.telegram.ui;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class ProfileActivity$$ExternalSyntheticLambda39 implements RecyclerListView.OnItemLongClickListener {
    public final /* synthetic */ ProfileActivity f$0;

    public /* synthetic */ ProfileActivity$$ExternalSyntheticLambda39(ProfileActivity profileActivity) {
        this.f$0 = profileActivity;
    }

    public final boolean onItemClick(View view, int i) {
        return this.f$0.lambda$createView$7(view, i);
    }
}
