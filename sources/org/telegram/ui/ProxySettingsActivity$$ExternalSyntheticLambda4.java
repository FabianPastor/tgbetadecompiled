package org.telegram.ui;

import android.view.View;

public final /* synthetic */ class ProxySettingsActivity$$ExternalSyntheticLambda4 implements View.OnClickListener {
    public final /* synthetic */ ProxySettingsActivity f$0;

    public /* synthetic */ ProxySettingsActivity$$ExternalSyntheticLambda4(ProxySettingsActivity proxySettingsActivity) {
        this.f$0 = proxySettingsActivity;
    }

    public final void onClick(View view) {
        this.f$0.lambda$createView$4(view);
    }
}
