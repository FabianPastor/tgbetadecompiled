package org.telegram.ui;

public final /* synthetic */ class ArticleViewer$$ExternalSyntheticLambda27 implements Runnable {
    public final /* synthetic */ ArticleViewer f$0;
    public final /* synthetic */ String f$1;
    public final /* synthetic */ int f$2;

    public /* synthetic */ ArticleViewer$$ExternalSyntheticLambda27(ArticleViewer articleViewer, String str, int i) {
        this.f$0 = articleViewer;
        this.f$1 = str;
        this.f$2 = i;
    }

    public final void run() {
        this.f$0.lambda$processSearch$28(this.f$1, this.f$2);
    }
}
