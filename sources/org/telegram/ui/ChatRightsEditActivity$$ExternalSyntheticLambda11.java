package org.telegram.ui;

import android.content.DialogInterface;
import android.widget.DatePicker;

public final /* synthetic */ class ChatRightsEditActivity$$ExternalSyntheticLambda11 implements DialogInterface.OnShowListener {
    public final /* synthetic */ DatePicker f$0;

    public /* synthetic */ ChatRightsEditActivity$$ExternalSyntheticLambda11(DatePicker datePicker) {
        this.f$0 = datePicker;
    }

    public final void onShow(DialogInterface dialogInterface) {
        ChatRightsEditActivity.lambda$createView$4(this.f$0, dialogInterface);
    }
}
