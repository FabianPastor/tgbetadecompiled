package org.telegram.ui;

import android.content.DialogInterface;

public final /* synthetic */ class ShareActivity$$ExternalSyntheticLambda0 implements DialogInterface.OnDismissListener {
    public final /* synthetic */ ShareActivity f$0;

    public /* synthetic */ ShareActivity$$ExternalSyntheticLambda0(ShareActivity shareActivity) {
        this.f$0 = shareActivity;
    }

    public final void onDismiss(DialogInterface dialogInterface) {
        this.f$0.lambda$onCreate$0(dialogInterface);
    }
}
