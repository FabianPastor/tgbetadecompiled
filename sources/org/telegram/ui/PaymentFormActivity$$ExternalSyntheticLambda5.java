package org.telegram.ui;

import android.view.View;

public final /* synthetic */ class PaymentFormActivity$$ExternalSyntheticLambda5 implements View.OnClickListener {
    public final /* synthetic */ PaymentFormActivity f$0;

    public /* synthetic */ PaymentFormActivity$$ExternalSyntheticLambda5(PaymentFormActivity paymentFormActivity) {
        this.f$0 = paymentFormActivity;
    }

    public final void onClick(View view) {
        this.f$0.lambda$createGooglePayButton$32(view);
    }
}
