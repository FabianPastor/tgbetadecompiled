package org.telegram.ui;

import java.util.ArrayList;

public final /* synthetic */ class ChatUsersActivity$$ExternalSyntheticLambda8 implements Runnable {
    public final /* synthetic */ ChatUsersActivity f$0;
    public final /* synthetic */ ArrayList f$1;
    public final /* synthetic */ ArrayList f$2;

    public /* synthetic */ ChatUsersActivity$$ExternalSyntheticLambda8(ChatUsersActivity chatUsersActivity, ArrayList arrayList, ArrayList arrayList2) {
        this.f$0 = chatUsersActivity;
        this.f$1 = arrayList;
        this.f$2 = arrayList2;
    }

    public final void run() {
        this.f$0.lambda$loadChatParticipants$14(this.f$1, this.f$2);
    }
}
