package org.telegram.ui;

import android.view.View;
import org.telegram.ui.StatisticActivity;

public final /* synthetic */ class StatisticActivity$BaseChartCell$$ExternalSyntheticLambda3 implements View.OnClickListener {
    public final /* synthetic */ StatisticActivity.BaseChartCell f$0;

    public /* synthetic */ StatisticActivity$BaseChartCell$$ExternalSyntheticLambda3(StatisticActivity.BaseChartCell baseChartCell) {
        this.f$0 = baseChartCell;
    }

    public final void onClick(View view) {
        this.f$0.lambda$new$0(view);
    }
}
