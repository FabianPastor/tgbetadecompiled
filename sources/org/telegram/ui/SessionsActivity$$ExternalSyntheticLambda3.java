package org.telegram.ui;

import android.view.View;
import org.telegram.ui.ActionBar.AlertDialog;

public final /* synthetic */ class SessionsActivity$$ExternalSyntheticLambda3 implements View.OnClickListener {
    public final /* synthetic */ SessionsActivity f$0;
    public final /* synthetic */ AlertDialog.Builder f$1;

    public /* synthetic */ SessionsActivity$$ExternalSyntheticLambda3(SessionsActivity sessionsActivity, AlertDialog.Builder builder) {
        this.f$0 = sessionsActivity;
        this.f$1 = builder;
    }

    public final void onClick(View view) {
        this.f$0.lambda$createView$1(this.f$1, view);
    }
}
