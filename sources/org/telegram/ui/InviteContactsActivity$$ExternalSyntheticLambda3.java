package org.telegram.ui;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class InviteContactsActivity$$ExternalSyntheticLambda3 implements RecyclerListView.OnItemClickListener {
    public final /* synthetic */ InviteContactsActivity f$0;

    public /* synthetic */ InviteContactsActivity$$ExternalSyntheticLambda3(InviteContactsActivity inviteContactsActivity) {
        this.f$0 = inviteContactsActivity;
    }

    public final void onItemClick(View view, int i) {
        this.f$0.lambda$createView$0(view, i);
    }
}
