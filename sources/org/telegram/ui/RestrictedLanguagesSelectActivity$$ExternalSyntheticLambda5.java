package org.telegram.ui;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class RestrictedLanguagesSelectActivity$$ExternalSyntheticLambda5 implements RecyclerListView.OnItemLongClickListener {
    public final /* synthetic */ RestrictedLanguagesSelectActivity f$0;

    public /* synthetic */ RestrictedLanguagesSelectActivity$$ExternalSyntheticLambda5(RestrictedLanguagesSelectActivity restrictedLanguagesSelectActivity) {
        this.f$0 = restrictedLanguagesSelectActivity;
    }

    public final boolean onItemClick(View view, int i) {
        return this.f$0.lambda$createView$3(view, i);
    }
}
