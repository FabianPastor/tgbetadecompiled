package org.telegram.ui;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class StatisticActivity$$ExternalSyntheticLambda7 implements RecyclerListView.OnItemClickListener {
    public final /* synthetic */ StatisticActivity f$0;

    public /* synthetic */ StatisticActivity$$ExternalSyntheticLambda7(StatisticActivity statisticActivity) {
        this.f$0 = statisticActivity;
    }

    public final void onItemClick(View view, int i) {
        this.f$0.lambda$createView$3(view, i);
    }
}
