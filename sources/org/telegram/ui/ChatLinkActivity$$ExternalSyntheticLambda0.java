package org.telegram.ui;

import android.content.DialogInterface;

public final /* synthetic */ class ChatLinkActivity$$ExternalSyntheticLambda0 implements DialogInterface.OnCancelListener {
    public final /* synthetic */ ChatLinkActivity f$0;

    public /* synthetic */ ChatLinkActivity$$ExternalSyntheticLambda0(ChatLinkActivity chatLinkActivity) {
        this.f$0 = chatLinkActivity;
    }

    public final void onCancel(DialogInterface dialogInterface) {
        this.f$0.lambda$showLinkAlert$7(dialogInterface);
    }
}
