package org.telegram.ui;

import android.view.View;

public final /* synthetic */ class ChatEditTypeActivity$$ExternalSyntheticLambda2 implements View.OnClickListener {
    public final /* synthetic */ ChatEditTypeActivity f$0;

    public /* synthetic */ ChatEditTypeActivity$$ExternalSyntheticLambda2(ChatEditTypeActivity chatEditTypeActivity) {
        this.f$0 = chatEditTypeActivity;
    }

    public final void onClick(View view) {
        this.f$0.lambda$loadAdminedChannels$12(view);
    }
}
