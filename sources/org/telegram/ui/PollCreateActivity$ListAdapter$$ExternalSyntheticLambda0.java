package org.telegram.ui;

import android.view.View;
import org.telegram.ui.PollCreateActivity;

public final /* synthetic */ class PollCreateActivity$ListAdapter$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ PollCreateActivity.ListAdapter f$0;

    public /* synthetic */ PollCreateActivity$ListAdapter$$ExternalSyntheticLambda0(PollCreateActivity.ListAdapter listAdapter) {
        this.f$0 = listAdapter;
    }

    public final void onClick(View view) {
        this.f$0.lambda$onCreateViewHolder$0(view);
    }
}
