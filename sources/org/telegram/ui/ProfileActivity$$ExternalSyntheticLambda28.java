package org.telegram.ui;

public final /* synthetic */ class ProfileActivity$$ExternalSyntheticLambda28 implements Runnable {
    public final /* synthetic */ ProfileActivity f$0;
    public final /* synthetic */ boolean[] f$1;
    public final /* synthetic */ String f$2;
    public final /* synthetic */ int f$3;
    public final /* synthetic */ String[] f$4;
    public final /* synthetic */ String f$5;

    public /* synthetic */ ProfileActivity$$ExternalSyntheticLambda28(ProfileActivity profileActivity, boolean[] zArr, String str, int i, String[] strArr, String str2) {
        this.f$0 = profileActivity;
        this.f$1 = zArr;
        this.f$2 = str;
        this.f$3 = i;
        this.f$4 = strArr;
        this.f$5 = str2;
    }

    public final void run() {
        this.f$0.lambda$processOnClickOrPress$22(this.f$1, this.f$2, this.f$3, this.f$4, this.f$5);
    }
}
