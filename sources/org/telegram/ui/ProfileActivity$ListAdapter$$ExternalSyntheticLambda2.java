package org.telegram.ui;

import android.view.View;

public final /* synthetic */ class ProfileActivity$ListAdapter$$ExternalSyntheticLambda2 implements View.OnClickListener {
    public final /* synthetic */ ProfileActivity f$0;

    public /* synthetic */ ProfileActivity$ListAdapter$$ExternalSyntheticLambda2(ProfileActivity profileActivity) {
        this.f$0 = profileActivity;
    }

    public final void onClick(View view) {
        this.f$0.onTextDetailCellImageClicked(view);
    }
}
