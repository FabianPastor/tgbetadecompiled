package org.telegram.ui;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class LanguageSelectActivity$$ExternalSyntheticLambda6 implements RecyclerListView.OnItemClickListener {
    public final /* synthetic */ LanguageSelectActivity f$0;

    public /* synthetic */ LanguageSelectActivity$$ExternalSyntheticLambda6(LanguageSelectActivity languageSelectActivity) {
        this.f$0 = languageSelectActivity;
    }

    public final void onItemClick(View view, int i) {
        this.f$0.lambda$createView$1(view, i);
    }
}
