package org.telegram.ui;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class WallpapersListActivity$$ExternalSyntheticLambda7 implements RecyclerListView.OnItemClickListener {
    public final /* synthetic */ WallpapersListActivity f$0;

    public /* synthetic */ WallpapersListActivity$$ExternalSyntheticLambda7(WallpapersListActivity wallpapersListActivity) {
        this.f$0 = wallpapersListActivity;
    }

    public final void onItemClick(View view, int i) {
        this.f$0.lambda$createView$4(view, i);
    }
}
