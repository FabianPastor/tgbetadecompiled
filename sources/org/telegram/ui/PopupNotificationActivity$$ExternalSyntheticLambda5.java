package org.telegram.ui;

import android.view.MotionEvent;
import android.view.View;

public final /* synthetic */ class PopupNotificationActivity$$ExternalSyntheticLambda5 implements View.OnTouchListener {
    public static final /* synthetic */ PopupNotificationActivity$$ExternalSyntheticLambda5 INSTANCE = new PopupNotificationActivity$$ExternalSyntheticLambda5();

    private /* synthetic */ PopupNotificationActivity$$ExternalSyntheticLambda5() {
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return PopupNotificationActivity.lambda$getButtonsViewForMessage$4(view, motionEvent);
    }
}
