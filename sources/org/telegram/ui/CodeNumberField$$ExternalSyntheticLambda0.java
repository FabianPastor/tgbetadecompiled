package org.telegram.ui;

import android.animation.ValueAnimator;

public final /* synthetic */ class CodeNumberField$$ExternalSyntheticLambda0 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ CodeNumberField f$0;

    public /* synthetic */ CodeNumberField$$ExternalSyntheticLambda0(CodeNumberField codeNumberField) {
        this.f$0 = codeNumberField;
    }

    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f$0.lambda$startExitAnimation$8(valueAnimator);
    }
}
