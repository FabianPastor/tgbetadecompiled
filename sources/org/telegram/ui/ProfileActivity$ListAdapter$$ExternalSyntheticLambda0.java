package org.telegram.ui;

import android.view.View;
import org.telegram.ui.ProfileActivity;

public final /* synthetic */ class ProfileActivity$ListAdapter$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ ProfileActivity.ListAdapter f$0;

    public /* synthetic */ ProfileActivity$ListAdapter$$ExternalSyntheticLambda0(ProfileActivity.ListAdapter listAdapter) {
        this.f$0 = listAdapter;
    }

    public final void onClick(View view) {
        this.f$0.lambda$onBindViewHolder$1(view);
    }
}
