package org.telegram.ui;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class ManageLinksActivity$$ExternalSyntheticLambda17 implements RecyclerListView.OnItemLongClickListener {
    public final /* synthetic */ ManageLinksActivity f$0;

    public /* synthetic */ ManageLinksActivity$$ExternalSyntheticLambda17(ManageLinksActivity manageLinksActivity) {
        this.f$0 = manageLinksActivity;
    }

    public final boolean onItemClick(View view, int i) {
        return this.f$0.lambda$createView$10(view, i);
    }
}
