package org.telegram.ui;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class ProxyListActivity$$ExternalSyntheticLambda3 implements RecyclerListView.OnItemClickListener {
    public final /* synthetic */ ProxyListActivity f$0;

    public /* synthetic */ ProxyListActivity$$ExternalSyntheticLambda3(ProxyListActivity proxyListActivity) {
        this.f$0 = proxyListActivity;
    }

    public final void onItemClick(View view, int i) {
        this.f$0.lambda$createView$0(view, i);
    }
}
