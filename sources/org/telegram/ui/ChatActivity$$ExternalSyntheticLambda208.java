package org.telegram.ui;

import java.util.Comparator;

public final /* synthetic */ class ChatActivity$$ExternalSyntheticLambda208 implements Comparator {
    public static final /* synthetic */ ChatActivity$$ExternalSyntheticLambda208 INSTANCE = new ChatActivity$$ExternalSyntheticLambda208();

    private /* synthetic */ ChatActivity$$ExternalSyntheticLambda208() {
    }

    public final int compare(Object obj, Object obj2) {
        return ((Integer) obj2).compareTo((Integer) obj);
    }
}
