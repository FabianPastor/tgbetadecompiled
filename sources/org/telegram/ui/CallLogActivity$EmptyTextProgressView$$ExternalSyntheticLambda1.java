package org.telegram.ui;

import android.view.MotionEvent;
import android.view.View;
import org.telegram.ui.CallLogActivity;

public final /* synthetic */ class CallLogActivity$EmptyTextProgressView$$ExternalSyntheticLambda1 implements View.OnTouchListener {
    public static final /* synthetic */ CallLogActivity$EmptyTextProgressView$$ExternalSyntheticLambda1 INSTANCE = new CallLogActivity$EmptyTextProgressView$$ExternalSyntheticLambda1();

    private /* synthetic */ CallLogActivity$EmptyTextProgressView$$ExternalSyntheticLambda1() {
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return CallLogActivity.EmptyTextProgressView.lambda$new$1(view, motionEvent);
    }
}
