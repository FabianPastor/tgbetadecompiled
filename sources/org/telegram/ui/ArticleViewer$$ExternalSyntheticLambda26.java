package org.telegram.ui;

import android.animation.AnimatorSet;

public final /* synthetic */ class ArticleViewer$$ExternalSyntheticLambda26 implements Runnable {
    public final /* synthetic */ ArticleViewer f$0;
    public final /* synthetic */ AnimatorSet f$1;

    public /* synthetic */ ArticleViewer$$ExternalSyntheticLambda26(ArticleViewer articleViewer, AnimatorSet animatorSet) {
        this.f$0 = articleViewer;
        this.f$1 = animatorSet;
    }

    public final void run() {
        this.f$0.lambda$open$33(this.f$1);
    }
}
