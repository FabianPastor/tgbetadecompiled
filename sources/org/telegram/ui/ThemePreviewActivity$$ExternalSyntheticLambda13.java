package org.telegram.ui;

import android.view.ViewTreeObserver;

public final /* synthetic */ class ThemePreviewActivity$$ExternalSyntheticLambda13 implements ViewTreeObserver.OnGlobalLayoutListener {
    public final /* synthetic */ ThemePreviewActivity f$0;

    public /* synthetic */ ThemePreviewActivity$$ExternalSyntheticLambda13(ThemePreviewActivity themePreviewActivity) {
        this.f$0 = themePreviewActivity;
    }

    public final void onGlobalLayout() {
        this.f$0.lambda$createView$12();
    }
}
