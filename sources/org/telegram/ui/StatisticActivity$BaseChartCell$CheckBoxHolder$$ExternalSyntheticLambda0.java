package org.telegram.ui;

import android.view.View;
import org.telegram.ui.Charts.view_data.LineViewData;
import org.telegram.ui.StatisticActivity;

public final /* synthetic */ class StatisticActivity$BaseChartCell$CheckBoxHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ StatisticActivity.BaseChartCell.CheckBoxHolder f$0;
    public final /* synthetic */ LineViewData f$1;

    public /* synthetic */ StatisticActivity$BaseChartCell$CheckBoxHolder$$ExternalSyntheticLambda0(StatisticActivity.BaseChartCell.CheckBoxHolder checkBoxHolder, LineViewData lineViewData) {
        this.f$0 = checkBoxHolder;
        this.f$1 = lineViewData;
    }

    public final void onClick(View view) {
        this.f$0.lambda$setData$0(this.f$1, view);
    }
}
