package org.telegram.ui;

import org.telegram.ui.LoginActivity;

public final /* synthetic */ class LoginActivity$PhoneView$$ExternalSyntheticLambda11 implements Runnable {
    public final /* synthetic */ LoginActivity.PhoneView f$0;
    public final /* synthetic */ String f$1;

    public /* synthetic */ LoginActivity$PhoneView$$ExternalSyntheticLambda11(LoginActivity.PhoneView phoneView, String str) {
        this.f$0 = phoneView;
        this.f$1 = str;
    }

    public final void run() {
        this.f$0.lambda$onNextPressed$15(this.f$1);
    }
}
