package org.telegram.ui;

import android.animation.ValueAnimator;

public final /* synthetic */ class LaunchActivity$$ExternalSyntheticLambda0 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ LaunchActivity f$0;

    public /* synthetic */ LaunchActivity$$ExternalSyntheticLambda0(LaunchActivity launchActivity) {
        this.f$0 = launchActivity;
    }

    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f$0.lambda$didReceivedNotification$88(valueAnimator);
    }
}
