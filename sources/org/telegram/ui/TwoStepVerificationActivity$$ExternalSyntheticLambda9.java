package org.telegram.ui;

import android.view.View;

public final /* synthetic */ class TwoStepVerificationActivity$$ExternalSyntheticLambda9 implements View.OnFocusChangeListener {
    public final /* synthetic */ TwoStepVerificationActivity f$0;

    public /* synthetic */ TwoStepVerificationActivity$$ExternalSyntheticLambda9(TwoStepVerificationActivity twoStepVerificationActivity) {
        this.f$0 = twoStepVerificationActivity;
    }

    public final void onFocusChange(View view, boolean z) {
        this.f$0.lambda$createView$1(view, z);
    }
}
