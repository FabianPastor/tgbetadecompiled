package org.telegram.ui;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class CallLogActivity$$ExternalSyntheticLambda9 implements RecyclerListView.OnItemLongClickListener {
    public final /* synthetic */ CallLogActivity f$0;

    public /* synthetic */ CallLogActivity$$ExternalSyntheticLambda9(CallLogActivity callLogActivity) {
        this.f$0 = callLogActivity;
    }

    public final boolean onItemClick(View view, int i) {
        return this.f$0.lambda$createView$1(view, i);
    }
}
