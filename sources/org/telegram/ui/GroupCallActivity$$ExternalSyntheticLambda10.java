package org.telegram.ui;

import android.content.DialogInterface;

public final /* synthetic */ class GroupCallActivity$$ExternalSyntheticLambda10 implements DialogInterface.OnDismissListener {
    public final /* synthetic */ GroupCallActivity f$0;

    public /* synthetic */ GroupCallActivity$$ExternalSyntheticLambda10(GroupCallActivity groupCallActivity) {
        this.f$0 = groupCallActivity;
    }

    public final void onDismiss(DialogInterface dialogInterface) {
        this.f$0.lambda$new$11(dialogInterface);
    }
}
