package org.telegram.ui;

import android.view.View;
import org.telegram.ui.ActionBar.AlertDialog;

public final /* synthetic */ class PrivacySettingsActivity$$ExternalSyntheticLambda5 implements View.OnClickListener {
    public final /* synthetic */ PrivacySettingsActivity f$0;
    public final /* synthetic */ AlertDialog.Builder f$1;

    public /* synthetic */ PrivacySettingsActivity$$ExternalSyntheticLambda5(PrivacySettingsActivity privacySettingsActivity, AlertDialog.Builder builder) {
        this.f$0 = privacySettingsActivity;
        this.f$1 = builder;
    }

    public final void onClick(View view) {
        this.f$0.lambda$createView$4(this.f$1, view);
    }
}
