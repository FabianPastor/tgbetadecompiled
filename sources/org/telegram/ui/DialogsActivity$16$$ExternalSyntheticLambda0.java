package org.telegram.ui;

import android.content.DialogInterface;
import org.telegram.ui.DialogsActivity;

public final /* synthetic */ class DialogsActivity$16$$ExternalSyntheticLambda0 implements DialogInterface.OnClickListener {
    public final /* synthetic */ DialogsActivity.AnonymousClass16 f$0;

    public /* synthetic */ DialogsActivity$16$$ExternalSyntheticLambda0(DialogsActivity.AnonymousClass16 r1) {
        this.f$0 = r1;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f$0.lambda$needClearList$1(dialogInterface, i);
    }
}
