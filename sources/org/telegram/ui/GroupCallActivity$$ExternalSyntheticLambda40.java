package org.telegram.ui;

import java.util.ArrayList;

public final /* synthetic */ class GroupCallActivity$$ExternalSyntheticLambda40 implements Runnable {
    public final /* synthetic */ GroupCallActivity f$0;
    public final /* synthetic */ ArrayList f$1;

    public /* synthetic */ GroupCallActivity$$ExternalSyntheticLambda40(GroupCallActivity groupCallActivity, ArrayList arrayList) {
        this.f$0 = groupCallActivity;
        this.f$1 = arrayList;
    }

    public final void run() {
        this.f$0.lambda$fullscreenFor$35(this.f$1);
    }
}
