package org.telegram.ui;

import android.view.MotionEvent;
import android.view.View;

public final /* synthetic */ class ArticleViewer$$ExternalSyntheticLambda13 implements View.OnTouchListener {
    public final /* synthetic */ ArticleViewer f$0;

    public /* synthetic */ ArticleViewer$$ExternalSyntheticLambda13(ArticleViewer articleViewer) {
        this.f$0 = articleViewer;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return this.f$0.lambda$showPopup$2(view, motionEvent);
    }
}
