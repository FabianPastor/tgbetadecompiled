package org.telegram.ui;

import org.telegram.ui.ActionBar.AlertDialog;

public final /* synthetic */ class ChatLinkActivity$$ExternalSyntheticLambda9 implements Runnable {
    public final /* synthetic */ ChatLinkActivity f$0;
    public final /* synthetic */ AlertDialog[] f$1;

    public /* synthetic */ ChatLinkActivity$$ExternalSyntheticLambda9(ChatLinkActivity chatLinkActivity, AlertDialog[] alertDialogArr) {
        this.f$0 = chatLinkActivity;
        this.f$1 = alertDialogArr;
    }

    public final void run() {
        this.f$0.lambda$createView$1(this.f$1);
    }
}
