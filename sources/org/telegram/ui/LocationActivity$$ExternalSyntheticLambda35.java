package org.telegram.ui;

import org.telegram.ui.Components.ProximitySheet;

public final /* synthetic */ class LocationActivity$$ExternalSyntheticLambda35 implements ProximitySheet.onRadiusPickerChange {
    public final /* synthetic */ LocationActivity f$0;

    public /* synthetic */ LocationActivity$$ExternalSyntheticLambda35(LocationActivity locationActivity) {
        this.f$0 = locationActivity;
    }

    public final boolean run(boolean z, int i) {
        return this.f$0.lambda$openProximityAlert$21(z, i);
    }
}
