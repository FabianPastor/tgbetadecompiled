package org.telegram.ui;

import android.content.Context;
import android.view.View;
import android.widget.ViewSwitcher;

public final /* synthetic */ class PasscodeActivity$$ExternalSyntheticLambda10 implements ViewSwitcher.ViewFactory {
    public final /* synthetic */ Context f$0;

    public /* synthetic */ PasscodeActivity$$ExternalSyntheticLambda10(Context context) {
        this.f$0 = context;
    }

    public final View makeView() {
        return PasscodeActivity.lambda$createView$6(this.f$0);
    }
}
