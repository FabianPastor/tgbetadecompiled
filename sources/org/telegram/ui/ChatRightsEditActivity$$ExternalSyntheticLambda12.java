package org.telegram.ui;

import android.view.View;
import org.telegram.ui.ActionBar.BottomSheet;

public final /* synthetic */ class ChatRightsEditActivity$$ExternalSyntheticLambda12 implements View.OnClickListener {
    public final /* synthetic */ ChatRightsEditActivity f$0;
    public final /* synthetic */ BottomSheet.Builder f$1;

    public /* synthetic */ ChatRightsEditActivity$$ExternalSyntheticLambda12(ChatRightsEditActivity chatRightsEditActivity, BottomSheet.Builder builder) {
        this.f$0 = chatRightsEditActivity;
        this.f$1 = builder;
    }

    public final void onClick(View view) {
        this.f$0.lambda$createView$5(this.f$1, view);
    }
}
