package org.telegram.ui;

import org.telegram.ui.ContactAddActivity;

public final /* synthetic */ class ChatActivity$$ExternalSyntheticLambda253 implements ContactAddActivity.ContactAddActivityDelegate {
    public final /* synthetic */ ChatActivity f$0;

    public /* synthetic */ ChatActivity$$ExternalSyntheticLambda253(ChatActivity chatActivity) {
        this.f$0 = chatActivity;
    }

    public final void didAddToContacts() {
        this.f$0.lambda$createView$35();
    }
}
