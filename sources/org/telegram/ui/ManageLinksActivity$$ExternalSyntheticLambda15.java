package org.telegram.ui;

import org.telegram.ui.ActionBar.ThemeDescription;

public final /* synthetic */ class ManageLinksActivity$$ExternalSyntheticLambda15 implements ThemeDescription.ThemeDescriptionDelegate {
    public final /* synthetic */ ManageLinksActivity f$0;

    public /* synthetic */ ManageLinksActivity$$ExternalSyntheticLambda15(ManageLinksActivity manageLinksActivity) {
        this.f$0 = manageLinksActivity;
    }

    public final void didSetColor() {
        this.f$0.lambda$getThemeDescriptions$17();
    }

    public /* synthetic */ void onAnimationProgress(float f) {
        ThemeDescription.ThemeDescriptionDelegate.CC.$default$onAnimationProgress(this, f);
    }
}
