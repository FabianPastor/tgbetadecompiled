package org.telegram.ui;

import android.widget.PopupWindow;

public final /* synthetic */ class ProfileActivity$$ExternalSyntheticLambda17 implements PopupWindow.OnDismissListener {
    public final /* synthetic */ ProfileActivity f$0;

    public /* synthetic */ ProfileActivity$$ExternalSyntheticLambda17(ProfileActivity profileActivity) {
        this.f$0 = profileActivity;
    }

    public final void onDismiss() {
        this.f$0.lambda$createActionBar$1();
    }
}
