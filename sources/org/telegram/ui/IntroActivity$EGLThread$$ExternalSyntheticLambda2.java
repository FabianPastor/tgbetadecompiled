package org.telegram.ui;

import org.telegram.messenger.GenericProvider;
import org.telegram.ui.IntroActivity;

public final /* synthetic */ class IntroActivity$EGLThread$$ExternalSyntheticLambda2 implements GenericProvider {
    public static final /* synthetic */ IntroActivity$EGLThread$$ExternalSyntheticLambda2 INSTANCE = new IntroActivity$EGLThread$$ExternalSyntheticLambda2();

    private /* synthetic */ IntroActivity$EGLThread$$ExternalSyntheticLambda2() {
    }

    public final Object provide(Object obj) {
        return IntroActivity.EGLThread.lambda$new$0((Void) obj);
    }
}
