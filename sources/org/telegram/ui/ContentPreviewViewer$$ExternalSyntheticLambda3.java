package org.telegram.ui;

import android.view.MotionEvent;
import android.view.View;

public final /* synthetic */ class ContentPreviewViewer$$ExternalSyntheticLambda3 implements View.OnTouchListener {
    public final /* synthetic */ ContentPreviewViewer f$0;

    public /* synthetic */ ContentPreviewViewer$$ExternalSyntheticLambda3(ContentPreviewViewer contentPreviewViewer) {
        this.f$0 = contentPreviewViewer;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return this.f$0.lambda$setParentActivity$5(view, motionEvent);
    }
}
