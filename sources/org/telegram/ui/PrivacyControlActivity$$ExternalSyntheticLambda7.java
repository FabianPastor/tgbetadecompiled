package org.telegram.ui;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class PrivacyControlActivity$$ExternalSyntheticLambda7 implements RecyclerListView.OnItemClickListener {
    public final /* synthetic */ PrivacyControlActivity f$0;

    public /* synthetic */ PrivacyControlActivity$$ExternalSyntheticLambda7(PrivacyControlActivity privacyControlActivity) {
        this.f$0 = privacyControlActivity;
    }

    public final void onItemClick(View view, int i) {
        this.f$0.lambda$createView$2(view, i);
    }
}
