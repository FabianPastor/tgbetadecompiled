package org.telegram.ui;

import org.telegram.tgnet.TLObject;
import org.telegram.ui.ActionBar.AlertDialog;

public final /* synthetic */ class LaunchActivity$$ExternalSyntheticLambda52 implements Runnable {
    public final /* synthetic */ LaunchActivity f$0;
    public final /* synthetic */ AlertDialog f$1;
    public final /* synthetic */ TLObject f$2;

    public /* synthetic */ LaunchActivity$$ExternalSyntheticLambda52(LaunchActivity launchActivity, AlertDialog alertDialog, TLObject tLObject) {
        this.f$0 = launchActivity;
        this.f$1 = alertDialog;
        this.f$2 = tLObject;
    }

    public final void run() {
        this.f$0.lambda$runLinkRequest$55(this.f$1, this.f$2);
    }
}
