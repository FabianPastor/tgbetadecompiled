package org.telegram.ui;

import org.telegram.ui.InviteContactsActivity;

public final /* synthetic */ class InviteContactsActivity$InviteAdapter$1$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ InviteContactsActivity.InviteAdapter.AnonymousClass1 f$0;
    public final /* synthetic */ String f$1;

    public /* synthetic */ InviteContactsActivity$InviteAdapter$1$$ExternalSyntheticLambda0(InviteContactsActivity.InviteAdapter.AnonymousClass1 r1, String str) {
        this.f$0 = r1;
        this.f$1 = str;
    }

    public final void run() {
        this.f$0.lambda$run$0(this.f$1);
    }
}
