package org.telegram.ui;

import android.content.DialogInterface;
import org.telegram.ui.WallpapersListActivity;

public final /* synthetic */ class WallpapersListActivity$2$$ExternalSyntheticLambda0 implements DialogInterface.OnClickListener {
    public final /* synthetic */ WallpapersListActivity.AnonymousClass2 f$0;

    public /* synthetic */ WallpapersListActivity$2$$ExternalSyntheticLambda0(WallpapersListActivity.AnonymousClass2 r1) {
        this.f$0 = r1;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f$0.lambda$onItemClick$2(dialogInterface, i);
    }
}
