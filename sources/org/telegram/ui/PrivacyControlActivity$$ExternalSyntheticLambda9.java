package org.telegram.ui;

import java.util.ArrayList;
import org.telegram.ui.PrivacyUsersActivity;

public final /* synthetic */ class PrivacyControlActivity$$ExternalSyntheticLambda9 implements PrivacyUsersActivity.PrivacyActivityDelegate {
    public final /* synthetic */ PrivacyControlActivity f$0;
    public final /* synthetic */ int f$1;

    public /* synthetic */ PrivacyControlActivity$$ExternalSyntheticLambda9(PrivacyControlActivity privacyControlActivity, int i) {
        this.f$0 = privacyControlActivity;
        this.f$1 = i;
    }

    public final void didUpdateUserList(ArrayList arrayList, boolean z) {
        this.f$0.lambda$createView$1(this.f$1, arrayList, z);
    }
}
