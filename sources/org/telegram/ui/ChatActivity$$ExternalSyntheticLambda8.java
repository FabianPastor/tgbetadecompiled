package org.telegram.ui;

import android.animation.ValueAnimator;

public final /* synthetic */ class ChatActivity$$ExternalSyntheticLambda8 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ ChatActivity f$0;

    public /* synthetic */ ChatActivity$$ExternalSyntheticLambda8(ChatActivity chatActivity) {
        this.f$0 = chatActivity;
    }

    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f$0.lambda$dimBehindView$77(valueAnimator);
    }
}
