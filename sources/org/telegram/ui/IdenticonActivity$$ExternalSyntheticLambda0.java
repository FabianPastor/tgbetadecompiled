package org.telegram.ui;

import android.view.MotionEvent;
import android.view.View;

public final /* synthetic */ class IdenticonActivity$$ExternalSyntheticLambda0 implements View.OnTouchListener {
    public static final /* synthetic */ IdenticonActivity$$ExternalSyntheticLambda0 INSTANCE = new IdenticonActivity$$ExternalSyntheticLambda0();

    private /* synthetic */ IdenticonActivity$$ExternalSyntheticLambda0() {
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return IdenticonActivity.lambda$createView$0(view, motionEvent);
    }
}
