package org.telegram.ui;

import com.google.android.gms.maps.MapView;

public final /* synthetic */ class LocationActivity$$ExternalSyntheticLambda21 implements Runnable {
    public final /* synthetic */ LocationActivity f$0;
    public final /* synthetic */ MapView f$1;

    public /* synthetic */ LocationActivity$$ExternalSyntheticLambda21(LocationActivity locationActivity, MapView mapView) {
        this.f$0 = locationActivity;
        this.f$1 = mapView;
    }

    public final void run() {
        this.f$0.lambda$createView$16(this.f$1);
    }
}
