package org.telegram.ui;

import org.telegram.ui.Adapters.StickersAdapter;

public final /* synthetic */ class ChatActivity$$ExternalSyntheticLambda235 implements StickersAdapter.StickersAdapterDelegate {
    public final /* synthetic */ ChatActivity f$0;

    public /* synthetic */ ChatActivity$$ExternalSyntheticLambda235(ChatActivity chatActivity) {
        this.f$0 = chatActivity;
    }

    public final void needChangePanelVisibility(boolean z) {
        this.f$0.lambda$initStickers$89(z);
    }
}
