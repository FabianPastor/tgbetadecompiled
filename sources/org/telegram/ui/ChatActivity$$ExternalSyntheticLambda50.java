package org.telegram.ui;

import android.content.DialogInterface;

public final /* synthetic */ class ChatActivity$$ExternalSyntheticLambda50 implements DialogInterface.OnDismissListener {
    public final /* synthetic */ ChatActivity f$0;

    public /* synthetic */ ChatActivity$$ExternalSyntheticLambda50(ChatActivity chatActivity) {
        this.f$0 = chatActivity;
    }

    public final void onDismiss(DialogInterface dialogInterface) {
        this.f$0.lambda$processSelectedOption$217(dialogInterface);
    }
}
