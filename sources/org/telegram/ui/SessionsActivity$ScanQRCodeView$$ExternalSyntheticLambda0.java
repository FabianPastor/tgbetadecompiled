package org.telegram.ui;

import android.view.View;
import org.telegram.ui.SessionsActivity;

public final /* synthetic */ class SessionsActivity$ScanQRCodeView$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ SessionsActivity.ScanQRCodeView f$0;

    public /* synthetic */ SessionsActivity$ScanQRCodeView$$ExternalSyntheticLambda0(SessionsActivity.ScanQRCodeView scanQRCodeView) {
        this.f$0 = scanQRCodeView;
    }

    public final void onClick(View view) {
        this.f$0.lambda$new$0(view);
    }
}
