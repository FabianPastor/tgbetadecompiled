package org.telegram.ui;

public final /* synthetic */ class LoginActivity$$ExternalSyntheticLambda16 implements Runnable {
    public final /* synthetic */ LoginActivity f$0;
    public final /* synthetic */ int f$1;
    public final /* synthetic */ boolean f$2;
    public final /* synthetic */ boolean f$3;

    public /* synthetic */ LoginActivity$$ExternalSyntheticLambda16(LoginActivity loginActivity, int i, boolean z, boolean z2) {
        this.f$0 = loginActivity;
        this.f$1 = i;
        this.f$2 = z;
        this.f$3 = z2;
    }

    public final void run() {
        this.f$0.lambda$showEditDoneProgress$14(this.f$1, this.f$2, this.f$3);
    }
}
