package org.telegram.ui;

import android.view.MotionEvent;
import android.view.View;

public final /* synthetic */ class WallpapersListActivity$$ExternalSyntheticLambda1 implements View.OnTouchListener {
    public static final /* synthetic */ WallpapersListActivity$$ExternalSyntheticLambda1 INSTANCE = new WallpapersListActivity$$ExternalSyntheticLambda1();

    private /* synthetic */ WallpapersListActivity$$ExternalSyntheticLambda1() {
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return WallpapersListActivity.lambda$createView$0(view, motionEvent);
    }
}
