package org.telegram.ui;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class TwoStepVerificationActivity$$ExternalSyntheticLambda38 implements RecyclerListView.OnItemClickListener {
    public final /* synthetic */ TwoStepVerificationActivity f$0;

    public /* synthetic */ TwoStepVerificationActivity$$ExternalSyntheticLambda38(TwoStepVerificationActivity twoStepVerificationActivity) {
        this.f$0 = twoStepVerificationActivity;
    }

    public final void onItemClick(View view, int i) {
        this.f$0.lambda$createView$7(view, i);
    }
}
