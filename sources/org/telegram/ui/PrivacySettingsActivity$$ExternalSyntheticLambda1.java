package org.telegram.ui;

import android.content.DialogInterface;

public final /* synthetic */ class PrivacySettingsActivity$$ExternalSyntheticLambda1 implements DialogInterface.OnClickListener {
    public final /* synthetic */ PrivacySettingsActivity f$0;

    public /* synthetic */ PrivacySettingsActivity$$ExternalSyntheticLambda1(PrivacySettingsActivity privacySettingsActivity) {
        this.f$0 = privacySettingsActivity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f$0.lambda$createView$13(dialogInterface, i);
    }
}
