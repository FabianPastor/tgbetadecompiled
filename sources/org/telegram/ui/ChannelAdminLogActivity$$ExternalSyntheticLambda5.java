package org.telegram.ui;

import android.view.MotionEvent;
import android.view.View;

public final /* synthetic */ class ChannelAdminLogActivity$$ExternalSyntheticLambda5 implements View.OnTouchListener {
    public static final /* synthetic */ ChannelAdminLogActivity$$ExternalSyntheticLambda5 INSTANCE = new ChannelAdminLogActivity$$ExternalSyntheticLambda5();

    private /* synthetic */ ChannelAdminLogActivity$$ExternalSyntheticLambda5() {
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return ChannelAdminLogActivity.lambda$createView$2(view, motionEvent);
    }
}
