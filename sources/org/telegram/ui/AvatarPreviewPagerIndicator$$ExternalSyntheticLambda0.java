package org.telegram.ui;

import android.animation.ValueAnimator;

public final /* synthetic */ class AvatarPreviewPagerIndicator$$ExternalSyntheticLambda0 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ AvatarPreviewPagerIndicator f$0;

    public /* synthetic */ AvatarPreviewPagerIndicator$$ExternalSyntheticLambda0(AvatarPreviewPagerIndicator avatarPreviewPagerIndicator) {
        this.f$0 = avatarPreviewPagerIndicator;
    }

    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f$0.lambda$new$0(valueAnimator);
    }
}
