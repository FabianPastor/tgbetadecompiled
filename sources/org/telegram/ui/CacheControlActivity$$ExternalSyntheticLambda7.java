package org.telegram.ui;

import org.telegram.ui.ActionBar.ThemeDescription;

public final /* synthetic */ class CacheControlActivity$$ExternalSyntheticLambda7 implements ThemeDescription.ThemeDescriptionDelegate {
    public final /* synthetic */ CacheControlActivity f$0;

    public /* synthetic */ CacheControlActivity$$ExternalSyntheticLambda7(CacheControlActivity cacheControlActivity) {
        this.f$0 = cacheControlActivity;
    }

    public final void didSetColor() {
        this.f$0.lambda$getThemeDescriptions$8();
    }

    public /* synthetic */ void onAnimationProgress(float f) {
        ThemeDescription.ThemeDescriptionDelegate.CC.$default$onAnimationProgress(this, f);
    }
}
