package org.telegram.ui;

import android.view.View;

public final /* synthetic */ class ChatActivity$$ExternalSyntheticLambda97 implements View.OnLongClickListener {
    public final /* synthetic */ ChatActivity f$0;

    public /* synthetic */ ChatActivity$$ExternalSyntheticLambda97(ChatActivity chatActivity) {
        this.f$0 = chatActivity;
    }

    public final boolean onLongClick(View view) {
        return this.f$0.lambda$createView$41(view);
    }
}
