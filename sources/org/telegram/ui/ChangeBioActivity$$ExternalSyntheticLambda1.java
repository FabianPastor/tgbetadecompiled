package org.telegram.ui;

import android.view.MotionEvent;
import android.view.View;

public final /* synthetic */ class ChangeBioActivity$$ExternalSyntheticLambda1 implements View.OnTouchListener {
    public static final /* synthetic */ ChangeBioActivity$$ExternalSyntheticLambda1 INSTANCE = new ChangeBioActivity$$ExternalSyntheticLambda1();

    private /* synthetic */ ChangeBioActivity$$ExternalSyntheticLambda1() {
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return ChangeBioActivity.lambda$createView$0(view, motionEvent);
    }
}
