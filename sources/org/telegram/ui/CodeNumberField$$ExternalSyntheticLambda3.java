package org.telegram.ui;

import org.telegram.ui.Components.SimpleFloatPropertyCompat;

public final /* synthetic */ class CodeNumberField$$ExternalSyntheticLambda3 implements SimpleFloatPropertyCompat.Getter {
    public static final /* synthetic */ CodeNumberField$$ExternalSyntheticLambda3 INSTANCE = new CodeNumberField$$ExternalSyntheticLambda3();

    private /* synthetic */ CodeNumberField$$ExternalSyntheticLambda3() {
    }

    public final float get(Object obj) {
        return ((CodeNumberField) obj).errorProgress;
    }
}
