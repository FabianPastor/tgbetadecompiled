package org.telegram.ui;

import android.content.Context;
import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class CacheControlActivity$$ExternalSyntheticLambda8 implements RecyclerListView.OnItemClickListener {
    public final /* synthetic */ CacheControlActivity f$0;
    public final /* synthetic */ Context f$1;

    public /* synthetic */ CacheControlActivity$$ExternalSyntheticLambda8(CacheControlActivity cacheControlActivity, Context context) {
        this.f$0 = cacheControlActivity;
        this.f$1 = context;
    }

    public final void onItemClick(View view, int i) {
        this.f$0.lambda$createView$6(this.f$1, view, i);
    }
}
