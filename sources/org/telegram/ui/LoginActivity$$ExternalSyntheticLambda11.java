package org.telegram.ui;

import androidx.dynamicanimation.animation.DynamicAnimation;

public final /* synthetic */ class LoginActivity$$ExternalSyntheticLambda11 implements DynamicAnimation.OnAnimationUpdateListener {
    public final /* synthetic */ LoginActivity f$0;

    public /* synthetic */ LoginActivity$$ExternalSyntheticLambda11(LoginActivity loginActivity) {
        this.f$0 = loginActivity;
    }

    public final void onAnimationUpdate(DynamicAnimation dynamicAnimation, float f, float f2) {
        this.f$0.lambda$createView$2(dynamicAnimation, f, f2);
    }
}
