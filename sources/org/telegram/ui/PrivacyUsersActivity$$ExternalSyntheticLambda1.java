package org.telegram.ui;

import org.telegram.ui.ActionBar.ThemeDescription;

public final /* synthetic */ class PrivacyUsersActivity$$ExternalSyntheticLambda1 implements ThemeDescription.ThemeDescriptionDelegate {
    public final /* synthetic */ PrivacyUsersActivity f$0;

    public /* synthetic */ PrivacyUsersActivity$$ExternalSyntheticLambda1(PrivacyUsersActivity privacyUsersActivity) {
        this.f$0 = privacyUsersActivity;
    }

    public final void didSetColor() {
        this.f$0.lambda$getThemeDescriptions$4();
    }

    public /* synthetic */ void onAnimationProgress(float f) {
        ThemeDescription.ThemeDescriptionDelegate.CC.$default$onAnimationProgress(this, f);
    }
}
