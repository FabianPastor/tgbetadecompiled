package org.telegram.ui;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class ChatReactionsEditActivity$$ExternalSyntheticLambda2 implements RecyclerListView.OnItemClickListener {
    public final /* synthetic */ ChatReactionsEditActivity f$0;

    public /* synthetic */ ChatReactionsEditActivity$$ExternalSyntheticLambda2(ChatReactionsEditActivity chatReactionsEditActivity) {
        this.f$0 = chatReactionsEditActivity;
    }

    public final void onItemClick(View view, int i) {
        this.f$0.lambda$createView$1(view, i);
    }
}
