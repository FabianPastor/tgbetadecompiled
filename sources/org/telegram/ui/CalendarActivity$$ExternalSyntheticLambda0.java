package org.telegram.ui;

import android.animation.ValueAnimator;

public final /* synthetic */ class CalendarActivity$$ExternalSyntheticLambda0 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ CalendarActivity f$0;

    public /* synthetic */ CalendarActivity$$ExternalSyntheticLambda0(CalendarActivity calendarActivity) {
        this.f$0 = calendarActivity;
    }

    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f$0.lambda$animateSelection$4(valueAnimator);
    }
}
