package org.telegram.ui;

import android.view.MotionEvent;
import android.view.View;

public final /* synthetic */ class PhotoAlbumPickerActivity$$ExternalSyntheticLambda4 implements View.OnTouchListener {
    public static final /* synthetic */ PhotoAlbumPickerActivity$$ExternalSyntheticLambda4 INSTANCE = new PhotoAlbumPickerActivity$$ExternalSyntheticLambda4();

    private /* synthetic */ PhotoAlbumPickerActivity$$ExternalSyntheticLambda4() {
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return PhotoAlbumPickerActivity.lambda$createView$1(view, motionEvent);
    }
}
