package org.telegram.ui;

import org.telegram.ui.ChatUsersActivity;

public final /* synthetic */ class ChatUsersActivity$SearchAdapter$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ ChatUsersActivity.SearchAdapter f$0;
    public final /* synthetic */ String f$1;

    public /* synthetic */ ChatUsersActivity$SearchAdapter$$ExternalSyntheticLambda0(ChatUsersActivity.SearchAdapter searchAdapter, String str) {
        this.f$0 = searchAdapter;
        this.f$1 = str;
    }

    public final void run() {
        this.f$0.lambda$processSearch$3(this.f$1);
    }
}
