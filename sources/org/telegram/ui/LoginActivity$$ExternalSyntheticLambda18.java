package org.telegram.ui;

import org.telegram.messenger.GenericProvider;

public final /* synthetic */ class LoginActivity$$ExternalSyntheticLambda18 implements GenericProvider {
    public final /* synthetic */ boolean f$0;

    public /* synthetic */ LoginActivity$$ExternalSyntheticLambda18(boolean z) {
        this.f$0 = z;
    }

    public final Object provide(Object obj) {
        return LoginActivity.lambda$needFinishActivity$16(this.f$0, (Void) obj);
    }
}
