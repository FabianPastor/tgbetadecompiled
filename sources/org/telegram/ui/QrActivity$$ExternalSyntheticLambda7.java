package org.telegram.ui;

import android.graphics.Bitmap;

public final /* synthetic */ class QrActivity$$ExternalSyntheticLambda7 implements Runnable {
    public final /* synthetic */ QrActivity f$0;
    public final /* synthetic */ Bitmap f$1;

    public /* synthetic */ QrActivity$$ExternalSyntheticLambda7(QrActivity qrActivity, Bitmap bitmap) {
        this.f$0 = qrActivity;
        this.f$1 = bitmap;
    }

    public final void run() {
        this.f$0.lambda$onItemSelected$6(this.f$1);
    }
}
