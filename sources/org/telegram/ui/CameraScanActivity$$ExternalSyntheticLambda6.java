package org.telegram.ui;

import androidx.dynamicanimation.animation.DynamicAnimation;

public final /* synthetic */ class CameraScanActivity$$ExternalSyntheticLambda6 implements DynamicAnimation.OnAnimationUpdateListener {
    public final /* synthetic */ CameraScanActivity f$0;

    public /* synthetic */ CameraScanActivity$$ExternalSyntheticLambda6(CameraScanActivity cameraScanActivity) {
        this.f$0 = cameraScanActivity;
    }

    public final void onAnimationUpdate(DynamicAnimation dynamicAnimation, float f, float f2) {
        this.f$0.lambda$initCameraView$7(dynamicAnimation, f, f2);
    }
}
