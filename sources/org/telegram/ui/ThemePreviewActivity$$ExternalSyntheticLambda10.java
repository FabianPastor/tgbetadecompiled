package org.telegram.ui;

import android.view.View;

public final /* synthetic */ class ThemePreviewActivity$$ExternalSyntheticLambda10 implements View.OnClickListener {
    public final /* synthetic */ ThemePreviewActivity f$0;
    public final /* synthetic */ int f$1;

    public /* synthetic */ ThemePreviewActivity$$ExternalSyntheticLambda10(ThemePreviewActivity themePreviewActivity, int i) {
        this.f$0 = themePreviewActivity;
        this.f$1 = i;
    }

    public final void onClick(View view) {
        this.f$0.lambda$createView$9(this.f$1, view);
    }
}
