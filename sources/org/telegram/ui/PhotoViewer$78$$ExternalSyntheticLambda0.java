package org.telegram.ui;

import org.telegram.ui.PhotoViewer;

public final /* synthetic */ class PhotoViewer$78$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ PhotoViewer.AnonymousClass78 f$0;
    public final /* synthetic */ Runnable f$1;
    public final /* synthetic */ int[] f$2;
    public final /* synthetic */ int f$3;

    public /* synthetic */ PhotoViewer$78$$ExternalSyntheticLambda0(PhotoViewer.AnonymousClass78 r1, Runnable runnable, int[] iArr, int i) {
        this.f$0 = r1;
        this.f$1 = runnable;
        this.f$2 = iArr;
        this.f$3 = i;
    }

    public final void run() {
        this.f$0.lambda$run$0(this.f$1, this.f$2, this.f$3);
    }
}
