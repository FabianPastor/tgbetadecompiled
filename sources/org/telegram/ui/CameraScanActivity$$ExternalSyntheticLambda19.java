package org.telegram.ui;

import org.telegram.messenger.camera.CameraView;

public final /* synthetic */ class CameraScanActivity$$ExternalSyntheticLambda19 implements CameraView.CameraViewDelegate {
    public final /* synthetic */ CameraScanActivity f$0;

    public /* synthetic */ CameraScanActivity$$ExternalSyntheticLambda19(CameraScanActivity cameraScanActivity) {
        this.f$0 = cameraScanActivity;
    }

    public final void onCameraInit() {
        this.f$0.lambda$initCameraView$9();
    }
}
