package org.telegram.ui;

import android.content.DialogInterface;

public final /* synthetic */ class NotificationsSettingsActivity$$ExternalSyntheticLambda0 implements DialogInterface.OnClickListener {
    public final /* synthetic */ NotificationsSettingsActivity f$0;

    public /* synthetic */ NotificationsSettingsActivity$$ExternalSyntheticLambda0(NotificationsSettingsActivity notificationsSettingsActivity) {
        this.f$0 = notificationsSettingsActivity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f$0.lambda$createView$4(dialogInterface, i);
    }
}
