package org.telegram.ui;

import android.view.View;
import org.telegram.ui.LoginActivity;

public final /* synthetic */ class LoginActivity$LoginActivityRecoverView$$ExternalSyntheticLambda3 implements View.OnFocusChangeListener {
    public final /* synthetic */ LoginActivity.LoginActivityRecoverView f$0;

    public /* synthetic */ LoginActivity$LoginActivityRecoverView$$ExternalSyntheticLambda3(LoginActivity.LoginActivityRecoverView loginActivityRecoverView) {
        this.f$0 = loginActivityRecoverView;
    }

    public final void onFocusChange(View view, boolean z) {
        this.f$0.lambda$new$1(view, z);
    }
}
