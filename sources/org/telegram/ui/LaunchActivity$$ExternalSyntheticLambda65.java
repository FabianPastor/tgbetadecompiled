package org.telegram.ui;

import org.telegram.messenger.GenericProvider;

public final /* synthetic */ class LaunchActivity$$ExternalSyntheticLambda65 implements GenericProvider {
    public static final /* synthetic */ LaunchActivity$$ExternalSyntheticLambda65 INSTANCE = new LaunchActivity$$ExternalSyntheticLambda65();

    private /* synthetic */ LaunchActivity$$ExternalSyntheticLambda65() {
    }

    public final Object provide(Object obj) {
        return LaunchActivity.lambda$switchToAccount$7((Void) obj);
    }
}
