package org.telegram.ui;

import android.os.Bundle;
import org.telegram.ui.ActionBar.AlertDialog;
import org.telegram.ui.ActionBar.BaseFragment;

public final /* synthetic */ class LaunchActivity$$ExternalSyntheticLambda36 implements Runnable {
    public final /* synthetic */ LaunchActivity f$0;
    public final /* synthetic */ Bundle f$1;
    public final /* synthetic */ Long f$2;
    public final /* synthetic */ int[] f$3;
    public final /* synthetic */ AlertDialog f$4;
    public final /* synthetic */ BaseFragment f$5;
    public final /* synthetic */ int f$6;

    public /* synthetic */ LaunchActivity$$ExternalSyntheticLambda36(LaunchActivity launchActivity, Bundle bundle, Long l, int[] iArr, AlertDialog alertDialog, BaseFragment baseFragment, int i) {
        this.f$0 = launchActivity;
        this.f$1 = bundle;
        this.f$2 = l;
        this.f$3 = iArr;
        this.f$4 = alertDialog;
        this.f$5 = baseFragment;
        this.f$6 = i;
    }

    public final void run() {
        this.f$0.lambda$runLinkRequest$69(this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, this.f$6);
    }
}
