package org.telegram.ui;

public final /* synthetic */ class TwoStepVerificationSetupActivity$$ExternalSyntheticLambda40 implements Runnable {
    public final /* synthetic */ TwoStepVerificationSetupActivity f$0;
    public final /* synthetic */ byte[] f$1;

    public /* synthetic */ TwoStepVerificationSetupActivity$$ExternalSyntheticLambda40(TwoStepVerificationSetupActivity twoStepVerificationSetupActivity, byte[] bArr) {
        this.f$0 = twoStepVerificationSetupActivity;
        this.f$1 = bArr;
    }

    public final void run() {
        this.f$0.lambda$processNext$28(this.f$1);
    }
}
