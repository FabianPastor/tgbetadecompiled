package org.telegram.ui;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class PremiumPreviewFragment$$ExternalSyntheticLambda11 implements RecyclerListView.OnItemClickListener {
    public final /* synthetic */ PremiumPreviewFragment f$0;

    public /* synthetic */ PremiumPreviewFragment$$ExternalSyntheticLambda11(PremiumPreviewFragment premiumPreviewFragment) {
        this.f$0 = premiumPreviewFragment;
    }

    public final void onItemClick(View view, int i) {
        this.f$0.lambda$createView$0(view, i);
    }
}
