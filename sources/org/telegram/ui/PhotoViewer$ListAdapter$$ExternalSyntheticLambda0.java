package org.telegram.ui;

import android.view.View;
import org.telegram.ui.PhotoViewer;

public final /* synthetic */ class PhotoViewer$ListAdapter$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ PhotoViewer.ListAdapter f$0;

    public /* synthetic */ PhotoViewer$ListAdapter$$ExternalSyntheticLambda0(PhotoViewer.ListAdapter listAdapter) {
        this.f$0 = listAdapter;
    }

    public final void onClick(View view) {
        this.f$0.lambda$onCreateViewHolder$0(view);
    }
}
