package org.telegram.ui;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class GroupInviteActivity$$ExternalSyntheticLambda3 implements RecyclerListView.OnItemClickListener {
    public final /* synthetic */ GroupInviteActivity f$0;

    public /* synthetic */ GroupInviteActivity$$ExternalSyntheticLambda3(GroupInviteActivity groupInviteActivity) {
        this.f$0 = groupInviteActivity;
    }

    public final void onItemClick(View view, int i) {
        this.f$0.lambda$createView$1(view, i);
    }
}
