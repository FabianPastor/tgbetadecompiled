package org.telegram.ui;

public final /* synthetic */ class ChatActivity$$ExternalSyntheticLambda197 implements Runnable {
    public final /* synthetic */ ChatActivity f$0;
    public final /* synthetic */ boolean f$1;
    public final /* synthetic */ boolean f$2;
    public final /* synthetic */ boolean f$3;

    public /* synthetic */ ChatActivity$$ExternalSyntheticLambda197(ChatActivity chatActivity, boolean z, boolean z2, boolean z3) {
        this.f$0 = chatActivity;
        this.f$1 = z;
        this.f$2 = z2;
        this.f$3 = z3;
    }

    public final void run() {
        this.f$0.lambda$processSelectedOption$193(this.f$1, this.f$2, this.f$3);
    }
}
