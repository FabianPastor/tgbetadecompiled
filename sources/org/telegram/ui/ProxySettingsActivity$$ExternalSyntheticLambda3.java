package org.telegram.ui;

import android.view.View;

public final /* synthetic */ class ProxySettingsActivity$$ExternalSyntheticLambda3 implements View.OnClickListener {
    public final /* synthetic */ ProxySettingsActivity f$0;

    public /* synthetic */ ProxySettingsActivity$$ExternalSyntheticLambda3(ProxySettingsActivity proxySettingsActivity) {
        this.f$0 = proxySettingsActivity;
    }

    public final void onClick(View view) {
        this.f$0.lambda$createView$3(view);
    }
}
