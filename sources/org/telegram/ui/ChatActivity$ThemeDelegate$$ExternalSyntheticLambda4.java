package org.telegram.ui;

import org.telegram.ui.ChatActivity;

public final /* synthetic */ class ChatActivity$ThemeDelegate$$ExternalSyntheticLambda4 implements Runnable {
    public final /* synthetic */ ChatActivity.ThemeDelegate f$0;

    public /* synthetic */ ChatActivity$ThemeDelegate$$ExternalSyntheticLambda4(ChatActivity.ThemeDelegate themeDelegate) {
        this.f$0 = themeDelegate;
    }

    public final void run() {
        this.f$0.lambda$setCurrentTheme$3();
    }
}
