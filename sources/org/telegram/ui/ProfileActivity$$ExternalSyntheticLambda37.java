package org.telegram.ui;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

public final /* synthetic */ class ProfileActivity$$ExternalSyntheticLambda37 implements RecyclerListView.OnItemClickListener {
    public final /* synthetic */ ProfileActivity f$0;

    public /* synthetic */ ProfileActivity$$ExternalSyntheticLambda37(ProfileActivity profileActivity) {
        this.f$0 = profileActivity;
    }

    public final void onItemClick(View view, int i) {
        this.f$0.lambda$createView$5(view, i);
    }
}
