package org.telegram.ui;

import android.view.View;

public final /* synthetic */ class ChatActivity$$ExternalSyntheticLambda94 implements View.OnClickListener {
    public final /* synthetic */ boolean[] f$0;

    public /* synthetic */ ChatActivity$$ExternalSyntheticLambda94(boolean[] zArr) {
        this.f$0 = zArr;
    }

    public final void onClick(View view) {
        ChatActivity.lambda$processSelectedOption$196(this.f$0, view);
    }
}
