package org.telegram.ui;

import android.view.View;
import android.widget.FrameLayout;

public final /* synthetic */ class DialogsActivity$$ExternalSyntheticLambda27 implements View.OnLongClickListener {
    public final /* synthetic */ DialogsActivity f$0;
    public final /* synthetic */ FrameLayout f$1;

    public /* synthetic */ DialogsActivity$$ExternalSyntheticLambda27(DialogsActivity dialogsActivity, FrameLayout frameLayout) {
        this.f$0 = dialogsActivity;
        this.f$1 = frameLayout;
    }

    public final boolean onLongClick(View view) {
        return this.f$0.lambda$createView$11(this.f$1, view);
    }
}
