package org.telegram.ui;

import android.view.View;

public final /* synthetic */ class LaunchActivity$$ExternalSyntheticLambda16 implements View.OnClickListener {
    public final /* synthetic */ LaunchActivity f$0;

    public /* synthetic */ LaunchActivity$$ExternalSyntheticLambda16(LaunchActivity launchActivity) {
        this.f$0 = launchActivity;
    }

    public final void onClick(View view) {
        this.f$0.lambda$showLanguageAlertInternal$97(view);
    }
}
