package org.telegram.ui;

import android.view.View;

public final /* synthetic */ class LinkEditActivity$$ExternalSyntheticLambda2 implements View.OnClickListener {
    public final /* synthetic */ LinkEditActivity f$0;

    public /* synthetic */ LinkEditActivity$$ExternalSyntheticLambda2(LinkEditActivity linkEditActivity) {
        this.f$0 = linkEditActivity;
    }

    public final void onClick(View view) {
        this.f$0.onCreateClicked(view);
    }
}
