package org.telegram.ui;

import android.widget.PopupWindow;

public final /* synthetic */ class ArticleViewer$$ExternalSyntheticLambda15 implements PopupWindow.OnDismissListener {
    public final /* synthetic */ ArticleViewer f$0;

    public /* synthetic */ ArticleViewer$$ExternalSyntheticLambda15(ArticleViewer articleViewer) {
        this.f$0 = articleViewer;
    }

    public final void onDismiss() {
        this.f$0.lambda$showPopup$5();
    }
}
