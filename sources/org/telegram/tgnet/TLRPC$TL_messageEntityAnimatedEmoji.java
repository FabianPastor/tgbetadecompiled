package org.telegram.tgnet;

public class TLRPC$TL_messageEntityAnimatedEmoji extends TLRPC$MessageEntity {
    public static int constructor = NUM;
    public int length;
    public int offset;

    public void readParams(AbstractSerializedData abstractSerializedData, boolean z) {
        this.offset = abstractSerializedData.readInt32(z);
        this.length = abstractSerializedData.readInt32(z);
    }

    public void serializeToStream(AbstractSerializedData abstractSerializedData) {
        abstractSerializedData.writeInt32(constructor);
        abstractSerializedData.writeInt32(this.offset);
        abstractSerializedData.writeInt32(this.length);
    }
}
