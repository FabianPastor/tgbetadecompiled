package org.telegram.tgnet;

public class TLRPC$TL_themeDocumentNotModified_layer106 extends TLRPC$TL_theme {
    public static int constructor = NUM;

    public void serializeToStream(AbstractSerializedData abstractSerializedData) {
        abstractSerializedData.writeInt32(constructor);
    }
}
