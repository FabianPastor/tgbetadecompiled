package org.telegram.messenger;

public final /* synthetic */ class MediaController$$ExternalSyntheticLambda15 implements Runnable {
    public final /* synthetic */ MediaController f$0;
    public final /* synthetic */ int f$1;

    public /* synthetic */ MediaController$$ExternalSyntheticLambda15(MediaController mediaController, int i) {
        this.f$0 = mediaController;
        this.f$1 = i;
    }

    public final void run() {
        this.f$0.lambda$onAudioFocusChange$5(this.f$1);
    }
}
