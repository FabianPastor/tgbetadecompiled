package org.telegram.messenger;

import java.util.HashMap;

public final /* synthetic */ class ContactsController$$ExternalSyntheticLambda30 implements Runnable {
    public final /* synthetic */ ContactsController f$0;
    public final /* synthetic */ HashMap f$1;
    public final /* synthetic */ HashMap f$2;

    public /* synthetic */ ContactsController$$ExternalSyntheticLambda30(ContactsController contactsController, HashMap hashMap, HashMap hashMap2) {
        this.f$0 = contactsController;
        this.f$1 = hashMap;
        this.f$2 = hashMap2;
    }

    public final void run() {
        this.f$0.lambda$processLoadedContacts$35(this.f$1, this.f$2);
    }
}
