package org.telegram.messenger;

import android.view.View;

public final /* synthetic */ class AndroidUtilities$$ExternalSyntheticLambda5 implements View.OnClickListener {
    public final /* synthetic */ String f$0;
    public final /* synthetic */ String f$1;
    public final /* synthetic */ String f$2;
    public final /* synthetic */ String f$3;
    public final /* synthetic */ String f$4;
    public final /* synthetic */ Runnable f$5;

    public /* synthetic */ AndroidUtilities$$ExternalSyntheticLambda5(String str, String str2, String str3, String str4, String str5, Runnable runnable) {
        this.f$0 = str;
        this.f$1 = str2;
        this.f$2 = str3;
        this.f$3 = str4;
        this.f$4 = str5;
        this.f$5 = runnable;
    }

    public final void onClick(View view) {
        AndroidUtilities.lambda$showProxyAlert$10(this.f$0, this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, view);
    }
}
