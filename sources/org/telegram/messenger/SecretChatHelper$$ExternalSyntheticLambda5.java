package org.telegram.messenger;

public final /* synthetic */ class SecretChatHelper$$ExternalSyntheticLambda5 implements Runnable {
    public final /* synthetic */ SecretChatHelper f$0;
    public final /* synthetic */ long f$1;

    public /* synthetic */ SecretChatHelper$$ExternalSyntheticLambda5(SecretChatHelper secretChatHelper, long j) {
        this.f$0 = secretChatHelper;
        this.f$1 = j;
    }

    public final void run() {
        this.f$0.lambda$processDecryptedObject$10(this.f$1);
    }
}
