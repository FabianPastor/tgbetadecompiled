package org.telegram.messenger;

public final /* synthetic */ class NotificationsController$$ExternalSyntheticLambda20 implements Runnable {
    public final /* synthetic */ NotificationsController f$0;
    public final /* synthetic */ int f$1;

    public /* synthetic */ NotificationsController$$ExternalSyntheticLambda20(NotificationsController notificationsController, int i) {
        this.f$0 = notificationsController;
        this.f$1 = i;
    }

    public final void run() {
        this.f$0.lambda$processNewMessages$17(this.f$1);
    }
}
