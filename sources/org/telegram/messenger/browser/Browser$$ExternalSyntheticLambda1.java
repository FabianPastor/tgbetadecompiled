package org.telegram.messenger.browser;

import org.telegram.ui.ActionBar.AlertDialog;

public final /* synthetic */ class Browser$$ExternalSyntheticLambda1 implements Runnable {
    public final /* synthetic */ AlertDialog[] f$0;
    public final /* synthetic */ int f$1;

    public /* synthetic */ Browser$$ExternalSyntheticLambda1(AlertDialog[] alertDialogArr, int i) {
        this.f$0 = alertDialogArr;
        this.f$1 = i;
    }

    public final void run() {
        Browser.lambda$openUrl$3(this.f$0, this.f$1);
    }
}
