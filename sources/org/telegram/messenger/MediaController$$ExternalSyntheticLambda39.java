package org.telegram.messenger;

import org.telegram.ui.ActionBar.AlertDialog;

public final /* synthetic */ class MediaController$$ExternalSyntheticLambda39 implements Runnable {
    public final /* synthetic */ boolean[] f$0;
    public final /* synthetic */ AlertDialog f$1;

    public /* synthetic */ MediaController$$ExternalSyntheticLambda39(boolean[] zArr, AlertDialog alertDialog) {
        this.f$0 = zArr;
        this.f$1 = alertDialog;
    }

    public final void run() {
        MediaController.lambda$saveFile$34(this.f$0, this.f$1);
    }
}
