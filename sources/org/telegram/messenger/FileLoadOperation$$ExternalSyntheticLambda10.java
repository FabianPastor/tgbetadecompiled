package org.telegram.messenger;

public final /* synthetic */ class FileLoadOperation$$ExternalSyntheticLambda10 implements Runnable {
    public final /* synthetic */ FileLoadOperation f$0;
    public final /* synthetic */ boolean[] f$1;

    public /* synthetic */ FileLoadOperation$$ExternalSyntheticLambda10(FileLoadOperation fileLoadOperation, boolean[] zArr) {
        this.f$0 = fileLoadOperation;
        this.f$1 = zArr;
    }

    public final void run() {
        this.f$0.lambda$start$5(this.f$1);
    }
}
