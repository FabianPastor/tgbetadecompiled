package org.telegram.messenger;

public final /* synthetic */ class FileUploadOperation$$ExternalSyntheticLambda3 implements Runnable {
    public final /* synthetic */ FileUploadOperation f$0;
    public final /* synthetic */ long f$1;
    public final /* synthetic */ long f$2;

    public /* synthetic */ FileUploadOperation$$ExternalSyntheticLambda3(FileUploadOperation fileUploadOperation, long j, long j2) {
        this.f$0 = fileUploadOperation;
        this.f$1 = j;
        this.f$2 = j2;
    }

    public final void run() {
        this.f$0.lambda$checkNewDataAvailable$3(this.f$1, this.f$2);
    }
}
