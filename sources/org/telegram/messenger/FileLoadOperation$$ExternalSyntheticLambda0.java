package org.telegram.messenger;

public final /* synthetic */ class FileLoadOperation$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ FileLoadOperation f$0;

    public /* synthetic */ FileLoadOperation$$ExternalSyntheticLambda0(FileLoadOperation fileLoadOperation) {
        this.f$0 = fileLoadOperation;
    }

    public final void run() {
        this.f$0.startDownloadRequest();
    }
}
