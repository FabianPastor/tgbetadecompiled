package org.telegram.messenger;

import android.view.View;
import org.telegram.messenger.FilesMigrationService;

public final /* synthetic */ class FilesMigrationService$FilesMigrationBottomSheet$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ FilesMigrationService.FilesMigrationBottomSheet f$0;

    public /* synthetic */ FilesMigrationService$FilesMigrationBottomSheet$$ExternalSyntheticLambda0(FilesMigrationService.FilesMigrationBottomSheet filesMigrationBottomSheet) {
        this.f$0 = filesMigrationBottomSheet;
    }

    public final void onClick(View view) {
        this.f$0.lambda$new$0(view);
    }
}
