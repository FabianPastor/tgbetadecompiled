package org.telegram.messenger;

public final /* synthetic */ class FileLoadOperation$$ExternalSyntheticLambda5 implements Runnable {
    public final /* synthetic */ FileLoadOperation f$0;
    public final /* synthetic */ boolean f$1;

    public /* synthetic */ FileLoadOperation$$ExternalSyntheticLambda5(FileLoadOperation fileLoadOperation, boolean z) {
        this.f$0 = fileLoadOperation;
        this.f$1 = z;
    }

    public final void run() {
        this.f$0.lambda$cancel$7(this.f$1);
    }
}
