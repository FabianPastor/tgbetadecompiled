package org.telegram.messenger;

import java.util.ArrayList;
import java.util.HashMap;

public final /* synthetic */ class ContactsController$$ExternalSyntheticLambda27 implements Runnable {
    public final /* synthetic */ ContactsController f$0;
    public final /* synthetic */ HashMap f$1;
    public final /* synthetic */ ArrayList f$2;
    public final /* synthetic */ HashMap f$3;

    public /* synthetic */ ContactsController$$ExternalSyntheticLambda27(ContactsController contactsController, HashMap hashMap, ArrayList arrayList, HashMap hashMap2) {
        this.f$0 = contactsController;
        this.f$1 = hashMap;
        this.f$2 = arrayList;
        this.f$3 = hashMap2;
    }

    public final void run() {
        this.f$0.lambda$performSyncPhoneBook$14(this.f$1, this.f$2, this.f$3);
    }
}
