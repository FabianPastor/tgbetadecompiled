package org.telegram.messenger;

public final /* synthetic */ class FileLog$$ExternalSyntheticLambda3 implements Runnable {
    public final /* synthetic */ String f$0;
    public final /* synthetic */ Throwable f$1;

    public /* synthetic */ FileLog$$ExternalSyntheticLambda3(String str, Throwable th) {
        this.f$0 = str;
        this.f$1 = th;
    }

    public final void run() {
        FileLog.lambda$e$0(this.f$0, this.f$1);
    }
}
