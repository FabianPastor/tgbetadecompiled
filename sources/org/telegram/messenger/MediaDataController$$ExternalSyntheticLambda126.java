package org.telegram.messenger;

import java.util.Comparator;
import org.telegram.tgnet.TLRPC$MessageEntity;

public final /* synthetic */ class MediaDataController$$ExternalSyntheticLambda126 implements Comparator {
    public static final /* synthetic */ MediaDataController$$ExternalSyntheticLambda126 INSTANCE = new MediaDataController$$ExternalSyntheticLambda126();

    private /* synthetic */ MediaDataController$$ExternalSyntheticLambda126() {
    }

    public final int compare(Object obj, Object obj2) {
        return MediaDataController.lambda$static$120((TLRPC$MessageEntity) obj, (TLRPC$MessageEntity) obj2);
    }
}
