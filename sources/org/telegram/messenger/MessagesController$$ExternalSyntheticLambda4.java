package org.telegram.messenger;

import android.content.Context;
import org.telegram.ui.ActionBar.AlertDialog;

public final /* synthetic */ class MessagesController$$ExternalSyntheticLambda4 implements Runnable {
    public final /* synthetic */ Context f$0;
    public final /* synthetic */ AlertDialog f$1;

    public /* synthetic */ MessagesController$$ExternalSyntheticLambda4(Context context, AlertDialog alertDialog) {
        this.f$0 = context;
        this.f$1 = alertDialog;
    }

    public final void run() {
        MessagesController.lambda$convertToGigaGroup$218(this.f$0, this.f$1);
    }
}
