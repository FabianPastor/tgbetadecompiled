package org.telegram.messenger;

import android.os.Handler;
import android.os.Message;

public final /* synthetic */ class DispatchQueueMainThreadSync$$ExternalSyntheticLambda0 implements Handler.Callback {
    public final /* synthetic */ DispatchQueueMainThreadSync f$0;

    public /* synthetic */ DispatchQueueMainThreadSync$$ExternalSyntheticLambda0(DispatchQueueMainThreadSync dispatchQueueMainThreadSync) {
        this.f$0 = dispatchQueueMainThreadSync;
    }

    public final boolean handleMessage(Message message) {
        return this.f$0.lambda$run$1(message);
    }
}
