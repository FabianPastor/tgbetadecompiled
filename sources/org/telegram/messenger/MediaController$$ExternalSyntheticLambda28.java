package org.telegram.messenger;

public final /* synthetic */ class MediaController$$ExternalSyntheticLambda28 implements Runnable {
    public final /* synthetic */ MediaController f$0;
    public final /* synthetic */ MessageObject f$1;

    public /* synthetic */ MediaController$$ExternalSyntheticLambda28(MediaController mediaController, MessageObject messageObject) {
        this.f$0 = mediaController;
        this.f$1 = messageObject;
    }

    public final void run() {
        this.f$0.lambda$startAudioAgain$7(this.f$1);
    }
}
