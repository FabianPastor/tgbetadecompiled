package org.telegram.messenger;

public final /* synthetic */ class MediaDataController$$ExternalSyntheticLambda53 implements Runnable {
    public final /* synthetic */ MediaDataController f$0;
    public final /* synthetic */ String f$1;

    public /* synthetic */ MediaDataController$$ExternalSyntheticLambda53(MediaDataController mediaDataController, String str) {
        this.f$0 = mediaDataController;
        this.f$1 = str;
    }

    public final void run() {
        this.f$0.lambda$putEmojiKeywords$169(this.f$1);
    }
}
