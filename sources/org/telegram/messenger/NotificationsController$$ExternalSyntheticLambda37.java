package org.telegram.messenger;

public final /* synthetic */ class NotificationsController$$ExternalSyntheticLambda37 implements Runnable {
    public final /* synthetic */ NotificationsController f$0;
    public final /* synthetic */ boolean f$1;
    public final /* synthetic */ long f$2;

    public /* synthetic */ NotificationsController$$ExternalSyntheticLambda37(NotificationsController notificationsController, boolean z, long j) {
        this.f$0 = notificationsController;
        this.f$1 = z;
        this.f$2 = j;
    }

    public final void run() {
        this.f$0.lambda$setOpenedInBubble$3(this.f$1, this.f$2);
    }
}
