package org.telegram.messenger;

public final /* synthetic */ class MessagesController$$ExternalSyntheticLambda66 implements Runnable {
    public final /* synthetic */ MessagesController f$0;
    public final /* synthetic */ long f$1;
    public final /* synthetic */ int f$2;

    public /* synthetic */ MessagesController$$ExternalSyntheticLambda66(MessagesController messagesController, long j, int i) {
        this.f$0 = messagesController;
        this.f$1 = j;
        this.f$2 = i;
    }

    public final void run() {
        this.f$0.lambda$processUpdateArray$331(this.f$1, this.f$2);
    }
}
