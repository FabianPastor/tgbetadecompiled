package org.telegram.messenger;

public final /* synthetic */ class MessagesStorage$$ExternalSyntheticLambda35 implements Runnable {
    public final /* synthetic */ MessagesStorage f$0;
    public final /* synthetic */ int f$1;
    public final /* synthetic */ int f$2;
    public final /* synthetic */ int f$3;
    public final /* synthetic */ int f$4;
    public final /* synthetic */ boolean f$5;
    public final /* synthetic */ long f$6;

    public /* synthetic */ MessagesStorage$$ExternalSyntheticLambda35(MessagesStorage messagesStorage, int i, int i2, int i3, int i4, boolean z, long j) {
        this.f$0 = messagesStorage;
        this.f$1 = i;
        this.f$2 = i2;
        this.f$3 = i3;
        this.f$4 = i4;
        this.f$5 = z;
        this.f$6 = j;
    }

    public final void run() {
        this.f$0.lambda$createTaskForMid$85(this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, this.f$6);
    }
}
