package org.telegram.messenger;

import java.io.File;

public final /* synthetic */ class MediaController$$ExternalSyntheticLambda24 implements Runnable {
    public final /* synthetic */ MediaController f$0;
    public final /* synthetic */ File f$1;

    public /* synthetic */ MediaController$$ExternalSyntheticLambda24(MediaController mediaController, File file) {
        this.f$0 = mediaController;
        this.f$1 = file;
    }

    public final void run() {
        this.f$0.lambda$playEmojiSound$16(this.f$1);
    }
}
