package org.telegram.messenger;

import java.util.Comparator;
import org.telegram.messenger.MessagesController;

public final /* synthetic */ class MessagesController$$ExternalSyntheticLambda224 implements Comparator {
    public static final /* synthetic */ MessagesController$$ExternalSyntheticLambda224 INSTANCE = new MessagesController$$ExternalSyntheticLambda224();

    private /* synthetic */ MessagesController$$ExternalSyntheticLambda224() {
    }

    public final int compare(Object obj, Object obj2) {
        return MessagesController.lambda$processLoadedDialogFilters$13((MessagesController.DialogFilter) obj, (MessagesController.DialogFilter) obj2);
    }
}
