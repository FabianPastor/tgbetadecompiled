package org.telegram.messenger;

public final /* synthetic */ class ContactsController$$ExternalSyntheticLambda16 implements Runnable {
    public final /* synthetic */ ContactsController f$0;
    public final /* synthetic */ Runnable f$1;

    public /* synthetic */ ContactsController$$ExternalSyntheticLambda16(ContactsController contactsController, Runnable runnable) {
        this.f$0 = contactsController;
        this.f$1 = runnable;
    }

    public final void run() {
        this.f$0.lambda$deleteAllContacts$7(this.f$1);
    }
}
