package org.telegram.messenger;

import java.util.HashMap;

public final /* synthetic */ class ContactsController$$ExternalSyntheticLambda35 implements Runnable {
    public final /* synthetic */ ContactsController f$0;
    public final /* synthetic */ HashMap f$1;
    public final /* synthetic */ boolean f$2;
    public final /* synthetic */ boolean f$3;
    public final /* synthetic */ boolean f$4;

    public /* synthetic */ ContactsController$$ExternalSyntheticLambda35(ContactsController contactsController, HashMap hashMap, boolean z, boolean z2, boolean z3) {
        this.f$0 = contactsController;
        this.f$1 = hashMap;
        this.f$2 = z;
        this.f$3 = z2;
        this.f$4 = z3;
    }

    public final void run() {
        this.f$0.lambda$syncPhoneBookByAlert$6(this.f$1, this.f$2, this.f$3, this.f$4);
    }
}
