package org.telegram.messenger;

import java.util.Comparator;

public final /* synthetic */ class MessageObject$$ExternalSyntheticLambda0 implements Comparator {
    public static final /* synthetic */ MessageObject$$ExternalSyntheticLambda0 INSTANCE = new MessageObject$$ExternalSyntheticLambda0();

    private /* synthetic */ MessageObject$$ExternalSyntheticLambda0() {
    }

    public final int compare(Object obj, Object obj2) {
        return MessageObject.lambda$handleFoundWords$1((String) obj, (String) obj2);
    }
}
