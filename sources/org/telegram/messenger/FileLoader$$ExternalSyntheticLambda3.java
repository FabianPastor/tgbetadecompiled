package org.telegram.messenger;

public final /* synthetic */ class FileLoader$$ExternalSyntheticLambda3 implements Runnable {
    public final /* synthetic */ FileLoader f$0;
    public final /* synthetic */ String f$1;
    public final /* synthetic */ boolean f$2;

    public /* synthetic */ FileLoader$$ExternalSyntheticLambda3(FileLoader fileLoader, String str, boolean z) {
        this.f$0 = fileLoader;
        this.f$1 = str;
        this.f$2 = z;
    }

    public final void run() {
        this.f$0.lambda$cancelLoadFile$7(this.f$1, this.f$2);
    }
}
