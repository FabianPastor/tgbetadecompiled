package org.telegram.messenger;

import org.telegram.messenger.ImageLoader;

public final /* synthetic */ class ImageLoader$HttpImageTask$$ExternalSyntheticLambda7 implements Runnable {
    public final /* synthetic */ ImageLoader.HttpImageTask f$0;
    public final /* synthetic */ Boolean f$1;

    public /* synthetic */ ImageLoader$HttpImageTask$$ExternalSyntheticLambda7(ImageLoader.HttpImageTask httpImageTask, Boolean bool) {
        this.f$0 = httpImageTask;
        this.f$1 = bool;
    }

    public final void run() {
        this.f$0.lambda$onPostExecute$3(this.f$1);
    }
}
