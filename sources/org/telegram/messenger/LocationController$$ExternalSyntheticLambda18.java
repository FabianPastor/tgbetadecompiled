package org.telegram.messenger;

import java.util.ArrayList;

public final /* synthetic */ class LocationController$$ExternalSyntheticLambda18 implements Runnable {
    public final /* synthetic */ LocationController f$0;
    public final /* synthetic */ ArrayList f$1;
    public final /* synthetic */ ArrayList f$2;
    public final /* synthetic */ ArrayList f$3;

    public /* synthetic */ LocationController$$ExternalSyntheticLambda18(LocationController locationController, ArrayList arrayList, ArrayList arrayList2, ArrayList arrayList3) {
        this.f$0 = locationController;
        this.f$1 = arrayList;
        this.f$2 = arrayList2;
        this.f$3 = arrayList3;
    }

    public final void run() {
        this.f$0.lambda$loadSharingLocations$17(this.f$1, this.f$2, this.f$3);
    }
}
