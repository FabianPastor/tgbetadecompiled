package org.telegram.messenger;

import java.util.ArrayList;

public final /* synthetic */ class MessagesStorage$$ExternalSyntheticLambda148 implements Runnable {
    public final /* synthetic */ MessagesStorage f$0;
    public final /* synthetic */ ArrayList f$1;
    public final /* synthetic */ boolean f$2;
    public final /* synthetic */ boolean f$3;
    public final /* synthetic */ int f$4;
    public final /* synthetic */ boolean f$5;
    public final /* synthetic */ boolean f$6;

    public /* synthetic */ MessagesStorage$$ExternalSyntheticLambda148(MessagesStorage messagesStorage, ArrayList arrayList, boolean z, boolean z2, int i, boolean z3, boolean z4) {
        this.f$0 = messagesStorage;
        this.f$1 = arrayList;
        this.f$2 = z;
        this.f$3 = z2;
        this.f$4 = i;
        this.f$5 = z3;
        this.f$6 = z4;
    }

    public final void run() {
        this.f$0.lambda$putMessages$159(this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, this.f$6);
    }
}
