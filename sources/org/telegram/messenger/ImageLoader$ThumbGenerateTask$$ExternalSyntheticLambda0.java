package org.telegram.messenger;

import org.telegram.messenger.ImageLoader;

public final /* synthetic */ class ImageLoader$ThumbGenerateTask$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ ImageLoader.ThumbGenerateTask f$0;
    public final /* synthetic */ String f$1;

    public /* synthetic */ ImageLoader$ThumbGenerateTask$$ExternalSyntheticLambda0(ImageLoader.ThumbGenerateTask thumbGenerateTask, String str) {
        this.f$0 = thumbGenerateTask;
        this.f$1 = str;
    }

    public final void run() {
        this.f$0.lambda$removeTask$0(this.f$1);
    }
}
