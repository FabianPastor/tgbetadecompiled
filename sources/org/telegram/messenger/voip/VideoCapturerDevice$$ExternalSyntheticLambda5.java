package org.telegram.messenger.voip;

public final /* synthetic */ class VideoCapturerDevice$$ExternalSyntheticLambda5 implements Runnable {
    public final /* synthetic */ VideoCapturerDevice f$0;
    public final /* synthetic */ long f$1;
    public final /* synthetic */ String f$2;

    public /* synthetic */ VideoCapturerDevice$$ExternalSyntheticLambda5(VideoCapturerDevice videoCapturerDevice, long j, String str) {
        this.f$0 = videoCapturerDevice;
        this.f$1 = j;
        this.f$2 = str;
    }

    public final void run() {
        this.f$0.lambda$init$5(this.f$1, this.f$2);
    }
}
