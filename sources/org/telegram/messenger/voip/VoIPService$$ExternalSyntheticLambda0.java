package org.telegram.messenger.voip;

import android.content.DialogInterface;

public final /* synthetic */ class VoIPService$$ExternalSyntheticLambda0 implements DialogInterface.OnClickListener {
    public final /* synthetic */ VoIPService f$0;

    public /* synthetic */ VoIPService$$ExternalSyntheticLambda0(VoIPService voIPService) {
        this.f$0 = voIPService;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f$0.lambda$toggleSpeakerphoneOrShowRouteSheet$62(dialogInterface, i);
    }
}
