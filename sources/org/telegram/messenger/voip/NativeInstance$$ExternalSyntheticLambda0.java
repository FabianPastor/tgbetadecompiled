package org.telegram.messenger.voip;

public final /* synthetic */ class NativeInstance$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ NativeInstance f$0;
    public final /* synthetic */ int f$1;
    public final /* synthetic */ String f$2;

    public /* synthetic */ NativeInstance$$ExternalSyntheticLambda0(NativeInstance nativeInstance, int i, String str) {
        this.f$0 = nativeInstance;
        this.f$1 = i;
        this.f$2 = str;
    }

    public final void run() {
        this.f$0.lambda$onEmitJoinPayload$3(this.f$1, this.f$2);
    }
}
