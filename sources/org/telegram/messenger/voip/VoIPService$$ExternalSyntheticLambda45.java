package org.telegram.messenger.voip;

public final /* synthetic */ class VoIPService$$ExternalSyntheticLambda45 implements Runnable {
    public final /* synthetic */ VoIPService f$0;
    public final /* synthetic */ String f$1;

    public /* synthetic */ VoIPService$$ExternalSyntheticLambda45(VoIPService voIPService, String str) {
        this.f$0 = voIPService;
        this.f$1 = str;
    }

    public final void run() {
        this.f$0.lambda$createGroupInstance$41(this.f$1);
    }
}
