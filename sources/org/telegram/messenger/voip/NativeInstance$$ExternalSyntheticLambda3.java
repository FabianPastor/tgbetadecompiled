package org.telegram.messenger.voip;

public final /* synthetic */ class NativeInstance$$ExternalSyntheticLambda3 implements Runnable {
    public final /* synthetic */ NativeInstance f$0;
    public final /* synthetic */ int[] f$1;
    public final /* synthetic */ float[] f$2;
    public final /* synthetic */ boolean[] f$3;

    public /* synthetic */ NativeInstance$$ExternalSyntheticLambda3(NativeInstance nativeInstance, int[] iArr, float[] fArr, boolean[] zArr) {
        this.f$0 = nativeInstance;
        this.f$1 = iArr;
        this.f$2 = fArr;
        this.f$3 = zArr;
    }

    public final void run() {
        this.f$0.lambda$onAudioLevelsUpdated$1(this.f$1, this.f$2, this.f$3);
    }
}
