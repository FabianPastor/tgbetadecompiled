package org.telegram.messenger.voip;

public final /* synthetic */ class VoIPService$$ExternalSyntheticLambda58 implements Runnable {
    public final /* synthetic */ VoIPService f$0;
    public final /* synthetic */ boolean f$1;

    public /* synthetic */ VoIPService$$ExternalSyntheticLambda58(VoIPService voIPService, boolean z) {
        this.f$0 = voIPService;
        this.f$1 = z;
    }

    public final void run() {
        this.f$0.lambda$startGroupCall$26(this.f$1);
    }
}
