package org.telegram.messenger;

import java.util.Comparator;

public final /* synthetic */ class MediaController$$ExternalSyntheticLambda41 implements Comparator {
    public static final /* synthetic */ MediaController$$ExternalSyntheticLambda41 INSTANCE = new MediaController$$ExternalSyntheticLambda41();

    private /* synthetic */ MediaController$$ExternalSyntheticLambda41() {
    }

    public final int compare(Object obj, Object obj2) {
        return MediaController.lambda$sortPlaylist$13((MessageObject) obj, (MessageObject) obj2);
    }
}
