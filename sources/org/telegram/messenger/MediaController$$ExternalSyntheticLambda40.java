package org.telegram.messenger;

import java.util.Comparator;
import org.telegram.messenger.MediaController;

public final /* synthetic */ class MediaController$$ExternalSyntheticLambda40 implements Comparator {
    public static final /* synthetic */ MediaController$$ExternalSyntheticLambda40 INSTANCE = new MediaController$$ExternalSyntheticLambda40();

    private /* synthetic */ MediaController$$ExternalSyntheticLambda40() {
    }

    public final int compare(Object obj, Object obj2) {
        return MediaController.lambda$loadGalleryPhotosAlbums$39((MediaController.PhotoEntry) obj, (MediaController.PhotoEntry) obj2);
    }
}
