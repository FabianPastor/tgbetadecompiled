package org.telegram.messenger;

import android.content.Intent;
import org.telegram.messenger.NotificationBadge;

public final /* synthetic */ class NotificationBadge$NewHtcHomeBadger$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ Intent f$0;
    public final /* synthetic */ Intent f$1;

    public /* synthetic */ NotificationBadge$NewHtcHomeBadger$$ExternalSyntheticLambda0(Intent intent, Intent intent2) {
        this.f$0 = intent;
        this.f$1 = intent2;
    }

    public final void run() {
        NotificationBadge.NewHtcHomeBadger.lambda$executeBadge$0(this.f$0, this.f$1);
    }
}
