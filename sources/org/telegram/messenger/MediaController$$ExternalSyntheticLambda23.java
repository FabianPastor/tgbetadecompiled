package org.telegram.messenger;

public final /* synthetic */ class MediaController$$ExternalSyntheticLambda23 implements Runnable {
    public final /* synthetic */ MediaController f$0;
    public final /* synthetic */ int f$1;
    public final /* synthetic */ boolean f$2;
    public final /* synthetic */ int f$3;

    public /* synthetic */ MediaController$$ExternalSyntheticLambda23(MediaController mediaController, int i, boolean z, int i2) {
        this.f$0 = mediaController;
        this.f$1 = i;
        this.f$2 = z;
        this.f$3 = i2;
    }

    public final void run() {
        this.f$0.lambda$stopRecording$32(this.f$1, this.f$2, this.f$3);
    }
}
