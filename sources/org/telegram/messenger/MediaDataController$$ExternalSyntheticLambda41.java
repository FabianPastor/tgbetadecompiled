package org.telegram.messenger;

public final /* synthetic */ class MediaDataController$$ExternalSyntheticLambda41 implements Runnable {
    public final /* synthetic */ MediaDataController f$0;
    public final /* synthetic */ long f$1;
    public final /* synthetic */ long f$2;
    public final /* synthetic */ long f$3;

    public /* synthetic */ MediaDataController$$ExternalSyntheticLambda41(MediaDataController mediaDataController, long j, long j2, long j3) {
        this.f$0 = mediaDataController;
        this.f$1 = j;
        this.f$2 = j2;
        this.f$3 = j3;
    }

    public final void run() {
        this.f$0.lambda$loadMusic$102(this.f$1, this.f$2, this.f$3);
    }
}
