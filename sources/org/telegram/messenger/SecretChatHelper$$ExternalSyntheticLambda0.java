package org.telegram.messenger;

import android.content.DialogInterface;

public final /* synthetic */ class SecretChatHelper$$ExternalSyntheticLambda0 implements DialogInterface.OnCancelListener {
    public final /* synthetic */ SecretChatHelper f$0;
    public final /* synthetic */ int f$1;

    public /* synthetic */ SecretChatHelper$$ExternalSyntheticLambda0(SecretChatHelper secretChatHelper, int i) {
        this.f$0 = secretChatHelper;
        this.f$1 = i;
    }

    public final void onCancel(DialogInterface dialogInterface) {
        this.f$0.lambda$startSecretChat$31(this.f$1, dialogInterface);
    }
}
