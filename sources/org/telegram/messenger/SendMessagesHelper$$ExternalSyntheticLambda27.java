package org.telegram.messenger;

public final /* synthetic */ class SendMessagesHelper$$ExternalSyntheticLambda27 implements Runnable {
    public final /* synthetic */ SendMessagesHelper f$0;
    public final /* synthetic */ String f$1;

    public /* synthetic */ SendMessagesHelper$$ExternalSyntheticLambda27(SendMessagesHelper sendMessagesHelper, String str) {
        this.f$0 = sendMessagesHelper;
        this.f$1 = str;
    }

    public final void run() {
        this.f$0.lambda$sendNotificationCallback$17(this.f$1);
    }
}
