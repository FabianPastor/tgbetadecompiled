package org.telegram.messenger;

import java.util.Comparator;
import org.telegram.messenger.FileLoadOperation;

public final /* synthetic */ class FileLoadOperation$$ExternalSyntheticLambda11 implements Comparator {
    public static final /* synthetic */ FileLoadOperation$$ExternalSyntheticLambda11 INSTANCE = new FileLoadOperation$$ExternalSyntheticLambda11();

    private /* synthetic */ FileLoadOperation$$ExternalSyntheticLambda11() {
    }

    public final int compare(Object obj, Object obj2) {
        return FileLoadOperation.lambda$removePart$0((FileLoadOperation.Range) obj, (FileLoadOperation.Range) obj2);
    }
}
