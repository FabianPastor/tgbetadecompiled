package org.telegram.messenger;

public final /* synthetic */ class LocationController$$ExternalSyntheticLambda10 implements Runnable {
    public final /* synthetic */ LocationController f$0;
    public final /* synthetic */ int f$1;
    public final /* synthetic */ long f$2;

    public /* synthetic */ LocationController$$ExternalSyntheticLambda10(LocationController locationController, int i, long j) {
        this.f$0 = locationController;
        this.f$1 = i;
        this.f$2 = j;
    }

    public final void run() {
        this.f$0.lambda$setProximityLocation$13(this.f$1, this.f$2);
    }
}
