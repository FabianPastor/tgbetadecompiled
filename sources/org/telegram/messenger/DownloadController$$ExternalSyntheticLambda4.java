package org.telegram.messenger;

import java.util.ArrayList;

public final /* synthetic */ class DownloadController$$ExternalSyntheticLambda4 implements Runnable {
    public final /* synthetic */ DownloadController f$0;
    public final /* synthetic */ ArrayList f$1;
    public final /* synthetic */ ArrayList f$2;

    public /* synthetic */ DownloadController$$ExternalSyntheticLambda4(DownloadController downloadController, ArrayList arrayList, ArrayList arrayList2) {
        this.f$0 = downloadController;
        this.f$1 = arrayList;
        this.f$2 = arrayList2;
    }

    public final void run() {
        this.f$0.lambda$loadDownloadingFiles$10(this.f$1, this.f$2);
    }
}
