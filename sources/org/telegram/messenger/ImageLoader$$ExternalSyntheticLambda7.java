package org.telegram.messenger;

public final /* synthetic */ class ImageLoader$$ExternalSyntheticLambda7 implements Runnable {
    public final /* synthetic */ ImageLoader f$0;
    public final /* synthetic */ String f$1;

    public /* synthetic */ ImageLoader$$ExternalSyntheticLambda7(ImageLoader imageLoader, String str) {
        this.f$0 = imageLoader;
        this.f$1 = str;
    }

    public final void run() {
        this.f$0.lambda$preloadArtwork$7(this.f$1);
    }
}
