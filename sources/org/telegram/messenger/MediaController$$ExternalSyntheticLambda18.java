package org.telegram.messenger;

public final /* synthetic */ class MediaController$$ExternalSyntheticLambda18 implements Runnable {
    public final /* synthetic */ MediaController f$0;
    public final /* synthetic */ int f$1;
    public final /* synthetic */ int f$2;

    public /* synthetic */ MediaController$$ExternalSyntheticLambda18(MediaController mediaController, int i, int i2) {
        this.f$0 = mediaController;
        this.f$1 = i;
        this.f$2 = i2;
    }

    public final void run() {
        this.f$0.lambda$startRecording$23(this.f$1, this.f$2);
    }
}
