package org.telegram.messenger;

public final /* synthetic */ class MediaDataController$$ExternalSyntheticLambda18 implements Runnable {
    public final /* synthetic */ MediaDataController f$0;
    public final /* synthetic */ int f$1;

    public /* synthetic */ MediaDataController$$ExternalSyntheticLambda18(MediaDataController mediaDataController, int i) {
        this.f$0 = mediaDataController;
        this.f$1 = i;
    }

    public final void run() {
        this.f$0.lambda$processLoadedFeaturedStickers$44(this.f$1);
    }
}
