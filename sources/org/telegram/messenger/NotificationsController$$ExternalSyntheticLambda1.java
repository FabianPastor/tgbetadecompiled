package org.telegram.messenger;

import android.graphics.Canvas;
import android.graphics.PostProcessor;

public final /* synthetic */ class NotificationsController$$ExternalSyntheticLambda1 implements PostProcessor {
    public static final /* synthetic */ NotificationsController$$ExternalSyntheticLambda1 INSTANCE = new NotificationsController$$ExternalSyntheticLambda1();

    private /* synthetic */ NotificationsController$$ExternalSyntheticLambda1() {
    }

    public final int onPostProcess(Canvas canvas) {
        return NotificationsController.lambda$loadRoundAvatar$35(canvas);
    }
}
