package org.telegram.messenger;

import java.util.Comparator;

public final /* synthetic */ class ContactsController$$ExternalSyntheticLambda45 implements Comparator {
    public static final /* synthetic */ ContactsController$$ExternalSyntheticLambda45 INSTANCE = new ContactsController$$ExternalSyntheticLambda45();

    private /* synthetic */ ContactsController$$ExternalSyntheticLambda45() {
    }

    public final int compare(Object obj, Object obj2) {
        return ContactsController.lambda$mergePhonebookAndTelegramContacts$39((String) obj, (String) obj2);
    }
}
