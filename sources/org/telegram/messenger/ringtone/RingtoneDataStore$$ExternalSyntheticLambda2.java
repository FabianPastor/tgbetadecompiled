package org.telegram.messenger.ringtone;

import java.util.ArrayList;

public final /* synthetic */ class RingtoneDataStore$$ExternalSyntheticLambda2 implements Runnable {
    public final /* synthetic */ RingtoneDataStore f$0;
    public final /* synthetic */ ArrayList f$1;

    public /* synthetic */ RingtoneDataStore$$ExternalSyntheticLambda2(RingtoneDataStore ringtoneDataStore, ArrayList arrayList) {
        this.f$0 = ringtoneDataStore;
        this.f$1 = arrayList;
    }

    public final void run() {
        this.f$0.lambda$checkRingtoneSoundsLoaded$5(this.f$1);
    }
}
