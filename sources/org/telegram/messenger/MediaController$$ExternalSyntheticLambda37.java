package org.telegram.messenger;

import org.telegram.ui.ActionBar.AlertDialog;

public final /* synthetic */ class MediaController$$ExternalSyntheticLambda37 implements Runnable {
    public final /* synthetic */ AlertDialog f$0;
    public final /* synthetic */ int f$1;

    public /* synthetic */ MediaController$$ExternalSyntheticLambda37(AlertDialog alertDialog, int i) {
        this.f$0 = alertDialog;
        this.f$1 = i;
    }

    public final void run() {
        MediaController.lambda$saveFile$36(this.f$0, this.f$1);
    }
}
