package org.telegram.messenger;

import org.telegram.tgnet.RequestDelegate;
import org.telegram.tgnet.TLObject;
import org.telegram.tgnet.TLRPC$TL_error;

public final /* synthetic */ class MessagesController$$ExternalSyntheticLambda346 implements RequestDelegate {
    public static final /* synthetic */ MessagesController$$ExternalSyntheticLambda346 INSTANCE = new MessagesController$$ExternalSyntheticLambda346();

    private /* synthetic */ MessagesController$$ExternalSyntheticLambda346() {
    }

    public final void run(TLObject tLObject, TLRPC$TL_error tLRPC$TL_error) {
        MessagesController.lambda$hidePeerSettingsBar$54(tLObject, tLRPC$TL_error);
    }
}
