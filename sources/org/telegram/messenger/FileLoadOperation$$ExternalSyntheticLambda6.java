package org.telegram.messenger;

public final /* synthetic */ class FileLoadOperation$$ExternalSyntheticLambda6 implements Runnable {
    public final /* synthetic */ FileLoadOperation f$0;
    public final /* synthetic */ boolean f$1;

    public /* synthetic */ FileLoadOperation$$ExternalSyntheticLambda6(FileLoadOperation fileLoadOperation, boolean z) {
        this.f$0 = fileLoadOperation;
        this.f$1 = z;
    }

    public final void run() {
        this.f$0.lambda$setIsPreloadVideoOperation$6(this.f$1);
    }
}
