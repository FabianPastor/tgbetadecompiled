package org.telegram.messenger;

public final /* synthetic */ class MessagesController$$ExternalSyntheticLambda41 implements Runnable {
    public final /* synthetic */ MessagesController f$0;
    public final /* synthetic */ int f$1;

    public /* synthetic */ MessagesController$$ExternalSyntheticLambda41(MessagesController messagesController, int i) {
        this.f$0 = messagesController;
        this.f$1 = i;
    }

    public final void run() {
        this.f$0.lambda$onFolderEmpty$160(this.f$1);
    }
}
