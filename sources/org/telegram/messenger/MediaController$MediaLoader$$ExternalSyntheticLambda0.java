package org.telegram.messenger;

import android.content.DialogInterface;
import org.telegram.messenger.MediaController;

public final /* synthetic */ class MediaController$MediaLoader$$ExternalSyntheticLambda0 implements DialogInterface.OnCancelListener {
    public final /* synthetic */ MediaController.MediaLoader f$0;

    public /* synthetic */ MediaController$MediaLoader$$ExternalSyntheticLambda0(MediaController.MediaLoader mediaLoader) {
        this.f$0 = mediaLoader;
    }

    public final void onCancel(DialogInterface dialogInterface) {
        this.f$0.lambda$new$0(dialogInterface);
    }
}
