package org.telegram.messenger;

public final /* synthetic */ class MessagesController$$ExternalSyntheticLambda44 implements Runnable {
    public final /* synthetic */ MessagesController f$0;
    public final /* synthetic */ int f$1;
    public final /* synthetic */ long f$2;

    public /* synthetic */ MessagesController$$ExternalSyntheticLambda44(MessagesController messagesController, int i, long j) {
        this.f$0 = messagesController;
        this.f$1 = i;
        this.f$2 = j;
    }

    public final void run() {
        this.f$0.lambda$markDialogAsReadNow$200(this.f$1, this.f$2);
    }
}
