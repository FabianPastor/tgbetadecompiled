package org.telegram.messenger;

import java.util.ArrayList;
import java.util.HashMap;
import org.telegram.tgnet.TLRPC$messages_Messages;

public final /* synthetic */ class MessagesController$$ExternalSyntheticLambda196 implements Runnable {
    public final /* synthetic */ MessagesController f$0;
    public final /* synthetic */ TLRPC$messages_Messages f$1;
    public final /* synthetic */ boolean f$10;
    public final /* synthetic */ int f$11;
    public final /* synthetic */ int f$12;
    public final /* synthetic */ boolean f$13;
    public final /* synthetic */ int f$14;
    public final /* synthetic */ int f$15;
    public final /* synthetic */ int f$16;
    public final /* synthetic */ int f$17;
    public final /* synthetic */ int f$18;
    public final /* synthetic */ int f$19;
    public final /* synthetic */ boolean f$2;
    public final /* synthetic */ ArrayList f$20;
    public final /* synthetic */ HashMap f$21;
    public final /* synthetic */ int f$3;
    public final /* synthetic */ boolean f$4;
    public final /* synthetic */ int f$5;
    public final /* synthetic */ int f$6;
    public final /* synthetic */ int f$7;
    public final /* synthetic */ long f$8;
    public final /* synthetic */ ArrayList f$9;

    public /* synthetic */ MessagesController$$ExternalSyntheticLambda196(MessagesController messagesController, TLRPC$messages_Messages tLRPC$messages_Messages, boolean z, int i, boolean z2, int i2, int i3, int i4, long j, ArrayList arrayList, boolean z3, int i5, int i6, boolean z4, int i7, int i8, int i9, int i10, int i11, int i12, ArrayList arrayList2, HashMap hashMap) {
        this.f$0 = messagesController;
        this.f$1 = tLRPC$messages_Messages;
        this.f$2 = z;
        this.f$3 = i;
        this.f$4 = z2;
        this.f$5 = i2;
        this.f$6 = i3;
        this.f$7 = i4;
        this.f$8 = j;
        this.f$9 = arrayList;
        this.f$10 = z3;
        this.f$11 = i5;
        this.f$12 = i6;
        this.f$13 = z4;
        this.f$14 = i7;
        this.f$15 = i8;
        this.f$16 = i9;
        this.f$17 = i10;
        this.f$18 = i11;
        this.f$19 = i12;
        this.f$20 = arrayList2;
        this.f$21 = hashMap;
    }

    public final void run() {
        MessagesController messagesController = this.f$0;
        MessagesController messagesController2 = messagesController;
        messagesController2.lambda$processLoadedMessages$157(this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, this.f$6, this.f$7, this.f$8, this.f$9, this.f$10, this.f$11, this.f$12, this.f$13, this.f$14, this.f$15, this.f$16, this.f$17, this.f$18, this.f$19, this.f$20, this.f$21);
    }
}
