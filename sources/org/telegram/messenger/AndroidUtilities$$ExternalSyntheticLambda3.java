package org.telegram.messenger;

import android.text.util.Linkify;

public final /* synthetic */ class AndroidUtilities$$ExternalSyntheticLambda3 implements Linkify.MatchFilter {
    public static final /* synthetic */ AndroidUtilities$$ExternalSyntheticLambda3 INSTANCE = new AndroidUtilities$$ExternalSyntheticLambda3();

    private /* synthetic */ AndroidUtilities$$ExternalSyntheticLambda3() {
    }

    public final boolean acceptMatch(CharSequence charSequence, int i, int i2) {
        return AndroidUtilities.lambda$static$2(charSequence, i, i2);
    }
}
