package org.telegram.messenger;

public final /* synthetic */ class NotificationsController$$ExternalSyntheticLambda21 implements Runnable {
    public final /* synthetic */ NotificationsController f$0;
    public final /* synthetic */ int f$1;
    public final /* synthetic */ int f$2;

    public /* synthetic */ NotificationsController$$ExternalSyntheticLambda21(NotificationsController notificationsController, int i, int i2) {
        this.f$0 = notificationsController;
        this.f$1 = i;
        this.f$2 = i2;
    }

    public final void run() {
        this.f$0.lambda$deleteNotificationChannelGlobal$32(this.f$1, this.f$2);
    }
}
