package org.telegram.messenger;

import java.util.HashMap;

public final /* synthetic */ class ContactsController$$ExternalSyntheticLambda12 implements Runnable {
    public final /* synthetic */ ContactsController f$0;
    public final /* synthetic */ int f$1;
    public final /* synthetic */ HashMap f$2;
    public final /* synthetic */ boolean f$3;
    public final /* synthetic */ boolean f$4;

    public /* synthetic */ ContactsController$$ExternalSyntheticLambda12(ContactsController contactsController, int i, HashMap hashMap, boolean z, boolean z2) {
        this.f$0 = contactsController;
        this.f$1 = i;
        this.f$2 = hashMap;
        this.f$3 = z;
        this.f$4 = z2;
    }

    public final void run() {
        this.f$0.lambda$performSyncPhoneBook$13(this.f$1, this.f$2, this.f$3, this.f$4);
    }
}
