package org.telegram.messenger;

import java.util.ArrayList;

public final /* synthetic */ class MessagesController$$ExternalSyntheticLambda207 implements Runnable {
    public final /* synthetic */ MessagesController f$0;
    public final /* synthetic */ boolean f$1;
    public final /* synthetic */ int f$10;
    public final /* synthetic */ int f$11;
    public final /* synthetic */ int f$12;
    public final /* synthetic */ int f$13;
    public final /* synthetic */ int f$14;
    public final /* synthetic */ int f$15;
    public final /* synthetic */ int f$16;
    public final /* synthetic */ int f$17;
    public final /* synthetic */ int f$2;
    public final /* synthetic */ int f$3;
    public final /* synthetic */ boolean f$4;
    public final /* synthetic */ boolean f$5;
    public final /* synthetic */ int f$6;
    public final /* synthetic */ long f$7;
    public final /* synthetic */ int f$8;
    public final /* synthetic */ ArrayList f$9;

    public /* synthetic */ MessagesController$$ExternalSyntheticLambda207(MessagesController messagesController, boolean z, int i, int i2, boolean z2, boolean z3, int i3, long j, int i4, ArrayList arrayList, int i5, int i6, int i7, int i8, int i9, int i10, int i11, int i12) {
        this.f$0 = messagesController;
        this.f$1 = z;
        this.f$2 = i;
        this.f$3 = i2;
        this.f$4 = z2;
        this.f$5 = z3;
        this.f$6 = i3;
        this.f$7 = j;
        this.f$8 = i4;
        this.f$9 = arrayList;
        this.f$10 = i5;
        this.f$11 = i6;
        this.f$12 = i7;
        this.f$13 = i8;
        this.f$14 = i9;
        this.f$15 = i10;
        this.f$16 = i11;
        this.f$17 = i12;
    }

    public final void run() {
        MessagesController messagesController = this.f$0;
        MessagesController messagesController2 = messagesController;
        messagesController2.lambda$processLoadedMessages$156(this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, this.f$6, this.f$7, this.f$8, this.f$9, this.f$10, this.f$11, this.f$12, this.f$13, this.f$14, this.f$15, this.f$16, this.f$17);
    }
}
