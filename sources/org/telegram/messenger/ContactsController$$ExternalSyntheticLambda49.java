package org.telegram.messenger;

import java.util.Comparator;
import org.telegram.messenger.ContactsController;

public final /* synthetic */ class ContactsController$$ExternalSyntheticLambda49 implements Comparator {
    public static final /* synthetic */ ContactsController$$ExternalSyntheticLambda49 INSTANCE = new ContactsController$$ExternalSyntheticLambda49();

    private /* synthetic */ ContactsController$$ExternalSyntheticLambda49() {
    }

    public final int compare(Object obj, Object obj2) {
        return ContactsController.lambda$updateUnregisteredContacts$42((ContactsController.Contact) obj, (ContactsController.Contact) obj2);
    }
}
