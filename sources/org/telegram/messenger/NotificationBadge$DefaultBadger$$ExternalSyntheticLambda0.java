package org.telegram.messenger;

import android.content.Intent;
import org.telegram.messenger.NotificationBadge;

public final /* synthetic */ class NotificationBadge$DefaultBadger$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ Intent f$0;

    public /* synthetic */ NotificationBadge$DefaultBadger$$ExternalSyntheticLambda0(Intent intent) {
        this.f$0 = intent;
    }

    public final void run() {
        NotificationBadge.DefaultBadger.lambda$executeBadge$0(this.f$0);
    }
}
