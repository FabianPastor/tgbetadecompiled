package org.telegram.messenger;

public final /* synthetic */ class FileLoader$$ExternalSyntheticLambda9 implements Runnable {
    public final /* synthetic */ FileLoader f$0;
    public final /* synthetic */ boolean f$1;

    public /* synthetic */ FileLoader$$ExternalSyntheticLambda9(FileLoader fileLoader, boolean z) {
        this.f$0 = fileLoader;
        this.f$1 = z;
    }

    public final void run() {
        this.f$0.lambda$onNetworkChanged$4(this.f$1);
    }
}
