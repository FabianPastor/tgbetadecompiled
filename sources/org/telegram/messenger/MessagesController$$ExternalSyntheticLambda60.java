package org.telegram.messenger;

public final /* synthetic */ class MessagesController$$ExternalSyntheticLambda60 implements Runnable {
    public final /* synthetic */ MessagesController f$0;
    public final /* synthetic */ long f$1;

    public /* synthetic */ MessagesController$$ExternalSyntheticLambda60(MessagesController messagesController, long j) {
        this.f$0 = messagesController;
        this.f$1 = j;
    }

    public final void run() {
        this.f$0.lambda$deleteParticipantFromChat$247(this.f$1);
    }
}
