package org.telegram.messenger;

public final /* synthetic */ class NotificationsController$$ExternalSyntheticLambda23 implements Runnable {
    public final /* synthetic */ NotificationsController f$0;
    public final /* synthetic */ long f$1;
    public final /* synthetic */ int f$2;

    public /* synthetic */ NotificationsController$$ExternalSyntheticLambda23(NotificationsController notificationsController, long j, int i) {
        this.f$0 = notificationsController;
        this.f$1 = j;
        this.f$2 = i;
    }

    public final void run() {
        this.f$0.lambda$deleteNotificationChannel$31(this.f$1, this.f$2);
    }
}
