package org.telegram.messenger;

import org.telegram.ui.ActionBar.Theme;

public final /* synthetic */ class MessagesController$$ExternalSyntheticLambda219 implements Runnable {
    public static final /* synthetic */ MessagesController$$ExternalSyntheticLambda219 INSTANCE = new MessagesController$$ExternalSyntheticLambda219();

    private /* synthetic */ MessagesController$$ExternalSyntheticLambda219() {
    }

    public final void run() {
        Theme.checkAutoNightThemeConditions();
    }
}
