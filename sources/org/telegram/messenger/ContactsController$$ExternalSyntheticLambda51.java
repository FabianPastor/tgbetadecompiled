package org.telegram.messenger;

import java.util.Comparator;

public final /* synthetic */ class ContactsController$$ExternalSyntheticLambda51 implements Comparator {
    public static final /* synthetic */ ContactsController$$ExternalSyntheticLambda51 INSTANCE = new ContactsController$$ExternalSyntheticLambda51();

    private /* synthetic */ ContactsController$$ExternalSyntheticLambda51() {
    }

    public final int compare(Object obj, Object obj2) {
        return ContactsController.lambda$mergePhonebookAndTelegramContacts$38(obj, obj2);
    }
}
