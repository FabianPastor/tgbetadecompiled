package org.telegram.messenger;

import java.util.Comparator;

public final /* synthetic */ class Emoji$$ExternalSyntheticLambda2 implements Comparator {
    public static final /* synthetic */ Emoji$$ExternalSyntheticLambda2 INSTANCE = new Emoji$$ExternalSyntheticLambda2();

    private /* synthetic */ Emoji$$ExternalSyntheticLambda2() {
    }

    public final int compare(Object obj, Object obj2) {
        return Emoji.lambda$sortEmoji$2((String) obj, (String) obj2);
    }
}
