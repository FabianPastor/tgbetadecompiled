package org.telegram.messenger;

public final /* synthetic */ class LocaleController$$ExternalSyntheticLambda3 implements Runnable {
    public final /* synthetic */ LocaleController f$0;
    public final /* synthetic */ int f$1;
    public final /* synthetic */ boolean f$2;

    public /* synthetic */ LocaleController$$ExternalSyntheticLambda3(LocaleController localeController, int i, boolean z) {
        this.f$0 = localeController;
        this.f$1 = i;
        this.f$2 = z;
    }

    public final void run() {
        this.f$0.lambda$applyLanguage$3(this.f$1, this.f$2);
    }
}
