package org.telegram.messenger;

import androidx.collection.LongSparseArray;
import java.util.ArrayList;
import org.telegram.messenger.support.LongSparseIntArray;

public final /* synthetic */ class MessagesController$$ExternalSyntheticLambda48 implements Runnable {
    public final /* synthetic */ MessagesController f$0;
    public final /* synthetic */ int f$1;
    public final /* synthetic */ LongSparseArray f$10;
    public final /* synthetic */ boolean f$11;
    public final /* synthetic */ ArrayList f$12;
    public final /* synthetic */ ArrayList f$13;
    public final /* synthetic */ LongSparseArray f$14;
    public final /* synthetic */ LongSparseArray f$15;
    public final /* synthetic */ LongSparseArray f$16;
    public final /* synthetic */ ArrayList f$17;
    public final /* synthetic */ ArrayList f$2;
    public final /* synthetic */ LongSparseArray f$3;
    public final /* synthetic */ int f$4;
    public final /* synthetic */ LongSparseIntArray f$5;
    public final /* synthetic */ LongSparseArray f$6;
    public final /* synthetic */ LongSparseArray f$7;
    public final /* synthetic */ ArrayList f$8;
    public final /* synthetic */ LongSparseArray f$9;

    public /* synthetic */ MessagesController$$ExternalSyntheticLambda48(MessagesController messagesController, int i, ArrayList arrayList, LongSparseArray longSparseArray, int i2, LongSparseIntArray longSparseIntArray, LongSparseArray longSparseArray2, LongSparseArray longSparseArray3, ArrayList arrayList2, LongSparseArray longSparseArray4, LongSparseArray longSparseArray5, boolean z, ArrayList arrayList3, ArrayList arrayList4, LongSparseArray longSparseArray6, LongSparseArray longSparseArray7, LongSparseArray longSparseArray8, ArrayList arrayList5) {
        this.f$0 = messagesController;
        this.f$1 = i;
        this.f$2 = arrayList;
        this.f$3 = longSparseArray;
        this.f$4 = i2;
        this.f$5 = longSparseIntArray;
        this.f$6 = longSparseArray2;
        this.f$7 = longSparseArray3;
        this.f$8 = arrayList2;
        this.f$9 = longSparseArray4;
        this.f$10 = longSparseArray5;
        this.f$11 = z;
        this.f$12 = arrayList3;
        this.f$13 = arrayList4;
        this.f$14 = longSparseArray6;
        this.f$15 = longSparseArray7;
        this.f$16 = longSparseArray8;
        this.f$17 = arrayList5;
    }

    public final void run() {
        MessagesController messagesController = this.f$0;
        MessagesController messagesController2 = messagesController;
        messagesController2.lambda$processUpdateArray$327(this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, this.f$6, this.f$7, this.f$8, this.f$9, this.f$10, this.f$11, this.f$12, this.f$13, this.f$14, this.f$15, this.f$16, this.f$17);
    }
}
