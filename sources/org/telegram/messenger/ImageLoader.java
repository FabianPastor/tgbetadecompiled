package org.telegram.messenger;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.SparseArray;
import j$.util.concurrent.ConcurrentHashMap;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.net.HttpURLConnection;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Locale;
import org.telegram.messenger.FileLoader;
import org.telegram.tgnet.TLObject;
import org.telegram.tgnet.TLRPC$Document;
import org.telegram.tgnet.TLRPC$FileLocation;
import org.telegram.tgnet.TLRPC$InputEncryptedFile;
import org.telegram.tgnet.TLRPC$InputFile;
import org.telegram.tgnet.TLRPC$Message;
import org.telegram.tgnet.TLRPC$MessageMedia;
import org.telegram.tgnet.TLRPC$Photo;
import org.telegram.tgnet.TLRPC$PhotoSize;
import org.telegram.tgnet.TLRPC$TL_documentAttributeVideo;
import org.telegram.tgnet.TLRPC$TL_error;
import org.telegram.tgnet.TLRPC$TL_fileLocationToBeDeprecated;
import org.telegram.tgnet.TLRPC$TL_fileLocationUnavailable;
import org.telegram.tgnet.TLRPC$TL_messageMediaDocument;
import org.telegram.tgnet.TLRPC$TL_messageMediaPhoto;
import org.telegram.tgnet.TLRPC$TL_messageMediaWebPage;
import org.telegram.tgnet.TLRPC$TL_photoCachedSize;
import org.telegram.tgnet.TLRPC$TL_photoSize_layer127;
import org.telegram.tgnet.TLRPC$TL_photoStrippedSize;
import org.telegram.ui.Cells.ChatMessageCell;
import org.telegram.ui.Components.AnimatedFileDrawable;
import org.telegram.ui.Components.RLottieDrawable;

public class ImageLoader {
    public static final String AUTOPLAY_FILTER = "g";
    private static volatile ImageLoader Instance = null;
    /* access modifiers changed from: private */
    public static ThreadLocal<byte[]> bytesLocal = new ThreadLocal<>();
    /* access modifiers changed from: private */
    public static ThreadLocal<byte[]> bytesThumbLocal = new ThreadLocal<>();
    /* access modifiers changed from: private */
    public static byte[] header = new byte[12];
    /* access modifiers changed from: private */
    public static byte[] headerThumb = new byte[12];
    /* access modifiers changed from: private */
    public LinkedList<ArtworkLoadTask> artworkTasks = new LinkedList<>();
    /* access modifiers changed from: private */
    public HashMap<String, Integer> bitmapUseCounts = new HashMap<>();
    /* access modifiers changed from: private */
    public DispatchQueue cacheOutQueue = new DispatchQueue("cacheOutQueue");
    /* access modifiers changed from: private */
    public DispatchQueue cacheThumbOutQueue = new DispatchQueue("cacheThumbOutQueue");
    ArrayList<AnimatedFileDrawable> cachedAnimatedFileDrawables = new ArrayList<>();
    /* access modifiers changed from: private */
    public boolean canForce8888;
    private int currentArtworkTasksCount;
    private int currentHttpFileLoadTasksCount;
    private int currentHttpTasksCount;
    /* access modifiers changed from: private */
    public ConcurrentHashMap<String, long[]> fileProgresses = new ConcurrentHashMap<>();
    /* access modifiers changed from: private */
    public HashMap<String, Integer> forceLoadingImages = new HashMap<>();
    private LinkedList<HttpFileTask> httpFileLoadTasks;
    private HashMap<String, HttpFileTask> httpFileLoadTasksByKeys;
    /* access modifiers changed from: private */
    public LinkedList<HttpImageTask> httpTasks = new LinkedList<>();
    /* access modifiers changed from: private */
    public String ignoreRemoval;
    /* access modifiers changed from: private */
    public DispatchQueue imageLoadQueue = new DispatchQueue("imageLoadQueue");
    /* access modifiers changed from: private */
    public HashMap<String, CacheImage> imageLoadingByKeys = new HashMap<>();
    /* access modifiers changed from: private */
    public SparseArray<CacheImage> imageLoadingByTag = new SparseArray<>();
    /* access modifiers changed from: private */
    public HashMap<String, CacheImage> imageLoadingByUrl = new HashMap<>();
    /* access modifiers changed from: private */
    public volatile long lastCacheOutTime;
    private int lastImageNum;
    /* access modifiers changed from: private */
    public LruCache<BitmapDrawable> lottieMemCache;
    /* access modifiers changed from: private */
    public LruCache<BitmapDrawable> memCache;
    private HashMap<String, String> replacedBitmaps = new HashMap<>();
    private HashMap<String, Runnable> retryHttpsTasks;
    /* access modifiers changed from: private */
    public LruCache<BitmapDrawable> smallImagesMemCache;
    private File telegramPath;
    /* access modifiers changed from: private */
    public ConcurrentHashMap<String, WebFile> testWebFile;
    /* access modifiers changed from: private */
    public HashMap<String, ThumbGenerateTask> thumbGenerateTasks = new HashMap<>();
    private DispatchQueue thumbGeneratingQueue = new DispatchQueue("thumbGeneratingQueue");
    private HashMap<String, ThumbGenerateInfo> waitingForQualityThumb = new HashMap<>();
    private SparseArray<String> waitingForQualityThumbByTag = new SparseArray<>();
    /* access modifiers changed from: private */
    public LruCache<BitmapDrawable> wallpaperMemCache;

    public void moveToFront(String str) {
        if (str != null) {
            if (this.memCache.get(str) != null) {
                this.memCache.moveToFront(str);
            }
            if (this.smallImagesMemCache.get(str) != null) {
                this.smallImagesMemCache.moveToFront(str);
            }
        }
    }

    public void putThumbsToCache(ArrayList<MessageThumb> arrayList) {
        for (int i = 0; i < arrayList.size(); i++) {
            putImageToCache(arrayList.get(i).drawable, arrayList.get(i).key, true);
        }
    }

    private static class ThumbGenerateInfo {
        /* access modifiers changed from: private */
        public boolean big;
        /* access modifiers changed from: private */
        public String filter;
        /* access modifiers changed from: private */
        public ArrayList<ImageReceiver> imageReceiverArray;
        /* access modifiers changed from: private */
        public ArrayList<Integer> imageReceiverGuidsArray;
        /* access modifiers changed from: private */
        public TLRPC$Document parentDocument;

        private ThumbGenerateInfo() {
            this.imageReceiverArray = new ArrayList<>();
            this.imageReceiverGuidsArray = new ArrayList<>();
        }
    }

    private class HttpFileTask extends AsyncTask<Void, Void, Boolean> {
        /* access modifiers changed from: private */
        public boolean canRetry = true;
        /* access modifiers changed from: private */
        public int currentAccount;
        /* access modifiers changed from: private */
        public String ext;
        private RandomAccessFile fileOutputStream = null;
        private int fileSize;
        private long lastProgressTime;
        /* access modifiers changed from: private */
        public File tempFile;
        /* access modifiers changed from: private */
        public String url;

        public HttpFileTask(String str, File file, String str2, int i) {
            this.url = str;
            this.tempFile = file;
            this.ext = str2;
            this.currentAccount = i;
        }

        private void reportProgress(long j, long j2) {
            long elapsedRealtime = SystemClock.elapsedRealtime();
            if (j != j2) {
                long j3 = this.lastProgressTime;
                if (j3 != 0 && j3 >= elapsedRealtime - 100) {
                    return;
                }
            }
            this.lastProgressTime = elapsedRealtime;
            Utilities.stageQueue.postRunnable(new ImageLoader$HttpFileTask$$ExternalSyntheticLambda0(this, j, j2));
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void lambda$reportProgress$1(long j, long j2) {
            ImageLoader.this.fileProgresses.put(this.url, new long[]{j, j2});
            AndroidUtilities.runOnUIThread(new ImageLoader$HttpFileTask$$ExternalSyntheticLambda1(this, j, j2));
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void lambda$reportProgress$0(long j, long j2) {
            NotificationCenter.getInstance(this.currentAccount).postNotificationName(NotificationCenter.fileLoadProgressChanged, this.url, Long.valueOf(j), Long.valueOf(j2));
        }

        /* access modifiers changed from: protected */
        /* JADX WARNING: Code restructure failed: missing block: B:81:0x0120, code lost:
            if (r5 != -1) goto L_0x012e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:83:?, code lost:
            r0 = r11.fileSize;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:84:0x0124, code lost:
            if (r0 == 0) goto L_0x013d;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:85:0x0126, code lost:
            reportProgress((long) r0, (long) r0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:86:0x012c, code lost:
            r0 = e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:87:0x012e, code lost:
            r1 = false;
         */
        /* JADX WARNING: Removed duplicated region for block: B:100:0x0142 A[Catch:{ all -> 0x0148 }] */
        /* JADX WARNING: Removed duplicated region for block: B:104:0x014e A[SYNTHETIC, Splitter:B:104:0x014e] */
        /* JADX WARNING: Removed duplicated region for block: B:24:0x0076  */
        /* JADX WARNING: Removed duplicated region for block: B:27:0x007f  */
        /* JADX WARNING: Removed duplicated region for block: B:43:0x00ad A[SYNTHETIC, Splitter:B:43:0x00ad] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.lang.Boolean doInBackground(java.lang.Void... r12) {
            /*
                r11 = this;
                java.lang.String r12 = "Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac OS X) AppleWebKit/602.1.38 (KHTML, like Gecko) Version/10.0 Mobile/14A5297c Safari/602.1"
                java.lang.String r0 = "User-Agent"
                r1 = 1
                r2 = 0
                r3 = 0
                java.net.URL r4 = new java.net.URL     // Catch:{ all -> 0x006f }
                java.lang.String r5 = r11.url     // Catch:{ all -> 0x006f }
                r4.<init>(r5)     // Catch:{ all -> 0x006f }
                java.net.URLConnection r4 = r4.openConnection()     // Catch:{ all -> 0x006f }
                r4.addRequestProperty(r0, r12)     // Catch:{ all -> 0x006c }
                r5 = 5000(0x1388, float:7.006E-42)
                r4.setConnectTimeout(r5)     // Catch:{ all -> 0x006c }
                r4.setReadTimeout(r5)     // Catch:{ all -> 0x006c }
                boolean r5 = r4 instanceof java.net.HttpURLConnection     // Catch:{ all -> 0x006c }
                if (r5 == 0) goto L_0x0054
                r5 = r4
                java.net.HttpURLConnection r5 = (java.net.HttpURLConnection) r5     // Catch:{ all -> 0x006c }
                r5.setInstanceFollowRedirects(r1)     // Catch:{ all -> 0x006c }
                int r6 = r5.getResponseCode()     // Catch:{ all -> 0x006c }
                r7 = 302(0x12e, float:4.23E-43)
                if (r6 == r7) goto L_0x0037
                r7 = 301(0x12d, float:4.22E-43)
                if (r6 == r7) goto L_0x0037
                r7 = 303(0x12f, float:4.25E-43)
                if (r6 != r7) goto L_0x0054
            L_0x0037:
                java.lang.String r6 = "Location"
                java.lang.String r6 = r5.getHeaderField(r6)     // Catch:{ all -> 0x006c }
                java.lang.String r7 = "Set-Cookie"
                java.lang.String r5 = r5.getHeaderField(r7)     // Catch:{ all -> 0x006c }
                java.net.URL r7 = new java.net.URL     // Catch:{ all -> 0x006c }
                r7.<init>(r6)     // Catch:{ all -> 0x006c }
                java.net.URLConnection r4 = r7.openConnection()     // Catch:{ all -> 0x006c }
                java.lang.String r6 = "Cookie"
                r4.setRequestProperty(r6, r5)     // Catch:{ all -> 0x006c }
                r4.addRequestProperty(r0, r12)     // Catch:{ all -> 0x006c }
            L_0x0054:
                r4.connect()     // Catch:{ all -> 0x006c }
                java.io.InputStream r12 = r4.getInputStream()     // Catch:{ all -> 0x006c }
                java.io.RandomAccessFile r0 = new java.io.RandomAccessFile     // Catch:{ all -> 0x0067 }
                java.io.File r5 = r11.tempFile     // Catch:{ all -> 0x0067 }
                java.lang.String r6 = "rws"
                r0.<init>(r5, r6)     // Catch:{ all -> 0x0067 }
                r11.fileOutputStream = r0     // Catch:{ all -> 0x0067 }
                goto L_0x00a9
            L_0x0067:
                r0 = move-exception
                r10 = r0
                r0 = r12
                r12 = r10
                goto L_0x0072
            L_0x006c:
                r12 = move-exception
                r0 = r2
                goto L_0x0072
            L_0x006f:
                r12 = move-exception
                r0 = r2
                r4 = r0
            L_0x0072:
                boolean r5 = r12 instanceof java.net.SocketTimeoutException
                if (r5 == 0) goto L_0x007f
                boolean r5 = org.telegram.messenger.ApplicationLoader.isNetworkOnline()
                if (r5 == 0) goto L_0x00a5
                r11.canRetry = r3
                goto L_0x00a5
            L_0x007f:
                boolean r5 = r12 instanceof java.net.UnknownHostException
                if (r5 == 0) goto L_0x0086
                r11.canRetry = r3
                goto L_0x00a5
            L_0x0086:
                boolean r5 = r12 instanceof java.net.SocketException
                if (r5 == 0) goto L_0x009f
                java.lang.String r5 = r12.getMessage()
                if (r5 == 0) goto L_0x00a5
                java.lang.String r5 = r12.getMessage()
                java.lang.String r6 = "ECONNRESET"
                boolean r5 = r5.contains(r6)
                if (r5 == 0) goto L_0x00a5
                r11.canRetry = r3
                goto L_0x00a5
            L_0x009f:
                boolean r5 = r12 instanceof java.io.FileNotFoundException
                if (r5 == 0) goto L_0x00a5
                r11.canRetry = r3
            L_0x00a5:
                org.telegram.messenger.FileLog.e((java.lang.Throwable) r12)
                r12 = r0
            L_0x00a9:
                boolean r0 = r11.canRetry
                if (r0 == 0) goto L_0x0156
                boolean r0 = r4 instanceof java.net.HttpURLConnection     // Catch:{ Exception -> 0x00c7 }
                if (r0 == 0) goto L_0x00cb
                r0 = r4
                java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x00c7 }
                int r0 = r0.getResponseCode()     // Catch:{ Exception -> 0x00c7 }
                r5 = 200(0xc8, float:2.8E-43)
                if (r0 == r5) goto L_0x00cb
                r5 = 202(0xca, float:2.83E-43)
                if (r0 == r5) goto L_0x00cb
                r5 = 304(0x130, float:4.26E-43)
                if (r0 == r5) goto L_0x00cb
                r11.canRetry = r3     // Catch:{ Exception -> 0x00c7 }
                goto L_0x00cb
            L_0x00c7:
                r0 = move-exception
                org.telegram.messenger.FileLog.e((java.lang.Throwable) r0)
            L_0x00cb:
                if (r4 == 0) goto L_0x00fa
                java.util.Map r0 = r4.getHeaderFields()     // Catch:{ Exception -> 0x00f6 }
                if (r0 == 0) goto L_0x00fa
                java.lang.String r4 = "content-Length"
                java.lang.Object r0 = r0.get(r4)     // Catch:{ Exception -> 0x00f6 }
                java.util.List r0 = (java.util.List) r0     // Catch:{ Exception -> 0x00f6 }
                if (r0 == 0) goto L_0x00fa
                boolean r4 = r0.isEmpty()     // Catch:{ Exception -> 0x00f6 }
                if (r4 != 0) goto L_0x00fa
                java.lang.Object r0 = r0.get(r3)     // Catch:{ Exception -> 0x00f6 }
                java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x00f6 }
                if (r0 == 0) goto L_0x00fa
                java.lang.Integer r0 = org.telegram.messenger.Utilities.parseInt((java.lang.CharSequence) r0)     // Catch:{ Exception -> 0x00f6 }
                int r0 = r0.intValue()     // Catch:{ Exception -> 0x00f6 }
                r11.fileSize = r0     // Catch:{ Exception -> 0x00f6 }
                goto L_0x00fa
            L_0x00f6:
                r0 = move-exception
                org.telegram.messenger.FileLog.e((java.lang.Throwable) r0)
            L_0x00fa:
                if (r12 == 0) goto L_0x013e
                r0 = 32768(0x8000, float:4.5918E-41)
                byte[] r0 = new byte[r0]     // Catch:{ all -> 0x0138 }
                r4 = 0
            L_0x0102:
                boolean r5 = r11.isCancelled()     // Catch:{ all -> 0x0138 }
                if (r5 == 0) goto L_0x0109
                goto L_0x012e
            L_0x0109:
                int r5 = r12.read(r0)     // Catch:{ Exception -> 0x0130 }
                if (r5 <= 0) goto L_0x011f
                java.io.RandomAccessFile r6 = r11.fileOutputStream     // Catch:{ Exception -> 0x0130 }
                r6.write(r0, r3, r5)     // Catch:{ Exception -> 0x0130 }
                int r4 = r4 + r5
                int r5 = r11.fileSize     // Catch:{ Exception -> 0x0130 }
                if (r5 <= 0) goto L_0x0102
                long r6 = (long) r4     // Catch:{ Exception -> 0x0130 }
                long r8 = (long) r5     // Catch:{ Exception -> 0x0130 }
                r11.reportProgress(r6, r8)     // Catch:{ Exception -> 0x0130 }
                goto L_0x0102
            L_0x011f:
                r0 = -1
                if (r5 != r0) goto L_0x012e
                int r0 = r11.fileSize     // Catch:{ Exception -> 0x012c }
                if (r0 == 0) goto L_0x013d
                long r3 = (long) r0     // Catch:{ Exception -> 0x012c }
                long r5 = (long) r0     // Catch:{ Exception -> 0x012c }
                r11.reportProgress(r3, r5)     // Catch:{ Exception -> 0x012c }
                goto L_0x013d
            L_0x012c:
                r0 = move-exception
                goto L_0x0132
            L_0x012e:
                r1 = 0
                goto L_0x013d
            L_0x0130:
                r0 = move-exception
                r1 = 0
            L_0x0132:
                org.telegram.messenger.FileLog.e((java.lang.Throwable) r0)     // Catch:{ all -> 0x0136 }
                goto L_0x013d
            L_0x0136:
                r0 = move-exception
                goto L_0x013a
            L_0x0138:
                r0 = move-exception
                r1 = 0
            L_0x013a:
                org.telegram.messenger.FileLog.e((java.lang.Throwable) r0)
            L_0x013d:
                r3 = r1
            L_0x013e:
                java.io.RandomAccessFile r0 = r11.fileOutputStream     // Catch:{ all -> 0x0148 }
                if (r0 == 0) goto L_0x014c
                r0.close()     // Catch:{ all -> 0x0148 }
                r11.fileOutputStream = r2     // Catch:{ all -> 0x0148 }
                goto L_0x014c
            L_0x0148:
                r0 = move-exception
                org.telegram.messenger.FileLog.e((java.lang.Throwable) r0)
            L_0x014c:
                if (r12 == 0) goto L_0x0156
                r12.close()     // Catch:{ all -> 0x0152 }
                goto L_0x0156
            L_0x0152:
                r12 = move-exception
                org.telegram.messenger.FileLog.e((java.lang.Throwable) r12)
            L_0x0156:
                java.lang.Boolean r12 = java.lang.Boolean.valueOf(r3)
                return r12
            */
            throw new UnsupportedOperationException("Method not decompiled: org.telegram.messenger.ImageLoader.HttpFileTask.doInBackground(java.lang.Void[]):java.lang.Boolean");
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Boolean bool) {
            ImageLoader.this.runHttpFileLoadTasks(this, bool.booleanValue() ? 2 : 1);
        }

        /* access modifiers changed from: protected */
        public void onCancelled() {
            ImageLoader.this.runHttpFileLoadTasks(this, 2);
        }
    }

    private class ArtworkLoadTask extends AsyncTask<Void, Void, String> {
        /* access modifiers changed from: private */
        public CacheImage cacheImage;
        private boolean canRetry = true;
        private HttpURLConnection httpConnection;
        private boolean small;

        public ArtworkLoadTask(CacheImage cacheImage2) {
            boolean z = true;
            this.cacheImage = cacheImage2;
            this.small = Uri.parse(cacheImage2.imageLocation.path).getQueryParameter("s") == null ? false : z;
        }

        /* access modifiers changed from: protected */
        /* JADX WARNING: Removed duplicated region for block: B:104:0x0129 A[Catch:{ all -> 0x012d }] */
        /* JADX WARNING: Removed duplicated region for block: B:107:0x0130 A[SYNTHETIC, Splitter:B:107:0x0130] */
        /* JADX WARNING: Removed duplicated region for block: B:112:0x013a A[SYNTHETIC, Splitter:B:112:0x013a] */
        /* JADX WARNING: Removed duplicated region for block: B:128:0x0154 A[SYNTHETIC, Splitter:B:128:0x0154] */
        /* JADX WARNING: Removed duplicated region for block: B:84:0x00f3 A[Catch:{ all -> 0x013e, all -> 0x0147, all -> 0x014e }] */
        /* JADX WARNING: Removed duplicated region for block: B:87:0x00fc A[Catch:{ all -> 0x013e, all -> 0x0147, all -> 0x014e }] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.lang.String doInBackground(java.lang.Void... r8) {
            /*
                r7 = this;
                r8 = 0
                r0 = 0
                org.telegram.messenger.ImageLoader$CacheImage r1 = r7.cacheImage     // Catch:{ all -> 0x00ec }
                org.telegram.messenger.ImageLocation r1 = r1.imageLocation     // Catch:{ all -> 0x00ec }
                java.lang.String r1 = r1.path     // Catch:{ all -> 0x00ec }
                java.net.URL r2 = new java.net.URL     // Catch:{ all -> 0x00ec }
                java.lang.String r3 = "athumb://"
                java.lang.String r4 = "https://"
                java.lang.String r1 = r1.replace(r3, r4)     // Catch:{ all -> 0x00ec }
                r2.<init>(r1)     // Catch:{ all -> 0x00ec }
                java.net.URLConnection r1 = r2.openConnection()     // Catch:{ all -> 0x00ec }
                java.net.HttpURLConnection r1 = (java.net.HttpURLConnection) r1     // Catch:{ all -> 0x00ec }
                r7.httpConnection = r1     // Catch:{ all -> 0x00ec }
                r2 = 5000(0x1388, float:7.006E-42)
                r1.setConnectTimeout(r2)     // Catch:{ all -> 0x00ec }
                java.net.HttpURLConnection r1 = r7.httpConnection     // Catch:{ all -> 0x00ec }
                r1.setReadTimeout(r2)     // Catch:{ all -> 0x00ec }
                java.net.HttpURLConnection r1 = r7.httpConnection     // Catch:{ all -> 0x00ec }
                r1.connect()     // Catch:{ all -> 0x00ec }
                java.net.HttpURLConnection r1 = r7.httpConnection     // Catch:{ Exception -> 0x0043 }
                if (r1 == 0) goto L_0x0047
                int r1 = r1.getResponseCode()     // Catch:{ Exception -> 0x0043 }
                r2 = 200(0xc8, float:2.8E-43)
                if (r1 == r2) goto L_0x0047
                r2 = 202(0xca, float:2.83E-43)
                if (r1 == r2) goto L_0x0047
                r2 = 304(0x130, float:4.26E-43)
                if (r1 == r2) goto L_0x0047
                r7.canRetry = r0     // Catch:{ Exception -> 0x0043 }
                goto L_0x0047
            L_0x0043:
                r1 = move-exception
                org.telegram.messenger.FileLog.e((java.lang.Throwable) r1, (boolean) r0)     // Catch:{ all -> 0x00ec }
            L_0x0047:
                java.net.HttpURLConnection r1 = r7.httpConnection     // Catch:{ all -> 0x00ec }
                java.io.InputStream r1 = r1.getInputStream()     // Catch:{ all -> 0x00ec }
                java.io.ByteArrayOutputStream r2 = new java.io.ByteArrayOutputStream     // Catch:{ all -> 0x00e6 }
                r2.<init>()     // Catch:{ all -> 0x00e6 }
                r3 = 32768(0x8000, float:4.5918E-41)
                byte[] r3 = new byte[r3]     // Catch:{ all -> 0x00e0 }
            L_0x0057:
                boolean r4 = r7.isCancelled()     // Catch:{ all -> 0x00e0 }
                if (r4 == 0) goto L_0x005e
                goto L_0x0068
            L_0x005e:
                int r4 = r1.read(r3)     // Catch:{ all -> 0x00e0 }
                if (r4 <= 0) goto L_0x0068
                r2.write(r3, r0, r4)     // Catch:{ all -> 0x00e0 }
                goto L_0x0057
            L_0x0068:
                r7.canRetry = r0     // Catch:{ all -> 0x00e0 }
                org.json.JSONObject r3 = new org.json.JSONObject     // Catch:{ all -> 0x00e0 }
                java.lang.String r4 = new java.lang.String     // Catch:{ all -> 0x00e0 }
                byte[] r5 = r2.toByteArray()     // Catch:{ all -> 0x00e0 }
                r4.<init>(r5)     // Catch:{ all -> 0x00e0 }
                r3.<init>(r4)     // Catch:{ all -> 0x00e0 }
                java.lang.String r4 = "results"
                org.json.JSONArray r3 = r3.getJSONArray(r4)     // Catch:{ all -> 0x00e0 }
                int r4 = r3.length()     // Catch:{ all -> 0x00e0 }
                if (r4 <= 0) goto L_0x00c8
                org.json.JSONObject r3 = r3.getJSONObject(r0)     // Catch:{ all -> 0x00e0 }
                java.lang.String r4 = "artworkUrl100"
                java.lang.String r3 = r3.getString(r4)     // Catch:{ all -> 0x00e0 }
                boolean r4 = r7.small     // Catch:{ all -> 0x00e0 }
                if (r4 == 0) goto L_0x00a9
                java.net.HttpURLConnection r8 = r7.httpConnection     // Catch:{ all -> 0x009a }
                if (r8 == 0) goto L_0x009b
                r8.disconnect()     // Catch:{ all -> 0x009a }
                goto L_0x009b
            L_0x009a:
            L_0x009b:
                if (r1 == 0) goto L_0x00a5
                r1.close()     // Catch:{ all -> 0x00a1 }
                goto L_0x00a5
            L_0x00a1:
                r8 = move-exception
                org.telegram.messenger.FileLog.e((java.lang.Throwable) r8)
            L_0x00a5:
                r2.close()     // Catch:{ Exception -> 0x00a8 }
            L_0x00a8:
                return r3
            L_0x00a9:
                java.lang.String r4 = "100x100"
                java.lang.String r5 = "600x600"
                java.lang.String r8 = r3.replace(r4, r5)     // Catch:{ all -> 0x00e0 }
                java.net.HttpURLConnection r0 = r7.httpConnection     // Catch:{ all -> 0x00b9 }
                if (r0 == 0) goto L_0x00ba
                r0.disconnect()     // Catch:{ all -> 0x00b9 }
                goto L_0x00ba
            L_0x00b9:
            L_0x00ba:
                if (r1 == 0) goto L_0x00c4
                r1.close()     // Catch:{ all -> 0x00c0 }
                goto L_0x00c4
            L_0x00c0:
                r0 = move-exception
                org.telegram.messenger.FileLog.e((java.lang.Throwable) r0)
            L_0x00c4:
                r2.close()     // Catch:{ Exception -> 0x00c7 }
            L_0x00c7:
                return r8
            L_0x00c8:
                java.net.HttpURLConnection r0 = r7.httpConnection     // Catch:{ all -> 0x00d0 }
                if (r0 == 0) goto L_0x00d1
                r0.disconnect()     // Catch:{ all -> 0x00d0 }
                goto L_0x00d1
            L_0x00d0:
            L_0x00d1:
                if (r1 == 0) goto L_0x00db
                r1.close()     // Catch:{ all -> 0x00d7 }
                goto L_0x00db
            L_0x00d7:
                r0 = move-exception
                org.telegram.messenger.FileLog.e((java.lang.Throwable) r0)
            L_0x00db:
                r2.close()     // Catch:{ Exception -> 0x013d }
                goto L_0x013d
            L_0x00e0:
                r3 = move-exception
                r6 = r2
                r2 = r1
                r1 = r3
                r3 = r6
                goto L_0x00ef
            L_0x00e6:
                r2 = move-exception
                r3 = r8
                r6 = r2
                r2 = r1
                r1 = r6
                goto L_0x00ef
            L_0x00ec:
                r1 = move-exception
                r2 = r8
                r3 = r2
            L_0x00ef:
                boolean r4 = r1 instanceof java.net.SocketTimeoutException     // Catch:{ all -> 0x013e }
                if (r4 == 0) goto L_0x00fc
                boolean r4 = org.telegram.messenger.ApplicationLoader.isNetworkOnline()     // Catch:{ all -> 0x013e }
                if (r4 == 0) goto L_0x0122
                r7.canRetry = r0     // Catch:{ all -> 0x013e }
                goto L_0x0122
            L_0x00fc:
                boolean r4 = r1 instanceof java.net.UnknownHostException     // Catch:{ all -> 0x013e }
                if (r4 == 0) goto L_0x0103
                r7.canRetry = r0     // Catch:{ all -> 0x013e }
                goto L_0x0122
            L_0x0103:
                boolean r4 = r1 instanceof java.net.SocketException     // Catch:{ all -> 0x013e }
                if (r4 == 0) goto L_0x011c
                java.lang.String r4 = r1.getMessage()     // Catch:{ all -> 0x013e }
                if (r4 == 0) goto L_0x0122
                java.lang.String r4 = r1.getMessage()     // Catch:{ all -> 0x013e }
                java.lang.String r5 = "ECONNRESET"
                boolean r4 = r4.contains(r5)     // Catch:{ all -> 0x013e }
                if (r4 == 0) goto L_0x0122
                r7.canRetry = r0     // Catch:{ all -> 0x013e }
                goto L_0x0122
            L_0x011c:
                boolean r4 = r1 instanceof java.io.FileNotFoundException     // Catch:{ all -> 0x013e }
                if (r4 == 0) goto L_0x0122
                r7.canRetry = r0     // Catch:{ all -> 0x013e }
            L_0x0122:
                org.telegram.messenger.FileLog.e((java.lang.Throwable) r1, (boolean) r0)     // Catch:{ all -> 0x013e }
                java.net.HttpURLConnection r0 = r7.httpConnection     // Catch:{ all -> 0x012d }
                if (r0 == 0) goto L_0x012e
                r0.disconnect()     // Catch:{ all -> 0x012d }
                goto L_0x012e
            L_0x012d:
            L_0x012e:
                if (r2 == 0) goto L_0x0138
                r2.close()     // Catch:{ all -> 0x0134 }
                goto L_0x0138
            L_0x0134:
                r0 = move-exception
                org.telegram.messenger.FileLog.e((java.lang.Throwable) r0)
            L_0x0138:
                if (r3 == 0) goto L_0x013d
                r3.close()     // Catch:{ Exception -> 0x013d }
            L_0x013d:
                return r8
            L_0x013e:
                r8 = move-exception
                java.net.HttpURLConnection r0 = r7.httpConnection     // Catch:{ all -> 0x0147 }
                if (r0 == 0) goto L_0x0148
                r0.disconnect()     // Catch:{ all -> 0x0147 }
                goto L_0x0148
            L_0x0147:
            L_0x0148:
                if (r2 == 0) goto L_0x0152
                r2.close()     // Catch:{ all -> 0x014e }
                goto L_0x0152
            L_0x014e:
                r0 = move-exception
                org.telegram.messenger.FileLog.e((java.lang.Throwable) r0)
            L_0x0152:
                if (r3 == 0) goto L_0x0157
                r3.close()     // Catch:{ Exception -> 0x0157 }
            L_0x0157:
                goto L_0x0159
            L_0x0158:
                throw r8
            L_0x0159:
                goto L_0x0158
            */
            throw new UnsupportedOperationException("Method not decompiled: org.telegram.messenger.ImageLoader.ArtworkLoadTask.doInBackground(java.lang.Void[]):java.lang.String");
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String str) {
            if (str != null) {
                ImageLoader.this.imageLoadQueue.postRunnable(new ImageLoader$ArtworkLoadTask$$ExternalSyntheticLambda2(this, str));
            } else if (this.canRetry) {
                ImageLoader.this.artworkLoadError(this.cacheImage.url);
            }
            ImageLoader.this.imageLoadQueue.postRunnable(new ImageLoader$ArtworkLoadTask$$ExternalSyntheticLambda0(this));
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void lambda$onPostExecute$0(String str) {
            CacheImage cacheImage2 = this.cacheImage;
            cacheImage2.httpTask = new HttpImageTask(cacheImage2, 0, str);
            ImageLoader.this.httpTasks.add(this.cacheImage.httpTask);
            ImageLoader.this.runHttpTasks(false);
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void lambda$onPostExecute$1() {
            ImageLoader.this.runArtworkTasks(true);
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void lambda$onCancelled$2() {
            ImageLoader.this.runArtworkTasks(true);
        }

        /* access modifiers changed from: protected */
        public void onCancelled() {
            ImageLoader.this.imageLoadQueue.postRunnable(new ImageLoader$ArtworkLoadTask$$ExternalSyntheticLambda1(this));
        }
    }

    private class HttpImageTask extends AsyncTask<Void, Void, Boolean> {
        /* access modifiers changed from: private */
        public CacheImage cacheImage;
        private boolean canRetry = true;
        private RandomAccessFile fileOutputStream;
        private HttpURLConnection httpConnection;
        /* access modifiers changed from: private */
        public long imageSize;
        private long lastProgressTime;
        private String overrideUrl;

        /* access modifiers changed from: private */
        public static /* synthetic */ void lambda$doInBackground$2(TLObject tLObject, TLRPC$TL_error tLRPC$TL_error) {
        }

        public HttpImageTask(CacheImage cacheImage2, long j) {
            this.cacheImage = cacheImage2;
            this.imageSize = j;
        }

        public HttpImageTask(CacheImage cacheImage2, int i, String str) {
            this.cacheImage = cacheImage2;
            this.imageSize = (long) i;
            this.overrideUrl = str;
        }

        private void reportProgress(long j, long j2) {
            long elapsedRealtime = SystemClock.elapsedRealtime();
            if (j != j2) {
                long j3 = this.lastProgressTime;
                if (j3 != 0 && j3 >= elapsedRealtime - 100) {
                    return;
                }
            }
            this.lastProgressTime = elapsedRealtime;
            Utilities.stageQueue.postRunnable(new ImageLoader$HttpImageTask$$ExternalSyntheticLambda4(this, j, j2));
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void lambda$reportProgress$1(long j, long j2) {
            ImageLoader.this.fileProgresses.put(this.cacheImage.url, new long[]{j, j2});
            AndroidUtilities.runOnUIThread(new ImageLoader$HttpImageTask$$ExternalSyntheticLambda5(this, j, j2));
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void lambda$reportProgress$0(long j, long j2) {
            NotificationCenter.getInstance(this.cacheImage.currentAccount).postNotificationName(NotificationCenter.fileLoadProgressChanged, this.cacheImage.url, Long.valueOf(j), Long.valueOf(j2));
        }

        /* access modifiers changed from: protected */
        /* JADX WARNING: Code restructure failed: missing block: B:100:0x0179, code lost:
            r2 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:101:0x017a, code lost:
            r1 = r2;
            r2 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:93:0x0169, code lost:
            if (r7 != -1) goto L_0x017d;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:95:?, code lost:
            r2 = r12.imageSize;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:96:0x016f, code lost:
            if (r2 == 0) goto L_0x0184;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:97:0x0171, code lost:
            reportProgress(r2, r2);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:98:0x0175, code lost:
            r2 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:99:0x0176, code lost:
            r1 = r2;
            r2 = true;
         */
        /* JADX WARNING: Removed duplicated region for block: B:113:0x018e A[Catch:{ all -> 0x0194 }] */
        /* JADX WARNING: Removed duplicated region for block: B:119:0x019c A[Catch:{ all -> 0x01a0 }] */
        /* JADX WARNING: Removed duplicated region for block: B:122:0x01a3 A[SYNTHETIC, Splitter:B:122:0x01a3] */
        /* JADX WARNING: Removed duplicated region for block: B:52:0x00ee A[SYNTHETIC, Splitter:B:52:0x00ee] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.lang.Boolean doInBackground(java.lang.Void... r13) {
            /*
                r12 = this;
                boolean r13 = r12.isCancelled()
                r0 = 0
                r1 = 1
                r2 = 0
                if (r13 != 0) goto L_0x00e7
                org.telegram.messenger.ImageLoader$CacheImage r13 = r12.cacheImage     // Catch:{ all -> 0x00a5 }
                org.telegram.messenger.ImageLocation r13 = r13.imageLocation     // Catch:{ all -> 0x00a5 }
                java.lang.String r13 = r13.path     // Catch:{ all -> 0x00a5 }
                java.lang.String r3 = "https://static-maps"
                boolean r3 = r13.startsWith(r3)     // Catch:{ all -> 0x00a5 }
                if (r3 != 0) goto L_0x001f
                java.lang.String r3 = "https://maps.googleapis"
                boolean r3 = r13.startsWith(r3)     // Catch:{ all -> 0x00a5 }
                if (r3 == 0) goto L_0x0057
            L_0x001f:
                org.telegram.messenger.ImageLoader$CacheImage r3 = r12.cacheImage     // Catch:{ all -> 0x00a5 }
                int r3 = r3.currentAccount     // Catch:{ all -> 0x00a5 }
                org.telegram.messenger.MessagesController r3 = org.telegram.messenger.MessagesController.getInstance(r3)     // Catch:{ all -> 0x00a5 }
                int r3 = r3.mapProvider     // Catch:{ all -> 0x00a5 }
                r4 = 3
                if (r3 == r4) goto L_0x002f
                r4 = 4
                if (r3 != r4) goto L_0x0057
            L_0x002f:
                org.telegram.messenger.ImageLoader r3 = org.telegram.messenger.ImageLoader.this     // Catch:{ all -> 0x00a5 }
                j$.util.concurrent.ConcurrentHashMap r3 = r3.testWebFile     // Catch:{ all -> 0x00a5 }
                java.lang.Object r3 = r3.get(r13)     // Catch:{ all -> 0x00a5 }
                org.telegram.messenger.WebFile r3 = (org.telegram.messenger.WebFile) r3     // Catch:{ all -> 0x00a5 }
                if (r3 == 0) goto L_0x0057
                org.telegram.tgnet.TLRPC$TL_upload_getWebFile r4 = new org.telegram.tgnet.TLRPC$TL_upload_getWebFile     // Catch:{ all -> 0x00a5 }
                r4.<init>()     // Catch:{ all -> 0x00a5 }
                org.telegram.tgnet.TLRPC$InputWebFileLocation r3 = r3.location     // Catch:{ all -> 0x00a5 }
                r4.location = r3     // Catch:{ all -> 0x00a5 }
                r4.offset = r2     // Catch:{ all -> 0x00a5 }
                r4.limit = r2     // Catch:{ all -> 0x00a5 }
                org.telegram.messenger.ImageLoader$CacheImage r3 = r12.cacheImage     // Catch:{ all -> 0x00a5 }
                int r3 = r3.currentAccount     // Catch:{ all -> 0x00a5 }
                org.telegram.tgnet.ConnectionsManager r3 = org.telegram.tgnet.ConnectionsManager.getInstance(r3)     // Catch:{ all -> 0x00a5 }
                org.telegram.messenger.ImageLoader$HttpImageTask$$ExternalSyntheticLambda8 r5 = org.telegram.messenger.ImageLoader$HttpImageTask$$ExternalSyntheticLambda8.INSTANCE     // Catch:{ all -> 0x00a5 }
                r3.sendRequest(r4, r5)     // Catch:{ all -> 0x00a5 }
            L_0x0057:
                java.net.URL r3 = new java.net.URL     // Catch:{ all -> 0x00a5 }
                java.lang.String r4 = r12.overrideUrl     // Catch:{ all -> 0x00a5 }
                if (r4 == 0) goto L_0x005e
                r13 = r4
            L_0x005e:
                r3.<init>(r13)     // Catch:{ all -> 0x00a5 }
                java.net.URLConnection r13 = r3.openConnection()     // Catch:{ all -> 0x00a5 }
                java.net.HttpURLConnection r13 = (java.net.HttpURLConnection) r13     // Catch:{ all -> 0x00a5 }
                r12.httpConnection = r13     // Catch:{ all -> 0x00a5 }
                java.lang.String r3 = "User-Agent"
                java.lang.String r4 = "Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac OS X) AppleWebKit/602.1.38 (KHTML, like Gecko) Version/10.0 Mobile/14A5297c Safari/602.1"
                r13.addRequestProperty(r3, r4)     // Catch:{ all -> 0x00a5 }
                java.net.HttpURLConnection r13 = r12.httpConnection     // Catch:{ all -> 0x00a5 }
                r3 = 5000(0x1388, float:7.006E-42)
                r13.setConnectTimeout(r3)     // Catch:{ all -> 0x00a5 }
                java.net.HttpURLConnection r13 = r12.httpConnection     // Catch:{ all -> 0x00a5 }
                r13.setReadTimeout(r3)     // Catch:{ all -> 0x00a5 }
                java.net.HttpURLConnection r13 = r12.httpConnection     // Catch:{ all -> 0x00a5 }
                r13.setInstanceFollowRedirects(r1)     // Catch:{ all -> 0x00a5 }
                boolean r13 = r12.isCancelled()     // Catch:{ all -> 0x00a5 }
                if (r13 != 0) goto L_0x00e7
                java.net.HttpURLConnection r13 = r12.httpConnection     // Catch:{ all -> 0x00a5 }
                r13.connect()     // Catch:{ all -> 0x00a5 }
                java.net.HttpURLConnection r13 = r12.httpConnection     // Catch:{ all -> 0x00a5 }
                java.io.InputStream r13 = r13.getInputStream()     // Catch:{ all -> 0x00a5 }
                java.io.RandomAccessFile r3 = new java.io.RandomAccessFile     // Catch:{ all -> 0x00a0 }
                org.telegram.messenger.ImageLoader$CacheImage r4 = r12.cacheImage     // Catch:{ all -> 0x00a0 }
                java.io.File r4 = r4.tempFilePath     // Catch:{ all -> 0x00a0 }
                java.lang.String r5 = "rws"
                r3.<init>(r4, r5)     // Catch:{ all -> 0x00a0 }
                r12.fileOutputStream = r3     // Catch:{ all -> 0x00a0 }
                goto L_0x00e8
            L_0x00a0:
                r3 = move-exception
                r11 = r3
                r3 = r13
                r13 = r11
                goto L_0x00a7
            L_0x00a5:
                r13 = move-exception
                r3 = r0
            L_0x00a7:
                boolean r4 = r13 instanceof java.net.SocketTimeoutException
                if (r4 == 0) goto L_0x00b4
                boolean r4 = org.telegram.messenger.ApplicationLoader.isNetworkOnline()
                if (r4 == 0) goto L_0x00ba
                r12.canRetry = r2
                goto L_0x00ba
            L_0x00b4:
                boolean r4 = r13 instanceof java.net.UnknownHostException
                if (r4 == 0) goto L_0x00bc
                r12.canRetry = r2
            L_0x00ba:
                r4 = 0
                goto L_0x00e2
            L_0x00bc:
                boolean r4 = r13 instanceof java.net.SocketException
                if (r4 == 0) goto L_0x00d5
                java.lang.String r4 = r13.getMessage()
                if (r4 == 0) goto L_0x00ba
                java.lang.String r4 = r13.getMessage()
                java.lang.String r5 = "ECONNRESET"
                boolean r4 = r4.contains(r5)
                if (r4 == 0) goto L_0x00ba
                r12.canRetry = r2
                goto L_0x00ba
            L_0x00d5:
                boolean r4 = r13 instanceof java.io.FileNotFoundException
                if (r4 == 0) goto L_0x00dc
                r12.canRetry = r2
                goto L_0x00ba
            L_0x00dc:
                boolean r4 = r13 instanceof java.io.InterruptedIOException
                if (r4 == 0) goto L_0x00e1
                goto L_0x00ba
            L_0x00e1:
                r4 = 1
            L_0x00e2:
                org.telegram.messenger.FileLog.e((java.lang.Throwable) r13, (boolean) r4)
                r13 = r3
                goto L_0x00e8
            L_0x00e7:
                r13 = r0
            L_0x00e8:
                boolean r3 = r12.isCancelled()
                if (r3 != 0) goto L_0x018a
                java.net.HttpURLConnection r3 = r12.httpConnection     // Catch:{ Exception -> 0x0105 }
                if (r3 == 0) goto L_0x0109
                int r3 = r3.getResponseCode()     // Catch:{ Exception -> 0x0105 }
                r4 = 200(0xc8, float:2.8E-43)
                if (r3 == r4) goto L_0x0109
                r4 = 202(0xca, float:2.83E-43)
                if (r3 == r4) goto L_0x0109
                r4 = 304(0x130, float:4.26E-43)
                if (r3 == r4) goto L_0x0109
                r12.canRetry = r2     // Catch:{ Exception -> 0x0105 }
                goto L_0x0109
            L_0x0105:
                r3 = move-exception
                org.telegram.messenger.FileLog.e((java.lang.Throwable) r3)
            L_0x0109:
                long r3 = r12.imageSize
                r5 = 0
                int r7 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
                if (r7 != 0) goto L_0x0143
                java.net.HttpURLConnection r3 = r12.httpConnection
                if (r3 == 0) goto L_0x0143
                java.util.Map r3 = r3.getHeaderFields()     // Catch:{ Exception -> 0x013f }
                if (r3 == 0) goto L_0x0143
                java.lang.String r4 = "content-Length"
                java.lang.Object r3 = r3.get(r4)     // Catch:{ Exception -> 0x013f }
                java.util.List r3 = (java.util.List) r3     // Catch:{ Exception -> 0x013f }
                if (r3 == 0) goto L_0x0143
                boolean r4 = r3.isEmpty()     // Catch:{ Exception -> 0x013f }
                if (r4 != 0) goto L_0x0143
                java.lang.Object r3 = r3.get(r2)     // Catch:{ Exception -> 0x013f }
                java.lang.String r3 = (java.lang.String) r3     // Catch:{ Exception -> 0x013f }
                if (r3 == 0) goto L_0x0143
                java.lang.Integer r3 = org.telegram.messenger.Utilities.parseInt((java.lang.CharSequence) r3)     // Catch:{ Exception -> 0x013f }
                int r3 = r3.intValue()     // Catch:{ Exception -> 0x013f }
                long r3 = (long) r3     // Catch:{ Exception -> 0x013f }
                r12.imageSize = r3     // Catch:{ Exception -> 0x013f }
                goto L_0x0143
            L_0x013f:
                r3 = move-exception
                org.telegram.messenger.FileLog.e((java.lang.Throwable) r3)
            L_0x0143:
                if (r13 == 0) goto L_0x018a
                r3 = 8192(0x2000, float:1.14794E-41)
                byte[] r3 = new byte[r3]     // Catch:{ all -> 0x0186 }
                r4 = 0
            L_0x014a:
                boolean r7 = r12.isCancelled()     // Catch:{ all -> 0x0186 }
                if (r7 == 0) goto L_0x0151
                goto L_0x017d
            L_0x0151:
                int r7 = r13.read(r3)     // Catch:{ Exception -> 0x017f }
                if (r7 <= 0) goto L_0x0168
                int r4 = r4 + r7
                java.io.RandomAccessFile r8 = r12.fileOutputStream     // Catch:{ Exception -> 0x017f }
                r8.write(r3, r2, r7)     // Catch:{ Exception -> 0x017f }
                long r7 = r12.imageSize     // Catch:{ Exception -> 0x017f }
                int r9 = (r7 > r5 ? 1 : (r7 == r5 ? 0 : -1))
                if (r9 == 0) goto L_0x014a
                long r9 = (long) r4     // Catch:{ Exception -> 0x017f }
                r12.reportProgress(r9, r7)     // Catch:{ Exception -> 0x017f }
                goto L_0x014a
            L_0x0168:
                r3 = -1
                if (r7 != r3) goto L_0x017d
                long r2 = r12.imageSize     // Catch:{ Exception -> 0x0179, all -> 0x0175 }
                int r4 = (r2 > r5 ? 1 : (r2 == r5 ? 0 : -1))
                if (r4 == 0) goto L_0x0184
                r12.reportProgress(r2, r2)     // Catch:{ Exception -> 0x0179, all -> 0x0175 }
                goto L_0x0184
            L_0x0175:
                r2 = move-exception
                r1 = r2
                r2 = 1
                goto L_0x0187
            L_0x0179:
                r2 = move-exception
                r1 = r2
                r2 = 1
                goto L_0x0180
            L_0x017d:
                r1 = 0
                goto L_0x0184
            L_0x017f:
                r1 = move-exception
            L_0x0180:
                org.telegram.messenger.FileLog.e((java.lang.Throwable) r1)     // Catch:{ all -> 0x0186 }
                r1 = r2
            L_0x0184:
                r2 = r1
                goto L_0x018a
            L_0x0186:
                r1 = move-exception
            L_0x0187:
                org.telegram.messenger.FileLog.e((java.lang.Throwable) r1)
            L_0x018a:
                java.io.RandomAccessFile r1 = r12.fileOutputStream     // Catch:{ all -> 0x0194 }
                if (r1 == 0) goto L_0x0198
                r1.close()     // Catch:{ all -> 0x0194 }
                r12.fileOutputStream = r0     // Catch:{ all -> 0x0194 }
                goto L_0x0198
            L_0x0194:
                r0 = move-exception
                org.telegram.messenger.FileLog.e((java.lang.Throwable) r0)
            L_0x0198:
                java.net.HttpURLConnection r0 = r12.httpConnection     // Catch:{ all -> 0x01a0 }
                if (r0 == 0) goto L_0x01a1
                r0.disconnect()     // Catch:{ all -> 0x01a0 }
                goto L_0x01a1
            L_0x01a0:
            L_0x01a1:
                if (r13 == 0) goto L_0x01ab
                r13.close()     // Catch:{ all -> 0x01a7 }
                goto L_0x01ab
            L_0x01a7:
                r13 = move-exception
                org.telegram.messenger.FileLog.e((java.lang.Throwable) r13)
            L_0x01ab:
                if (r2 == 0) goto L_0x01c1
                org.telegram.messenger.ImageLoader$CacheImage r13 = r12.cacheImage
                java.io.File r0 = r13.tempFilePath
                if (r0 == 0) goto L_0x01c1
                java.io.File r13 = r13.finalFilePath
                boolean r13 = r0.renameTo(r13)
                if (r13 != 0) goto L_0x01c1
                org.telegram.messenger.ImageLoader$CacheImage r13 = r12.cacheImage
                java.io.File r0 = r13.tempFilePath
                r13.finalFilePath = r0
            L_0x01c1:
                java.lang.Boolean r13 = java.lang.Boolean.valueOf(r2)
                return r13
            */
            throw new UnsupportedOperationException("Method not decompiled: org.telegram.messenger.ImageLoader.HttpImageTask.doInBackground(java.lang.Void[]):java.lang.Boolean");
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Boolean bool) {
            if (bool.booleanValue() || !this.canRetry) {
                ImageLoader imageLoader = ImageLoader.this;
                CacheImage cacheImage2 = this.cacheImage;
                imageLoader.fileDidLoaded(cacheImage2.url, cacheImage2.finalFilePath, 0);
            } else {
                ImageLoader.this.httpFileLoadError(this.cacheImage.url);
            }
            Utilities.stageQueue.postRunnable(new ImageLoader$HttpImageTask$$ExternalSyntheticLambda6(this, bool));
            ImageLoader.this.imageLoadQueue.postRunnable(new ImageLoader$HttpImageTask$$ExternalSyntheticLambda0(this));
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void lambda$onPostExecute$4(Boolean bool) {
            ImageLoader.this.fileProgresses.remove(this.cacheImage.url);
            AndroidUtilities.runOnUIThread(new ImageLoader$HttpImageTask$$ExternalSyntheticLambda7(this, bool));
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void lambda$onPostExecute$3(Boolean bool) {
            if (bool.booleanValue()) {
                NotificationCenter instance = NotificationCenter.getInstance(this.cacheImage.currentAccount);
                int i = NotificationCenter.fileLoaded;
                CacheImage cacheImage2 = this.cacheImage;
                instance.postNotificationName(i, cacheImage2.url, cacheImage2.finalFilePath);
                return;
            }
            NotificationCenter.getInstance(this.cacheImage.currentAccount).postNotificationName(NotificationCenter.fileLoadFailed, this.cacheImage.url, 2);
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void lambda$onPostExecute$5() {
            ImageLoader.this.runHttpTasks(true);
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void lambda$onCancelled$6() {
            ImageLoader.this.runHttpTasks(true);
        }

        /* access modifiers changed from: protected */
        public void onCancelled() {
            ImageLoader.this.imageLoadQueue.postRunnable(new ImageLoader$HttpImageTask$$ExternalSyntheticLambda2(this));
            Utilities.stageQueue.postRunnable(new ImageLoader$HttpImageTask$$ExternalSyntheticLambda3(this));
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void lambda$onCancelled$8() {
            ImageLoader.this.fileProgresses.remove(this.cacheImage.url);
            AndroidUtilities.runOnUIThread(new ImageLoader$HttpImageTask$$ExternalSyntheticLambda1(this));
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void lambda$onCancelled$7() {
            NotificationCenter.getInstance(this.cacheImage.currentAccount).postNotificationName(NotificationCenter.fileLoadFailed, this.cacheImage.url, 1);
        }
    }

    private class ThumbGenerateTask implements Runnable {
        private ThumbGenerateInfo info;
        private int mediaType;
        private File originalPath;

        public ThumbGenerateTask(int i, File file, ThumbGenerateInfo thumbGenerateInfo) {
            this.mediaType = i;
            this.originalPath = file;
            this.info = thumbGenerateInfo;
        }

        private void removeTask() {
            ThumbGenerateInfo thumbGenerateInfo = this.info;
            if (thumbGenerateInfo != null) {
                ImageLoader.this.imageLoadQueue.postRunnable(new ImageLoader$ThumbGenerateTask$$ExternalSyntheticLambda0(this, FileLoader.getAttachFileName(thumbGenerateInfo.parentDocument)));
            }
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void lambda$removeTask$0(String str) {
            ImageLoader.this.thumbGenerateTasks.remove(str);
        }

        public void run() {
            int i;
            Bitmap createScaledBitmap;
            try {
                if (this.info == null) {
                    removeTask();
                    return;
                }
                String str = "q_" + this.info.parentDocument.dc_id + "_" + this.info.parentDocument.id;
                File file = new File(FileLoader.getDirectory(4), str + ".jpg");
                if (!file.exists()) {
                    if (this.originalPath.exists()) {
                        if (this.info.big) {
                            Point point = AndroidUtilities.displaySize;
                            i = Math.max(point.x, point.y);
                        } else {
                            Point point2 = AndroidUtilities.displaySize;
                            i = Math.min(180, Math.min(point2.x, point2.y) / 4);
                        }
                        int i2 = this.mediaType;
                        Bitmap bitmap = null;
                        if (i2 == 0) {
                            float f = (float) i;
                            bitmap = ImageLoader.loadBitmap(this.originalPath.toString(), (Uri) null, f, f, false);
                        } else {
                            int i3 = 2;
                            if (i2 == 2) {
                                String file2 = this.originalPath.toString();
                                if (!this.info.big) {
                                    i3 = 1;
                                }
                                bitmap = SendMessagesHelper.createVideoThumbnail(file2, i3);
                            } else if (i2 == 3) {
                                String lowerCase = this.originalPath.toString().toLowerCase();
                                if (lowerCase.endsWith("mp4")) {
                                    String file3 = this.originalPath.toString();
                                    if (!this.info.big) {
                                        i3 = 1;
                                    }
                                    bitmap = SendMessagesHelper.createVideoThumbnail(file3, i3);
                                } else if (lowerCase.endsWith(".jpg") || lowerCase.endsWith(".jpeg") || lowerCase.endsWith(".png") || lowerCase.endsWith(".gif")) {
                                    float f2 = (float) i;
                                    bitmap = ImageLoader.loadBitmap(lowerCase, (Uri) null, f2, f2, false);
                                }
                            }
                        }
                        if (bitmap == null) {
                            removeTask();
                            return;
                        }
                        int width = bitmap.getWidth();
                        int height = bitmap.getHeight();
                        if (width != 0) {
                            if (height != 0) {
                                float f3 = (float) width;
                                float f4 = (float) i;
                                float f5 = (float) height;
                                float min = Math.min(f3 / f4, f5 / f4);
                                if (min > 1.0f && (createScaledBitmap = Bitmaps.createScaledBitmap(bitmap, (int) (f3 / min), (int) (f5 / min), true)) != bitmap) {
                                    bitmap.recycle();
                                    bitmap = createScaledBitmap;
                                }
                                FileOutputStream fileOutputStream = new FileOutputStream(file);
                                bitmap.compress(Bitmap.CompressFormat.JPEG, this.info.big ? 83 : 60, fileOutputStream);
                                fileOutputStream.close();
                                AndroidUtilities.runOnUIThread(new ImageLoader$ThumbGenerateTask$$ExternalSyntheticLambda1(this, str, new ArrayList(this.info.imageReceiverArray), new BitmapDrawable(bitmap), new ArrayList(this.info.imageReceiverGuidsArray)));
                                return;
                            }
                        }
                        removeTask();
                        return;
                    }
                }
                removeTask();
            } catch (Exception e) {
                FileLog.e((Throwable) e);
            } catch (Throwable th) {
                FileLog.e(th);
                removeTask();
            }
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void lambda$run$1(String str, ArrayList arrayList, BitmapDrawable bitmapDrawable, ArrayList arrayList2) {
            removeTask();
            if (this.info.filter != null) {
                str = str + "@" + this.info.filter;
            }
            for (int i = 0; i < arrayList.size(); i++) {
                ((ImageReceiver) arrayList.get(i)).setImageBitmapByKey(bitmapDrawable, str, 0, false, ((Integer) arrayList2.get(i)).intValue());
            }
            ImageLoader.this.memCache.put(str, bitmapDrawable);
        }
    }

    /* JADX WARNING: Missing exception handler attribute for start block: B:20:0x0039 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:25:0x003e */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String decompressGzip(java.io.File r5) {
        /*
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = ""
            if (r5 != 0) goto L_0x000a
            return r1
        L_0x000a:
            java.util.zip.GZIPInputStream r2 = new java.util.zip.GZIPInputStream     // Catch:{ Exception -> 0x003f }
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ Exception -> 0x003f }
            r3.<init>(r5)     // Catch:{ Exception -> 0x003f }
            r2.<init>(r3)     // Catch:{ Exception -> 0x003f }
            java.io.BufferedReader r5 = new java.io.BufferedReader     // Catch:{ all -> 0x003a }
            java.io.InputStreamReader r3 = new java.io.InputStreamReader     // Catch:{ all -> 0x003a }
            java.lang.String r4 = "UTF-8"
            r3.<init>(r2, r4)     // Catch:{ all -> 0x003a }
            r5.<init>(r3)     // Catch:{ all -> 0x003a }
        L_0x0020:
            java.lang.String r3 = r5.readLine()     // Catch:{ all -> 0x0035 }
            if (r3 == 0) goto L_0x002a
            r0.append(r3)     // Catch:{ all -> 0x0035 }
            goto L_0x0020
        L_0x002a:
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0035 }
            r5.close()     // Catch:{ all -> 0x003a }
            r2.close()     // Catch:{ Exception -> 0x003f }
            return r0
        L_0x0035:
            r0 = move-exception
            r5.close()     // Catch:{ all -> 0x0039 }
        L_0x0039:
            throw r0     // Catch:{ all -> 0x003a }
        L_0x003a:
            r5 = move-exception
            r2.close()     // Catch:{ all -> 0x003e }
        L_0x003e:
            throw r5     // Catch:{ Exception -> 0x003f }
        L_0x003f:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: org.telegram.messenger.ImageLoader.decompressGzip(java.io.File):java.lang.String");
    }

    private class CacheOutTask implements Runnable {
        /* access modifiers changed from: private */
        public CacheImage cacheImage;
        private boolean isCancelled;
        private Thread runningThread;
        private final Object sync = new Object();

        public CacheOutTask(CacheImage cacheImage2) {
            this.cacheImage = cacheImage2;
        }

        /*  JADX ERROR: IndexOutOfBoundsException in pass: RegionMakerVisitor
            java.lang.IndexOutOfBoundsException: Index: 0, Size: 0
            	at java.util.ArrayList.rangeCheck(ArrayList.java:659)
            	at java.util.ArrayList.get(ArrayList.java:435)
            	at jadx.core.dex.nodes.InsnNode.getArg(InsnNode.java:101)
            	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:611)
            	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
            	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
            	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
            	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
            	at jadx.core.dex.visitors.regions.RegionMaker.processMonitorEnter(RegionMaker.java:561)
            	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:133)
            	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
            	at jadx.core.dex.visitors.regions.RegionMaker.processIf(RegionMaker.java:698)
            	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:123)
            	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
            	at jadx.core.dex.visitors.regions.RegionMaker.processExcHandler(RegionMaker.java:1043)
            	at jadx.core.dex.visitors.regions.RegionMaker.processTryCatchBlocks(RegionMaker.java:975)
            	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:52)
            */
        public void run() {
            /*
                r36 = this;
                r1 = r36
                java.lang.Object r2 = r1.sync
                monitor-enter(r2)
                java.lang.Thread r0 = java.lang.Thread.currentThread()     // Catch:{ all -> 0x0ce5 }
                r1.runningThread = r0     // Catch:{ all -> 0x0ce5 }
                java.lang.Thread.interrupted()     // Catch:{ all -> 0x0ce5 }
                boolean r0 = r1.isCancelled     // Catch:{ all -> 0x0ce5 }
                if (r0 == 0) goto L_0x0014
                monitor-exit(r2)     // Catch:{ all -> 0x0ce5 }
                return
            L_0x0014:
                monitor-exit(r2)     // Catch:{ all -> 0x0ce5 }
                org.telegram.messenger.ImageLoader$CacheImage r0 = r1.cacheImage
                org.telegram.messenger.ImageLocation r2 = r0.imageLocation
                org.telegram.tgnet.TLRPC$PhotoSize r3 = r2.photoSize
                boolean r4 = r3 instanceof org.telegram.tgnet.TLRPC$TL_photoStrippedSize
                if (r4 == 0) goto L_0x0037
                org.telegram.tgnet.TLRPC$TL_photoStrippedSize r3 = (org.telegram.tgnet.TLRPC$TL_photoStrippedSize) r3
                byte[] r0 = r3.bytes
                java.lang.String r2 = "b"
                android.graphics.Bitmap r0 = org.telegram.messenger.ImageLoader.getStrippedPhotoBitmap(r0, r2)
                if (r0 == 0) goto L_0x0031
                android.graphics.drawable.BitmapDrawable r5 = new android.graphics.drawable.BitmapDrawable
                r5.<init>(r0)
                goto L_0x0032
            L_0x0031:
                r5 = 0
            L_0x0032:
                r1.onPostExecute(r5)
                goto L_0x0ce4
            L_0x0037:
                int r3 = r0.imageType
                r4 = 5
                if (r3 != r4) goto L_0x0057
                org.telegram.ui.Components.ThemePreviewDrawable r0 = new org.telegram.ui.Components.ThemePreviewDrawable     // Catch:{ all -> 0x004d }
                org.telegram.messenger.ImageLoader$CacheImage r2 = r1.cacheImage     // Catch:{ all -> 0x004d }
                java.io.File r3 = r2.finalFilePath     // Catch:{ all -> 0x004d }
                org.telegram.messenger.ImageLocation r2 = r2.imageLocation     // Catch:{ all -> 0x004d }
                org.telegram.tgnet.TLRPC$Document r2 = r2.document     // Catch:{ all -> 0x004d }
                org.telegram.messenger.DocumentObject$ThemeDocument r2 = (org.telegram.messenger.DocumentObject.ThemeDocument) r2     // Catch:{ all -> 0x004d }
                r0.<init>(r3, r2)     // Catch:{ all -> 0x004d }
                r5 = r0
                goto L_0x0052
            L_0x004d:
                r0 = move-exception
                org.telegram.messenger.FileLog.e((java.lang.Throwable) r0)
                r5 = 0
            L_0x0052:
                r1.onPostExecute(r5)
                goto L_0x0ce4
            L_0x0057:
                r6 = 4
                r7 = 3
                r8 = 2
                r9 = 1
                r10 = 0
                if (r3 == r7) goto L_0x0CLASSNAME
                if (r3 != r6) goto L_0x0062
                goto L_0x0CLASSNAME
            L_0x0062:
                r12 = 1119092736(0x42b40000, float:90.0)
                if (r3 != r9) goto L_0x024d
                r0 = 1126865306(0x432a999a, float:170.6)
                int r2 = org.telegram.messenger.AndroidUtilities.dp(r0)
                r3 = 512(0x200, float:7.175E-43)
                int r2 = java.lang.Math.min(r3, r2)
                int r0 = org.telegram.messenger.AndroidUtilities.dp(r0)
                int r0 = java.lang.Math.min(r3, r0)
                org.telegram.messenger.ImageLoader$CacheImage r13 = r1.cacheImage
                java.lang.String r13 = r13.filter
                if (r13 == 0) goto L_0x017b
                java.lang.String r14 = "_"
                java.lang.String[] r13 = r13.split(r14)
                int r14 = r13.length
                if (r14 < r8) goto L_0x00fc
                r0 = r13[r10]
                float r0 = java.lang.Float.parseFloat(r0)
                r2 = r13[r9]
                float r2 = java.lang.Float.parseFloat(r2)
                float r14 = org.telegram.messenger.AndroidUtilities.density
                float r14 = r14 * r0
                int r14 = (int) r14
                int r14 = java.lang.Math.min(r3, r14)
                float r15 = org.telegram.messenger.AndroidUtilities.density
                float r15 = r15 * r2
                int r15 = (int) r15
                int r3 = java.lang.Math.min(r3, r15)
                int r0 = (r0 > r12 ? 1 : (r0 == r12 ? 0 : -1))
                if (r0 > 0) goto L_0x00c8
                int r0 = (r2 > r12 ? 1 : (r2 == r12 ? 0 : -1))
                if (r0 > 0) goto L_0x00c8
                org.telegram.messenger.ImageLoader$CacheImage r0 = r1.cacheImage
                java.lang.String r0 = r0.filter
                java.lang.String r2 = "nolimit"
                boolean r0 = r0.contains(r2)
                if (r0 != 0) goto L_0x00c8
                r0 = 160(0xa0, float:2.24E-43)
                int r2 = java.lang.Math.min(r14, r0)
                int r0 = java.lang.Math.min(r3, r0)
                r3 = 1
                goto L_0x00cb
            L_0x00c8:
                r0 = r3
                r2 = r14
                r3 = 0
            L_0x00cb:
                int r12 = r13.length
                if (r12 < r7) goto L_0x00da
                java.lang.String r12 = "pcache"
                r14 = r13[r8]
                boolean r12 = r12.equals(r14)
                if (r12 == 0) goto L_0x00da
            L_0x00d8:
                r12 = 1
                goto L_0x00ee
            L_0x00da:
                org.telegram.messenger.ImageLoader$CacheImage r12 = r1.cacheImage
                java.lang.String r12 = r12.filter
                java.lang.String r14 = "nolimit"
                boolean r12 = r12.contains(r14)
                if (r12 != 0) goto L_0x00ed
                int r12 = org.telegram.messenger.SharedConfig.getDevicePerformanceClass()
                if (r12 == r8) goto L_0x00ed
                goto L_0x00d8
            L_0x00ed:
                r12 = 0
            L_0x00ee:
                org.telegram.messenger.ImageLoader$CacheImage r14 = r1.cacheImage
                java.lang.String r14 = r14.filter
                java.lang.String r15 = "lastframe"
                boolean r14 = r14.contains(r15)
                if (r14 == 0) goto L_0x00fe
                r14 = 1
                goto L_0x00ff
            L_0x00fc:
                r3 = 0
                r12 = 0
            L_0x00fe:
                r14 = 0
            L_0x00ff:
                int r15 = r13.length
                if (r15 < r7) goto L_0x0129
                java.lang.String r15 = "nr"
                r11 = r13[r8]
                boolean r11 = r15.equals(r11)
                if (r11 == 0) goto L_0x010f
                r11 = 0
            L_0x010d:
                r15 = 2
                goto L_0x012b
            L_0x010f:
                java.lang.String r11 = "nrs"
                r15 = r13[r8]
                boolean r11 = r11.equals(r15)
                if (r11 == 0) goto L_0x011c
                r11 = 0
                r15 = 3
                goto L_0x012b
            L_0x011c:
                java.lang.String r11 = "dice"
                r15 = r13[r8]
                boolean r11 = r11.equals(r15)
                if (r11 == 0) goto L_0x0129
                r11 = r13[r7]
                goto L_0x010d
            L_0x0129:
                r11 = 0
                r15 = 1
            L_0x012b:
                int r7 = r13.length
                if (r7 < r4) goto L_0x0176
                java.lang.String r7 = "c1"
                r4 = r13[r6]
                boolean r4 = r7.equals(r4)
                if (r4 == 0) goto L_0x013e
                r4 = 12
                r4 = r3
                r16 = 12
                goto L_0x0179
            L_0x013e:
                java.lang.String r4 = "c2"
                r7 = r13[r6]
                boolean r4 = r4.equals(r7)
                if (r4 == 0) goto L_0x014c
                r4 = r3
                r16 = 3
                goto L_0x0179
            L_0x014c:
                java.lang.String r4 = "c3"
                r7 = r13[r6]
                boolean r4 = r4.equals(r7)
                if (r4 == 0) goto L_0x015a
                r4 = r3
                r16 = 4
                goto L_0x0179
            L_0x015a:
                java.lang.String r4 = "c4"
                r7 = r13[r6]
                boolean r4 = r4.equals(r7)
                if (r4 == 0) goto L_0x0168
                r4 = r3
                r16 = 5
                goto L_0x0179
            L_0x0168:
                java.lang.String r4 = "c5"
                r6 = r13[r6]
                boolean r4 = r4.equals(r6)
                if (r4 == 0) goto L_0x0176
                r4 = r3
                r16 = 6
                goto L_0x0179
            L_0x0176:
                r4 = r3
                r16 = 0
            L_0x0179:
                r3 = r2
                goto L_0x0183
            L_0x017b:
                r3 = r2
                r4 = 0
                r11 = 0
                r12 = 0
                r14 = 0
                r15 = 1
                r16 = 0
            L_0x0183:
                r2 = r0
                if (r11 == 0) goto L_0x019c
                java.lang.String r0 = "🎰"
                boolean r0 = r0.equals(r11)
                if (r0 == 0) goto L_0x0195
                org.telegram.ui.Components.SlotsDrawable r0 = new org.telegram.ui.Components.SlotsDrawable
                r0.<init>(r11, r3, r2)
                goto L_0x0230
            L_0x0195:
                org.telegram.ui.Components.RLottieDrawable r0 = new org.telegram.ui.Components.RLottieDrawable
                r0.<init>(r11, r3, r2)
                goto L_0x0230
            L_0x019c:
                org.telegram.messenger.ImageLoader$CacheImage r0 = r1.cacheImage
                java.io.File r0 = r0.finalFilePath
                java.io.RandomAccessFile r6 = new java.io.RandomAccessFile     // Catch:{ Exception -> 0x01e2, all -> 0x01dd }
                org.telegram.messenger.ImageLoader$CacheImage r0 = r1.cacheImage     // Catch:{ Exception -> 0x01e2, all -> 0x01dd }
                java.io.File r0 = r0.finalFilePath     // Catch:{ Exception -> 0x01e2, all -> 0x01dd }
                java.lang.String r7 = "r"
                r6.<init>(r0, r7)     // Catch:{ Exception -> 0x01e2, all -> 0x01dd }
                org.telegram.messenger.ImageLoader$CacheImage r0 = r1.cacheImage     // Catch:{ Exception -> 0x01da, all -> 0x01d5 }
                int r0 = r0.type     // Catch:{ Exception -> 0x01da, all -> 0x01d5 }
                if (r0 != r9) goto L_0x01b6
                byte[] r0 = org.telegram.messenger.ImageLoader.headerThumb     // Catch:{ Exception -> 0x01da, all -> 0x01d5 }
                goto L_0x01ba
            L_0x01b6:
                byte[] r0 = org.telegram.messenger.ImageLoader.header     // Catch:{ Exception -> 0x01da, all -> 0x01d5 }
            L_0x01ba:
                r6.readFully(r0, r10, r8)     // Catch:{ Exception -> 0x01da, all -> 0x01d5 }
                byte r5 = r0[r10]     // Catch:{ Exception -> 0x01da, all -> 0x01d5 }
                r7 = 31
                if (r5 != r7) goto L_0x01ca
                byte r0 = r0[r9]     // Catch:{ Exception -> 0x01da, all -> 0x01d5 }
                r5 = -117(0xffffffffffffff8b, float:NaN)
                if (r0 != r5) goto L_0x01ca
                goto L_0x01cb
            L_0x01ca:
                r9 = 0
            L_0x01cb:
                r6.close()     // Catch:{ Exception -> 0x01cf }
                goto L_0x01f3
            L_0x01cf:
                r0 = move-exception
                r5 = r0
                org.telegram.messenger.FileLog.e((java.lang.Throwable) r5)
                goto L_0x01f3
            L_0x01d5:
                r0 = move-exception
                r2 = r0
                r5 = r6
                goto L_0x0241
            L_0x01da:
                r0 = move-exception
                r5 = r6
                goto L_0x01e4
            L_0x01dd:
                r0 = move-exception
                r2 = r0
                r5 = 0
                goto L_0x0241
            L_0x01e2:
                r0 = move-exception
                r5 = 0
            L_0x01e4:
                org.telegram.messenger.FileLog.e((java.lang.Throwable) r0, (boolean) r10)     // Catch:{ all -> 0x023f }
                if (r5 == 0) goto L_0x01f2
                r5.close()     // Catch:{ Exception -> 0x01ed }
                goto L_0x01f2
            L_0x01ed:
                r0 = move-exception
                r5 = r0
                org.telegram.messenger.FileLog.e((java.lang.Throwable) r5)
            L_0x01f2:
                r9 = 0
            L_0x01f3:
                if (r14 == 0) goto L_0x01f6
                goto L_0x01f7
            L_0x01f6:
                r10 = r12
            L_0x01f7:
                if (r9 == 0) goto L_0x0217
                org.telegram.ui.Components.RLottieDrawable r0 = new org.telegram.ui.Components.RLottieDrawable
                org.telegram.messenger.ImageLoader$CacheImage r5 = r1.cacheImage
                java.io.File r5 = r5.finalFilePath
                java.lang.String r19 = org.telegram.messenger.ImageLoader.decompressGzip(r5)
                r24 = 0
                r17 = r0
                r18 = r5
                r20 = r3
                r21 = r2
                r22 = r10
                r23 = r4
                r25 = r16
                r17.<init>(r18, r19, r20, r21, r22, r23, r24, r25)
                goto L_0x0230
            L_0x0217:
                org.telegram.ui.Components.RLottieDrawable r0 = new org.telegram.ui.Components.RLottieDrawable
                org.telegram.messenger.ImageLoader$CacheImage r5 = r1.cacheImage
                java.io.File r5 = r5.finalFilePath
                r23 = 0
                r17 = r0
                r18 = r5
                r19 = r3
                r20 = r2
                r21 = r10
                r22 = r4
                r24 = r16
                r17.<init>(r18, r19, r20, r21, r22, r23, r24)
            L_0x0230:
                if (r14 == 0) goto L_0x0237
                r1.loadLastFrame(r0, r2, r3)
                goto L_0x0ce4
            L_0x0237:
                r0.setAutoRepeat(r15)
                r1.onPostExecute(r0)
                goto L_0x0ce4
            L_0x023f:
                r0 = move-exception
                r2 = r0
            L_0x0241:
                if (r5 == 0) goto L_0x024c
                r5.close()     // Catch:{ Exception -> 0x0247 }
                goto L_0x024c
            L_0x0247:
                r0 = move-exception
                r3 = r0
                org.telegram.messenger.FileLog.e((java.lang.Throwable) r3)
            L_0x024c:
                throw r2
            L_0x024d:
                if (r3 != r8) goto L_0x037e
                if (r2 == 0) goto L_0x0256
                long r6 = r2.videoSeekTo
                r21 = r6
                goto L_0x0258
            L_0x0256:
                r21 = 0
            L_0x0258:
                java.lang.String r0 = r0.filter
                if (r0 == 0) goto L_0x0287
                java.lang.String r2 = "_"
                java.lang.String[] r0 = r0.split(r2)
                int r2 = r0.length
                if (r2 < r8) goto L_0x0287
                r2 = r0[r10]
                float r2 = java.lang.Float.parseFloat(r2)
                r0 = r0[r9]
                float r0 = java.lang.Float.parseFloat(r0)
                int r2 = (r2 > r12 ? 1 : (r2 == r12 ? 0 : -1))
                if (r2 > 0) goto L_0x0287
                int r0 = (r0 > r12 ? 1 : (r0 == r12 ? 0 : -1))
                if (r0 > 0) goto L_0x0287
                org.telegram.messenger.ImageLoader$CacheImage r0 = r1.cacheImage
                java.lang.String r0 = r0.filter
                java.lang.String r2 = "nolimit"
                boolean r0 = r0.contains(r2)
                if (r0 != 0) goto L_0x0287
                r0 = 1
                goto L_0x0288
            L_0x0287:
                r0 = 0
            L_0x0288:
                org.telegram.messenger.ImageLoader r2 = org.telegram.messenger.ImageLoader.this
                org.telegram.messenger.ImageLoader$CacheImage r3 = r1.cacheImage
                java.lang.String r3 = r3.filter
                boolean r2 = r2.isAnimatedAvatar(r3)
                if (r2 != 0) goto L_0x02a0
                java.lang.String r2 = "g"
                org.telegram.messenger.ImageLoader$CacheImage r3 = r1.cacheImage
                java.lang.String r3 = r3.filter
                boolean r2 = r2.equals(r3)
                if (r2 == 0) goto L_0x02f8
            L_0x02a0:
                org.telegram.messenger.ImageLoader$CacheImage r2 = r1.cacheImage
                org.telegram.messenger.ImageLocation r3 = r2.imageLocation
                org.telegram.tgnet.TLRPC$Document r4 = r3.document
                boolean r6 = r4 instanceof org.telegram.tgnet.TLRPC$TL_documentEncrypted
                if (r6 != 0) goto L_0x02f8
                boolean r6 = r4 instanceof org.telegram.tgnet.TLRPC$Document
                if (r6 == 0) goto L_0x02af
                goto L_0x02b0
            L_0x02af:
                r4 = 0
            L_0x02b0:
                if (r4 == 0) goto L_0x02b5
                long r2 = r2.size
                goto L_0x02b7
            L_0x02b5:
                long r2 = r3.currentSize
            L_0x02b7:
                r16 = r2
                org.telegram.ui.Components.AnimatedFileDrawable r2 = new org.telegram.ui.Components.AnimatedFileDrawable
                org.telegram.messenger.ImageLoader$CacheImage r3 = r1.cacheImage
                java.io.File r14 = r3.finalFilePath
                r15 = 0
                if (r4 != 0) goto L_0x02c7
                org.telegram.messenger.ImageLocation r5 = r3.imageLocation
                r19 = r5
                goto L_0x02c9
            L_0x02c7:
                r19 = 0
            L_0x02c9:
                java.lang.Object r5 = r3.parentObject
                int r3 = r3.currentAccount
                r24 = 0
                r13 = r2
                r18 = r4
                r20 = r5
                r23 = r3
                r13.<init>(r14, r15, r16, r18, r19, r20, r21, r23, r24)
                boolean r3 = org.telegram.messenger.MessageObject.isWebM(r4)
                if (r3 != 0) goto L_0x02f3
                boolean r3 = org.telegram.messenger.MessageObject.isVideoSticker(r4)
                if (r3 != 0) goto L_0x02f3
                org.telegram.messenger.ImageLoader r3 = org.telegram.messenger.ImageLoader.this
                org.telegram.messenger.ImageLoader$CacheImage r4 = r1.cacheImage
                java.lang.String r4 = r4.filter
                boolean r3 = r3.isAnimatedAvatar(r4)
                if (r3 == 0) goto L_0x02f2
                goto L_0x02f3
            L_0x02f2:
                r9 = 0
            L_0x02f3:
                r2.setIsWebmSticker(r9)
                goto L_0x0373
            L_0x02f8:
                org.telegram.messenger.ImageLoader$CacheImage r2 = r1.cacheImage
                java.lang.String r2 = r2.filter
                if (r2 == 0) goto L_0x0320
                java.lang.String r3 = "_"
                java.lang.String[] r2 = r2.split(r3)
                int r3 = r2.length
                if (r3 < r8) goto L_0x0320
                r3 = r2[r10]
                float r3 = java.lang.Float.parseFloat(r3)
                r2 = r2[r9]
                float r2 = java.lang.Float.parseFloat(r2)
                float r4 = org.telegram.messenger.AndroidUtilities.density
                float r3 = r3 * r4
                int r3 = (int) r3
                float r2 = r2 * r4
                int r2 = (int) r2
                r26 = r2
                r25 = r3
                goto L_0x0324
            L_0x0320:
                r25 = 0
                r26 = 0
            L_0x0324:
                org.telegram.ui.Components.AnimatedFileDrawable r2 = new org.telegram.ui.Components.AnimatedFileDrawable
                org.telegram.messenger.ImageLoader$CacheImage r3 = r1.cacheImage
                java.io.File r14 = r3.finalFilePath
                java.lang.String r4 = "d"
                java.lang.String r3 = r3.filter
                boolean r15 = r4.equals(r3)
                r16 = 0
                org.telegram.messenger.ImageLoader$CacheImage r3 = r1.cacheImage
                org.telegram.messenger.ImageLocation r4 = r3.imageLocation
                org.telegram.tgnet.TLRPC$Document r4 = r4.document
                r19 = 0
                r20 = 0
                int r3 = r3.currentAccount
                r24 = 0
                r13 = r2
                r18 = r4
                r23 = r3
                r13.<init>(r14, r15, r16, r18, r19, r20, r21, r23, r24, r25, r26)
                org.telegram.messenger.ImageLoader$CacheImage r3 = r1.cacheImage
                org.telegram.messenger.ImageLocation r3 = r3.imageLocation
                org.telegram.tgnet.TLRPC$Document r3 = r3.document
                boolean r3 = org.telegram.messenger.MessageObject.isWebM(r3)
                if (r3 != 0) goto L_0x0370
                org.telegram.messenger.ImageLoader$CacheImage r3 = r1.cacheImage
                org.telegram.messenger.ImageLocation r3 = r3.imageLocation
                org.telegram.tgnet.TLRPC$Document r3 = r3.document
                boolean r3 = org.telegram.messenger.MessageObject.isVideoSticker(r3)
                if (r3 != 0) goto L_0x0370
                org.telegram.messenger.ImageLoader r3 = org.telegram.messenger.ImageLoader.this
                org.telegram.messenger.ImageLoader$CacheImage r4 = r1.cacheImage
                java.lang.String r4 = r4.filter
                boolean r3 = r3.isAnimatedAvatar(r4)
                if (r3 == 0) goto L_0x036f
                goto L_0x0370
            L_0x036f:
                r9 = 0
            L_0x0370:
                r2.setIsWebmSticker(r9)
            L_0x0373:
                r2.setLimitFps(r0)
                java.lang.Thread.interrupted()
                r1.onPostExecute(r2)
                goto L_0x0ce4
            L_0x037e:
                java.io.File r2 = r0.finalFilePath
                org.telegram.messenger.SecureDocument r3 = r0.secureDocument
                if (r3 != 0) goto L_0x0399
                java.io.File r0 = r0.encryptionKeyPath
                if (r0 == 0) goto L_0x0397
                if (r2 == 0) goto L_0x0397
                java.lang.String r0 = r2.getAbsolutePath()
                java.lang.String r3 = ".enc"
                boolean r0 = r0.endsWith(r3)
                if (r0 == 0) goto L_0x0397
                goto L_0x0399
            L_0x0397:
                r3 = 0
                goto L_0x039a
            L_0x0399:
                r3 = 1
            L_0x039a:
                boolean r0 = org.telegram.messenger.BuildVars.LOGS_ENABLED
                if (r0 == 0) goto L_0x040e
                java.lang.StringBuilder r0 = new java.lang.StringBuilder
                r0.<init>()
                java.lang.String r4 = "Image Loader inEncryptedFile = "
                r0.append(r4)
                r0.append(r3)
                java.lang.String r4 = " "
                r0.append(r4)
                r0.append(r2)
                java.lang.String r0 = r0.toString()
                org.telegram.messenger.FileLog.e((java.lang.String) r0)
                if (r2 == 0) goto L_0x03d4
                java.lang.StringBuilder r0 = new java.lang.StringBuilder
                r0.<init>()
                java.lang.String r4 = "Image Loader file exist = "
                r0.append(r4)
                boolean r4 = r2.exists()
                r0.append(r4)
                java.lang.String r0 = r0.toString()
                org.telegram.messenger.FileLog.e((java.lang.String) r0)
            L_0x03d4:
                org.telegram.messenger.ImageLoader$CacheImage r0 = r1.cacheImage
                java.io.File r0 = r0.encryptionKeyPath
                if (r0 == 0) goto L_0x040e
                java.lang.StringBuilder r0 = new java.lang.StringBuilder
                r0.<init>()
                java.lang.String r4 = "Image Loader encryption key = "
                r0.append(r4)
                org.telegram.messenger.ImageLoader$CacheImage r4 = r1.cacheImage
                java.io.File r4 = r4.encryptionKeyPath
                r0.append(r4)
                java.lang.String r0 = r0.toString()
                org.telegram.messenger.FileLog.e((java.lang.String) r0)
                java.lang.StringBuilder r0 = new java.lang.StringBuilder
                r0.<init>()
                java.lang.String r4 = "Image Loader encryption key "
                r0.append(r4)
                org.telegram.messenger.ImageLoader$CacheImage r4 = r1.cacheImage
                java.io.File r4 = r4.encryptionKeyPath
                boolean r4 = r4.exists()
                r0.append(r4)
                java.lang.String r0 = r0.toString()
                org.telegram.messenger.FileLog.e((java.lang.String) r0)
            L_0x040e:
                org.telegram.messenger.ImageLoader$CacheImage r0 = r1.cacheImage
                org.telegram.messenger.SecureDocument r0 = r0.secureDocument
                if (r0 == 0) goto L_0x0422
                org.telegram.messenger.SecureDocumentKey r4 = r0.secureDocumentKey
                org.telegram.tgnet.TLRPC$TL_secureFile r11 = r0.secureFile
                if (r11 == 0) goto L_0x041f
                byte[] r11 = r11.file_hash
                if (r11 == 0) goto L_0x041f
                goto L_0x0424
            L_0x041f:
                byte[] r11 = r0.fileHash
                goto L_0x0424
            L_0x0422:
                r4 = 0
                r11 = 0
            L_0x0424:
                int r0 = android.os.Build.VERSION.SDK_INT
                r12 = 19
                if (r0 >= r12) goto L_0x0494
                java.io.RandomAccessFile r12 = new java.io.RandomAccessFile     // Catch:{ Exception -> 0x0474, all -> 0x0470 }
                java.lang.String r0 = "r"
                r12.<init>(r2, r0)     // Catch:{ Exception -> 0x0474, all -> 0x0470 }
                org.telegram.messenger.ImageLoader$CacheImage r0 = r1.cacheImage     // Catch:{ Exception -> 0x046e }
                int r0 = r0.type     // Catch:{ Exception -> 0x046e }
                if (r0 != r9) goto L_0x043c
                byte[] r0 = org.telegram.messenger.ImageLoader.headerThumb     // Catch:{ Exception -> 0x046e }
                goto L_0x0440
            L_0x043c:
                byte[] r0 = org.telegram.messenger.ImageLoader.header     // Catch:{ Exception -> 0x046e }
            L_0x0440:
                int r13 = r0.length     // Catch:{ Exception -> 0x046e }
                r12.readFully(r0, r10, r13)     // Catch:{ Exception -> 0x046e }
                java.lang.String r13 = new java.lang.String     // Catch:{ Exception -> 0x046e }
                r13.<init>(r0)     // Catch:{ Exception -> 0x046e }
                java.lang.String r0 = r13.toLowerCase()     // Catch:{ Exception -> 0x046e }
                java.lang.String r0 = r0.toLowerCase()     // Catch:{ Exception -> 0x046e }
                java.lang.String r13 = "riff"
                boolean r13 = r0.startsWith(r13)     // Catch:{ Exception -> 0x046e }
                if (r13 == 0) goto L_0x0463
                java.lang.String r13 = "webp"
                boolean r0 = r0.endsWith(r13)     // Catch:{ Exception -> 0x046e }
                if (r0 == 0) goto L_0x0463
                r13 = 1
                goto L_0x0464
            L_0x0463:
                r13 = 0
            L_0x0464:
                r12.close()     // Catch:{ Exception -> 0x0468 }
                goto L_0x0495
            L_0x0468:
                r0 = move-exception
                r12 = r0
                org.telegram.messenger.FileLog.e((java.lang.Throwable) r12)
                goto L_0x0495
            L_0x046e:
                r0 = move-exception
                goto L_0x0476
            L_0x0470:
                r0 = move-exception
                r2 = r0
                r5 = 0
                goto L_0x0488
            L_0x0474:
                r0 = move-exception
                r12 = 0
            L_0x0476:
                org.telegram.messenger.FileLog.e((java.lang.Throwable) r0)     // Catch:{ all -> 0x0485 }
                if (r12 == 0) goto L_0x0494
                r12.close()     // Catch:{ Exception -> 0x047f }
                goto L_0x0494
            L_0x047f:
                r0 = move-exception
                r12 = r0
                org.telegram.messenger.FileLog.e((java.lang.Throwable) r12)
                goto L_0x0494
            L_0x0485:
                r0 = move-exception
                r2 = r0
                r5 = r12
            L_0x0488:
                if (r5 == 0) goto L_0x0493
                r5.close()     // Catch:{ Exception -> 0x048e }
                goto L_0x0493
            L_0x048e:
                r0 = move-exception
                r3 = r0
                org.telegram.messenger.FileLog.e((java.lang.Throwable) r3)
            L_0x0493:
                throw r2
            L_0x0494:
                r13 = 0
            L_0x0495:
                org.telegram.messenger.ImageLoader$CacheImage r0 = r1.cacheImage
                org.telegram.messenger.ImageLocation r0 = r0.imageLocation
                java.lang.String r0 = r0.path
                r12 = 8
                if (r0 == 0) goto L_0x0501
                java.lang.String r14 = "thumb://"
                boolean r14 = r0.startsWith(r14)
                if (r14 == 0) goto L_0x04ca
                java.lang.String r14 = ":"
                int r14 = r0.indexOf(r14, r12)
                if (r14 < 0) goto L_0x04c1
                java.lang.String r15 = r0.substring(r12, r14)
                long r18 = java.lang.Long.parseLong(r15)
                java.lang.Long r15 = java.lang.Long.valueOf(r18)
                int r14 = r14 + r9
                java.lang.String r0 = r0.substring(r14)
                goto L_0x04c3
            L_0x04c1:
                r0 = 0
                r15 = 0
            L_0x04c3:
                r14 = r0
                r18 = r15
                r15 = 0
            L_0x04c7:
                r19 = 0
                goto L_0x0507
            L_0x04ca:
                java.lang.String r14 = "vthumb://"
                boolean r14 = r0.startsWith(r14)
                if (r14 == 0) goto L_0x04f4
                java.lang.String r14 = ":"
                r15 = 9
                int r14 = r0.indexOf(r14, r15)
                if (r14 < 0) goto L_0x04eb
                java.lang.String r0 = r0.substring(r15, r14)
                long r14 = java.lang.Long.parseLong(r0)
                java.lang.Long r0 = java.lang.Long.valueOf(r14)
                r15 = r0
                r0 = 1
                goto L_0x04ed
            L_0x04eb:
                r0 = 0
                r15 = 0
            L_0x04ed:
                r18 = r15
                r14 = 0
                r19 = 0
                r15 = r0
                goto L_0x0507
            L_0x04f4:
                java.lang.String r14 = "http"
                boolean r0 = r0.startsWith(r14)
                if (r0 != 0) goto L_0x0501
                r14 = 0
                r15 = 0
                r18 = 0
                goto L_0x04c7
            L_0x0501:
                r14 = 0
                r15 = 0
                r18 = 0
                r19 = 1
            L_0x0507:
                android.graphics.BitmapFactory$Options r12 = new android.graphics.BitmapFactory$Options
                r12.<init>()
                r12.inSampleSize = r9
                int r0 = android.os.Build.VERSION.SDK_INT
                r6 = 21
                if (r0 >= r6) goto L_0x0516
                r12.inPurgeable = r9
            L_0x0516:
                org.telegram.messenger.ImageLoader r0 = org.telegram.messenger.ImageLoader.this
                boolean r7 = r0.canForce8888
                r23 = 0
                r24 = 1065353216(0x3var_, float:1.0)
                org.telegram.messenger.ImageLoader$CacheImage r0 = r1.cacheImage     // Catch:{ all -> 0x0743 }
                java.lang.String r0 = r0.filter     // Catch:{ all -> 0x0743 }
                if (r0 == 0) goto L_0x06cf
                java.lang.String r6 = "_"
                java.lang.String[] r0 = r0.split(r6)     // Catch:{ all -> 0x0743 }
                int r6 = r0.length     // Catch:{ all -> 0x0743 }
                if (r6 < r8) goto L_0x054c
                r6 = r0[r10]     // Catch:{ all -> 0x0743 }
                float r6 = java.lang.Float.parseFloat(r6)     // Catch:{ all -> 0x0743 }
                float r26 = org.telegram.messenger.AndroidUtilities.density     // Catch:{ all -> 0x0743 }
                float r6 = r6 * r26
                r0 = r0[r9]     // Catch:{ all -> 0x0547 }
                float r0 = java.lang.Float.parseFloat(r0)     // Catch:{ all -> 0x0547 }
                float r26 = org.telegram.messenger.AndroidUtilities.density     // Catch:{ all -> 0x0547 }
                float r0 = r0 * r26
                r26 = r6
                r6 = r0
                goto L_0x054f
            L_0x0547:
                r0 = move-exception
                r10 = r0
                r5 = 0
                goto L_0x0747
            L_0x054c:
                r6 = 0
                r26 = 0
            L_0x054f:
                org.telegram.messenger.ImageLoader$CacheImage r0 = r1.cacheImage     // Catch:{ all -> 0x06c2 }
                java.lang.String r0 = r0.filter     // Catch:{ all -> 0x06c2 }
                java.lang.String r8 = "b2"
                boolean r0 = r0.contains(r8)     // Catch:{ all -> 0x06c2 }
                if (r0 == 0) goto L_0x055d
                r8 = 3
                goto L_0x057a
            L_0x055d:
                org.telegram.messenger.ImageLoader$CacheImage r0 = r1.cacheImage     // Catch:{ all -> 0x06c2 }
                java.lang.String r0 = r0.filter     // Catch:{ all -> 0x06c2 }
                java.lang.String r8 = "b1"
                boolean r0 = r0.contains(r8)     // Catch:{ all -> 0x06c2 }
                if (r0 == 0) goto L_0x056b
                r8 = 2
                goto L_0x057a
            L_0x056b:
                org.telegram.messenger.ImageLoader$CacheImage r0 = r1.cacheImage     // Catch:{ all -> 0x06c2 }
                java.lang.String r0 = r0.filter     // Catch:{ all -> 0x06c2 }
                java.lang.String r8 = "b"
                boolean r0 = r0.contains(r8)     // Catch:{ all -> 0x06c2 }
                if (r0 == 0) goto L_0x0579
                r8 = 1
                goto L_0x057a
            L_0x0579:
                r8 = 0
            L_0x057a:
                org.telegram.messenger.ImageLoader$CacheImage r0 = r1.cacheImage     // Catch:{ all -> 0x06bc }
                java.lang.String r0 = r0.filter     // Catch:{ all -> 0x06bc }
                java.lang.String r5 = "i"
                boolean r5 = r0.contains(r5)     // Catch:{ all -> 0x06bc }
                org.telegram.messenger.ImageLoader$CacheImage r0 = r1.cacheImage     // Catch:{ all -> 0x06b5 }
                java.lang.String r0 = r0.filter     // Catch:{ all -> 0x06b5 }
                java.lang.String r10 = "f"
                boolean r0 = r0.contains(r10)     // Catch:{ all -> 0x06b5 }
                if (r0 == 0) goto L_0x0591
                r7 = 1
            L_0x0591:
                if (r13 != 0) goto L_0x06a6
                int r0 = (r26 > r23 ? 1 : (r26 == r23 ? 0 : -1))
                if (r0 == 0) goto L_0x06a6
                int r0 = (r6 > r23 ? 1 : (r6 == r23 ? 0 : -1))
                if (r0 == 0) goto L_0x06a6
                r12.inJustDecodeBounds = r9     // Catch:{ all -> 0x06a0 }
                if (r18 == 0) goto L_0x05d1
                if (r14 != 0) goto L_0x05d1
                if (r15 == 0) goto L_0x05b9
                android.content.Context r0 = org.telegram.messenger.ApplicationLoader.applicationContext     // Catch:{ all -> 0x05b4 }
                android.content.ContentResolver r0 = r0.getContentResolver()     // Catch:{ all -> 0x05b4 }
                long r9 = r18.longValue()     // Catch:{ all -> 0x05b4 }
                r28 = r5
                r5 = 1
                android.provider.MediaStore.Video.Thumbnails.getThumbnail(r0, r9, r5, r12)     // Catch:{ all -> 0x05cf }
                goto L_0x05c9
            L_0x05b4:
                r0 = move-exception
                r28 = r5
                goto L_0x06ba
            L_0x05b9:
                r28 = r5
                android.content.Context r0 = org.telegram.messenger.ApplicationLoader.applicationContext     // Catch:{ all -> 0x05cf }
                android.content.ContentResolver r0 = r0.getContentResolver()     // Catch:{ all -> 0x05cf }
                long r9 = r18.longValue()     // Catch:{ all -> 0x05cf }
                r5 = 1
                android.provider.MediaStore.Images.Thumbnails.getThumbnail(r0, r9, r5, r12)     // Catch:{ all -> 0x05cf }
            L_0x05c9:
                r30 = r7
                r29 = r8
                goto L_0x064e
            L_0x05cf:
                r0 = move-exception
                goto L_0x062d
            L_0x05d1:
                r28 = r5
                if (r4 == 0) goto L_0x0632
                java.io.RandomAccessFile r0 = new java.io.RandomAccessFile     // Catch:{ all -> 0x0628 }
                java.lang.String r5 = "r"
                r0.<init>(r2, r5)     // Catch:{ all -> 0x0628 }
                long r9 = r0.length()     // Catch:{ all -> 0x0628 }
                int r5 = (int) r9     // Catch:{ all -> 0x0628 }
                java.lang.ThreadLocal r9 = org.telegram.messenger.ImageLoader.bytesLocal     // Catch:{ all -> 0x0628 }
                java.lang.Object r9 = r9.get()     // Catch:{ all -> 0x0628 }
                byte[] r9 = (byte[]) r9     // Catch:{ all -> 0x0628 }
                if (r9 == 0) goto L_0x05f1
                int r10 = r9.length     // Catch:{ all -> 0x05cf }
                if (r10 < r5) goto L_0x05f1
                goto L_0x05f2
            L_0x05f1:
                r9 = 0
            L_0x05f2:
                if (r9 != 0) goto L_0x05fd
                byte[] r9 = new byte[r5]     // Catch:{ all -> 0x05cf }
                java.lang.ThreadLocal r10 = org.telegram.messenger.ImageLoader.bytesLocal     // Catch:{ all -> 0x05cf }
                r10.set(r9)     // Catch:{ all -> 0x05cf }
            L_0x05fd:
                r10 = 0
                r0.readFully(r9, r10, r5)     // Catch:{ all -> 0x0628 }
                r0.close()     // Catch:{ all -> 0x0628 }
                org.telegram.messenger.secretmedia.EncryptedFileInputStream.decryptBytesWithKeyFile((byte[]) r9, (int) r10, (int) r5, (org.telegram.messenger.SecureDocumentKey) r4)     // Catch:{ all -> 0x0628 }
                r30 = r7
                r29 = r8
                long r7 = (long) r5
                byte[] r0 = org.telegram.messenger.Utilities.computeSHA256(r9, r10, r7)     // Catch:{ all -> 0x0697 }
                if (r11 == 0) goto L_0x061b
                boolean r0 = java.util.Arrays.equals(r0, r11)     // Catch:{ all -> 0x0697 }
                if (r0 != 0) goto L_0x0619
                goto L_0x061b
            L_0x0619:
                r0 = 0
                goto L_0x061c
            L_0x061b:
                r0 = 1
            L_0x061c:
                r7 = 0
                byte r8 = r9[r7]     // Catch:{ all -> 0x0697 }
                r7 = r8 & 255(0xff, float:3.57E-43)
                int r5 = r5 - r7
                if (r0 != 0) goto L_0x064e
                android.graphics.BitmapFactory.decodeByteArray(r9, r7, r5, r12)     // Catch:{ all -> 0x0697 }
                goto L_0x064e
            L_0x0628:
                r0 = move-exception
                r30 = r7
                r29 = r8
            L_0x062d:
                r10 = r0
                r5 = r28
                goto L_0x06c6
            L_0x0632:
                r30 = r7
                r29 = r8
                if (r3 == 0) goto L_0x0642
                org.telegram.messenger.secretmedia.EncryptedFileInputStream r0 = new org.telegram.messenger.secretmedia.EncryptedFileInputStream     // Catch:{ all -> 0x0697 }
                org.telegram.messenger.ImageLoader$CacheImage r5 = r1.cacheImage     // Catch:{ all -> 0x0697 }
                java.io.File r5 = r5.encryptionKeyPath     // Catch:{ all -> 0x0697 }
                r0.<init>((java.io.File) r2, (java.io.File) r5)     // Catch:{ all -> 0x0697 }
                goto L_0x0647
            L_0x0642:
                java.io.FileInputStream r0 = new java.io.FileInputStream     // Catch:{ all -> 0x0697 }
                r0.<init>(r2)     // Catch:{ all -> 0x0697 }
            L_0x0647:
                r5 = 0
                android.graphics.BitmapFactory.decodeStream(r0, r5, r12)     // Catch:{ all -> 0x0697 }
                r0.close()     // Catch:{ all -> 0x0697 }
            L_0x064e:
                int r0 = r12.outWidth     // Catch:{ all -> 0x0697 }
                float r0 = (float) r0     // Catch:{ all -> 0x0697 }
                int r5 = r12.outHeight     // Catch:{ all -> 0x0697 }
                float r5 = (float) r5     // Catch:{ all -> 0x0697 }
                int r7 = (r26 > r6 ? 1 : (r26 == r6 ? 0 : -1))
                if (r7 < 0) goto L_0x0665
                int r7 = (r0 > r5 ? 1 : (r0 == r5 ? 0 : -1))
                if (r7 <= 0) goto L_0x0665
                float r7 = r0 / r26
                float r8 = r5 / r6
                float r7 = java.lang.Math.max(r7, r8)     // Catch:{ all -> 0x0697 }
                goto L_0x066d
            L_0x0665:
                float r7 = r0 / r26
                float r8 = r5 / r6
                float r7 = java.lang.Math.min(r7, r8)     // Catch:{ all -> 0x0697 }
            L_0x066d:
                r8 = 1067030938(0x3var_a, float:1.2)
                int r8 = (r7 > r8 ? 1 : (r7 == r8 ? 0 : -1))
                if (r8 >= 0) goto L_0x0676
                r7 = 1065353216(0x3var_, float:1.0)
            L_0x0676:
                r8 = 0
                r12.inJustDecodeBounds = r8     // Catch:{ all -> 0x0697 }
                int r8 = (r7 > r24 ? 1 : (r7 == r24 ? 0 : -1))
                if (r8 <= 0) goto L_0x0693
                int r0 = (r0 > r26 ? 1 : (r0 == r26 ? 0 : -1))
                if (r0 > 0) goto L_0x0685
                int r0 = (r5 > r6 ? 1 : (r5 == r6 ? 0 : -1))
                if (r0 <= 0) goto L_0x0693
            L_0x0685:
                r0 = 1
            L_0x0686:
                r5 = 2
                int r0 = r0 * 2
                int r5 = r0 * 2
                float r5 = (float) r5     // Catch:{ all -> 0x0697 }
                int r5 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
                if (r5 < 0) goto L_0x0686
                r12.inSampleSize = r0     // Catch:{ all -> 0x0697 }
                goto L_0x06ac
            L_0x0693:
                int r0 = (int) r7     // Catch:{ all -> 0x0697 }
                r12.inSampleSize = r0     // Catch:{ all -> 0x0697 }
                goto L_0x06ac
            L_0x0697:
                r0 = move-exception
                r10 = r0
                r5 = r28
                r8 = r29
                r7 = r30
                goto L_0x06c6
            L_0x06a0:
                r0 = move-exception
                r28 = r5
                r30 = r7
                goto L_0x06b8
            L_0x06a6:
                r28 = r5
                r30 = r7
                r29 = r8
            L_0x06ac:
                r5 = r28
                r8 = r29
                r7 = r30
                r0 = 0
                goto L_0x073c
            L_0x06b5:
                r0 = move-exception
                r28 = r5
            L_0x06b8:
                r29 = r8
            L_0x06ba:
                r10 = r0
                goto L_0x06c6
            L_0x06bc:
                r0 = move-exception
                r29 = r8
                r10 = r0
                r5 = 0
                goto L_0x06c6
            L_0x06c2:
                r0 = move-exception
                r10 = r0
                r5 = 0
                r8 = 0
            L_0x06c6:
                r9 = 0
                r35 = r26
                r26 = r6
                r6 = r35
                goto L_0x074b
            L_0x06cf:
                if (r14 == 0) goto L_0x0736
                r5 = 1
                r12.inJustDecodeBounds = r5     // Catch:{ all -> 0x0743 }
                if (r7 == 0) goto L_0x06d9
                android.graphics.Bitmap$Config r0 = android.graphics.Bitmap.Config.ARGB_8888     // Catch:{ all -> 0x0743 }
                goto L_0x06db
            L_0x06d9:
                android.graphics.Bitmap$Config r0 = android.graphics.Bitmap.Config.RGB_565     // Catch:{ all -> 0x0743 }
            L_0x06db:
                r12.inPreferredConfig = r0     // Catch:{ all -> 0x0743 }
                java.io.FileInputStream r0 = new java.io.FileInputStream     // Catch:{ all -> 0x0743 }
                r0.<init>(r2)     // Catch:{ all -> 0x0743 }
                r5 = 0
                android.graphics.Bitmap r6 = android.graphics.BitmapFactory.decodeStream(r0, r5, r12)     // Catch:{ all -> 0x0743 }
                r0.close()     // Catch:{ all -> 0x072f }
                int r0 = r12.outWidth     // Catch:{ all -> 0x072f }
                int r5 = r12.outHeight     // Catch:{ all -> 0x072f }
                r8 = 0
                r12.inJustDecodeBounds = r8     // Catch:{ all -> 0x072f }
                r8 = 66
                android.graphics.Point r9 = org.telegram.messenger.AndroidUtilities.getRealScreenSize()     // Catch:{ all -> 0x072f }
                int r9 = r9.x     // Catch:{ all -> 0x072f }
                android.graphics.Point r10 = org.telegram.messenger.AndroidUtilities.getRealScreenSize()     // Catch:{ all -> 0x072f }
                int r10 = r10.y     // Catch:{ all -> 0x072f }
                int r9 = java.lang.Math.min(r9, r10)     // Catch:{ all -> 0x072f }
                int r8 = java.lang.Math.max(r8, r9)     // Catch:{ all -> 0x072f }
                int r0 = java.lang.Math.min(r5, r0)     // Catch:{ all -> 0x072f }
                float r0 = (float) r0     // Catch:{ all -> 0x072f }
                float r5 = (float) r8     // Catch:{ all -> 0x072f }
                float r0 = r0 / r5
                r5 = 1086324736(0x40CLASSNAME, float:6.0)
                float r0 = r0 * r5
                int r5 = (r0 > r24 ? 1 : (r0 == r24 ? 0 : -1))
                if (r5 >= 0) goto L_0x0718
                r0 = 1065353216(0x3var_, float:1.0)
            L_0x0718:
                int r5 = (r0 > r24 ? 1 : (r0 == r24 ? 0 : -1))
                if (r5 <= 0) goto L_0x072a
                r5 = 1
            L_0x071d:
                r8 = 2
                int r5 = r5 * 2
                int r8 = r5 * 2
                float r8 = (float) r8     // Catch:{ all -> 0x072f }
                int r8 = (r8 > r0 ? 1 : (r8 == r0 ? 0 : -1))
                if (r8 <= 0) goto L_0x071d
                r12.inSampleSize = r5     // Catch:{ all -> 0x072f }
                goto L_0x072d
            L_0x072a:
                int r0 = (int) r0     // Catch:{ all -> 0x072f }
                r12.inSampleSize = r0     // Catch:{ all -> 0x072f }
            L_0x072d:
                r0 = r6
                goto L_0x0737
            L_0x072f:
                r0 = move-exception
                r10 = r0
                r9 = r6
                r5 = 0
                r6 = 0
                r8 = 0
                goto L_0x0749
            L_0x0736:
                r0 = 0
            L_0x0737:
                r5 = 0
                r6 = 0
                r8 = 0
                r26 = 0
            L_0x073c:
                r9 = r0
                r28 = r5
                r0 = r26
                r5 = 1
                goto L_0x0757
            L_0x0743:
                r0 = move-exception
                r10 = r0
                r5 = 0
                r6 = 0
            L_0x0747:
                r8 = 0
                r9 = 0
            L_0x0749:
                r26 = 0
            L_0x074b:
                boolean r0 = r10 instanceof java.io.FileNotFoundException
                r28 = r5
                r5 = 1
                r0 = r0 ^ r5
                org.telegram.messenger.FileLog.e((java.lang.Throwable) r10, (boolean) r0)
                r0 = r6
                r6 = r26
            L_0x0757:
                org.telegram.messenger.ImageLoader$CacheImage r10 = r1.cacheImage
                int r10 = r10.type
                r26 = 1101004800(0x41a00000, float:20.0)
                if (r10 != r5) goto L_0x096d
                org.telegram.messenger.ImageLoader r5 = org.telegram.messenger.ImageLoader.this     // Catch:{ all -> 0x0964 }
                long r6 = android.os.SystemClock.elapsedRealtime()     // Catch:{ all -> 0x0964 }
                long unused = r5.lastCacheOutTime = r6     // Catch:{ all -> 0x0964 }
                java.lang.Object r5 = r1.sync     // Catch:{ all -> 0x0964 }
                monitor-enter(r5)     // Catch:{ all -> 0x0964 }
                boolean r6 = r1.isCancelled     // Catch:{ all -> 0x0961 }
                if (r6 == 0) goto L_0x0771
                monitor-exit(r5)     // Catch:{ all -> 0x0961 }
                return
            L_0x0771:
                monitor-exit(r5)     // Catch:{ all -> 0x0961 }
                if (r13 == 0) goto L_0x07b9
                java.io.RandomAccessFile r4 = new java.io.RandomAccessFile     // Catch:{ all -> 0x0964 }
                java.lang.String r5 = "r"
                r4.<init>(r2, r5)     // Catch:{ all -> 0x0964 }
                java.nio.channels.FileChannel r29 = r4.getChannel()     // Catch:{ all -> 0x0964 }
                java.nio.channels.FileChannel$MapMode r30 = java.nio.channels.FileChannel.MapMode.READ_ONLY     // Catch:{ all -> 0x0964 }
                r31 = 0
                long r33 = r2.length()     // Catch:{ all -> 0x0964 }
                java.nio.MappedByteBuffer r5 = r29.map(r30, r31, r33)     // Catch:{ all -> 0x0964 }
                android.graphics.BitmapFactory$Options r6 = new android.graphics.BitmapFactory$Options     // Catch:{ all -> 0x0964 }
                r6.<init>()     // Catch:{ all -> 0x0964 }
                r7 = 1
                r6.inJustDecodeBounds = r7     // Catch:{ all -> 0x0964 }
                int r10 = r5.limit()     // Catch:{ all -> 0x0964 }
                r11 = 0
                org.telegram.messenger.Utilities.loadWebpImage(r11, r5, r10, r6, r7)     // Catch:{ all -> 0x0964 }
                int r7 = r6.outWidth     // Catch:{ all -> 0x0964 }
                int r6 = r6.outHeight     // Catch:{ all -> 0x0964 }
                android.graphics.Bitmap$Config r10 = android.graphics.Bitmap.Config.ARGB_8888     // Catch:{ all -> 0x0964 }
                android.graphics.Bitmap r9 = org.telegram.messenger.Bitmaps.createBitmap(r7, r6, r10)     // Catch:{ all -> 0x0964 }
                int r6 = r5.limit()     // Catch:{ all -> 0x0964 }
                boolean r7 = r12.inPurgeable     // Catch:{ all -> 0x0964 }
                if (r7 != 0) goto L_0x07af
                r7 = 1
                goto L_0x07b0
            L_0x07af:
                r7 = 0
            L_0x07b0:
                r10 = 0
                org.telegram.messenger.Utilities.loadWebpImage(r9, r5, r6, r10, r7)     // Catch:{ all -> 0x0964 }
                r4.close()     // Catch:{ all -> 0x0964 }
                goto L_0x0838
            L_0x07b9:
                boolean r5 = r12.inPurgeable     // Catch:{ all -> 0x0964 }
                if (r5 != 0) goto L_0x07da
                if (r4 == 0) goto L_0x07c0
                goto L_0x07da
            L_0x07c0:
                if (r3 == 0) goto L_0x07cc
                org.telegram.messenger.secretmedia.EncryptedFileInputStream r4 = new org.telegram.messenger.secretmedia.EncryptedFileInputStream     // Catch:{ all -> 0x0964 }
                org.telegram.messenger.ImageLoader$CacheImage r5 = r1.cacheImage     // Catch:{ all -> 0x0964 }
                java.io.File r5 = r5.encryptionKeyPath     // Catch:{ all -> 0x0964 }
                r4.<init>((java.io.File) r2, (java.io.File) r5)     // Catch:{ all -> 0x0964 }
                goto L_0x07d1
            L_0x07cc:
                java.io.FileInputStream r4 = new java.io.FileInputStream     // Catch:{ all -> 0x0964 }
                r4.<init>(r2)     // Catch:{ all -> 0x0964 }
            L_0x07d1:
                r5 = 0
                android.graphics.Bitmap r9 = android.graphics.BitmapFactory.decodeStream(r4, r5, r12)     // Catch:{ all -> 0x0964 }
                r4.close()     // Catch:{ all -> 0x0964 }
                goto L_0x0838
            L_0x07da:
                java.io.RandomAccessFile r5 = new java.io.RandomAccessFile     // Catch:{ all -> 0x0964 }
                java.lang.String r6 = "r"
                r5.<init>(r2, r6)     // Catch:{ all -> 0x0964 }
                long r6 = r5.length()     // Catch:{ all -> 0x0964 }
                int r7 = (int) r6     // Catch:{ all -> 0x0964 }
                java.lang.ThreadLocal r6 = org.telegram.messenger.ImageLoader.bytesThumbLocal     // Catch:{ all -> 0x0964 }
                java.lang.Object r6 = r6.get()     // Catch:{ all -> 0x0964 }
                byte[] r6 = (byte[]) r6     // Catch:{ all -> 0x0964 }
                if (r6 == 0) goto L_0x07f6
                int r10 = r6.length     // Catch:{ all -> 0x0964 }
                if (r10 < r7) goto L_0x07f6
                goto L_0x07f7
            L_0x07f6:
                r6 = 0
            L_0x07f7:
                if (r6 != 0) goto L_0x0802
                byte[] r6 = new byte[r7]     // Catch:{ all -> 0x0964 }
                java.lang.ThreadLocal r10 = org.telegram.messenger.ImageLoader.bytesThumbLocal     // Catch:{ all -> 0x0964 }
                r10.set(r6)     // Catch:{ all -> 0x0964 }
            L_0x0802:
                r10 = 0
                r5.readFully(r6, r10, r7)     // Catch:{ all -> 0x0964 }
                r5.close()     // Catch:{ all -> 0x0964 }
                if (r4 == 0) goto L_0x0826
                org.telegram.messenger.secretmedia.EncryptedFileInputStream.decryptBytesWithKeyFile((byte[]) r6, (int) r10, (int) r7, (org.telegram.messenger.SecureDocumentKey) r4)     // Catch:{ all -> 0x0964 }
                long r4 = (long) r7     // Catch:{ all -> 0x0964 }
                byte[] r4 = org.telegram.messenger.Utilities.computeSHA256(r6, r10, r4)     // Catch:{ all -> 0x0964 }
                if (r11 == 0) goto L_0x081e
                boolean r4 = java.util.Arrays.equals(r4, r11)     // Catch:{ all -> 0x0964 }
                if (r4 != 0) goto L_0x081c
                goto L_0x081e
            L_0x081c:
                r4 = 0
                goto L_0x081f
            L_0x081e:
                r4 = 1
            L_0x081f:
                r5 = 0
                byte r10 = r6[r5]     // Catch:{ all -> 0x0964 }
                r5 = r10 & 255(0xff, float:3.57E-43)
                int r7 = r7 - r5
                goto L_0x0832
            L_0x0826:
                if (r3 == 0) goto L_0x0830
                org.telegram.messenger.ImageLoader$CacheImage r4 = r1.cacheImage     // Catch:{ all -> 0x0964 }
                java.io.File r4 = r4.encryptionKeyPath     // Catch:{ all -> 0x0964 }
                r5 = 0
                org.telegram.messenger.secretmedia.EncryptedFileInputStream.decryptBytesWithKeyFile((byte[]) r6, (int) r5, (int) r7, (java.io.File) r4)     // Catch:{ all -> 0x0964 }
            L_0x0830:
                r4 = 0
                r5 = 0
            L_0x0832:
                if (r4 != 0) goto L_0x0838
                android.graphics.Bitmap r9 = android.graphics.BitmapFactory.decodeByteArray(r6, r5, r7, r12)     // Catch:{ all -> 0x0964 }
            L_0x0838:
                if (r9 != 0) goto L_0x0850
                long r4 = r2.length()     // Catch:{ all -> 0x0964 }
                r6 = 0
                int r0 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
                if (r0 == 0) goto L_0x084a
                org.telegram.messenger.ImageLoader$CacheImage r0 = r1.cacheImage     // Catch:{ all -> 0x0964 }
                java.lang.String r0 = r0.filter     // Catch:{ all -> 0x0964 }
                if (r0 != 0) goto L_0x084d
            L_0x084a:
                r2.delete()     // Catch:{ all -> 0x0964 }
            L_0x084d:
                r4 = 0
                goto L_0x0969
            L_0x0850:
                org.telegram.messenger.ImageLoader$CacheImage r4 = r1.cacheImage     // Catch:{ all -> 0x0964 }
                java.lang.String r4 = r4.filter     // Catch:{ all -> 0x0964 }
                if (r4 == 0) goto L_0x0881
                int r4 = r9.getWidth()     // Catch:{ all -> 0x0964 }
                float r4 = (float) r4     // Catch:{ all -> 0x0964 }
                int r5 = r9.getHeight()     // Catch:{ all -> 0x0964 }
                float r5 = (float) r5     // Catch:{ all -> 0x0964 }
                boolean r6 = r12.inPurgeable     // Catch:{ all -> 0x0964 }
                if (r6 != 0) goto L_0x0881
                int r6 = (r0 > r23 ? 1 : (r0 == r23 ? 0 : -1))
                if (r6 == 0) goto L_0x0881
                int r6 = (r4 > r0 ? 1 : (r4 == r0 ? 0 : -1))
                if (r6 == 0) goto L_0x0881
                float r26 = r0 + r26
                int r6 = (r4 > r26 ? 1 : (r4 == r26 ? 0 : -1))
                if (r6 <= 0) goto L_0x0881
                float r4 = r4 / r0
                int r0 = (int) r0     // Catch:{ all -> 0x0964 }
                float r5 = r5 / r4
                int r4 = (int) r5     // Catch:{ all -> 0x0964 }
                r5 = 1
                android.graphics.Bitmap r0 = org.telegram.messenger.Bitmaps.createScaledBitmap(r9, r0, r4, r5)     // Catch:{ all -> 0x0964 }
                if (r9 == r0) goto L_0x0881
                r9.recycle()     // Catch:{ all -> 0x0964 }
                r9 = r0
            L_0x0881:
                if (r28 == 0) goto L_0x08a1
                boolean r0 = r12.inPurgeable     // Catch:{ all -> 0x0964 }
                if (r0 == 0) goto L_0x0889
                r0 = 0
                goto L_0x088a
            L_0x0889:
                r0 = 1
            L_0x088a:
                int r4 = r9.getWidth()     // Catch:{ all -> 0x0964 }
                int r5 = r9.getHeight()     // Catch:{ all -> 0x0964 }
                int r6 = r9.getRowBytes()     // Catch:{ all -> 0x0964 }
                int r0 = org.telegram.messenger.Utilities.needInvert(r9, r0, r4, r5, r6)     // Catch:{ all -> 0x0964 }
                if (r0 == 0) goto L_0x089e
                r0 = 1
                goto L_0x089f
            L_0x089e:
                r0 = 0
            L_0x089f:
                r4 = r0
                goto L_0x08a2
            L_0x08a1:
                r4 = 0
            L_0x08a2:
                r5 = 1
                if (r8 != r5) goto L_0x08ce
                android.graphics.Bitmap$Config r0 = r9.getConfig()     // Catch:{ all -> 0x08cb }
                android.graphics.Bitmap$Config r5 = android.graphics.Bitmap.Config.ARGB_8888     // Catch:{ all -> 0x08cb }
                if (r0 != r5) goto L_0x0969
                r19 = 3
                boolean r0 = r12.inPurgeable     // Catch:{ all -> 0x08cb }
                if (r0 == 0) goto L_0x08b6
                r20 = 0
                goto L_0x08b8
            L_0x08b6:
                r20 = 1
            L_0x08b8:
                int r21 = r9.getWidth()     // Catch:{ all -> 0x08cb }
                int r22 = r9.getHeight()     // Catch:{ all -> 0x08cb }
                int r23 = r9.getRowBytes()     // Catch:{ all -> 0x08cb }
                r18 = r9
                org.telegram.messenger.Utilities.blurBitmap(r18, r19, r20, r21, r22, r23)     // Catch:{ all -> 0x08cb }
                goto L_0x0969
            L_0x08cb:
                r0 = move-exception
                goto L_0x0966
            L_0x08ce:
                r5 = 2
                if (r8 != r5) goto L_0x08f7
                android.graphics.Bitmap$Config r0 = r9.getConfig()     // Catch:{ all -> 0x08cb }
                android.graphics.Bitmap$Config r5 = android.graphics.Bitmap.Config.ARGB_8888     // Catch:{ all -> 0x08cb }
                if (r0 != r5) goto L_0x0969
                r19 = 1
                boolean r0 = r12.inPurgeable     // Catch:{ all -> 0x08cb }
                if (r0 == 0) goto L_0x08e2
                r20 = 0
                goto L_0x08e4
            L_0x08e2:
                r20 = 1
            L_0x08e4:
                int r21 = r9.getWidth()     // Catch:{ all -> 0x08cb }
                int r22 = r9.getHeight()     // Catch:{ all -> 0x08cb }
                int r23 = r9.getRowBytes()     // Catch:{ all -> 0x08cb }
                r18 = r9
                org.telegram.messenger.Utilities.blurBitmap(r18, r19, r20, r21, r22, r23)     // Catch:{ all -> 0x08cb }
                goto L_0x0969
            L_0x08f7:
                r5 = 3
                if (r8 != r5) goto L_0x0957
                android.graphics.Bitmap$Config r0 = r9.getConfig()     // Catch:{ all -> 0x08cb }
                android.graphics.Bitmap$Config r5 = android.graphics.Bitmap.Config.ARGB_8888     // Catch:{ all -> 0x08cb }
                if (r0 != r5) goto L_0x0969
                r19 = 7
                boolean r0 = r12.inPurgeable     // Catch:{ all -> 0x08cb }
                if (r0 == 0) goto L_0x090b
                r20 = 0
                goto L_0x090d
            L_0x090b:
                r20 = 1
            L_0x090d:
                int r21 = r9.getWidth()     // Catch:{ all -> 0x08cb }
                int r22 = r9.getHeight()     // Catch:{ all -> 0x08cb }
                int r23 = r9.getRowBytes()     // Catch:{ all -> 0x08cb }
                r18 = r9
                org.telegram.messenger.Utilities.blurBitmap(r18, r19, r20, r21, r22, r23)     // Catch:{ all -> 0x08cb }
                r19 = 7
                boolean r0 = r12.inPurgeable     // Catch:{ all -> 0x08cb }
                if (r0 == 0) goto L_0x0927
                r20 = 0
                goto L_0x0929
            L_0x0927:
                r20 = 1
            L_0x0929:
                int r21 = r9.getWidth()     // Catch:{ all -> 0x08cb }
                int r22 = r9.getHeight()     // Catch:{ all -> 0x08cb }
                int r23 = r9.getRowBytes()     // Catch:{ all -> 0x08cb }
                r18 = r9
                org.telegram.messenger.Utilities.blurBitmap(r18, r19, r20, r21, r22, r23)     // Catch:{ all -> 0x08cb }
                r19 = 7
                boolean r0 = r12.inPurgeable     // Catch:{ all -> 0x08cb }
                if (r0 == 0) goto L_0x0943
                r20 = 0
                goto L_0x0945
            L_0x0943:
                r20 = 1
            L_0x0945:
                int r21 = r9.getWidth()     // Catch:{ all -> 0x08cb }
                int r22 = r9.getHeight()     // Catch:{ all -> 0x08cb }
                int r23 = r9.getRowBytes()     // Catch:{ all -> 0x08cb }
                r18 = r9
                org.telegram.messenger.Utilities.blurBitmap(r18, r19, r20, r21, r22, r23)     // Catch:{ all -> 0x08cb }
                goto L_0x0969
            L_0x0957:
                if (r8 != 0) goto L_0x0969
                boolean r0 = r12.inPurgeable     // Catch:{ all -> 0x08cb }
                if (r0 == 0) goto L_0x0969
                org.telegram.messenger.Utilities.pinBitmap(r9)     // Catch:{ all -> 0x08cb }
                goto L_0x0969
            L_0x0961:
                r0 = move-exception
                monitor-exit(r5)     // Catch:{ all -> 0x0961 }
                throw r0     // Catch:{ all -> 0x0964 }
            L_0x0964:
                r0 = move-exception
                r4 = 0
            L_0x0966:
                org.telegram.messenger.FileLog.e((java.lang.Throwable) r0)
            L_0x0969:
                r7 = 0
                r10 = 0
                goto L_0x0CLASSNAME
            L_0x096d:
                r5 = 20
                if (r18 == 0) goto L_0x0972
                r5 = 0
            L_0x0972:
                if (r5 == 0) goto L_0x09ad
                org.telegram.messenger.ImageLoader r10 = org.telegram.messenger.ImageLoader.this     // Catch:{ all -> 0x09a6 }
                long r29 = r10.lastCacheOutTime     // Catch:{ all -> 0x09a6 }
                r21 = 0
                int r10 = (r29 > r21 ? 1 : (r29 == r21 ? 0 : -1))
                if (r10 == 0) goto L_0x09ad
                org.telegram.messenger.ImageLoader r10 = org.telegram.messenger.ImageLoader.this     // Catch:{ all -> 0x09a6 }
                long r29 = r10.lastCacheOutTime     // Catch:{ all -> 0x09a6 }
                long r31 = android.os.SystemClock.elapsedRealtime()     // Catch:{ all -> 0x09a6 }
                r27 = r9
                long r9 = (long) r5
                long r31 = r31 - r9
                int r5 = (r29 > r31 ? 1 : (r29 == r31 ? 0 : -1))
                if (r5 <= 0) goto L_0x09a3
                int r5 = android.os.Build.VERSION.SDK_INT     // Catch:{ all -> 0x099f }
                r29 = r6
                r6 = 21
                if (r5 >= r6) goto L_0x09b1
                java.lang.Thread.sleep(r9)     // Catch:{ all -> 0x099f }
                goto L_0x09b1
            L_0x099f:
                r0 = move-exception
                r9 = r27
                goto L_0x09a9
            L_0x09a3:
                r29 = r6
                goto L_0x09b1
            L_0x09a6:
                r0 = move-exception
                r27 = r9
            L_0x09a9:
                r5 = 0
            L_0x09aa:
                r7 = 0
                goto L_0x0CLASSNAME
            L_0x09ad:
                r29 = r6
                r27 = r9
            L_0x09b1:
                org.telegram.messenger.ImageLoader r5 = org.telegram.messenger.ImageLoader.this     // Catch:{ all -> 0x0c3e }
                long r9 = android.os.SystemClock.elapsedRealtime()     // Catch:{ all -> 0x0c3e }
                long unused = r5.lastCacheOutTime = r9     // Catch:{ all -> 0x0c3e }
                java.lang.Object r5 = r1.sync     // Catch:{ all -> 0x0c3e }
                monitor-enter(r5)     // Catch:{ all -> 0x0c3e }
                boolean r6 = r1.isCancelled     // Catch:{ all -> 0x0CLASSNAME }
                if (r6 == 0) goto L_0x09c3
                monitor-exit(r5)     // Catch:{ all -> 0x0CLASSNAME }
                return
            L_0x09c3:
                monitor-exit(r5)     // Catch:{ all -> 0x0CLASSNAME }
                if (r7 != 0) goto L_0x09da
                org.telegram.messenger.ImageLoader$CacheImage r5 = r1.cacheImage     // Catch:{ all -> 0x099f }
                java.lang.String r6 = r5.filter     // Catch:{ all -> 0x099f }
                if (r6 == 0) goto L_0x09da
                if (r8 != 0) goto L_0x09da
                org.telegram.messenger.ImageLocation r5 = r5.imageLocation     // Catch:{ all -> 0x099f }
                java.lang.String r5 = r5.path     // Catch:{ all -> 0x099f }
                if (r5 == 0) goto L_0x09d5
                goto L_0x09da
            L_0x09d5:
                android.graphics.Bitmap$Config r5 = android.graphics.Bitmap.Config.RGB_565     // Catch:{ all -> 0x099f }
                r12.inPreferredConfig = r5     // Catch:{ all -> 0x099f }
                goto L_0x09de
            L_0x09da:
                android.graphics.Bitmap$Config r5 = android.graphics.Bitmap.Config.ARGB_8888     // Catch:{ all -> 0x0c3e }
                r12.inPreferredConfig = r5     // Catch:{ all -> 0x0c3e }
            L_0x09de:
                r5 = 0
                r12.inDither = r5     // Catch:{ all -> 0x0c3e }
                if (r18 == 0) goto L_0x0a08
                if (r14 != 0) goto L_0x0a08
                if (r15 == 0) goto L_0x09f7
                android.content.Context r5 = org.telegram.messenger.ApplicationLoader.applicationContext     // Catch:{ all -> 0x099f }
                android.content.ContentResolver r5 = r5.getContentResolver()     // Catch:{ all -> 0x099f }
                long r6 = r18.longValue()     // Catch:{ all -> 0x099f }
                r9 = 1
                android.graphics.Bitmap r5 = android.provider.MediaStore.Video.Thumbnails.getThumbnail(r5, r6, r9, r12)     // Catch:{ all -> 0x099f }
                goto L_0x0a06
            L_0x09f7:
                android.content.Context r5 = org.telegram.messenger.ApplicationLoader.applicationContext     // Catch:{ all -> 0x099f }
                android.content.ContentResolver r5 = r5.getContentResolver()     // Catch:{ all -> 0x099f }
                long r6 = r18.longValue()     // Catch:{ all -> 0x099f }
                r9 = 1
                android.graphics.Bitmap r5 = android.provider.MediaStore.Images.Thumbnails.getThumbnail(r5, r6, r9, r12)     // Catch:{ all -> 0x099f }
            L_0x0a06:
                r9 = r5
                goto L_0x0a0a
            L_0x0a08:
                r9 = r27
            L_0x0a0a:
                if (r9 != 0) goto L_0x0b2a
                if (r13 == 0) goto L_0x0a5e
                java.io.RandomAccessFile r4 = new java.io.RandomAccessFile     // Catch:{ all -> 0x0a5b }
                java.lang.String r5 = "r"
                r4.<init>(r2, r5)     // Catch:{ all -> 0x0a5b }
                java.nio.channels.FileChannel r13 = r4.getChannel()     // Catch:{ all -> 0x0a5b }
                java.nio.channels.FileChannel$MapMode r14 = java.nio.channels.FileChannel.MapMode.READ_ONLY     // Catch:{ all -> 0x0a5b }
                r15 = 0
                long r17 = r2.length()     // Catch:{ all -> 0x0a5b }
                java.nio.MappedByteBuffer r5 = r13.map(r14, r15, r17)     // Catch:{ all -> 0x0a5b }
                android.graphics.BitmapFactory$Options r6 = new android.graphics.BitmapFactory$Options     // Catch:{ all -> 0x0a5b }
                r6.<init>()     // Catch:{ all -> 0x0a5b }
                r7 = 1
                r6.inJustDecodeBounds = r7     // Catch:{ all -> 0x0a5b }
                int r10 = r5.limit()     // Catch:{ all -> 0x0a5b }
                r11 = 0
                org.telegram.messenger.Utilities.loadWebpImage(r11, r5, r10, r6, r7)     // Catch:{ all -> 0x0a57 }
                int r7 = r6.outWidth     // Catch:{ all -> 0x0a5b }
                int r6 = r6.outHeight     // Catch:{ all -> 0x0a5b }
                android.graphics.Bitmap$Config r10 = android.graphics.Bitmap.Config.ARGB_8888     // Catch:{ all -> 0x0a5b }
                android.graphics.Bitmap r9 = org.telegram.messenger.Bitmaps.createBitmap(r7, r6, r10)     // Catch:{ all -> 0x0a5b }
                int r6 = r5.limit()     // Catch:{ all -> 0x0a5b }
                boolean r7 = r12.inPurgeable     // Catch:{ all -> 0x0a5b }
                if (r7 != 0) goto L_0x0a49
                r7 = 1
                goto L_0x0a4a
            L_0x0a49:
                r7 = 0
            L_0x0a4a:
                r10 = 0
                org.telegram.messenger.Utilities.loadWebpImage(r9, r5, r6, r10, r7)     // Catch:{ all -> 0x0a53 }
                r4.close()     // Catch:{ all -> 0x0a5b }
                goto L_0x0b2a
            L_0x0a53:
                r0 = move-exception
                r7 = r10
                goto L_0x0CLASSNAME
            L_0x0a57:
                r0 = move-exception
                r7 = r11
                goto L_0x0CLASSNAME
            L_0x0a5b:
                r0 = move-exception
                goto L_0x09a9
            L_0x0a5e:
                boolean r5 = r12.inPurgeable     // Catch:{ all -> 0x0b26 }
                if (r5 != 0) goto L_0x0ac3
                if (r4 != 0) goto L_0x0ac3
                int r5 = android.os.Build.VERSION.SDK_INT     // Catch:{ all -> 0x0b26 }
                r6 = 28
                if (r5 > r6) goto L_0x0a6b
                goto L_0x0ac3
            L_0x0a6b:
                if (r3 == 0) goto L_0x0a77
                org.telegram.messenger.secretmedia.EncryptedFileInputStream r4 = new org.telegram.messenger.secretmedia.EncryptedFileInputStream     // Catch:{ all -> 0x0a5b }
                org.telegram.messenger.ImageLoader$CacheImage r5 = r1.cacheImage     // Catch:{ all -> 0x0a5b }
                java.io.File r5 = r5.encryptionKeyPath     // Catch:{ all -> 0x0a5b }
                r4.<init>((java.io.File) r2, (java.io.File) r5)     // Catch:{ all -> 0x0a5b }
                goto L_0x0a7c
            L_0x0a77:
                java.io.FileInputStream r4 = new java.io.FileInputStream     // Catch:{ all -> 0x0b26 }
                r4.<init>(r2)     // Catch:{ all -> 0x0b26 }
            L_0x0a7c:
                org.telegram.messenger.ImageLoader$CacheImage r5 = r1.cacheImage     // Catch:{ all -> 0x0b26 }
                org.telegram.messenger.ImageLocation r5 = r5.imageLocation     // Catch:{ all -> 0x0b26 }
                org.telegram.tgnet.TLRPC$Document r5 = r5.document     // Catch:{ all -> 0x0b26 }
                boolean r5 = r5 instanceof org.telegram.tgnet.TLRPC$TL_document     // Catch:{ all -> 0x0b26 }
                if (r5 == 0) goto L_0x0ab4
                androidx.exifinterface.media.ExifInterface r5 = new androidx.exifinterface.media.ExifInterface     // Catch:{ all -> 0x0aa6 }
                r5.<init>((java.io.InputStream) r4)     // Catch:{ all -> 0x0aa6 }
                java.lang.String r6 = "Orientation"
                r7 = 1
                int r5 = r5.getAttributeInt(r6, r7)     // Catch:{ all -> 0x0aa6 }
                r6 = 3
                if (r5 == r6) goto L_0x0aa3
                r6 = 6
                if (r5 == r6) goto L_0x0aa0
                r6 = 8
                if (r5 == r6) goto L_0x0a9d
                goto L_0x0aa6
            L_0x0a9d:
                r5 = 270(0x10e, float:3.78E-43)
                goto L_0x0aa7
            L_0x0aa0:
                r5 = 90
                goto L_0x0aa7
            L_0x0aa3:
                r5 = 180(0xb4, float:2.52E-43)
                goto L_0x0aa7
            L_0x0aa6:
                r5 = 0
            L_0x0aa7:
                java.nio.channels.FileChannel r6 = r4.getChannel()     // Catch:{ all -> 0x0ab1 }
                r10 = 0
                r6.position(r10)     // Catch:{ all -> 0x0ab1 }
                goto L_0x0ab5
            L_0x0ab1:
                r0 = move-exception
                goto L_0x09aa
            L_0x0ab4:
                r5 = 0
            L_0x0ab5:
                r7 = 0
                android.graphics.Bitmap r9 = android.graphics.BitmapFactory.decodeStream(r4, r7, r12)     // Catch:{ all -> 0x0ac0 }
                r4.close()     // Catch:{ all -> 0x0ac0 }
                r10 = r5
                goto L_0x0b2c
            L_0x0ac0:
                r0 = move-exception
                goto L_0x0CLASSNAME
            L_0x0ac3:
                r7 = 0
                java.io.RandomAccessFile r5 = new java.io.RandomAccessFile     // Catch:{ all -> 0x0b23 }
                java.lang.String r6 = "r"
                r5.<init>(r2, r6)     // Catch:{ all -> 0x0b23 }
                long r13 = r5.length()     // Catch:{ all -> 0x0b23 }
                int r6 = (int) r13     // Catch:{ all -> 0x0b23 }
                java.lang.ThreadLocal r10 = org.telegram.messenger.ImageLoader.bytesLocal     // Catch:{ all -> 0x0b23 }
                java.lang.Object r10 = r10.get()     // Catch:{ all -> 0x0b23 }
                byte[] r10 = (byte[]) r10     // Catch:{ all -> 0x0b23 }
                if (r10 == 0) goto L_0x0ae0
                int r13 = r10.length     // Catch:{ all -> 0x0b23 }
                if (r13 < r6) goto L_0x0ae0
                goto L_0x0ae1
            L_0x0ae0:
                r10 = r7
            L_0x0ae1:
                if (r10 != 0) goto L_0x0aec
                byte[] r10 = new byte[r6]     // Catch:{ all -> 0x0b23 }
                java.lang.ThreadLocal r13 = org.telegram.messenger.ImageLoader.bytesLocal     // Catch:{ all -> 0x0b23 }
                r13.set(r10)     // Catch:{ all -> 0x0b23 }
            L_0x0aec:
                r13 = 0
                r5.readFully(r10, r13, r6)     // Catch:{ all -> 0x0b23 }
                r5.close()     // Catch:{ all -> 0x0b23 }
                if (r4 == 0) goto L_0x0b10
                org.telegram.messenger.secretmedia.EncryptedFileInputStream.decryptBytesWithKeyFile((byte[]) r10, (int) r13, (int) r6, (org.telegram.messenger.SecureDocumentKey) r4)     // Catch:{ all -> 0x0b23 }
                long r4 = (long) r6     // Catch:{ all -> 0x0b23 }
                byte[] r4 = org.telegram.messenger.Utilities.computeSHA256(r10, r13, r4)     // Catch:{ all -> 0x0b23 }
                if (r11 == 0) goto L_0x0b08
                boolean r4 = java.util.Arrays.equals(r4, r11)     // Catch:{ all -> 0x0b23 }
                if (r4 != 0) goto L_0x0b06
                goto L_0x0b08
            L_0x0b06:
                r4 = 0
                goto L_0x0b09
            L_0x0b08:
                r4 = 1
            L_0x0b09:
                r5 = 0
                byte r11 = r10[r5]     // Catch:{ all -> 0x0b23 }
                r5 = r11 & 255(0xff, float:3.57E-43)
                int r6 = r6 - r5
                goto L_0x0b1c
            L_0x0b10:
                if (r3 == 0) goto L_0x0b1a
                org.telegram.messenger.ImageLoader$CacheImage r4 = r1.cacheImage     // Catch:{ all -> 0x0b23 }
                java.io.File r4 = r4.encryptionKeyPath     // Catch:{ all -> 0x0b23 }
                r5 = 0
                org.telegram.messenger.secretmedia.EncryptedFileInputStream.decryptBytesWithKeyFile((byte[]) r10, (int) r5, (int) r6, (java.io.File) r4)     // Catch:{ all -> 0x0b23 }
            L_0x0b1a:
                r4 = 0
                r5 = 0
            L_0x0b1c:
                if (r4 != 0) goto L_0x0b2b
                android.graphics.Bitmap r9 = android.graphics.BitmapFactory.decodeByteArray(r10, r5, r6, r12)     // Catch:{ all -> 0x0b23 }
                goto L_0x0b2b
            L_0x0b23:
                r0 = move-exception
                goto L_0x0CLASSNAME
            L_0x0b26:
                r0 = move-exception
                r7 = 0
                goto L_0x0CLASSNAME
            L_0x0b2a:
                r7 = 0
            L_0x0b2b:
                r10 = 0
            L_0x0b2c:
                if (r9 != 0) goto L_0x0b46
                if (r19 == 0) goto L_0x0b43
                long r4 = r2.length()     // Catch:{ all -> 0x0CLASSNAME }
                r11 = 0
                int r0 = (r4 > r11 ? 1 : (r4 == r11 ? 0 : -1))
                if (r0 == 0) goto L_0x0b40
                org.telegram.messenger.ImageLoader$CacheImage r0 = r1.cacheImage     // Catch:{ all -> 0x0CLASSNAME }
                java.lang.String r0 = r0.filter     // Catch:{ all -> 0x0CLASSNAME }
                if (r0 != 0) goto L_0x0b43
            L_0x0b40:
                r2.delete()     // Catch:{ all -> 0x0CLASSNAME }
            L_0x0b43:
                r6 = 0
                goto L_0x0CLASSNAME
            L_0x0b46:
                org.telegram.messenger.ImageLoader$CacheImage r4 = r1.cacheImage     // Catch:{ all -> 0x0CLASSNAME }
                java.lang.String r4 = r4.filter     // Catch:{ all -> 0x0CLASSNAME }
                if (r4 == 0) goto L_0x0CLASSNAME
                int r4 = r9.getWidth()     // Catch:{ all -> 0x0CLASSNAME }
                float r4 = (float) r4     // Catch:{ all -> 0x0CLASSNAME }
                int r5 = r9.getHeight()     // Catch:{ all -> 0x0CLASSNAME }
                float r5 = (float) r5     // Catch:{ all -> 0x0CLASSNAME }
                boolean r6 = r12.inPurgeable     // Catch:{ all -> 0x0CLASSNAME }
                if (r6 != 0) goto L_0x0b99
                int r6 = (r0 > r23 ? 1 : (r0 == r23 ? 0 : -1))
                if (r6 == 0) goto L_0x0b99
                int r6 = (r4 > r0 ? 1 : (r4 == r0 ? 0 : -1))
                if (r6 == 0) goto L_0x0b99
                float r26 = r0 + r26
                int r6 = (r4 > r26 ? 1 : (r4 == r26 ? 0 : -1))
                if (r6 <= 0) goto L_0x0b99
                int r6 = (r4 > r5 ? 1 : (r4 == r5 ? 0 : -1))
                if (r6 <= 0) goto L_0x0b80
                int r6 = (r0 > r29 ? 1 : (r0 == r29 ? 0 : -1))
                if (r6 <= 0) goto L_0x0b80
                float r6 = r4 / r0
                int r11 = (r6 > r24 ? 1 : (r6 == r24 ? 0 : -1))
                if (r11 <= 0) goto L_0x0b92
                int r0 = (int) r0     // Catch:{ all -> 0x0CLASSNAME }
                float r6 = r5 / r6
                int r6 = (int) r6     // Catch:{ all -> 0x0CLASSNAME }
                r11 = 1
                android.graphics.Bitmap r0 = org.telegram.messenger.Bitmaps.createScaledBitmap(r9, r0, r6, r11)     // Catch:{ all -> 0x0CLASSNAME }
                goto L_0x0b93
            L_0x0b80:
                float r0 = r5 / r29
                int r6 = (r0 > r24 ? 1 : (r0 == r24 ? 0 : -1))
                if (r6 <= 0) goto L_0x0b92
                float r0 = r4 / r0
                int r0 = (int) r0     // Catch:{ all -> 0x0CLASSNAME }
                r6 = r29
                int r6 = (int) r6     // Catch:{ all -> 0x0CLASSNAME }
                r11 = 1
                android.graphics.Bitmap r0 = org.telegram.messenger.Bitmaps.createScaledBitmap(r9, r0, r6, r11)     // Catch:{ all -> 0x0CLASSNAME }
                goto L_0x0b93
            L_0x0b92:
                r0 = r9
            L_0x0b93:
                if (r9 == r0) goto L_0x0b99
                r9.recycle()     // Catch:{ all -> 0x0CLASSNAME }
                r9 = r0
            L_0x0b99:
                if (r9 == 0) goto L_0x0CLASSNAME
                if (r28 == 0) goto L_0x0bd8
                int r0 = r9.getWidth()     // Catch:{ all -> 0x0CLASSNAME }
                int r6 = r9.getHeight()     // Catch:{ all -> 0x0CLASSNAME }
                int r0 = r0 * r6
                r6 = 22500(0x57e4, float:3.1529E-41)
                if (r0 <= r6) goto L_0x0bb5
                r0 = 100
                r6 = 100
                r11 = 0
                android.graphics.Bitmap r0 = org.telegram.messenger.Bitmaps.createScaledBitmap(r9, r0, r6, r11)     // Catch:{ all -> 0x0CLASSNAME }
                goto L_0x0bb6
            L_0x0bb5:
                r0 = r9
            L_0x0bb6:
                boolean r6 = r12.inPurgeable     // Catch:{ all -> 0x0CLASSNAME }
                if (r6 == 0) goto L_0x0bbc
                r6 = 0
                goto L_0x0bbd
            L_0x0bbc:
                r6 = 1
            L_0x0bbd:
                int r11 = r0.getWidth()     // Catch:{ all -> 0x0CLASSNAME }
                int r13 = r0.getHeight()     // Catch:{ all -> 0x0CLASSNAME }
                int r14 = r0.getRowBytes()     // Catch:{ all -> 0x0CLASSNAME }
                int r6 = org.telegram.messenger.Utilities.needInvert(r0, r6, r11, r13, r14)     // Catch:{ all -> 0x0CLASSNAME }
                if (r6 == 0) goto L_0x0bd1
                r6 = 1
                goto L_0x0bd2
            L_0x0bd1:
                r6 = 0
            L_0x0bd2:
                if (r0 == r9) goto L_0x0bd9
                r0.recycle()     // Catch:{ all -> 0x0c2d }
                goto L_0x0bd9
            L_0x0bd8:
                r6 = 0
            L_0x0bd9:
                r0 = 1120403456(0x42CLASSNAME, float:100.0)
                if (r8 == 0) goto L_0x0bf3
                int r11 = (r5 > r0 ? 1 : (r5 == r0 ? 0 : -1))
                if (r11 > 0) goto L_0x0be5
                int r11 = (r4 > r0 ? 1 : (r4 == r0 ? 0 : -1))
                if (r11 <= 0) goto L_0x0bf3
            L_0x0be5:
                r4 = 80
                r5 = 0
                android.graphics.Bitmap r4 = org.telegram.messenger.Bitmaps.createScaledBitmap(r9, r4, r4, r5)     // Catch:{ all -> 0x0c2d }
                r5 = 1117782016(0x42a00000, float:80.0)
                r9 = 1117782016(0x42a00000, float:80.0)
                r9 = r4
                r4 = 1117782016(0x42a00000, float:80.0)
            L_0x0bf3:
                if (r8 == 0) goto L_0x0c1f
                int r5 = (r5 > r0 ? 1 : (r5 == r0 ? 0 : -1))
                if (r5 >= 0) goto L_0x0c1f
                int r0 = (r4 > r0 ? 1 : (r4 == r0 ? 0 : -1))
                if (r0 >= 0) goto L_0x0c1f
                android.graphics.Bitmap$Config r0 = r9.getConfig()     // Catch:{ all -> 0x0c2d }
                android.graphics.Bitmap$Config r4 = android.graphics.Bitmap.Config.ARGB_8888     // Catch:{ all -> 0x0c2d }
                if (r0 != r4) goto L_0x0c1d
                r14 = 3
                boolean r0 = r12.inPurgeable     // Catch:{ all -> 0x0c2d }
                if (r0 == 0) goto L_0x0c0c
                r15 = 0
                goto L_0x0c0d
            L_0x0c0c:
                r15 = 1
            L_0x0c0d:
                int r16 = r9.getWidth()     // Catch:{ all -> 0x0c2d }
                int r17 = r9.getHeight()     // Catch:{ all -> 0x0c2d }
                int r18 = r9.getRowBytes()     // Catch:{ all -> 0x0c2d }
                r13 = r9
                org.telegram.messenger.Utilities.blurBitmap(r13, r14, r15, r16, r17, r18)     // Catch:{ all -> 0x0c2d }
            L_0x0c1d:
                r5 = 1
                goto L_0x0CLASSNAME
            L_0x0c1f:
                r5 = 0
                goto L_0x0CLASSNAME
            L_0x0CLASSNAME:
                r5 = 0
                r6 = 0
            L_0x0CLASSNAME:
                if (r5 != 0) goto L_0x0CLASSNAME
                boolean r0 = r12.inPurgeable     // Catch:{ all -> 0x0c2d }
                if (r0 == 0) goto L_0x0CLASSNAME
                org.telegram.messenger.Utilities.pinBitmap(r9)     // Catch:{ all -> 0x0c2d }
                goto L_0x0CLASSNAME
            L_0x0c2d:
                r0 = move-exception
                r5 = r10
                r10 = r6
                goto L_0x0CLASSNAME
            L_0x0CLASSNAME:
                r4 = r6
                goto L_0x0CLASSNAME
            L_0x0CLASSNAME:
                r0 = move-exception
                r5 = r10
                goto L_0x0CLASSNAME
            L_0x0CLASSNAME:
                r0 = move-exception
                r7 = 0
            L_0x0CLASSNAME:
                monitor-exit(r5)     // Catch:{ all -> 0x0c3c }
                throw r0     // Catch:{ all -> 0x0c3a }
            L_0x0c3a:
                r0 = move-exception
                goto L_0x0CLASSNAME
            L_0x0c3c:
                r0 = move-exception
                goto L_0x0CLASSNAME
            L_0x0c3e:
                r0 = move-exception
                r7 = 0
            L_0x0CLASSNAME:
                r9 = r27
            L_0x0CLASSNAME:
                r5 = 0
            L_0x0CLASSNAME:
                r10 = 0
            L_0x0CLASSNAME:
                org.telegram.messenger.FileLog.e((java.lang.Throwable) r0)
                r4 = r10
                r10 = r5
            L_0x0CLASSNAME:
                java.lang.Thread.interrupted()
                boolean r0 = org.telegram.messenger.BuildVars.LOGS_ENABLED
                if (r0 == 0) goto L_0x0CLASSNAME
                if (r3 == 0) goto L_0x0CLASSNAME
                java.lang.StringBuilder r0 = new java.lang.StringBuilder
                r0.<init>()
                java.lang.String r3 = "Image Loader image is empty = "
                r0.append(r3)
                if (r9 != 0) goto L_0x0CLASSNAME
                r3 = 1
                goto L_0x0CLASSNAME
            L_0x0CLASSNAME:
                r3 = 0
            L_0x0CLASSNAME:
                r0.append(r3)
                java.lang.String r3 = " "
                r0.append(r3)
                r0.append(r2)
                java.lang.String r0 = r0.toString()
                org.telegram.messenger.FileLog.e((java.lang.String) r0)
            L_0x0CLASSNAME:
                if (r4 != 0) goto L_0x0CLASSNAME
                if (r10 == 0) goto L_0x0CLASSNAME
                goto L_0x0CLASSNAME
            L_0x0CLASSNAME:
                if (r9 == 0) goto L_0x0CLASSNAME
                android.graphics.drawable.BitmapDrawable r5 = new android.graphics.drawable.BitmapDrawable
                r5.<init>(r9)
                goto L_0x0CLASSNAME
            L_0x0CLASSNAME:
                r5 = r7
            L_0x0CLASSNAME:
                r1.onPostExecute(r5)
                goto L_0x0ce4
            L_0x0CLASSNAME:
                if (r9 == 0) goto L_0x0c8d
                org.telegram.messenger.ExtendedBitmapDrawable r5 = new org.telegram.messenger.ExtendedBitmapDrawable
                r5.<init>(r9, r4, r10)
                goto L_0x0c8e
            L_0x0c8d:
                r5 = r7
            L_0x0c8e:
                r1.onPostExecute(r5)
                goto L_0x0ce4
            L_0x0CLASSNAME:
                r7 = 0
                android.graphics.Point r2 = org.telegram.messenger.AndroidUtilities.displaySize
                int r3 = r2.x
                int r2 = r2.y
                java.lang.String r0 = r0.filter
                if (r0 == 0) goto L_0x0cc0
                java.lang.String r4 = "_"
                java.lang.String[] r0 = r0.split(r4)
                int r4 = r0.length
                r5 = 2
                if (r4 < r5) goto L_0x0cc0
                r4 = 0
                r2 = r0[r4]
                float r2 = java.lang.Float.parseFloat(r2)
                r5 = 1
                r0 = r0[r5]
                float r0 = java.lang.Float.parseFloat(r0)
                float r3 = org.telegram.messenger.AndroidUtilities.density
                float r2 = r2 * r3
                int r2 = (int) r2
                float r0 = r0 * r3
                int r0 = (int) r0
                r3 = r2
                r2 = r0
                goto L_0x0cc2
            L_0x0cc0:
                r4 = 0
                r5 = 1
            L_0x0cc2:
                org.telegram.messenger.ImageLoader$CacheImage r0 = r1.cacheImage     // Catch:{ all -> 0x0cd2 }
                java.io.File r8 = r0.finalFilePath     // Catch:{ all -> 0x0cd2 }
                int r0 = r0.imageType     // Catch:{ all -> 0x0cd2 }
                if (r0 != r6) goto L_0x0ccc
                r9 = 1
                goto L_0x0ccd
            L_0x0ccc:
                r9 = 0
            L_0x0ccd:
                android.graphics.Bitmap r5 = org.telegram.messenger.SvgHelper.getBitmap((java.io.File) r8, (int) r3, (int) r2, (boolean) r9)     // Catch:{ all -> 0x0cd2 }
                goto L_0x0cd7
            L_0x0cd2:
                r0 = move-exception
                org.telegram.messenger.FileLog.e((java.lang.Throwable) r0)
                r5 = r7
            L_0x0cd7:
                if (r5 == 0) goto L_0x0ce0
                android.graphics.drawable.BitmapDrawable r0 = new android.graphics.drawable.BitmapDrawable
                r0.<init>(r5)
                r5 = r0
                goto L_0x0ce1
            L_0x0ce0:
                r5 = r7
            L_0x0ce1:
                r1.onPostExecute(r5)
            L_0x0ce4:
                return
            L_0x0ce5:
                r0 = move-exception
                monitor-exit(r2)     // Catch:{ all -> 0x0ce5 }
                goto L_0x0ce9
            L_0x0ce8:
                throw r0
            L_0x0ce9:
                goto L_0x0ce8
            */
            throw new UnsupportedOperationException("Method not decompiled: org.telegram.messenger.ImageLoader.CacheOutTask.run():void");
        }

        private void loadLastFrame(RLottieDrawable rLottieDrawable, int i, int i2) {
            Bitmap createBitmap = Bitmap.createBitmap(i, i2, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(createBitmap);
            canvas.scale(2.0f, 2.0f, ((float) i) / 2.0f, ((float) i2) / 2.0f);
            AndroidUtilities.runOnUIThread(new ImageLoader$CacheOutTask$$ExternalSyntheticLambda3(this, rLottieDrawable, canvas, createBitmap));
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void lambda$loadLastFrame$1(RLottieDrawable rLottieDrawable, Canvas canvas, Bitmap bitmap) {
            rLottieDrawable.setOnFrameReadyRunnable(new ImageLoader$CacheOutTask$$ExternalSyntheticLambda2(this, rLottieDrawable, canvas, bitmap));
            rLottieDrawable.setCurrentFrame(rLottieDrawable.getFramesCount() - 1, true, true);
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void lambda$loadLastFrame$0(RLottieDrawable rLottieDrawable, Canvas canvas, Bitmap bitmap) {
            BitmapDrawable bitmapDrawable = null;
            rLottieDrawable.setOnFrameReadyRunnable((Runnable) null);
            if (!(rLottieDrawable.getBackgroundBitmap() == null && rLottieDrawable.getRenderingBitmap() == null)) {
                canvas.drawBitmap(rLottieDrawable.getBackgroundBitmap() != null ? rLottieDrawable.getBackgroundBitmap() : rLottieDrawable.getRenderingBitmap(), 0.0f, 0.0f, (Paint) null);
                bitmapDrawable = new BitmapDrawable(bitmap);
            }
            onPostExecute(bitmapDrawable);
            rLottieDrawable.recycle();
        }

        private void onPostExecute(Drawable drawable) {
            AndroidUtilities.runOnUIThread(new ImageLoader$CacheOutTask$$ExternalSyntheticLambda0(this, drawable));
        }

        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v1, resolved type: java.lang.String} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v2, resolved type: android.graphics.drawable.BitmapDrawable} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v0, resolved type: android.graphics.drawable.BitmapDrawable} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v3, resolved type: java.lang.String} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v4, resolved type: java.lang.String} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v6, resolved type: android.graphics.drawable.BitmapDrawable} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v8, resolved type: android.graphics.drawable.BitmapDrawable} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v11, resolved type: android.graphics.drawable.Drawable} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v14, resolved type: org.telegram.ui.Components.AnimatedFileDrawable} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v19, resolved type: android.graphics.drawable.BitmapDrawable} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v20, resolved type: android.graphics.drawable.BitmapDrawable} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v21, resolved type: android.graphics.drawable.BitmapDrawable} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v17, resolved type: android.graphics.drawable.BitmapDrawable} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v23, resolved type: android.graphics.drawable.Drawable} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v24, resolved type: org.telegram.ui.Components.RLottieDrawable} */
        /* JADX WARNING: type inference failed for: r1v6, types: [java.lang.String] */
        /* JADX WARNING: type inference failed for: r1v11, types: [java.lang.String] */
        /* JADX WARNING: type inference failed for: r1v16, types: [java.lang.String] */
        /* access modifiers changed from: private */
        /* JADX WARNING: Failed to insert additional move for type inference */
        /* JADX WARNING: Multi-variable type inference failed */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public /* synthetic */ void lambda$onPostExecute$3(android.graphics.drawable.Drawable r7) {
            /*
                r6 = this;
                boolean r0 = r7 instanceof org.telegram.ui.Components.RLottieDrawable
                r1 = 0
                if (r0 == 0) goto L_0x003b
                org.telegram.ui.Components.RLottieDrawable r7 = (org.telegram.ui.Components.RLottieDrawable) r7
                org.telegram.messenger.ImageLoader r0 = org.telegram.messenger.ImageLoader.this
                org.telegram.messenger.LruCache r0 = r0.lottieMemCache
                org.telegram.messenger.ImageLoader$CacheImage r2 = r6.cacheImage
                java.lang.String r2 = r2.key
                java.lang.Object r0 = r0.get(r2)
                android.graphics.drawable.Drawable r0 = (android.graphics.drawable.Drawable) r0
                if (r0 != 0) goto L_0x0027
                org.telegram.messenger.ImageLoader r0 = org.telegram.messenger.ImageLoader.this
                org.telegram.messenger.LruCache r0 = r0.lottieMemCache
                org.telegram.messenger.ImageLoader$CacheImage r2 = r6.cacheImage
                java.lang.String r2 = r2.key
                r0.put(r2, r7)
                goto L_0x002b
            L_0x0027:
                r7.recycle()
                r7 = r0
            L_0x002b:
                if (r7 == 0) goto L_0x0071
                org.telegram.messenger.ImageLoader r0 = org.telegram.messenger.ImageLoader.this
                org.telegram.messenger.ImageLoader$CacheImage r1 = r6.cacheImage
                java.lang.String r1 = r1.key
                r0.incrementUseCount(r1)
                org.telegram.messenger.ImageLoader$CacheImage r0 = r6.cacheImage
                java.lang.String r1 = r0.key
                goto L_0x0071
            L_0x003b:
                boolean r0 = r7 instanceof org.telegram.ui.Components.AnimatedFileDrawable
                if (r0 == 0) goto L_0x0076
                r0 = r7
                org.telegram.ui.Components.AnimatedFileDrawable r0 = (org.telegram.ui.Components.AnimatedFileDrawable) r0
                boolean r2 = r0.isWebmSticker
                if (r2 == 0) goto L_0x0071
                org.telegram.messenger.ImageLoader r7 = org.telegram.messenger.ImageLoader.this
                org.telegram.messenger.ImageLoader$CacheImage r1 = r6.cacheImage
                java.lang.String r1 = r1.key
                android.graphics.drawable.BitmapDrawable r7 = r7.getFromLottieCache(r1)
                if (r7 != 0) goto L_0x0061
                org.telegram.messenger.ImageLoader r7 = org.telegram.messenger.ImageLoader.this
                org.telegram.messenger.LruCache r7 = r7.lottieMemCache
                org.telegram.messenger.ImageLoader$CacheImage r1 = r6.cacheImage
                java.lang.String r1 = r1.key
                r7.put(r1, r0)
                r7 = r0
                goto L_0x0064
            L_0x0061:
                r0.recycle()
            L_0x0064:
                org.telegram.messenger.ImageLoader r0 = org.telegram.messenger.ImageLoader.this
                org.telegram.messenger.ImageLoader$CacheImage r1 = r6.cacheImage
                java.lang.String r1 = r1.key
                r0.incrementUseCount(r1)
                org.telegram.messenger.ImageLoader$CacheImage r0 = r6.cacheImage
                java.lang.String r1 = r0.key
            L_0x0071:
                r5 = r1
                r1 = r7
                r7 = r5
                goto L_0x010d
            L_0x0076:
                boolean r0 = r7 instanceof android.graphics.drawable.BitmapDrawable
                if (r0 == 0) goto L_0x010c
                android.graphics.drawable.BitmapDrawable r7 = (android.graphics.drawable.BitmapDrawable) r7
                org.telegram.messenger.ImageLoader r0 = org.telegram.messenger.ImageLoader.this
                org.telegram.messenger.ImageLoader$CacheImage r2 = r6.cacheImage
                java.lang.String r2 = r2.key
                android.graphics.drawable.BitmapDrawable r0 = r0.getFromMemCache(r2)
                r2 = 1
                if (r0 != 0) goto L_0x00f1
                org.telegram.messenger.ImageLoader$CacheImage r0 = r6.cacheImage
                java.lang.String r0 = r0.key
                java.lang.String r3 = "_f"
                boolean r0 = r0.endsWith(r3)
                if (r0 == 0) goto L_0x00a5
                org.telegram.messenger.ImageLoader r0 = org.telegram.messenger.ImageLoader.this
                org.telegram.messenger.LruCache r0 = r0.wallpaperMemCache
                org.telegram.messenger.ImageLoader$CacheImage r2 = r6.cacheImage
                java.lang.String r2 = r2.key
                r0.put(r2, r7)
                r0 = 0
                r2 = 0
                goto L_0x00f9
            L_0x00a5:
                org.telegram.messenger.ImageLoader$CacheImage r0 = r6.cacheImage
                java.lang.String r0 = r0.key
                java.lang.String r3 = "_isc"
                boolean r0 = r0.endsWith(r3)
                if (r0 != 0) goto L_0x00e3
                android.graphics.Bitmap r0 = r7.getBitmap()
                int r0 = r0.getWidth()
                float r0 = (float) r0
                float r3 = org.telegram.messenger.AndroidUtilities.density
                r4 = 1117782016(0x42a00000, float:80.0)
                float r3 = r3 * r4
                int r0 = (r0 > r3 ? 1 : (r0 == r3 ? 0 : -1))
                if (r0 > 0) goto L_0x00e3
                android.graphics.Bitmap r0 = r7.getBitmap()
                int r0 = r0.getHeight()
                float r0 = (float) r0
                float r3 = org.telegram.messenger.AndroidUtilities.density
                float r3 = r3 * r4
                int r0 = (r0 > r3 ? 1 : (r0 == r3 ? 0 : -1))
                if (r0 > 0) goto L_0x00e3
                org.telegram.messenger.ImageLoader r0 = org.telegram.messenger.ImageLoader.this
                org.telegram.messenger.LruCache r0 = r0.smallImagesMemCache
                org.telegram.messenger.ImageLoader$CacheImage r3 = r6.cacheImage
                java.lang.String r3 = r3.key
                r0.put(r3, r7)
                goto L_0x00f9
            L_0x00e3:
                org.telegram.messenger.ImageLoader r0 = org.telegram.messenger.ImageLoader.this
                org.telegram.messenger.LruCache r0 = r0.memCache
                org.telegram.messenger.ImageLoader$CacheImage r3 = r6.cacheImage
                java.lang.String r3 = r3.key
                r0.put(r3, r7)
                goto L_0x00f9
            L_0x00f1:
                android.graphics.Bitmap r7 = r7.getBitmap()
                r7.recycle()
                r7 = r0
            L_0x00f9:
                if (r7 == 0) goto L_0x0071
                if (r2 == 0) goto L_0x0071
                org.telegram.messenger.ImageLoader r0 = org.telegram.messenger.ImageLoader.this
                org.telegram.messenger.ImageLoader$CacheImage r1 = r6.cacheImage
                java.lang.String r1 = r1.key
                r0.incrementUseCount(r1)
                org.telegram.messenger.ImageLoader$CacheImage r0 = r6.cacheImage
                java.lang.String r1 = r0.key
                goto L_0x0071
            L_0x010c:
                r7 = r1
            L_0x010d:
                org.telegram.messenger.ImageLoader r0 = org.telegram.messenger.ImageLoader.this
                org.telegram.messenger.DispatchQueue r0 = r0.imageLoadQueue
                org.telegram.messenger.ImageLoader$CacheOutTask$$ExternalSyntheticLambda1 r2 = new org.telegram.messenger.ImageLoader$CacheOutTask$$ExternalSyntheticLambda1
                r2.<init>(r6, r1, r7)
                r0.postRunnable(r2)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: org.telegram.messenger.ImageLoader.CacheOutTask.lambda$onPostExecute$3(android.graphics.drawable.Drawable):void");
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void lambda$onPostExecute$2(Drawable drawable, String str) {
            this.cacheImage.setImageAndClear(drawable, str);
        }

        /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
        /* JADX WARNING: Missing exception handler attribute for start block: B:8:0x0010 */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void cancel() {
            /*
                r2 = this;
                java.lang.Object r0 = r2.sync
                monitor-enter(r0)
                r1 = 1
                r2.isCancelled = r1     // Catch:{ Exception -> 0x0010 }
                java.lang.Thread r1 = r2.runningThread     // Catch:{ Exception -> 0x0010 }
                if (r1 == 0) goto L_0x0010
                r1.interrupt()     // Catch:{ Exception -> 0x0010 }
                goto L_0x0010
            L_0x000e:
                r1 = move-exception
                goto L_0x0012
            L_0x0010:
                monitor-exit(r0)     // Catch:{ all -> 0x000e }
                return
            L_0x0012:
                monitor-exit(r0)     // Catch:{ all -> 0x000e }
                throw r1
            */
            throw new UnsupportedOperationException("Method not decompiled: org.telegram.messenger.ImageLoader.CacheOutTask.cancel():void");
        }
    }

    /* access modifiers changed from: private */
    public boolean isAnimatedAvatar(String str) {
        return str != null && str.endsWith("avatar");
    }

    /* access modifiers changed from: private */
    public BitmapDrawable getFromMemCache(String str) {
        BitmapDrawable bitmapDrawable = this.memCache.get(str);
        if (bitmapDrawable == null) {
            bitmapDrawable = this.smallImagesMemCache.get(str);
        }
        if (bitmapDrawable == null) {
            bitmapDrawable = this.wallpaperMemCache.get(str);
        }
        return bitmapDrawable == null ? getFromLottieCache(str) : bitmapDrawable;
    }

    public static Bitmap getStrippedPhotoBitmap(byte[] bArr, String str) {
        int length = (bArr.length - 3) + Bitmaps.header.length + Bitmaps.footer.length;
        byte[] bArr2 = bytesLocal.get();
        if (bArr2 == null || bArr2.length < length) {
            bArr2 = null;
        }
        if (bArr2 == null) {
            bArr2 = new byte[length];
            bytesLocal.set(bArr2);
        }
        byte[] bArr3 = Bitmaps.header;
        System.arraycopy(bArr3, 0, bArr2, 0, bArr3.length);
        System.arraycopy(bArr, 3, bArr2, Bitmaps.header.length, bArr.length - 3);
        System.arraycopy(Bitmaps.footer, 0, bArr2, (Bitmaps.header.length + bArr.length) - 3, Bitmaps.footer.length);
        bArr2[164] = bArr[1];
        bArr2[166] = bArr[2];
        Bitmap decodeByteArray = BitmapFactory.decodeByteArray(bArr2, 0, length);
        if (decodeByteArray != null && !TextUtils.isEmpty(str) && str.contains("b")) {
            Utilities.blurBitmap(decodeByteArray, 3, 1, decodeByteArray.getWidth(), decodeByteArray.getHeight(), decodeByteArray.getRowBytes());
        }
        return decodeByteArray;
    }

    private class CacheImage {
        protected ArtworkLoadTask artworkTask;
        protected CacheOutTask cacheTask;
        protected int currentAccount;
        protected File encryptionKeyPath;
        protected String ext;
        protected String filter;
        protected ArrayList<String> filters;
        protected File finalFilePath;
        protected HttpImageTask httpTask;
        protected ImageLocation imageLocation;
        protected ArrayList<ImageReceiver> imageReceiverArray;
        protected ArrayList<Integer> imageReceiverGuidsArray;
        protected int imageType;
        protected String key;
        protected ArrayList<String> keys;
        protected Object parentObject;
        protected SecureDocument secureDocument;
        protected long size;
        protected File tempFilePath;
        protected int type;
        protected ArrayList<Integer> types;
        protected String url;

        private CacheImage() {
            this.imageReceiverArray = new ArrayList<>();
            this.imageReceiverGuidsArray = new ArrayList<>();
            this.keys = new ArrayList<>();
            this.filters = new ArrayList<>();
            this.types = new ArrayList<>();
        }

        public void addImageReceiver(ImageReceiver imageReceiver, String str, String str2, int i, int i2) {
            int indexOf = this.imageReceiverArray.indexOf(imageReceiver);
            if (indexOf >= 0) {
                this.imageReceiverGuidsArray.set(indexOf, Integer.valueOf(i2));
                return;
            }
            this.imageReceiverArray.add(imageReceiver);
            this.imageReceiverGuidsArray.add(Integer.valueOf(i2));
            this.keys.add(str);
            this.filters.add(str2);
            this.types.add(Integer.valueOf(i));
            ImageLoader.this.imageLoadingByTag.put(imageReceiver.getTag(i), this);
        }

        public void replaceImageReceiver(ImageReceiver imageReceiver, String str, String str2, int i, int i2) {
            int indexOf = this.imageReceiverArray.indexOf(imageReceiver);
            if (indexOf != -1) {
                if (this.types.get(indexOf).intValue() != i) {
                    ArrayList<ImageReceiver> arrayList = this.imageReceiverArray;
                    indexOf = arrayList.subList(indexOf + 1, arrayList.size()).indexOf(imageReceiver);
                    if (indexOf == -1) {
                        return;
                    }
                }
                this.imageReceiverGuidsArray.set(indexOf, Integer.valueOf(i2));
                this.keys.set(indexOf, str);
                this.filters.set(indexOf, str2);
            }
        }

        public void setImageReceiverGuid(ImageReceiver imageReceiver, int i) {
            int indexOf = this.imageReceiverArray.indexOf(imageReceiver);
            if (indexOf != -1) {
                this.imageReceiverGuidsArray.set(indexOf, Integer.valueOf(i));
            }
        }

        public void removeImageReceiver(ImageReceiver imageReceiver) {
            int i = this.type;
            int i2 = 0;
            while (i2 < this.imageReceiverArray.size()) {
                ImageReceiver imageReceiver2 = this.imageReceiverArray.get(i2);
                if (imageReceiver2 == null || imageReceiver2 == imageReceiver) {
                    this.imageReceiverArray.remove(i2);
                    this.imageReceiverGuidsArray.remove(i2);
                    this.keys.remove(i2);
                    this.filters.remove(i2);
                    i = this.types.remove(i2).intValue();
                    if (imageReceiver2 != null) {
                        ImageLoader.this.imageLoadingByTag.remove(imageReceiver2.getTag(i));
                    }
                    i2--;
                }
                i2++;
            }
            if (this.imageReceiverArray.isEmpty()) {
                if (this.imageLocation != null && !ImageLoader.this.forceLoadingImages.containsKey(this.key)) {
                    ImageLocation imageLocation2 = this.imageLocation;
                    if (imageLocation2.location != null) {
                        FileLoader.getInstance(this.currentAccount).cancelLoadFile((TLRPC$FileLocation) this.imageLocation.location, this.ext);
                    } else if (imageLocation2.document != null) {
                        FileLoader.getInstance(this.currentAccount).cancelLoadFile(this.imageLocation.document);
                    } else if (imageLocation2.secureDocument != null) {
                        FileLoader.getInstance(this.currentAccount).cancelLoadFile(this.imageLocation.secureDocument);
                    } else if (imageLocation2.webFile != null) {
                        FileLoader.getInstance(this.currentAccount).cancelLoadFile(this.imageLocation.webFile);
                    }
                }
                if (this.cacheTask != null) {
                    if (i == 1) {
                        ImageLoader.this.cacheThumbOutQueue.cancelRunnable(this.cacheTask);
                    } else {
                        ImageLoader.this.cacheOutQueue.cancelRunnable(this.cacheTask);
                    }
                    this.cacheTask.cancel();
                    this.cacheTask = null;
                }
                if (this.httpTask != null) {
                    ImageLoader.this.httpTasks.remove(this.httpTask);
                    this.httpTask.cancel(true);
                    this.httpTask = null;
                }
                if (this.artworkTask != null) {
                    ImageLoader.this.artworkTasks.remove(this.artworkTask);
                    this.artworkTask.cancel(true);
                    this.artworkTask = null;
                }
                if (this.url != null) {
                    ImageLoader.this.imageLoadingByUrl.remove(this.url);
                }
                if (this.key != null) {
                    ImageLoader.this.imageLoadingByKeys.remove(this.key);
                }
            }
        }

        public void setImageAndClear(Drawable drawable, String str) {
            if (drawable != null) {
                AndroidUtilities.runOnUIThread(new ImageLoader$CacheImage$$ExternalSyntheticLambda0(this, drawable, new ArrayList(this.imageReceiverArray), new ArrayList(this.imageReceiverGuidsArray), str));
            }
            for (int i = 0; i < this.imageReceiverArray.size(); i++) {
                ImageLoader.this.imageLoadingByTag.remove(this.imageReceiverArray.get(i).getTag(this.type));
            }
            this.imageReceiverArray.clear();
            this.imageReceiverGuidsArray.clear();
            if (this.url != null) {
                ImageLoader.this.imageLoadingByUrl.remove(this.url);
            }
            if (this.key != null) {
                ImageLoader.this.imageLoadingByKeys.remove(this.key);
            }
        }

        /* access modifiers changed from: private */
        /* JADX WARNING: Removed duplicated region for block: B:24:0x0079  */
        /* JADX WARNING: Removed duplicated region for block: B:33:? A[RETURN, SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public /* synthetic */ void lambda$setImageAndClear$0(android.graphics.drawable.Drawable r10, java.util.ArrayList r11, java.util.ArrayList r12, java.lang.String r13) {
            /*
                r9 = this;
                boolean r0 = r10 instanceof org.telegram.ui.Components.AnimatedFileDrawable
                r1 = 0
                if (r0 == 0) goto L_0x004a
                r0 = r10
                org.telegram.ui.Components.AnimatedFileDrawable r0 = (org.telegram.ui.Components.AnimatedFileDrawable) r0
                boolean r2 = r0.isWebmSticker
                if (r2 != 0) goto L_0x004a
                r10 = 0
            L_0x000d:
                int r2 = r11.size()
                if (r1 >= r2) goto L_0x0044
                java.lang.Object r2 = r11.get(r1)
                r3 = r2
                org.telegram.messenger.ImageReceiver r3 = (org.telegram.messenger.ImageReceiver) r3
                if (r1 != 0) goto L_0x001e
                r2 = r0
                goto L_0x0022
            L_0x001e:
                org.telegram.ui.Components.AnimatedFileDrawable r2 = r0.makeCopy()
            L_0x0022:
                java.lang.String r5 = r9.key
                int r6 = r9.type
                r7 = 0
                java.lang.Object r4 = r12.get(r1)
                java.lang.Integer r4 = (java.lang.Integer) r4
                int r8 = r4.intValue()
                r4 = r2
                boolean r3 = r3.setImageBitmapByKey(r4, r5, r6, r7, r8)
                if (r3 == 0) goto L_0x003c
                if (r2 != r0) goto L_0x0041
                r10 = 1
                goto L_0x0041
            L_0x003c:
                if (r2 == r0) goto L_0x0041
                r2.recycle()
            L_0x0041:
                int r1 = r1 + 1
                goto L_0x000d
            L_0x0044:
                if (r10 != 0) goto L_0x0077
                r0.recycle()
                goto L_0x0077
            L_0x004a:
                int r0 = r11.size()
                if (r1 >= r0) goto L_0x0077
                java.lang.Object r0 = r11.get(r1)
                r2 = r0
                org.telegram.messenger.ImageReceiver r2 = (org.telegram.messenger.ImageReceiver) r2
                java.lang.String r4 = r9.key
                java.util.ArrayList<java.lang.Integer> r0 = r9.types
                java.lang.Object r0 = r0.get(r1)
                java.lang.Integer r0 = (java.lang.Integer) r0
                int r5 = r0.intValue()
                r6 = 0
                java.lang.Object r0 = r12.get(r1)
                java.lang.Integer r0 = (java.lang.Integer) r0
                int r7 = r0.intValue()
                r3 = r10
                r2.setImageBitmapByKey(r3, r4, r5, r6, r7)
                int r1 = r1 + 1
                goto L_0x004a
            L_0x0077:
                if (r13 == 0) goto L_0x007e
                org.telegram.messenger.ImageLoader r10 = org.telegram.messenger.ImageLoader.this
                r10.decrementUseCount(r13)
            L_0x007e:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: org.telegram.messenger.ImageLoader.CacheImage.lambda$setImageAndClear$0(android.graphics.drawable.Drawable, java.util.ArrayList, java.util.ArrayList, java.lang.String):void");
        }
    }

    public static ImageLoader getInstance() {
        ImageLoader imageLoader = Instance;
        if (imageLoader == null) {
            synchronized (ImageLoader.class) {
                imageLoader = Instance;
                if (imageLoader == null) {
                    imageLoader = new ImageLoader();
                    Instance = imageLoader;
                }
            }
        }
        return imageLoader;
    }

    public ImageLoader() {
        this.currentHttpTasksCount = 0;
        this.currentArtworkTasksCount = 0;
        this.testWebFile = new ConcurrentHashMap<>();
        this.httpFileLoadTasks = new LinkedList<>();
        this.httpFileLoadTasksByKeys = new HashMap<>();
        this.retryHttpsTasks = new HashMap<>();
        this.currentHttpFileLoadTasksCount = 0;
        this.ignoreRemoval = null;
        this.lastCacheOutTime = 0;
        this.lastImageNum = 0;
        this.telegramPath = null;
        boolean z = true;
        this.thumbGeneratingQueue.setPriority(1);
        int memoryClass = ((ActivityManager) ApplicationLoader.applicationContext.getSystemService("activity")).getMemoryClass();
        z = memoryClass < 192 ? false : z;
        this.canForce8888 = z;
        int min = Math.min(z ? 30 : 15, memoryClass / 7) * 1024 * 1024;
        float f = (float) min;
        this.memCache = new LruCache<BitmapDrawable>((int) (0.8f * f)) {
            /* access modifiers changed from: protected */
            public int sizeOf(String str, BitmapDrawable bitmapDrawable) {
                return bitmapDrawable.getBitmap().getByteCount();
            }

            /* access modifiers changed from: protected */
            public void entryRemoved(boolean z, String str, BitmapDrawable bitmapDrawable, BitmapDrawable bitmapDrawable2) {
                if (ImageLoader.this.ignoreRemoval == null || !ImageLoader.this.ignoreRemoval.equals(str)) {
                    Integer num = (Integer) ImageLoader.this.bitmapUseCounts.get(str);
                    if (num == null || num.intValue() == 0) {
                        Bitmap bitmap = bitmapDrawable.getBitmap();
                        if (!bitmap.isRecycled()) {
                            ArrayList arrayList = new ArrayList();
                            arrayList.add(bitmap);
                            AndroidUtilities.recycleBitmaps(arrayList);
                        }
                    }
                }
            }
        };
        this.smallImagesMemCache = new LruCache<BitmapDrawable>((int) (f * 0.2f)) {
            /* access modifiers changed from: protected */
            public int sizeOf(String str, BitmapDrawable bitmapDrawable) {
                return bitmapDrawable.getBitmap().getByteCount();
            }

            /* access modifiers changed from: protected */
            public void entryRemoved(boolean z, String str, BitmapDrawable bitmapDrawable, BitmapDrawable bitmapDrawable2) {
                if (ImageLoader.this.ignoreRemoval == null || !ImageLoader.this.ignoreRemoval.equals(str)) {
                    Integer num = (Integer) ImageLoader.this.bitmapUseCounts.get(str);
                    if (num == null || num.intValue() == 0) {
                        Bitmap bitmap = bitmapDrawable.getBitmap();
                        if (!bitmap.isRecycled()) {
                            ArrayList arrayList = new ArrayList();
                            arrayList.add(bitmap);
                            AndroidUtilities.recycleBitmaps(arrayList);
                        }
                    }
                }
            }
        };
        this.wallpaperMemCache = new LruCache<BitmapDrawable>(min / 4) {
            /* access modifiers changed from: protected */
            public int sizeOf(String str, BitmapDrawable bitmapDrawable) {
                return bitmapDrawable.getBitmap().getByteCount();
            }
        };
        this.lottieMemCache = new LruCache<BitmapDrawable>(10485760) {
            /* access modifiers changed from: protected */
            public int sizeOf(String str, BitmapDrawable bitmapDrawable) {
                return bitmapDrawable.getIntrinsicWidth() * bitmapDrawable.getIntrinsicHeight() * 4 * 2;
            }

            public BitmapDrawable put(String str, BitmapDrawable bitmapDrawable) {
                if (bitmapDrawable instanceof AnimatedFileDrawable) {
                    ImageLoader.this.cachedAnimatedFileDrawables.add((AnimatedFileDrawable) bitmapDrawable);
                }
                return (BitmapDrawable) super.put(str, bitmapDrawable);
            }

            /* access modifiers changed from: protected */
            public void entryRemoved(boolean z, String str, BitmapDrawable bitmapDrawable, BitmapDrawable bitmapDrawable2) {
                Integer num = (Integer) ImageLoader.this.bitmapUseCounts.get(str);
                boolean z2 = bitmapDrawable instanceof AnimatedFileDrawable;
                if (z2) {
                    ImageLoader.this.cachedAnimatedFileDrawables.remove((AnimatedFileDrawable) bitmapDrawable);
                }
                if (num == null || num.intValue() == 0) {
                    if (z2) {
                        ((AnimatedFileDrawable) bitmapDrawable).recycle();
                    }
                    if (bitmapDrawable instanceof RLottieDrawable) {
                        ((RLottieDrawable) bitmapDrawable).recycle();
                    }
                }
            }
        };
        SparseArray sparseArray = new SparseArray();
        File cacheDir = AndroidUtilities.getCacheDir();
        if (!cacheDir.isDirectory()) {
            try {
                cacheDir.mkdirs();
            } catch (Exception e) {
                FileLog.e((Throwable) e);
            }
        }
        AndroidUtilities.createEmptyFile(new File(cacheDir, ".nomedia"));
        sparseArray.put(4, cacheDir);
        for (final int i = 0; i < 4; i++) {
            FileLoader.getInstance(i).setDelegate(new FileLoader.FileLoaderDelegate() {
                public void fileUploadProgressChanged(FileUploadOperation fileUploadOperation, String str, long j, long j2, boolean z) {
                    FileUploadOperation fileUploadOperation2 = fileUploadOperation;
                    String str2 = str;
                    ImageLoader.this.fileProgresses.put(str, new long[]{j, j2});
                    long elapsedRealtime = SystemClock.elapsedRealtime();
                    long j3 = fileUploadOperation2.lastProgressUpdateTime;
                    if (j3 == 0 || j3 < elapsedRealtime - 100 || j == j2) {
                        fileUploadOperation2.lastProgressUpdateTime = elapsedRealtime;
                        AndroidUtilities.runOnUIThread(new ImageLoader$5$$ExternalSyntheticLambda1(i, str, j, j2, z));
                    }
                }

                public void fileDidUploaded(String str, TLRPC$InputFile tLRPC$InputFile, TLRPC$InputEncryptedFile tLRPC$InputEncryptedFile, byte[] bArr, byte[] bArr2, long j) {
                    Utilities.stageQueue.postRunnable(new ImageLoader$5$$ExternalSyntheticLambda4(this, i, str, tLRPC$InputFile, tLRPC$InputEncryptedFile, bArr, bArr2, j));
                }

                /* access modifiers changed from: private */
                public /* synthetic */ void lambda$fileDidUploaded$2(int i, String str, TLRPC$InputFile tLRPC$InputFile, TLRPC$InputEncryptedFile tLRPC$InputEncryptedFile, byte[] bArr, byte[] bArr2, long j) {
                    AndroidUtilities.runOnUIThread(new ImageLoader$5$$ExternalSyntheticLambda2(i, str, tLRPC$InputFile, tLRPC$InputEncryptedFile, bArr, bArr2, j));
                    ImageLoader.this.fileProgresses.remove(str);
                }

                public void fileDidFailedUpload(String str, boolean z) {
                    Utilities.stageQueue.postRunnable(new ImageLoader$5$$ExternalSyntheticLambda5(this, i, str, z));
                }

                /* access modifiers changed from: private */
                public /* synthetic */ void lambda$fileDidFailedUpload$4(int i, String str, boolean z) {
                    AndroidUtilities.runOnUIThread(new ImageLoader$5$$ExternalSyntheticLambda3(i, str, z));
                    ImageLoader.this.fileProgresses.remove(str);
                }

                public void fileDidLoaded(String str, File file, Object obj, int i) {
                    ImageLoader.this.fileProgresses.remove(str);
                    AndroidUtilities.runOnUIThread(new ImageLoader$5$$ExternalSyntheticLambda6(this, file, str, obj, i, i));
                }

                /* access modifiers changed from: private */
                public /* synthetic */ void lambda$fileDidLoaded$5(File file, String str, Object obj, int i, int i2) {
                    int i3;
                    if (!(SharedConfig.saveToGalleryFlags == 0 || file == null || ((!str.endsWith(".mp4") && !str.endsWith(".jpg")) || !(obj instanceof MessageObject)))) {
                        long dialogId = ((MessageObject) obj).getDialogId();
                        if (dialogId >= 0) {
                            i3 = 1;
                        } else {
                            i3 = ChatObject.isChannelAndNotMegaGroup(MessagesController.getInstance(i).getChat(Long.valueOf(-dialogId))) ? 4 : 2;
                        }
                        if ((i3 & SharedConfig.saveToGalleryFlags) != 0) {
                            AndroidUtilities.addMediaToGallery(file.toString());
                        }
                    }
                    NotificationCenter.getInstance(i).postNotificationName(NotificationCenter.fileLoaded, str, file);
                    ImageLoader.this.fileDidLoaded(str, file, i2);
                }

                public void fileDidFailedLoad(String str, int i) {
                    ImageLoader.this.fileProgresses.remove(str);
                    AndroidUtilities.runOnUIThread(new ImageLoader$5$$ExternalSyntheticLambda7(this, str, i, i));
                }

                /* access modifiers changed from: private */
                public /* synthetic */ void lambda$fileDidFailedLoad$6(String str, int i, int i2) {
                    ImageLoader.this.fileDidFailedLoad(str, i);
                    NotificationCenter.getInstance(i2).postNotificationName(NotificationCenter.fileLoadFailed, str, Integer.valueOf(i));
                }

                public void fileLoadProgressChanged(FileLoadOperation fileLoadOperation, String str, long j, long j2) {
                    FileLoadOperation fileLoadOperation2 = fileLoadOperation;
                    String str2 = str;
                    ImageLoader.this.fileProgresses.put(str, new long[]{j, j2});
                    long elapsedRealtime = SystemClock.elapsedRealtime();
                    long j3 = fileLoadOperation2.lastProgressUpdateTime;
                    if (j3 == 0 || j3 < elapsedRealtime - 500 || j == 0) {
                        fileLoadOperation2.lastProgressUpdateTime = elapsedRealtime;
                        AndroidUtilities.runOnUIThread(new ImageLoader$5$$ExternalSyntheticLambda0(i, str, j, j2));
                    }
                }
            });
        }
        FileLoader.setMediaDirs(sparseArray);
        AnonymousClass6 r0 = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                if (BuildVars.LOGS_ENABLED) {
                    FileLog.d("file system changed");
                }
                ImageLoader$6$$ExternalSyntheticLambda0 imageLoader$6$$ExternalSyntheticLambda0 = new ImageLoader$6$$ExternalSyntheticLambda0(this);
                if ("android.intent.action.MEDIA_UNMOUNTED".equals(intent.getAction())) {
                    AndroidUtilities.runOnUIThread(imageLoader$6$$ExternalSyntheticLambda0, 1000);
                } else {
                    imageLoader$6$$ExternalSyntheticLambda0.run();
                }
            }

            /* access modifiers changed from: private */
            public /* synthetic */ void lambda$onReceive$0() {
                ImageLoader.this.checkMediaPaths();
            }
        };
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.MEDIA_BAD_REMOVAL");
        intentFilter.addAction("android.intent.action.MEDIA_CHECKING");
        intentFilter.addAction("android.intent.action.MEDIA_EJECT");
        intentFilter.addAction("android.intent.action.MEDIA_MOUNTED");
        intentFilter.addAction("android.intent.action.MEDIA_NOFS");
        intentFilter.addAction("android.intent.action.MEDIA_REMOVED");
        intentFilter.addAction("android.intent.action.MEDIA_SHARED");
        intentFilter.addAction("android.intent.action.MEDIA_UNMOUNTABLE");
        intentFilter.addAction("android.intent.action.MEDIA_UNMOUNTED");
        intentFilter.addDataScheme("file");
        try {
            ApplicationLoader.applicationContext.registerReceiver(r0, intentFilter);
        } catch (Throwable unused) {
        }
        checkMediaPaths();
    }

    public void checkMediaPaths() {
        this.cacheOutQueue.postRunnable(new ImageLoader$$ExternalSyntheticLambda1(this));
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$checkMediaPaths$1() {
        AndroidUtilities.runOnUIThread(new ImageLoader$$ExternalSyntheticLambda0(createMediaPaths()));
    }

    public void addTestWebFile(String str, WebFile webFile) {
        if (str != null && webFile != null) {
            this.testWebFile.put(str, webFile);
        }
    }

    public void removeTestWebFile(String str) {
        if (str != null) {
            this.testWebFile.remove(str);
        }
    }

    /* JADX WARNING: Missing exception handler attribute for start block: B:16:0x0031 */
    @android.annotation.TargetApi(26)
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void moveDirectory(java.io.File r1, java.io.File r2) {
        /*
            boolean r0 = r1.exists()
            if (r0 == 0) goto L_0x0036
            boolean r0 = r2.exists()
            if (r0 != 0) goto L_0x0013
            boolean r0 = r2.mkdir()
            if (r0 != 0) goto L_0x0013
            goto L_0x0036
        L_0x0013:
            java.nio.file.Path r1 = r1.toPath()     // Catch:{ Exception -> 0x0032 }
            java.util.stream.Stream r1 = java.nio.file.Files.list(r1)     // Catch:{ Exception -> 0x0032 }
            j$.util.stream.Stream r1 = j$.wrappers.C$r8$wrapper$java$util$stream$Stream$VWRP.convert(r1)     // Catch:{ Exception -> 0x0032 }
            org.telegram.messenger.ImageLoader$$ExternalSyntheticLambda13 r0 = new org.telegram.messenger.ImageLoader$$ExternalSyntheticLambda13     // Catch:{ all -> 0x002b }
            r0.<init>(r2)     // Catch:{ all -> 0x002b }
            r1.forEach(r0)     // Catch:{ all -> 0x002b }
            r1.close()     // Catch:{ Exception -> 0x0032 }
            goto L_0x0036
        L_0x002b:
            r2 = move-exception
            if (r1 == 0) goto L_0x0031
            r1.close()     // Catch:{ all -> 0x0031 }
        L_0x0031:
            throw r2     // Catch:{ Exception -> 0x0032 }
        L_0x0032:
            r1 = move-exception
            org.telegram.messenger.FileLog.e((java.lang.Throwable) r1)
        L_0x0036:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.telegram.messenger.ImageLoader.moveDirectory(java.io.File, java.io.File):void");
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void lambda$moveDirectory$2(File file, Path path) {
        File file2 = new File(file, path.getFileName().toString());
        if (Files.isDirectory(path, new LinkOption[0])) {
            moveDirectory(path.toFile(), file2);
            return;
        }
        try {
            Files.move(path, file2.toPath(), new CopyOption[0]);
        } catch (Exception e) {
            FileLog.e((Throwable) e);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:124:0x0264 A[Catch:{ Exception -> 0x0277 }] */
    /* JADX WARNING: Removed duplicated region for block: B:135:0x0298 A[Catch:{ Exception -> 0x02ab }] */
    /* JADX WARNING: Removed duplicated region for block: B:148:0x0103 A[EDGE_INSN: B:148:0x0103->B:56:0x0103 ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x00e3 A[Catch:{ Exception -> 0x02bd }] */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x0114 A[SYNTHETIC, Splitter:B:59:0x0114] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.util.SparseArray<java.io.File> createMediaPaths() {
        /*
            r14 = this;
            android.util.SparseArray r0 = new android.util.SparseArray
            r0.<init>()
            java.io.File r1 = org.telegram.messenger.AndroidUtilities.getCacheDir()
            boolean r2 = r1.isDirectory()
            if (r2 != 0) goto L_0x0017
            r1.mkdirs()     // Catch:{ Exception -> 0x0013 }
            goto L_0x0017
        L_0x0013:
            r2 = move-exception
            org.telegram.messenger.FileLog.e((java.lang.Throwable) r2)
        L_0x0017:
            java.io.File r2 = new java.io.File
            java.lang.String r3 = ".nomedia"
            r2.<init>(r1, r3)
            org.telegram.messenger.AndroidUtilities.createEmptyFile(r2)
            r2 = 4
            r0.put(r2, r1)
            boolean r2 = org.telegram.messenger.BuildVars.LOGS_ENABLED
            if (r2 == 0) goto L_0x003d
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r4 = "cache path = "
            r2.append(r4)
            r2.append(r1)
            java.lang.String r2 = r2.toString()
            org.telegram.messenger.FileLog.d(r2)
        L_0x003d:
            java.lang.String r2 = "mounted"
            java.lang.String r4 = android.os.Environment.getExternalStorageState()     // Catch:{ Exception -> 0x02bd }
            boolean r2 = r2.equals(r4)     // Catch:{ Exception -> 0x02bd }
            if (r2 == 0) goto L_0x02b0
            java.io.File r2 = android.os.Environment.getExternalStorageDirectory()     // Catch:{ Exception -> 0x02bd }
            int r4 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x02bd }
            r5 = 19
            r6 = 0
            if (r4 < r5) goto L_0x0080
            java.lang.String r4 = org.telegram.messenger.SharedConfig.storageCacheDir     // Catch:{ Exception -> 0x02bd }
            boolean r4 = android.text.TextUtils.isEmpty(r4)     // Catch:{ Exception -> 0x02bd }
            if (r4 != 0) goto L_0x0080
            java.util.ArrayList r4 = org.telegram.messenger.AndroidUtilities.getRootDirs()     // Catch:{ Exception -> 0x02bd }
            if (r4 == 0) goto L_0x0080
            int r7 = r4.size()     // Catch:{ Exception -> 0x02bd }
            r8 = 0
        L_0x0067:
            if (r8 >= r7) goto L_0x0080
            java.lang.Object r9 = r4.get(r8)     // Catch:{ Exception -> 0x02bd }
            java.io.File r9 = (java.io.File) r9     // Catch:{ Exception -> 0x02bd }
            java.lang.String r10 = r9.getAbsolutePath()     // Catch:{ Exception -> 0x02bd }
            java.lang.String r11 = org.telegram.messenger.SharedConfig.storageCacheDir     // Catch:{ Exception -> 0x02bd }
            boolean r10 = r10.startsWith(r11)     // Catch:{ Exception -> 0x02bd }
            if (r10 == 0) goto L_0x007d
            r2 = r9
            goto L_0x0080
        L_0x007d:
            int r8 = r8 + 1
            goto L_0x0067
        L_0x0080:
            int r4 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x02bd }
            r7 = 30
            r8 = 0
            java.lang.String r9 = "Telegram"
            if (r4 < r7) goto L_0x00c0
            android.content.Context r2 = org.telegram.messenger.ApplicationLoader.applicationContext     // Catch:{ Exception -> 0x00ac }
            java.io.File[] r2 = r2.getExternalMediaDirs()     // Catch:{ Exception -> 0x00ac }
            int r2 = r2.length     // Catch:{ Exception -> 0x00ac }
            if (r2 <= 0) goto L_0x00aa
            android.content.Context r2 = org.telegram.messenger.ApplicationLoader.applicationContext     // Catch:{ Exception -> 0x00ac }
            java.io.File[] r2 = r2.getExternalMediaDirs()     // Catch:{ Exception -> 0x00ac }
            r2 = r2[r6]     // Catch:{ Exception -> 0x00ac }
            java.io.File r4 = new java.io.File     // Catch:{ Exception -> 0x00a5 }
            r4.<init>(r2, r9)     // Catch:{ Exception -> 0x00a5 }
            r4.mkdirs()     // Catch:{ Exception -> 0x00a3 }
            goto L_0x00b1
        L_0x00a3:
            r2 = move-exception
            goto L_0x00ae
        L_0x00a5:
            r4 = move-exception
            r13 = r4
            r4 = r2
            r2 = r13
            goto L_0x00ae
        L_0x00aa:
            r4 = r8
            goto L_0x00b1
        L_0x00ac:
            r2 = move-exception
            r4 = r8
        L_0x00ae:
            org.telegram.messenger.FileLog.e((java.lang.Throwable) r2)     // Catch:{ Exception -> 0x02bd }
        L_0x00b1:
            android.content.Context r2 = org.telegram.messenger.ApplicationLoader.applicationContext     // Catch:{ Exception -> 0x02bd }
            java.io.File r2 = r2.getExternalFilesDir(r8)     // Catch:{ Exception -> 0x02bd }
            java.io.File r7 = new java.io.File     // Catch:{ Exception -> 0x02bd }
            r7.<init>(r2, r9)     // Catch:{ Exception -> 0x02bd }
            r14.telegramPath = r7     // Catch:{ Exception -> 0x02bd }
            r8 = r4
            goto L_0x00c7
        L_0x00c0:
            java.io.File r4 = new java.io.File     // Catch:{ Exception -> 0x02bd }
            r4.<init>(r2, r9)     // Catch:{ Exception -> 0x02bd }
            r14.telegramPath = r4     // Catch:{ Exception -> 0x02bd }
        L_0x00c7:
            java.io.File r2 = r14.telegramPath     // Catch:{ Exception -> 0x02bd }
            r2.mkdirs()     // Catch:{ Exception -> 0x02bd }
            int r2 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x02bd }
            if (r2 < r5) goto L_0x0103
            java.io.File r2 = r14.telegramPath     // Catch:{ Exception -> 0x02bd }
            boolean r2 = r2.isDirectory()     // Catch:{ Exception -> 0x02bd }
            if (r2 != 0) goto L_0x0103
            java.util.ArrayList r2 = org.telegram.messenger.AndroidUtilities.getDataDirs()     // Catch:{ Exception -> 0x02bd }
            int r4 = r2.size()     // Catch:{ Exception -> 0x02bd }
            r5 = 0
        L_0x00e1:
            if (r5 >= r4) goto L_0x0103
            java.lang.Object r7 = r2.get(r5)     // Catch:{ Exception -> 0x02bd }
            java.io.File r7 = (java.io.File) r7     // Catch:{ Exception -> 0x02bd }
            java.lang.String r10 = r7.getAbsolutePath()     // Catch:{ Exception -> 0x02bd }
            java.lang.String r11 = org.telegram.messenger.SharedConfig.storageCacheDir     // Catch:{ Exception -> 0x02bd }
            boolean r10 = r10.startsWith(r11)     // Catch:{ Exception -> 0x02bd }
            if (r10 == 0) goto L_0x0100
            java.io.File r2 = new java.io.File     // Catch:{ Exception -> 0x02bd }
            r2.<init>(r7, r9)     // Catch:{ Exception -> 0x02bd }
            r14.telegramPath = r2     // Catch:{ Exception -> 0x02bd }
            r2.mkdirs()     // Catch:{ Exception -> 0x02bd }
            goto L_0x0103
        L_0x0100:
            int r5 = r5 + 1
            goto L_0x00e1
        L_0x0103:
            java.io.File r2 = r14.telegramPath     // Catch:{ Exception -> 0x02bd }
            boolean r2 = r2.isDirectory()     // Catch:{ Exception -> 0x02bd }
            java.lang.String r4 = "video path = "
            java.lang.String r5 = "image path = "
            java.lang.String r7 = "Telegram Video"
            java.lang.String r9 = "Telegram Images"
            r10 = 2
            if (r2 == 0) goto L_0x023f
            java.io.File r2 = new java.io.File     // Catch:{ Exception -> 0x0144 }
            java.io.File r11 = r14.telegramPath     // Catch:{ Exception -> 0x0144 }
            r2.<init>(r11, r9)     // Catch:{ Exception -> 0x0144 }
            r2.mkdir()     // Catch:{ Exception -> 0x0144 }
            boolean r11 = r2.isDirectory()     // Catch:{ Exception -> 0x0144 }
            if (r11 == 0) goto L_0x0148
            boolean r11 = r14.canMoveFiles(r1, r2, r6)     // Catch:{ Exception -> 0x0144 }
            if (r11 == 0) goto L_0x0148
            r0.put(r6, r2)     // Catch:{ Exception -> 0x0144 }
            boolean r11 = org.telegram.messenger.BuildVars.LOGS_ENABLED     // Catch:{ Exception -> 0x0144 }
            if (r11 == 0) goto L_0x0148
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0144 }
            r11.<init>()     // Catch:{ Exception -> 0x0144 }
            r11.append(r5)     // Catch:{ Exception -> 0x0144 }
            r11.append(r2)     // Catch:{ Exception -> 0x0144 }
            java.lang.String r2 = r11.toString()     // Catch:{ Exception -> 0x0144 }
            org.telegram.messenger.FileLog.d(r2)     // Catch:{ Exception -> 0x0144 }
            goto L_0x0148
        L_0x0144:
            r2 = move-exception
            org.telegram.messenger.FileLog.e((java.lang.Throwable) r2)     // Catch:{ Exception -> 0x02bd }
        L_0x0148:
            java.io.File r2 = new java.io.File     // Catch:{ Exception -> 0x0178 }
            java.io.File r11 = r14.telegramPath     // Catch:{ Exception -> 0x0178 }
            r2.<init>(r11, r7)     // Catch:{ Exception -> 0x0178 }
            r2.mkdir()     // Catch:{ Exception -> 0x0178 }
            boolean r11 = r2.isDirectory()     // Catch:{ Exception -> 0x0178 }
            if (r11 == 0) goto L_0x017c
            boolean r11 = r14.canMoveFiles(r1, r2, r10)     // Catch:{ Exception -> 0x0178 }
            if (r11 == 0) goto L_0x017c
            r0.put(r10, r2)     // Catch:{ Exception -> 0x0178 }
            boolean r11 = org.telegram.messenger.BuildVars.LOGS_ENABLED     // Catch:{ Exception -> 0x0178 }
            if (r11 == 0) goto L_0x017c
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0178 }
            r11.<init>()     // Catch:{ Exception -> 0x0178 }
            r11.append(r4)     // Catch:{ Exception -> 0x0178 }
            r11.append(r2)     // Catch:{ Exception -> 0x0178 }
            java.lang.String r2 = r11.toString()     // Catch:{ Exception -> 0x0178 }
            org.telegram.messenger.FileLog.d(r2)     // Catch:{ Exception -> 0x0178 }
            goto L_0x017c
        L_0x0178:
            r2 = move-exception
            org.telegram.messenger.FileLog.e((java.lang.Throwable) r2)     // Catch:{ Exception -> 0x02bd }
        L_0x017c:
            java.io.File r2 = new java.io.File     // Catch:{ Exception -> 0x01b9 }
            java.io.File r11 = r14.telegramPath     // Catch:{ Exception -> 0x01b9 }
            java.lang.String r12 = "Telegram Audio"
            r2.<init>(r11, r12)     // Catch:{ Exception -> 0x01b9 }
            r2.mkdir()     // Catch:{ Exception -> 0x01b9 }
            boolean r11 = r2.isDirectory()     // Catch:{ Exception -> 0x01b9 }
            if (r11 == 0) goto L_0x01bd
            r11 = 1
            boolean r12 = r14.canMoveFiles(r1, r2, r11)     // Catch:{ Exception -> 0x01b9 }
            if (r12 == 0) goto L_0x01bd
            java.io.File r12 = new java.io.File     // Catch:{ Exception -> 0x01b9 }
            r12.<init>(r2, r3)     // Catch:{ Exception -> 0x01b9 }
            org.telegram.messenger.AndroidUtilities.createEmptyFile(r12)     // Catch:{ Exception -> 0x01b9 }
            r0.put(r11, r2)     // Catch:{ Exception -> 0x01b9 }
            boolean r11 = org.telegram.messenger.BuildVars.LOGS_ENABLED     // Catch:{ Exception -> 0x01b9 }
            if (r11 == 0) goto L_0x01bd
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01b9 }
            r11.<init>()     // Catch:{ Exception -> 0x01b9 }
            java.lang.String r12 = "audio path = "
            r11.append(r12)     // Catch:{ Exception -> 0x01b9 }
            r11.append(r2)     // Catch:{ Exception -> 0x01b9 }
            java.lang.String r2 = r11.toString()     // Catch:{ Exception -> 0x01b9 }
            org.telegram.messenger.FileLog.d(r2)     // Catch:{ Exception -> 0x01b9 }
            goto L_0x01bd
        L_0x01b9:
            r2 = move-exception
            org.telegram.messenger.FileLog.e((java.lang.Throwable) r2)     // Catch:{ Exception -> 0x02bd }
        L_0x01bd:
            java.io.File r2 = new java.io.File     // Catch:{ Exception -> 0x01fa }
            java.io.File r11 = r14.telegramPath     // Catch:{ Exception -> 0x01fa }
            java.lang.String r12 = "Telegram Documents"
            r2.<init>(r11, r12)     // Catch:{ Exception -> 0x01fa }
            r2.mkdir()     // Catch:{ Exception -> 0x01fa }
            boolean r11 = r2.isDirectory()     // Catch:{ Exception -> 0x01fa }
            if (r11 == 0) goto L_0x01fe
            r11 = 3
            boolean r12 = r14.canMoveFiles(r1, r2, r11)     // Catch:{ Exception -> 0x01fa }
            if (r12 == 0) goto L_0x01fe
            java.io.File r12 = new java.io.File     // Catch:{ Exception -> 0x01fa }
            r12.<init>(r2, r3)     // Catch:{ Exception -> 0x01fa }
            org.telegram.messenger.AndroidUtilities.createEmptyFile(r12)     // Catch:{ Exception -> 0x01fa }
            r0.put(r11, r2)     // Catch:{ Exception -> 0x01fa }
            boolean r11 = org.telegram.messenger.BuildVars.LOGS_ENABLED     // Catch:{ Exception -> 0x01fa }
            if (r11 == 0) goto L_0x01fe
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01fa }
            r11.<init>()     // Catch:{ Exception -> 0x01fa }
            java.lang.String r12 = "documents path = "
            r11.append(r12)     // Catch:{ Exception -> 0x01fa }
            r11.append(r2)     // Catch:{ Exception -> 0x01fa }
            java.lang.String r2 = r11.toString()     // Catch:{ Exception -> 0x01fa }
            org.telegram.messenger.FileLog.d(r2)     // Catch:{ Exception -> 0x01fa }
            goto L_0x01fe
        L_0x01fa:
            r2 = move-exception
            org.telegram.messenger.FileLog.e((java.lang.Throwable) r2)     // Catch:{ Exception -> 0x02bd }
        L_0x01fe:
            java.io.File r2 = new java.io.File     // Catch:{ Exception -> 0x023b }
            java.io.File r11 = r14.telegramPath     // Catch:{ Exception -> 0x023b }
            java.lang.String r12 = "Telegram Files"
            r2.<init>(r11, r12)     // Catch:{ Exception -> 0x023b }
            r2.mkdir()     // Catch:{ Exception -> 0x023b }
            boolean r11 = r2.isDirectory()     // Catch:{ Exception -> 0x023b }
            if (r11 == 0) goto L_0x023f
            r11 = 5
            boolean r12 = r14.canMoveFiles(r1, r2, r11)     // Catch:{ Exception -> 0x023b }
            if (r12 == 0) goto L_0x023f
            java.io.File r12 = new java.io.File     // Catch:{ Exception -> 0x023b }
            r12.<init>(r2, r3)     // Catch:{ Exception -> 0x023b }
            org.telegram.messenger.AndroidUtilities.createEmptyFile(r12)     // Catch:{ Exception -> 0x023b }
            r0.put(r11, r2)     // Catch:{ Exception -> 0x023b }
            boolean r3 = org.telegram.messenger.BuildVars.LOGS_ENABLED     // Catch:{ Exception -> 0x023b }
            if (r3 == 0) goto L_0x023f
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x023b }
            r3.<init>()     // Catch:{ Exception -> 0x023b }
            java.lang.String r11 = "files path = "
            r3.append(r11)     // Catch:{ Exception -> 0x023b }
            r3.append(r2)     // Catch:{ Exception -> 0x023b }
            java.lang.String r2 = r3.toString()     // Catch:{ Exception -> 0x023b }
            org.telegram.messenger.FileLog.d(r2)     // Catch:{ Exception -> 0x023b }
            goto L_0x023f
        L_0x023b:
            r2 = move-exception
            org.telegram.messenger.FileLog.e((java.lang.Throwable) r2)     // Catch:{ Exception -> 0x02bd }
        L_0x023f:
            if (r8 == 0) goto L_0x02b9
            boolean r2 = r8.isDirectory()     // Catch:{ Exception -> 0x02bd }
            if (r2 == 0) goto L_0x02b9
            java.io.File r2 = new java.io.File     // Catch:{ Exception -> 0x0277 }
            r2.<init>(r8, r9)     // Catch:{ Exception -> 0x0277 }
            r2.mkdir()     // Catch:{ Exception -> 0x0277 }
            boolean r3 = r2.isDirectory()     // Catch:{ Exception -> 0x0277 }
            if (r3 == 0) goto L_0x027b
            boolean r3 = r14.canMoveFiles(r1, r2, r6)     // Catch:{ Exception -> 0x0277 }
            if (r3 == 0) goto L_0x027b
            r3 = 100
            r0.put(r3, r2)     // Catch:{ Exception -> 0x0277 }
            boolean r3 = org.telegram.messenger.BuildVars.LOGS_ENABLED     // Catch:{ Exception -> 0x0277 }
            if (r3 == 0) goto L_0x027b
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0277 }
            r3.<init>()     // Catch:{ Exception -> 0x0277 }
            r3.append(r5)     // Catch:{ Exception -> 0x0277 }
            r3.append(r2)     // Catch:{ Exception -> 0x0277 }
            java.lang.String r2 = r3.toString()     // Catch:{ Exception -> 0x0277 }
            org.telegram.messenger.FileLog.d(r2)     // Catch:{ Exception -> 0x0277 }
            goto L_0x027b
        L_0x0277:
            r2 = move-exception
            org.telegram.messenger.FileLog.e((java.lang.Throwable) r2)     // Catch:{ Exception -> 0x02bd }
        L_0x027b:
            java.io.File r2 = new java.io.File     // Catch:{ Exception -> 0x02ab }
            r2.<init>(r8, r7)     // Catch:{ Exception -> 0x02ab }
            r2.mkdir()     // Catch:{ Exception -> 0x02ab }
            boolean r3 = r2.isDirectory()     // Catch:{ Exception -> 0x02ab }
            if (r3 == 0) goto L_0x02b9
            boolean r1 = r14.canMoveFiles(r1, r2, r10)     // Catch:{ Exception -> 0x02ab }
            if (r1 == 0) goto L_0x02b9
            r1 = 101(0x65, float:1.42E-43)
            r0.put(r1, r2)     // Catch:{ Exception -> 0x02ab }
            boolean r1 = org.telegram.messenger.BuildVars.LOGS_ENABLED     // Catch:{ Exception -> 0x02ab }
            if (r1 == 0) goto L_0x02b9
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02ab }
            r1.<init>()     // Catch:{ Exception -> 0x02ab }
            r1.append(r4)     // Catch:{ Exception -> 0x02ab }
            r1.append(r2)     // Catch:{ Exception -> 0x02ab }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x02ab }
            org.telegram.messenger.FileLog.d(r1)     // Catch:{ Exception -> 0x02ab }
            goto L_0x02b9
        L_0x02ab:
            r1 = move-exception
            org.telegram.messenger.FileLog.e((java.lang.Throwable) r1)     // Catch:{ Exception -> 0x02bd }
            goto L_0x02b9
        L_0x02b0:
            boolean r1 = org.telegram.messenger.BuildVars.LOGS_ENABLED     // Catch:{ Exception -> 0x02bd }
            if (r1 == 0) goto L_0x02b9
            java.lang.String r1 = "this Android can't rename files"
            org.telegram.messenger.FileLog.d(r1)     // Catch:{ Exception -> 0x02bd }
        L_0x02b9:
            org.telegram.messenger.SharedConfig.checkSaveToGalleryFiles()     // Catch:{ Exception -> 0x02bd }
            goto L_0x02c1
        L_0x02bd:
            r1 = move-exception
            org.telegram.messenger.FileLog.e((java.lang.Throwable) r1)
        L_0x02c1:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.telegram.messenger.ImageLoader.createMediaPaths():android.util.SparseArray");
    }

    /* JADX WARNING: Removed duplicated region for block: B:34:0x0072 A[SYNTHETIC, Splitter:B:34:0x0072] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x007e A[SYNTHETIC, Splitter:B:39:0x007e] */
    /* JADX WARNING: Removed duplicated region for block: B:46:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean canMoveFiles(java.io.File r6, java.io.File r7, int r8) {
        /*
            r5 = this;
            r0 = 1
            java.lang.String r1 = "000000000_999999.f"
            java.lang.String r2 = "000000000_999999_temp.f"
            r3 = 0
            if (r8 != 0) goto L_0x0018
            java.io.File r8 = new java.io.File     // Catch:{ Exception -> 0x0016 }
            r8.<init>(r6, r2)     // Catch:{ Exception -> 0x0016 }
            java.io.File r6 = new java.io.File     // Catch:{ Exception -> 0x0016 }
            r6.<init>(r7, r1)     // Catch:{ Exception -> 0x0016 }
            goto L_0x0047
        L_0x0013:
            r6 = move-exception
            goto L_0x007c
        L_0x0016:
            r6 = move-exception
            goto L_0x006d
        L_0x0018:
            r4 = 3
            if (r8 == r4) goto L_0x003d
            r4 = 5
            if (r8 != r4) goto L_0x001f
            goto L_0x003d
        L_0x001f:
            if (r8 != r0) goto L_0x002c
            java.io.File r8 = new java.io.File     // Catch:{ Exception -> 0x0016 }
            r8.<init>(r6, r2)     // Catch:{ Exception -> 0x0016 }
            java.io.File r6 = new java.io.File     // Catch:{ Exception -> 0x0016 }
            r6.<init>(r7, r1)     // Catch:{ Exception -> 0x0016 }
            goto L_0x0047
        L_0x002c:
            r4 = 2
            if (r8 != r4) goto L_0x003a
            java.io.File r8 = new java.io.File     // Catch:{ Exception -> 0x0016 }
            r8.<init>(r6, r2)     // Catch:{ Exception -> 0x0016 }
            java.io.File r6 = new java.io.File     // Catch:{ Exception -> 0x0016 }
            r6.<init>(r7, r1)     // Catch:{ Exception -> 0x0016 }
            goto L_0x0047
        L_0x003a:
            r6 = r3
            r8 = r6
            goto L_0x0047
        L_0x003d:
            java.io.File r8 = new java.io.File     // Catch:{ Exception -> 0x0016 }
            r8.<init>(r6, r2)     // Catch:{ Exception -> 0x0016 }
            java.io.File r6 = new java.io.File     // Catch:{ Exception -> 0x0016 }
            r6.<init>(r7, r1)     // Catch:{ Exception -> 0x0016 }
        L_0x0047:
            r7 = 1024(0x400, float:1.435E-42)
            byte[] r7 = new byte[r7]     // Catch:{ Exception -> 0x0016 }
            r8.createNewFile()     // Catch:{ Exception -> 0x0016 }
            java.io.RandomAccessFile r1 = new java.io.RandomAccessFile     // Catch:{ Exception -> 0x0016 }
            java.lang.String r2 = "rws"
            r1.<init>(r8, r2)     // Catch:{ Exception -> 0x0016 }
            r1.write(r7)     // Catch:{ Exception -> 0x006b, all -> 0x0068 }
            r1.close()     // Catch:{ Exception -> 0x006b, all -> 0x0068 }
            boolean r7 = r8.renameTo(r6)     // Catch:{ Exception -> 0x0016 }
            r8.delete()     // Catch:{ Exception -> 0x0016 }
            r6.delete()     // Catch:{ Exception -> 0x0016 }
            if (r7 == 0) goto L_0x007a
            return r0
        L_0x0068:
            r6 = move-exception
            r3 = r1
            goto L_0x007c
        L_0x006b:
            r6 = move-exception
            r3 = r1
        L_0x006d:
            org.telegram.messenger.FileLog.e((java.lang.Throwable) r6)     // Catch:{ all -> 0x0013 }
            if (r3 == 0) goto L_0x007a
            r3.close()     // Catch:{ Exception -> 0x0076 }
            goto L_0x007a
        L_0x0076:
            r6 = move-exception
            org.telegram.messenger.FileLog.e((java.lang.Throwable) r6)
        L_0x007a:
            r6 = 0
            return r6
        L_0x007c:
            if (r3 == 0) goto L_0x0086
            r3.close()     // Catch:{ Exception -> 0x0082 }
            goto L_0x0086
        L_0x0082:
            r7 = move-exception
            org.telegram.messenger.FileLog.e((java.lang.Throwable) r7)
        L_0x0086:
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: org.telegram.messenger.ImageLoader.canMoveFiles(java.io.File, java.io.File, int):boolean");
    }

    public Float getFileProgress(String str) {
        long[] jArr;
        if (str == null || (jArr = this.fileProgresses.get(str)) == null) {
            return null;
        }
        if (jArr[1] == 0) {
            return Float.valueOf(0.0f);
        }
        return Float.valueOf(Math.min(1.0f, ((float) jArr[0]) / ((float) jArr[1])));
    }

    public long[] getFileProgressSizes(String str) {
        if (str == null) {
            return null;
        }
        return this.fileProgresses.get(str);
    }

    public String getReplacedKey(String str) {
        if (str == null) {
            return null;
        }
        return this.replacedBitmaps.get(str);
    }

    private void performReplace(String str, String str2) {
        LruCache<BitmapDrawable> lruCache = this.memCache;
        BitmapDrawable bitmapDrawable = lruCache.get(str);
        if (bitmapDrawable == null) {
            lruCache = this.smallImagesMemCache;
            bitmapDrawable = lruCache.get(str);
        }
        this.replacedBitmaps.put(str, str2);
        if (bitmapDrawable != null) {
            BitmapDrawable bitmapDrawable2 = lruCache.get(str2);
            boolean z = false;
            if (!(bitmapDrawable2 == null || bitmapDrawable2.getBitmap() == null || bitmapDrawable.getBitmap() == null)) {
                Bitmap bitmap = bitmapDrawable2.getBitmap();
                Bitmap bitmap2 = bitmapDrawable.getBitmap();
                if (bitmap.getWidth() > bitmap2.getWidth() || bitmap.getHeight() > bitmap2.getHeight()) {
                    z = true;
                }
            }
            if (!z) {
                this.ignoreRemoval = str;
                lruCache.remove(str);
                lruCache.put(str2, bitmapDrawable);
                this.ignoreRemoval = null;
            } else {
                lruCache.remove(str);
            }
        }
        Integer num = this.bitmapUseCounts.get(str);
        if (num != null) {
            this.bitmapUseCounts.put(str2, num);
            this.bitmapUseCounts.remove(str);
        }
    }

    public void incrementUseCount(String str) {
        Integer num = this.bitmapUseCounts.get(str);
        if (num == null) {
            this.bitmapUseCounts.put(str, 1);
        } else {
            this.bitmapUseCounts.put(str, Integer.valueOf(num.intValue() + 1));
        }
    }

    public boolean decrementUseCount(String str) {
        Integer num = this.bitmapUseCounts.get(str);
        if (num == null) {
            return true;
        }
        if (num.intValue() == 1) {
            this.bitmapUseCounts.remove(str);
            return true;
        }
        this.bitmapUseCounts.put(str, Integer.valueOf(num.intValue() - 1));
        return false;
    }

    public void removeImage(String str) {
        this.bitmapUseCounts.remove(str);
        this.memCache.remove(str);
        this.smallImagesMemCache.remove(str);
    }

    public boolean isInMemCache(String str, boolean z) {
        if (z) {
            return getFromLottieCache(str) != null;
        }
        if (getFromMemCache(str) != null) {
            return true;
        }
        return false;
    }

    public void clearMemory() {
        this.smallImagesMemCache.evictAll();
        this.memCache.evictAll();
        this.lottieMemCache.evictAll();
    }

    private void removeFromWaitingForThumb(int i, ImageReceiver imageReceiver) {
        String str = this.waitingForQualityThumbByTag.get(i);
        if (str != null) {
            ThumbGenerateInfo thumbGenerateInfo = this.waitingForQualityThumb.get(str);
            if (thumbGenerateInfo != null) {
                int indexOf = thumbGenerateInfo.imageReceiverArray.indexOf(imageReceiver);
                if (indexOf >= 0) {
                    thumbGenerateInfo.imageReceiverArray.remove(indexOf);
                    thumbGenerateInfo.imageReceiverGuidsArray.remove(indexOf);
                }
                if (thumbGenerateInfo.imageReceiverArray.isEmpty()) {
                    this.waitingForQualityThumb.remove(str);
                }
            }
            this.waitingForQualityThumbByTag.remove(i);
        }
    }

    public void cancelLoadingForImageReceiver(ImageReceiver imageReceiver, boolean z) {
        if (imageReceiver != null) {
            ArrayList<Runnable> loadingOperations = imageReceiver.getLoadingOperations();
            if (!loadingOperations.isEmpty()) {
                for (int i = 0; i < loadingOperations.size(); i++) {
                    this.imageLoadQueue.cancelRunnable(loadingOperations.get(i));
                }
                loadingOperations.clear();
            }
            imageReceiver.addLoadingImageRunnable((Runnable) null);
            this.imageLoadQueue.postRunnable(new ImageLoader$$ExternalSyntheticLambda12(this, z, imageReceiver));
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$cancelLoadingForImageReceiver$3(boolean z, ImageReceiver imageReceiver) {
        int i = 0;
        while (true) {
            int i2 = 3;
            if (i >= 3) {
                return;
            }
            if (i <= 0 || z) {
                if (i == 0) {
                    i2 = 1;
                } else if (i == 1) {
                    i2 = 0;
                }
                int tag = imageReceiver.getTag(i2);
                if (tag != 0) {
                    if (i == 0) {
                        removeFromWaitingForThumb(tag, imageReceiver);
                    }
                    CacheImage cacheImage = this.imageLoadingByTag.get(tag);
                    if (cacheImage != null) {
                        cacheImage.removeImageReceiver(imageReceiver);
                    }
                }
                i++;
            } else {
                return;
            }
        }
    }

    public BitmapDrawable getImageFromMemory(TLObject tLObject, String str, String str2) {
        String str3 = null;
        if (tLObject == null && str == null) {
            return null;
        }
        if (str != null) {
            str3 = Utilities.MD5(str);
        } else if (tLObject instanceof TLRPC$FileLocation) {
            TLRPC$FileLocation tLRPC$FileLocation = (TLRPC$FileLocation) tLObject;
            str3 = tLRPC$FileLocation.volume_id + "_" + tLRPC$FileLocation.local_id;
        } else if (tLObject instanceof TLRPC$Document) {
            TLRPC$Document tLRPC$Document = (TLRPC$Document) tLObject;
            str3 = tLRPC$Document.dc_id + "_" + tLRPC$Document.id;
        } else if (tLObject instanceof SecureDocument) {
            SecureDocument secureDocument = (SecureDocument) tLObject;
            str3 = secureDocument.secureFile.dc_id + "_" + secureDocument.secureFile.id;
        } else if (tLObject instanceof WebFile) {
            str3 = Utilities.MD5(((WebFile) tLObject).url);
        }
        if (str2 != null) {
            str3 = str3 + "@" + str2;
        }
        return getFromMemCache(str3);
    }

    /* access modifiers changed from: private */
    /* renamed from: replaceImageInCacheInternal */
    public void lambda$replaceImageInCache$4(String str, String str2, ImageLocation imageLocation) {
        ArrayList<String> arrayList;
        for (int i = 0; i < 2; i++) {
            if (i == 0) {
                arrayList = this.memCache.getFilterKeys(str);
            } else {
                arrayList = this.smallImagesMemCache.getFilterKeys(str);
            }
            if (arrayList != null) {
                for (int i2 = 0; i2 < arrayList.size(); i2++) {
                    String str3 = arrayList.get(i2);
                    String str4 = str + "@" + str3;
                    String str5 = str2 + "@" + str3;
                    performReplace(str4, str5);
                    NotificationCenter.getGlobalInstance().postNotificationName(NotificationCenter.didReplacedPhotoInMemCache, str4, str5, imageLocation);
                }
            } else {
                performReplace(str, str2);
                NotificationCenter.getGlobalInstance().postNotificationName(NotificationCenter.didReplacedPhotoInMemCache, str, str2, imageLocation);
            }
        }
    }

    public void replaceImageInCache(String str, String str2, ImageLocation imageLocation, boolean z) {
        if (z) {
            AndroidUtilities.runOnUIThread(new ImageLoader$$ExternalSyntheticLambda9(this, str, str2, imageLocation));
        } else {
            lambda$replaceImageInCache$4(str, str2, imageLocation);
        }
    }

    public void putImageToCache(BitmapDrawable bitmapDrawable, String str, boolean z) {
        if (z) {
            this.smallImagesMemCache.put(str, bitmapDrawable);
        } else {
            this.memCache.put(str, bitmapDrawable);
        }
    }

    private void generateThumb(int i, File file, ThumbGenerateInfo thumbGenerateInfo) {
        if ((i == 0 || i == 2 || i == 3) && file != null && thumbGenerateInfo != null) {
            if (this.thumbGenerateTasks.get(FileLoader.getAttachFileName(thumbGenerateInfo.parentDocument)) == null) {
                this.thumbGeneratingQueue.postRunnable(new ThumbGenerateTask(i, file, thumbGenerateInfo));
            }
        }
    }

    public void cancelForceLoadingForImageReceiver(ImageReceiver imageReceiver) {
        String imageKey;
        if (imageReceiver != null && (imageKey = imageReceiver.getImageKey()) != null) {
            this.imageLoadQueue.postRunnable(new ImageLoader$$ExternalSyntheticLambda4(this, imageKey));
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$cancelForceLoadingForImageReceiver$5(String str) {
        this.forceLoadingImages.remove(str);
    }

    private void createLoadOperationForImageReceiver(ImageReceiver imageReceiver, String str, String str2, String str3, ImageLocation imageLocation, String str4, long j, int i, int i2, int i3, int i4) {
        ImageReceiver imageReceiver2 = imageReceiver;
        int i5 = i2;
        if (imageReceiver2 == null || str2 == null || str == null || imageLocation == null) {
            return;
        }
        int tag = imageReceiver2.getTag(i5);
        if (tag == 0) {
            tag = this.lastImageNum;
            imageReceiver2.setTag(tag, i5);
            int i6 = this.lastImageNum + 1;
            this.lastImageNum = i6;
            if (i6 == Integer.MAX_VALUE) {
                this.lastImageNum = 0;
            }
        }
        int i7 = tag;
        boolean isNeedsQualityThumb = imageReceiver.isNeedsQualityThumb();
        Object parentObject = imageReceiver.getParentObject();
        TLRPC$Document qualityThumbDocument = imageReceiver.getQualityThumbDocument();
        boolean isShouldGenerateQualityThumb = imageReceiver.isShouldGenerateQualityThumb();
        ImageLoader$$ExternalSyntheticLambda2 imageLoader$$ExternalSyntheticLambda2 = r0;
        ImageLoader$$ExternalSyntheticLambda2 imageLoader$$ExternalSyntheticLambda22 = new ImageLoader$$ExternalSyntheticLambda2(this, i3, str2, str, i7, imageReceiver, i4, str4, i2, imageLocation, i5 == 0 && imageReceiver.isCurrentKeyQuality(), parentObject, imageReceiver.getCurrentAccount(), qualityThumbDocument, isNeedsQualityThumb, isShouldGenerateQualityThumb, str3, i, j);
        ImageLoader$$ExternalSyntheticLambda2 imageLoader$$ExternalSyntheticLambda23 = imageLoader$$ExternalSyntheticLambda2;
        this.imageLoadQueue.postRunnable(imageLoader$$ExternalSyntheticLambda23);
        imageReceiver.addLoadingImageRunnable(imageLoader$$ExternalSyntheticLambda23);
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:229:0x0491, code lost:
        if (r10.exists() == false) goto L_0x0496;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x01a9, code lost:
        if (r9.exists() == false) goto L_0x01ab;
     */
    /* JADX WARNING: Removed duplicated region for block: B:123:0x02a5  */
    /* JADX WARNING: Removed duplicated region for block: B:239:0x04d8  */
    /* JADX WARNING: Removed duplicated region for block: B:240:0x04da  */
    /* JADX WARNING: Removed duplicated region for block: B:242:0x04e5  */
    /* JADX WARNING: Removed duplicated region for block: B:245:0x050e  */
    /* JADX WARNING: Removed duplicated region for block: B:248:0x0513  */
    /* JADX WARNING: Removed duplicated region for block: B:251:0x0549 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:301:0x0637  */
    /* JADX WARNING: Removed duplicated region for block: B:302:0x063f  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x019e  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x01ae  */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x01b0  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x01b3  */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x0204  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ void lambda$createLoadOperationForImageReceiver$6(int r24, java.lang.String r25, java.lang.String r26, int r27, org.telegram.messenger.ImageReceiver r28, int r29, java.lang.String r30, int r31, org.telegram.messenger.ImageLocation r32, boolean r33, java.lang.Object r34, int r35, org.telegram.tgnet.TLRPC$Document r36, boolean r37, boolean r38, java.lang.String r39, int r40, long r41) {
        /*
            r23 = this;
            r0 = r23
            r1 = r24
            r2 = r25
            r9 = r26
            r10 = r27
            r11 = r28
            r12 = r30
            r13 = r32
            r14 = r34
            r15 = r36
            r8 = r39
            r7 = r40
            r5 = r41
            r3 = 2
            if (r1 == r3) goto L_0x00a5
            java.util.HashMap<java.lang.String, org.telegram.messenger.ImageLoader$CacheImage> r3 = r0.imageLoadingByUrl
            java.lang.Object r3 = r3.get(r2)
            org.telegram.messenger.ImageLoader$CacheImage r3 = (org.telegram.messenger.ImageLoader.CacheImage) r3
            java.util.HashMap<java.lang.String, org.telegram.messenger.ImageLoader$CacheImage> r4 = r0.imageLoadingByKeys
            java.lang.Object r4 = r4.get(r9)
            org.telegram.messenger.ImageLoader$CacheImage r4 = (org.telegram.messenger.ImageLoader.CacheImage) r4
            android.util.SparseArray<org.telegram.messenger.ImageLoader$CacheImage> r12 = r0.imageLoadingByTag
            java.lang.Object r12 = r12.get(r10)
            org.telegram.messenger.ImageLoader$CacheImage r12 = (org.telegram.messenger.ImageLoader.CacheImage) r12
            if (r12 == 0) goto L_0x0072
            if (r12 != r4) goto L_0x0046
            r9 = r29
            r12.setImageReceiverGuid(r11, r9)
            r16 = r3
            r17 = r4
            r4 = 1
            r8 = 0
            r9 = 2
            goto L_0x0079
        L_0x0046:
            r9 = r29
            if (r12 != r3) goto L_0x0068
            r16 = r3
            if (r4 != 0) goto L_0x0061
            r9 = 2
            r3 = r12
            r17 = r4
            r12 = 0
            r4 = r28
            r5 = r26
            r6 = r30
            r7 = r31
            r8 = r29
            r3.replaceImageReceiver(r4, r5, r6, r7, r8)
            goto L_0x0065
        L_0x0061:
            r17 = r4
            r9 = 2
            r12 = 0
        L_0x0065:
            r4 = 1
            r8 = 0
            goto L_0x0079
        L_0x0068:
            r16 = r3
            r17 = r4
            r8 = 0
            r9 = 2
            r12.removeImageReceiver(r11)
            goto L_0x0078
        L_0x0072:
            r16 = r3
            r17 = r4
            r8 = 0
            r9 = 2
        L_0x0078:
            r4 = 0
        L_0x0079:
            if (r4 != 0) goto L_0x008f
            if (r17 == 0) goto L_0x008f
            r3 = r17
            r4 = r28
            r5 = r26
            r6 = r30
            r7 = r31
            r12 = 0
            r8 = r29
            r3.addImageReceiver(r4, r5, r6, r7, r8)
            r4 = 1
            goto L_0x0090
        L_0x008f:
            r12 = 0
        L_0x0090:
            if (r4 != 0) goto L_0x00a8
            if (r16 == 0) goto L_0x00a8
            r3 = r16
            r4 = r28
            r5 = r26
            r6 = r30
            r7 = r31
            r8 = r29
            r3.addImageReceiver(r4, r5, r6, r7, r8)
            r4 = 1
            goto L_0x00a8
        L_0x00a5:
            r9 = 2
            r12 = 0
            r4 = 0
        L_0x00a8:
            if (r4 != 0) goto L_0x0646
            java.lang.String r3 = r13.path
            java.lang.String r8 = "athumb"
            java.lang.String r4 = "_"
            r16 = 4
            if (r3 == 0) goto L_0x0110
            java.lang.String r7 = "http"
            boolean r7 = r3.startsWith(r7)
            if (r7 != 0) goto L_0x0105
            boolean r7 = r3.startsWith(r8)
            if (r7 != 0) goto L_0x0105
            java.lang.String r7 = "thumb://"
            boolean r7 = r3.startsWith(r7)
            java.lang.String r10 = ":"
            if (r7 == 0) goto L_0x00e2
            r7 = 8
            int r7 = r3.indexOf(r10, r7)
            if (r7 < 0) goto L_0x00e0
            java.io.File r10 = new java.io.File
            r15 = 1
            int r7 = r7 + r15
            java.lang.String r3 = r3.substring(r7)
            r10.<init>(r3)
            goto L_0x0103
        L_0x00e0:
            r10 = 0
            goto L_0x0103
        L_0x00e2:
            java.lang.String r7 = "vthumb://"
            boolean r7 = r3.startsWith(r7)
            if (r7 == 0) goto L_0x00fe
            r7 = 9
            int r7 = r3.indexOf(r10, r7)
            if (r7 < 0) goto L_0x00e0
            java.io.File r10 = new java.io.File
            r15 = 1
            int r7 = r7 + r15
            java.lang.String r3 = r3.substring(r7)
            r10.<init>(r3)
            goto L_0x0103
        L_0x00fe:
            java.io.File r10 = new java.io.File
            r10.<init>(r3)
        L_0x0103:
            r3 = 1
            goto L_0x0107
        L_0x0105:
            r3 = 0
            r10 = 0
        L_0x0107:
            r20 = r8
            r5 = 0
            r6 = 2
            r12 = 1
            r8 = r30
            goto L_0x021b
        L_0x0110:
            if (r1 != 0) goto L_0x0212
            if (r33 == 0) goto L_0x0212
            boolean r3 = r14 instanceof org.telegram.messenger.MessageObject
            if (r3 == 0) goto L_0x0139
            r3 = r14
            org.telegram.messenger.MessageObject r3 = (org.telegram.messenger.MessageObject) r3
            org.telegram.tgnet.TLRPC$Document r7 = r3.getDocument()
            org.telegram.tgnet.TLRPC$Message r15 = r3.messageOwner
            java.lang.String r15 = r15.attachPath
            org.telegram.messenger.FileLoader r12 = org.telegram.messenger.FileLoader.getInstance(r35)
            org.telegram.tgnet.TLRPC$Message r5 = r3.messageOwner
            java.io.File r5 = r12.getPathToMessage(r5)
            int r3 = r3.getMediaType()
            r12 = r5
            r36 = r15
            r5 = 1
            r15 = r7
            r7 = r3
            r3 = 0
            goto L_0x0159
        L_0x0139:
            if (r15 == 0) goto L_0x0152
            org.telegram.messenger.FileLoader r3 = org.telegram.messenger.FileLoader.getInstance(r35)
            r5 = 1
            java.io.File r3 = r3.getPathToAttach(r15, r5)
            boolean r7 = org.telegram.messenger.MessageObject.isVideoDocument(r36)
            if (r7 == 0) goto L_0x014c
            r7 = 2
            goto L_0x014d
        L_0x014c:
            r7 = 3
        L_0x014d:
            r12 = r3
            r36 = 0
            r3 = 1
            goto L_0x0159
        L_0x0152:
            r5 = 1
            r36 = 0
            r3 = 0
            r7 = 0
            r12 = 0
            r15 = 0
        L_0x0159:
            if (r15 == 0) goto L_0x020b
            if (r37 == 0) goto L_0x0193
            java.io.File r5 = new java.io.File
            java.io.File r9 = org.telegram.messenger.FileLoader.getDirectory(r16)
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            r20 = r8
            java.lang.String r8 = "q_"
            r6.append(r8)
            int r8 = r15.dc_id
            r6.append(r8)
            r6.append(r4)
            r8 = r12
            long r12 = r15.id
            r6.append(r12)
            java.lang.String r12 = ".jpg"
            r6.append(r12)
            java.lang.String r6 = r6.toString()
            r5.<init>(r9, r6)
            boolean r6 = r5.exists()
            if (r6 != 0) goto L_0x0190
            goto L_0x0196
        L_0x0190:
            r6 = r5
            r5 = 1
            goto L_0x0198
        L_0x0193:
            r20 = r8
            r8 = r12
        L_0x0196:
            r5 = 0
            r6 = 0
        L_0x0198:
            boolean r9 = android.text.TextUtils.isEmpty(r36)
            if (r9 != 0) goto L_0x01ab
            java.io.File r9 = new java.io.File
            r12 = r36
            r9.<init>(r12)
            boolean r12 = r9.exists()
            if (r12 != 0) goto L_0x01ac
        L_0x01ab:
            r9 = 0
        L_0x01ac:
            if (r9 != 0) goto L_0x01b0
            r12 = r8
            goto L_0x01b1
        L_0x01b0:
            r12 = r9
        L_0x01b1:
            if (r6 != 0) goto L_0x0204
            java.lang.String r1 = org.telegram.messenger.FileLoader.getAttachFileName(r15)
            java.util.HashMap<java.lang.String, org.telegram.messenger.ImageLoader$ThumbGenerateInfo> r2 = r0.waitingForQualityThumb
            java.lang.Object r2 = r2.get(r1)
            org.telegram.messenger.ImageLoader$ThumbGenerateInfo r2 = (org.telegram.messenger.ImageLoader.ThumbGenerateInfo) r2
            if (r2 != 0) goto L_0x01d7
            org.telegram.messenger.ImageLoader$ThumbGenerateInfo r2 = new org.telegram.messenger.ImageLoader$ThumbGenerateInfo
            r4 = 0
            r2.<init>()
            org.telegram.tgnet.TLRPC$Document unused = r2.parentDocument = r15
            r8 = r30
            java.lang.String unused = r2.filter = r8
            boolean unused = r2.big = r3
            java.util.HashMap<java.lang.String, org.telegram.messenger.ImageLoader$ThumbGenerateInfo> r3 = r0.waitingForQualityThumb
            r3.put(r1, r2)
        L_0x01d7:
            java.util.ArrayList r3 = r2.imageReceiverArray
            boolean r3 = r3.contains(r11)
            if (r3 != 0) goto L_0x01f3
            java.util.ArrayList r3 = r2.imageReceiverArray
            r3.add(r11)
            java.util.ArrayList r3 = r2.imageReceiverGuidsArray
            java.lang.Integer r4 = java.lang.Integer.valueOf(r29)
            r3.add(r4)
        L_0x01f3:
            android.util.SparseArray<java.lang.String> r3 = r0.waitingForQualityThumbByTag
            r3.put(r10, r1)
            boolean r1 = r12.exists()
            if (r1 == 0) goto L_0x0203
            if (r38 == 0) goto L_0x0203
            r0.generateThumb(r7, r12, r2)
        L_0x0203:
            return
        L_0x0204:
            r8 = r30
            r12 = 1
            r10 = r6
            r3 = 1
            r6 = 2
            goto L_0x021b
        L_0x020b:
            r20 = r8
            r12 = 1
            r8 = r30
            r3 = 1
            goto L_0x0218
        L_0x0212:
            r20 = r8
            r12 = 1
            r8 = r30
            r3 = 0
        L_0x0218:
            r5 = 0
            r6 = 2
            r10 = 0
        L_0x021b:
            if (r1 == r6) goto L_0x0646
            boolean r7 = r32.isEncrypted()
            org.telegram.messenger.ImageLoader$CacheImage r9 = new org.telegram.messenger.ImageLoader$CacheImage
            r13 = 0
            r9.<init>()
            r13 = r32
            if (r33 != 0) goto L_0x029f
            int r15 = r13.imageType
            if (r15 == r6) goto L_0x029a
            org.telegram.messenger.WebFile r6 = r13.webFile
            boolean r6 = org.telegram.messenger.MessageObject.isGifDocument((org.telegram.messenger.WebFile) r6)
            if (r6 != 0) goto L_0x0296
            org.telegram.tgnet.TLRPC$Document r6 = r13.document
            boolean r6 = org.telegram.messenger.MessageObject.isGifDocument((org.telegram.tgnet.TLRPC$Document) r6)
            if (r6 != 0) goto L_0x0296
            org.telegram.tgnet.TLRPC$Document r6 = r13.document
            boolean r6 = org.telegram.messenger.MessageObject.isRoundVideoDocument(r6)
            if (r6 != 0) goto L_0x0296
            org.telegram.tgnet.TLRPC$Document r6 = r13.document
            boolean r6 = org.telegram.messenger.MessageObject.isVideoSticker(r6)
            if (r6 == 0) goto L_0x0250
            goto L_0x0296
        L_0x0250:
            java.lang.String r6 = r13.path
            if (r6 == 0) goto L_0x029f
            java.lang.String r15 = "vthumb"
            boolean r15 = r6.startsWith(r15)
            if (r15 != 0) goto L_0x029f
            java.lang.String r15 = "thumb"
            boolean r15 = r6.startsWith(r15)
            if (r15 != 0) goto L_0x029f
            java.lang.String r15 = "jpg"
            java.lang.String r6 = getHttpUrlExtension(r6, r15)
            java.lang.String r15 = "webm"
            boolean r15 = r6.equals(r15)
            if (r15 != 0) goto L_0x0290
            java.lang.String r15 = "mp4"
            boolean r15 = r6.equals(r15)
            if (r15 != 0) goto L_0x0290
            java.lang.String r15 = "gif"
            boolean r6 = r6.equals(r15)
            if (r6 == 0) goto L_0x0283
            goto L_0x0290
        L_0x0283:
            java.lang.String r6 = "tgs"
            r15 = r39
            boolean r6 = r6.equals(r15)
            if (r6 == 0) goto L_0x02a1
            r9.imageType = r12
            goto L_0x02a1
        L_0x0290:
            r15 = r39
            r6 = 2
            r9.imageType = r6
            goto L_0x02a1
        L_0x0296:
            r15 = r39
            r6 = 2
            goto L_0x029c
        L_0x029a:
            r15 = r39
        L_0x029c:
            r9.imageType = r6
            goto L_0x02a1
        L_0x029f:
            r15 = r39
        L_0x02a1:
            r21 = 0
            if (r10 != 0) goto L_0x04e5
            org.telegram.tgnet.TLRPC$PhotoSize r6 = r13.photoSize
            boolean r12 = r6 instanceof org.telegram.tgnet.TLRPC$TL_photoStrippedSize
            r27 = r3
            java.lang.String r3 = "g"
            if (r12 != 0) goto L_0x04c2
            boolean r6 = r6 instanceof org.telegram.tgnet.TLRPC$TL_photoPathSize
            if (r6 == 0) goto L_0x02b5
            goto L_0x04c2
        L_0x02b5:
            org.telegram.messenger.SecureDocument r6 = r13.secureDocument
            if (r6 == 0) goto L_0x02d7
            r9.secureDocument = r6
            org.telegram.tgnet.TLRPC$TL_secureFile r4 = r6.secureFile
            int r4 = r4.dc_id
            r6 = -2147483648(0xfffffffvar_, float:-0.0)
            if (r4 != r6) goto L_0x02c5
            r4 = 1
            goto L_0x02c6
        L_0x02c5:
            r4 = 0
        L_0x02c6:
            java.io.File r10 = new java.io.File
            java.io.File r6 = org.telegram.messenger.FileLoader.getDirectory(r16)
            r10.<init>(r6, r2)
            r1 = r40
            r12 = r4
            r7 = r5
        L_0x02d3:
            r4 = r21
            goto L_0x04cb
        L_0x02d7:
            boolean r6 = r3.equals(r8)
            java.lang.String r10 = ".svg"
            java.lang.String r12 = "application/x-tgwallpattern"
            r36 = r5
            java.lang.String r5 = "application/x-tgsticker"
            java.lang.String r11 = "application/x-tgsdice"
            if (r6 != 0) goto L_0x0384
            boolean r6 = r0.isAnimatedAvatar(r8)
            if (r6 != 0) goto L_0x0384
            r6 = r40
            if (r6 != 0) goto L_0x02fd
            r14 = r41
            int r19 = (r14 > r21 ? 1 : (r14 == r21 ? 0 : -1))
            if (r19 <= 0) goto L_0x02fd
            java.lang.String r14 = r13.path
            if (r14 != 0) goto L_0x02fd
            if (r7 == 0) goto L_0x0386
        L_0x02fd:
            java.io.File r4 = new java.io.File
            java.io.File r7 = org.telegram.messenger.FileLoader.getDirectory(r16)
            r4.<init>(r7, r2)
            boolean r7 = r4.exists()
            if (r7 == 0) goto L_0x030e
            r7 = 1
            goto L_0x032d
        L_0x030e:
            r7 = 2
            if (r6 != r7) goto L_0x032b
            java.io.File r4 = new java.io.File
            java.io.File r7 = org.telegram.messenger.FileLoader.getDirectory(r16)
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            r14.<init>()
            r14.append(r2)
            java.lang.String r15 = ".enc"
            r14.append(r15)
            java.lang.String r14 = r14.toString()
            r4.<init>(r7, r14)
        L_0x032b:
            r7 = r36
        L_0x032d:
            org.telegram.tgnet.TLRPC$Document r14 = r13.document
            if (r14 == 0) goto L_0x037e
            boolean r15 = r14 instanceof org.telegram.messenger.DocumentObject.ThemeDocument
            if (r15 == 0) goto L_0x0344
            org.telegram.messenger.DocumentObject$ThemeDocument r14 = (org.telegram.messenger.DocumentObject.ThemeDocument) r14
            org.telegram.tgnet.TLRPC$Document r5 = r14.wallpaper
            if (r5 != 0) goto L_0x033e
            r5 = 5
            r12 = 1
            goto L_0x0341
        L_0x033e:
            r12 = r27
            r5 = 5
        L_0x0341:
            r9.imageType = r5
            goto L_0x0380
        L_0x0344:
            java.lang.String r14 = r14.mime_type
            boolean r11 = r11.equals(r14)
            if (r11 == 0) goto L_0x0353
            r11 = 1
            r9.imageType = r11
            r10 = r4
            r1 = r6
            goto L_0x04c8
        L_0x0353:
            r11 = 1
            org.telegram.tgnet.TLRPC$Document r14 = r13.document
            java.lang.String r14 = r14.mime_type
            boolean r5 = r5.equals(r14)
            if (r5 == 0) goto L_0x0361
            r9.imageType = r11
            goto L_0x037e
        L_0x0361:
            org.telegram.tgnet.TLRPC$Document r5 = r13.document
            java.lang.String r5 = r5.mime_type
            boolean r5 = r12.equals(r5)
            if (r5 == 0) goto L_0x036f
            r5 = 3
            r9.imageType = r5
            goto L_0x037e
        L_0x036f:
            r5 = 3
            org.telegram.tgnet.TLRPC$Document r11 = r13.document
            java.lang.String r11 = org.telegram.messenger.FileLoader.getDocumentFileName(r11)
            boolean r10 = r11.endsWith(r10)
            if (r10 == 0) goto L_0x037e
            r9.imageType = r5
        L_0x037e:
            r12 = r27
        L_0x0380:
            r10 = r4
            r1 = r6
            goto L_0x02d3
        L_0x0384:
            r6 = r40
        L_0x0386:
            org.telegram.tgnet.TLRPC$Document r7 = r13.document
            java.lang.String r14 = ".temp"
            if (r7 == 0) goto L_0x0447
            boolean r15 = r7 instanceof org.telegram.tgnet.TLRPC$TL_documentEncrypted
            if (r15 == 0) goto L_0x039a
            java.io.File r15 = new java.io.File
            java.io.File r1 = org.telegram.messenger.FileLoader.getDirectory(r16)
            r15.<init>(r1, r2)
            goto L_0x03b5
        L_0x039a:
            boolean r1 = org.telegram.messenger.MessageObject.isVideoDocument(r7)
            if (r1 == 0) goto L_0x03ab
            java.io.File r15 = new java.io.File
            r1 = 2
            java.io.File r6 = org.telegram.messenger.FileLoader.getDirectory(r1)
            r15.<init>(r6, r2)
            goto L_0x03b5
        L_0x03ab:
            java.io.File r15 = new java.io.File
            r1 = 3
            java.io.File r6 = org.telegram.messenger.FileLoader.getDirectory(r1)
            r15.<init>(r6, r2)
        L_0x03b5:
            boolean r1 = r0.isAnimatedAvatar(r8)
            if (r1 != 0) goto L_0x03c1
            boolean r1 = r3.equals(r8)
            if (r1 == 0) goto L_0x03ed
        L_0x03c1:
            boolean r1 = r15.exists()
            if (r1 != 0) goto L_0x03ed
            java.io.File r1 = new java.io.File
            java.io.File r6 = org.telegram.messenger.FileLoader.getDirectory(r16)
            java.lang.StringBuilder r15 = new java.lang.StringBuilder
            r15.<init>()
            r37 = r3
            int r3 = r7.dc_id
            r15.append(r3)
            r15.append(r4)
            long r3 = r7.id
            r15.append(r3)
            r15.append(r14)
            java.lang.String r3 = r15.toString()
            r1.<init>(r6, r3)
            r15 = r1
            goto L_0x03ef
        L_0x03ed:
            r37 = r3
        L_0x03ef:
            boolean r1 = r7 instanceof org.telegram.messenger.DocumentObject.ThemeDocument
            if (r1 == 0) goto L_0x0403
            r1 = r7
            org.telegram.messenger.DocumentObject$ThemeDocument r1 = (org.telegram.messenger.DocumentObject.ThemeDocument) r1
            org.telegram.tgnet.TLRPC$Document r1 = r1.wallpaper
            if (r1 != 0) goto L_0x03fd
            r1 = 5
            r12 = 1
            goto L_0x0400
        L_0x03fd:
            r12 = r27
            r1 = 5
        L_0x0400:
            r9.imageType = r1
            goto L_0x043b
        L_0x0403:
            org.telegram.tgnet.TLRPC$Document r1 = r13.document
            java.lang.String r1 = r1.mime_type
            boolean r1 = r11.equals(r1)
            if (r1 == 0) goto L_0x0412
            r1 = 1
            r9.imageType = r1
            r12 = 1
            goto L_0x043b
        L_0x0412:
            r1 = 1
            java.lang.String r3 = r7.mime_type
            boolean r3 = r5.equals(r3)
            if (r3 == 0) goto L_0x041e
            r9.imageType = r1
            goto L_0x0439
        L_0x041e:
            java.lang.String r1 = r7.mime_type
            boolean r1 = r12.equals(r1)
            if (r1 == 0) goto L_0x042a
            r1 = 3
            r9.imageType = r1
            goto L_0x0439
        L_0x042a:
            r1 = 3
            org.telegram.tgnet.TLRPC$Document r3 = r13.document
            java.lang.String r3 = org.telegram.messenger.FileLoader.getDocumentFileName(r3)
            boolean r3 = r3.endsWith(r10)
            if (r3 == 0) goto L_0x0439
            r9.imageType = r1
        L_0x0439:
            r12 = r27
        L_0x043b:
            long r3 = r7.size
            r7 = r36
            r1 = r40
            r4 = r3
            r10 = r15
            r3 = r37
            goto L_0x04cb
        L_0x0447:
            r37 = r3
            r1 = 3
            org.telegram.messenger.WebFile r3 = r13.webFile
            if (r3 == 0) goto L_0x0461
            java.io.File r10 = new java.io.File
            java.io.File r1 = org.telegram.messenger.FileLoader.getDirectory(r1)
            r10.<init>(r1, r2)
            r12 = r27
            r7 = r36
            r3 = r37
            r1 = r40
            goto L_0x02d3
        L_0x0461:
            r1 = r40
            r11 = 1
            if (r1 != r11) goto L_0x0470
            java.io.File r3 = new java.io.File
            java.io.File r5 = org.telegram.messenger.FileLoader.getDirectory(r16)
            r3.<init>(r5, r2)
            goto L_0x047a
        L_0x0470:
            java.io.File r3 = new java.io.File
            r5 = 0
            java.io.File r6 = org.telegram.messenger.FileLoader.getDirectory(r5)
            r3.<init>(r6, r2)
        L_0x047a:
            r10 = r3
            boolean r3 = r0.isAnimatedAvatar(r8)
            if (r3 != 0) goto L_0x0494
            r3 = r37
            boolean r5 = r3.equals(r8)
            if (r5 == 0) goto L_0x04bc
            org.telegram.tgnet.TLRPC$TL_fileLocationToBeDeprecated r5 = r13.location
            if (r5 == 0) goto L_0x04bc
            boolean r5 = r10.exists()
            if (r5 != 0) goto L_0x04bc
            goto L_0x0496
        L_0x0494:
            r3 = r37
        L_0x0496:
            java.io.File r10 = new java.io.File
            java.io.File r5 = org.telegram.messenger.FileLoader.getDirectory(r16)
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            org.telegram.tgnet.TLRPC$TL_fileLocationToBeDeprecated r7 = r13.location
            long r11 = r7.volume_id
            r6.append(r11)
            r6.append(r4)
            org.telegram.tgnet.TLRPC$TL_fileLocationToBeDeprecated r4 = r13.location
            int r4 = r4.local_id
            r6.append(r4)
            r6.append(r14)
            java.lang.String r4 = r6.toString()
            r10.<init>(r5, r4)
        L_0x04bc:
            r12 = r27
            r7 = r36
            goto L_0x02d3
        L_0x04c2:
            r1 = r40
            r36 = r5
            r7 = r36
        L_0x04c8:
            r4 = r21
            r12 = 1
        L_0x04cb:
            boolean r3 = r3.equals(r8)
            if (r3 != 0) goto L_0x04da
            boolean r3 = r0.isAnimatedAvatar(r8)
            if (r3 == 0) goto L_0x04d8
            goto L_0x04da
        L_0x04d8:
            r11 = r10
            goto L_0x04e1
        L_0x04da:
            r3 = 2
            r9.imageType = r3
            r9.size = r4
            r11 = r10
            r12 = 1
        L_0x04e1:
            r10 = r7
            r7 = r31
            goto L_0x04f2
        L_0x04e5:
            r1 = r40
            r27 = r3
            r36 = r5
            r12 = r27
            r7 = r31
            r11 = r10
            r10 = r36
        L_0x04f2:
            r9.type = r7
            r14 = r26
            r9.key = r14
            r9.filter = r8
            r9.imageLocation = r13
            r15 = r39
            r5 = r41
            r9.ext = r15
            r4 = r35
            r9.currentAccount = r4
            r3 = r34
            r9.parentObject = r3
            int r3 = r13.imageType
            if (r3 == 0) goto L_0x0510
            r9.imageType = r3
        L_0x0510:
            r3 = 2
            if (r1 != r3) goto L_0x052f
            java.io.File r3 = new java.io.File
            java.io.File r1 = org.telegram.messenger.FileLoader.getInternalCacheDir()
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            r4.append(r2)
            java.lang.String r5 = ".enc.key"
            r4.append(r5)
            java.lang.String r4 = r4.toString()
            r3.<init>(r1, r4)
            r9.encryptionKeyPath = r3
        L_0x052f:
            r1 = r34
            r18 = 2
            r3 = r9
            r4 = r28
            r14 = r41
            r5 = r26
            r1 = r40
            r6 = r30
            r7 = r31
            r1 = r20
            r8 = r29
            r3.addImageReceiver(r4, r5, r6, r7, r8)
            if (r12 != 0) goto L_0x0623
            if (r10 != 0) goto L_0x0623
            boolean r3 = r11.exists()
            if (r3 == 0) goto L_0x0553
            goto L_0x0623
        L_0x0553:
            r9.url = r2
            java.util.HashMap<java.lang.String, org.telegram.messenger.ImageLoader$CacheImage> r3 = r0.imageLoadingByUrl
            r3.put(r2, r9)
            java.lang.String r2 = r13.path
            if (r2 == 0) goto L_0x05ac
            java.lang.String r2 = org.telegram.messenger.Utilities.MD5(r2)
            java.io.File r3 = org.telegram.messenger.FileLoader.getDirectory(r16)
            java.io.File r4 = new java.io.File
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            r5.append(r2)
            java.lang.String r2 = "_temp.jpg"
            r5.append(r2)
            java.lang.String r2 = r5.toString()
            r4.<init>(r3, r2)
            r9.tempFilePath = r4
            r9.finalFilePath = r11
            java.lang.String r2 = r13.path
            boolean r1 = r2.startsWith(r1)
            if (r1 == 0) goto L_0x059a
            org.telegram.messenger.ImageLoader$ArtworkLoadTask r1 = new org.telegram.messenger.ImageLoader$ArtworkLoadTask
            r1.<init>(r9)
            r9.artworkTask = r1
            java.util.LinkedList<org.telegram.messenger.ImageLoader$ArtworkLoadTask> r2 = r0.artworkTasks
            r2.add(r1)
            r1 = 0
            r0.runArtworkTasks(r1)
            goto L_0x0646
        L_0x059a:
            r1 = 0
            org.telegram.messenger.ImageLoader$HttpImageTask r2 = new org.telegram.messenger.ImageLoader$HttpImageTask
            r2.<init>(r9, r14)
            r9.httpTask = r2
            java.util.LinkedList<org.telegram.messenger.ImageLoader$HttpImageTask> r3 = r0.httpTasks
            r3.add(r2)
            r0.runHttpTasks(r1)
            goto L_0x0646
        L_0x05ac:
            org.telegram.tgnet.TLRPC$TL_fileLocationToBeDeprecated r1 = r13.location
            if (r1 == 0) goto L_0x05d4
            r1 = r40
            if (r1 != 0) goto L_0x05be
            int r2 = (r14 > r21 ? 1 : (r14 == r21 ? 0 : -1))
            if (r2 <= 0) goto L_0x05bc
            byte[] r2 = r13.key
            if (r2 == 0) goto L_0x05be
        L_0x05bc:
            r6 = 1
            goto L_0x05bf
        L_0x05be:
            r6 = r1
        L_0x05bf:
            org.telegram.messenger.FileLoader r1 = org.telegram.messenger.FileLoader.getInstance(r35)
            r3 = r34
            if (r24 == 0) goto L_0x05c9
            r5 = 2
            goto L_0x05ca
        L_0x05c9:
            r5 = 1
        L_0x05ca:
            r2 = r32
            r3 = r34
            r4 = r39
            r1.loadFile(r2, r3, r4, r5, r6)
            goto L_0x0610
        L_0x05d4:
            r3 = r34
            r1 = r40
            org.telegram.tgnet.TLRPC$Document r2 = r13.document
            if (r2 == 0) goto L_0x05eb
            org.telegram.messenger.FileLoader r2 = org.telegram.messenger.FileLoader.getInstance(r35)
            org.telegram.tgnet.TLRPC$Document r4 = r13.document
            if (r24 == 0) goto L_0x05e6
            r5 = 2
            goto L_0x05e7
        L_0x05e6:
            r5 = 1
        L_0x05e7:
            r2.loadFile(r4, r3, r5, r1)
            goto L_0x0610
        L_0x05eb:
            org.telegram.messenger.SecureDocument r2 = r13.secureDocument
            if (r2 == 0) goto L_0x05fe
            org.telegram.messenger.FileLoader r1 = org.telegram.messenger.FileLoader.getInstance(r35)
            org.telegram.messenger.SecureDocument r2 = r13.secureDocument
            if (r24 == 0) goto L_0x05f9
            r3 = 2
            goto L_0x05fa
        L_0x05f9:
            r3 = 1
        L_0x05fa:
            r1.loadFile(r2, r3)
            goto L_0x0610
        L_0x05fe:
            org.telegram.messenger.WebFile r2 = r13.webFile
            if (r2 == 0) goto L_0x0610
            org.telegram.messenger.FileLoader r2 = org.telegram.messenger.FileLoader.getInstance(r35)
            org.telegram.messenger.WebFile r3 = r13.webFile
            if (r24 == 0) goto L_0x060c
            r4 = 2
            goto L_0x060d
        L_0x060c:
            r4 = 1
        L_0x060d:
            r2.loadFile(r3, r4, r1)
        L_0x0610:
            boolean r1 = r28.isForceLoding()
            if (r1 == 0) goto L_0x0646
            java.util.HashMap<java.lang.String, java.lang.Integer> r1 = r0.forceLoadingImages
            java.lang.String r2 = r9.key
            r3 = 0
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            r1.put(r2, r3)
            goto L_0x0646
        L_0x0623:
            r9.finalFilePath = r11
            r9.imageLocation = r13
            org.telegram.messenger.ImageLoader$CacheOutTask r1 = new org.telegram.messenger.ImageLoader$CacheOutTask
            r1.<init>(r9)
            r9.cacheTask = r1
            java.util.HashMap<java.lang.String, org.telegram.messenger.ImageLoader$CacheImage> r1 = r0.imageLoadingByKeys
            r2 = r26
            r1.put(r2, r9)
            if (r24 == 0) goto L_0x063f
            org.telegram.messenger.DispatchQueue r1 = r0.cacheThumbOutQueue
            org.telegram.messenger.ImageLoader$CacheOutTask r2 = r9.cacheTask
            r1.postRunnable(r2)
            goto L_0x0646
        L_0x063f:
            org.telegram.messenger.DispatchQueue r1 = r0.cacheOutQueue
            org.telegram.messenger.ImageLoader$CacheOutTask r2 = r9.cacheTask
            r1.postRunnable(r2)
        L_0x0646:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.telegram.messenger.ImageLoader.lambda$createLoadOperationForImageReceiver$6(int, java.lang.String, java.lang.String, int, org.telegram.messenger.ImageReceiver, int, java.lang.String, int, org.telegram.messenger.ImageLocation, boolean, java.lang.Object, int, org.telegram.tgnet.TLRPC$Document, boolean, boolean, java.lang.String, int, long):void");
    }

    public void preloadArtwork(String str) {
        this.imageLoadQueue.postRunnable(new ImageLoader$$ExternalSyntheticLambda7(this, str));
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$preloadArtwork$7(String str) {
        String httpUrlExtension = getHttpUrlExtension(str, "jpg");
        String str2 = Utilities.MD5(str) + "." + httpUrlExtension;
        File file = new File(FileLoader.getDirectory(4), str2);
        if (!file.exists()) {
            ImageLocation forPath = ImageLocation.getForPath(str);
            CacheImage cacheImage = new CacheImage();
            cacheImage.type = 1;
            cacheImage.key = Utilities.MD5(str);
            cacheImage.filter = null;
            cacheImage.imageLocation = forPath;
            cacheImage.ext = httpUrlExtension;
            cacheImage.parentObject = null;
            int i = forPath.imageType;
            if (i != 0) {
                cacheImage.imageType = i;
            }
            cacheImage.url = str2;
            this.imageLoadingByUrl.put(str2, cacheImage);
            String MD5 = Utilities.MD5(forPath.path);
            cacheImage.tempFilePath = new File(FileLoader.getDirectory(4), MD5 + "_temp.jpg");
            cacheImage.finalFilePath = file;
            ArtworkLoadTask artworkLoadTask = new ArtworkLoadTask(cacheImage);
            cacheImage.artworkTask = artworkLoadTask;
            this.artworkTasks.add(artworkLoadTask);
            runArtworkTasks(false);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:144:0x0258, code lost:
        if (r6.local_id < 0) goto L_0x025d;
     */
    /* JADX WARNING: Removed duplicated region for block: B:102:0x019a  */
    /* JADX WARNING: Removed duplicated region for block: B:104:0x019d  */
    /* JADX WARNING: Removed duplicated region for block: B:105:0x01a0  */
    /* JADX WARNING: Removed duplicated region for block: B:107:0x01a4  */
    /* JADX WARNING: Removed duplicated region for block: B:111:0x01b9  */
    /* JADX WARNING: Removed duplicated region for block: B:193:0x0377  */
    /* JADX WARNING: Removed duplicated region for block: B:210:0x03e2  */
    /* JADX WARNING: Removed duplicated region for block: B:213:0x03ea A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:217:0x0407 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:221:0x0422 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:226:0x0443  */
    /* JADX WARNING: Removed duplicated region for block: B:227:0x0458  */
    /* JADX WARNING: Removed duplicated region for block: B:229:0x045b  */
    /* JADX WARNING: Removed duplicated region for block: B:237:0x0499  */
    /* JADX WARNING: Removed duplicated region for block: B:239:0x049e  */
    /* JADX WARNING: Removed duplicated region for block: B:253:0x0506  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x007f  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0084  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0087  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00ba  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x00d9  */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x0166  */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x0173  */
    /* JADX WARNING: Removed duplicated region for block: B:94:0x0185  */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x0188  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void loadImageForImageReceiver(org.telegram.messenger.ImageReceiver r37) {
        /*
            r36 = this;
            r13 = r36
            r14 = r37
            if (r14 != 0) goto L_0x0007
            return
        L_0x0007:
            java.lang.String r6 = r37.getMediaKey()
            int r15 = r37.getNewGuid()
            r7 = 0
            r8 = 1
            if (r6 == 0) goto L_0x006c
            org.telegram.messenger.ImageLocation r0 = r37.getMediaLocation()
            boolean r0 = r13.useLottieMemCache(r0, r6)
            if (r0 == 0) goto L_0x0023
            android.graphics.drawable.BitmapDrawable r0 = r13.getFromLottieCache(r6)
        L_0x0021:
            r1 = r0
            goto L_0x0055
        L_0x0023:
            org.telegram.messenger.LruCache<android.graphics.drawable.BitmapDrawable> r0 = r13.memCache
            java.lang.Object r0 = r0.get(r6)
            android.graphics.drawable.Drawable r0 = (android.graphics.drawable.Drawable) r0
            if (r0 == 0) goto L_0x0032
            org.telegram.messenger.LruCache<android.graphics.drawable.BitmapDrawable> r1 = r13.memCache
            r1.moveToFront(r6)
        L_0x0032:
            if (r0 != 0) goto L_0x0043
            org.telegram.messenger.LruCache<android.graphics.drawable.BitmapDrawable> r0 = r13.smallImagesMemCache
            java.lang.Object r0 = r0.get(r6)
            android.graphics.drawable.Drawable r0 = (android.graphics.drawable.Drawable) r0
            if (r0 == 0) goto L_0x0043
            org.telegram.messenger.LruCache<android.graphics.drawable.BitmapDrawable> r1 = r13.smallImagesMemCache
            r1.moveToFront(r6)
        L_0x0043:
            if (r0 != 0) goto L_0x0021
            org.telegram.messenger.LruCache<android.graphics.drawable.BitmapDrawable> r0 = r13.wallpaperMemCache
            java.lang.Object r0 = r0.get(r6)
            android.graphics.drawable.Drawable r0 = (android.graphics.drawable.Drawable) r0
            if (r0 == 0) goto L_0x0021
            org.telegram.messenger.LruCache<android.graphics.drawable.BitmapDrawable> r1 = r13.wallpaperMemCache
            r1.moveToFront(r6)
            goto L_0x0021
        L_0x0055:
            if (r1 == 0) goto L_0x006c
            r13.cancelLoadingForImageReceiver(r14, r8)
            r3 = 3
            r4 = 1
            r0 = r37
            r2 = r6
            r5 = r15
            r0.setImageBitmapByKey(r1, r2, r3, r4, r5)
            boolean r0 = r37.isForcePreview()
            if (r0 != 0) goto L_0x006a
            return
        L_0x006a:
            r0 = 1
            goto L_0x006d
        L_0x006c:
            r0 = 0
        L_0x006d:
            java.lang.String r2 = r37.getImageKey()
            if (r0 != 0) goto L_0x00d1
            if (r2 == 0) goto L_0x00d1
            org.telegram.messenger.ImageLocation r1 = r37.getImageLocation()
            boolean r1 = r13.useLottieMemCache(r1, r2)
            if (r1 == 0) goto L_0x0084
            android.graphics.drawable.BitmapDrawable r1 = r13.getFromLottieCache(r2)
            goto L_0x0085
        L_0x0084:
            r1 = 0
        L_0x0085:
            if (r1 != 0) goto L_0x00b8
            org.telegram.messenger.LruCache<android.graphics.drawable.BitmapDrawable> r1 = r13.memCache
            java.lang.Object r1 = r1.get(r2)
            android.graphics.drawable.Drawable r1 = (android.graphics.drawable.Drawable) r1
            if (r1 == 0) goto L_0x0096
            org.telegram.messenger.LruCache<android.graphics.drawable.BitmapDrawable> r3 = r13.memCache
            r3.moveToFront(r2)
        L_0x0096:
            if (r1 != 0) goto L_0x00a7
            org.telegram.messenger.LruCache<android.graphics.drawable.BitmapDrawable> r1 = r13.smallImagesMemCache
            java.lang.Object r1 = r1.get(r2)
            android.graphics.drawable.Drawable r1 = (android.graphics.drawable.Drawable) r1
            if (r1 == 0) goto L_0x00a7
            org.telegram.messenger.LruCache<android.graphics.drawable.BitmapDrawable> r3 = r13.smallImagesMemCache
            r3.moveToFront(r2)
        L_0x00a7:
            if (r1 != 0) goto L_0x00b8
            org.telegram.messenger.LruCache<android.graphics.drawable.BitmapDrawable> r1 = r13.wallpaperMemCache
            java.lang.Object r1 = r1.get(r2)
            android.graphics.drawable.Drawable r1 = (android.graphics.drawable.Drawable) r1
            if (r1 == 0) goto L_0x00b8
            org.telegram.messenger.LruCache<android.graphics.drawable.BitmapDrawable> r3 = r13.wallpaperMemCache
            r3.moveToFront(r2)
        L_0x00b8:
            if (r1 == 0) goto L_0x00d1
            r13.cancelLoadingForImageReceiver(r14, r8)
            r3 = 0
            r4 = 1
            r0 = r37
            r5 = r15
            r0.setImageBitmapByKey(r1, r2, r3, r4, r5)
            boolean r0 = r37.isForcePreview()
            if (r0 != 0) goto L_0x00ce
            if (r6 != 0) goto L_0x00ce
            return
        L_0x00ce:
            r16 = 1
            goto L_0x00d3
        L_0x00d1:
            r16 = r0
        L_0x00d3:
            java.lang.String r2 = r37.getThumbKey()
            if (r2 == 0) goto L_0x0133
            org.telegram.messenger.ImageLocation r0 = r37.getThumbLocation()
            boolean r0 = r13.useLottieMemCache(r0, r2)
            if (r0 == 0) goto L_0x00e9
            android.graphics.drawable.BitmapDrawable r0 = r13.getFromLottieCache(r2)
        L_0x00e7:
            r1 = r0
            goto L_0x011b
        L_0x00e9:
            org.telegram.messenger.LruCache<android.graphics.drawable.BitmapDrawable> r0 = r13.memCache
            java.lang.Object r0 = r0.get(r2)
            android.graphics.drawable.Drawable r0 = (android.graphics.drawable.Drawable) r0
            if (r0 == 0) goto L_0x00f8
            org.telegram.messenger.LruCache<android.graphics.drawable.BitmapDrawable> r1 = r13.memCache
            r1.moveToFront(r2)
        L_0x00f8:
            if (r0 != 0) goto L_0x0109
            org.telegram.messenger.LruCache<android.graphics.drawable.BitmapDrawable> r0 = r13.smallImagesMemCache
            java.lang.Object r0 = r0.get(r2)
            android.graphics.drawable.Drawable r0 = (android.graphics.drawable.Drawable) r0
            if (r0 == 0) goto L_0x0109
            org.telegram.messenger.LruCache<android.graphics.drawable.BitmapDrawable> r1 = r13.smallImagesMemCache
            r1.moveToFront(r2)
        L_0x0109:
            if (r0 != 0) goto L_0x00e7
            org.telegram.messenger.LruCache<android.graphics.drawable.BitmapDrawable> r0 = r13.wallpaperMemCache
            java.lang.Object r0 = r0.get(r2)
            android.graphics.drawable.Drawable r0 = (android.graphics.drawable.Drawable) r0
            if (r0 == 0) goto L_0x00e7
            org.telegram.messenger.LruCache<android.graphics.drawable.BitmapDrawable> r1 = r13.wallpaperMemCache
            r1.moveToFront(r2)
            goto L_0x00e7
        L_0x011b:
            if (r1 == 0) goto L_0x0133
            r3 = 1
            r4 = 1
            r0 = r37
            r5 = r15
            r0.setImageBitmapByKey(r1, r2, r3, r4, r5)
            r13.cancelLoadingForImageReceiver(r14, r7)
            if (r16 == 0) goto L_0x0131
            boolean r0 = r37.isForcePreview()
            if (r0 == 0) goto L_0x0131
            return
        L_0x0131:
            r0 = 1
            goto L_0x0134
        L_0x0133:
            r0 = 0
        L_0x0134:
            java.lang.Object r1 = r37.getParentObject()
            org.telegram.tgnet.TLRPC$Document r2 = r37.getQualityThumbDocument()
            org.telegram.messenger.ImageLocation r5 = r37.getThumbLocation()
            java.lang.String r6 = r37.getThumbFilter()
            org.telegram.messenger.ImageLocation r3 = r37.getMediaLocation()
            java.lang.String r12 = r37.getMediaFilter()
            org.telegram.messenger.ImageLocation r4 = r37.getImageLocation()
            java.lang.String r11 = r37.getImageFilter()
            if (r4 != 0) goto L_0x017a
            boolean r10 = r37.isNeedsQualityThumb()
            if (r10 == 0) goto L_0x017a
            boolean r10 = r37.isCurrentKeyQuality()
            if (r10 == 0) goto L_0x017a
            boolean r10 = r1 instanceof org.telegram.messenger.MessageObject
            if (r10 == 0) goto L_0x0173
            r2 = r1
            org.telegram.messenger.MessageObject r2 = (org.telegram.messenger.MessageObject) r2
            org.telegram.tgnet.TLRPC$Document r2 = r2.getDocument()
            org.telegram.messenger.ImageLocation r2 = org.telegram.messenger.ImageLocation.getForDocument(r2)
        L_0x0171:
            r10 = 1
            goto L_0x017c
        L_0x0173:
            if (r2 == 0) goto L_0x017a
            org.telegram.messenger.ImageLocation r2 = org.telegram.messenger.ImageLocation.getForDocument(r2)
            goto L_0x0171
        L_0x017a:
            r2 = r4
            r10 = 0
        L_0x017c:
            java.lang.String r17 = "mp4"
            r9 = 2
            if (r2 == 0) goto L_0x0188
            int r8 = r2.imageType
            if (r8 != r9) goto L_0x0188
            r8 = r17
            goto L_0x0189
        L_0x0188:
            r8 = 0
        L_0x0189:
            if (r3 == 0) goto L_0x0190
            int r7 = r3.imageType
            if (r7 != r9) goto L_0x0190
            goto L_0x0192
        L_0x0190:
            r17 = 0
        L_0x0192:
            java.lang.String r7 = r37.getExt()
            java.lang.String r9 = "jpg"
            if (r7 != 0) goto L_0x019b
            r7 = r9
        L_0x019b:
            if (r8 != 0) goto L_0x01a0
            r22 = r7
            goto L_0x01a2
        L_0x01a0:
            r22 = r8
        L_0x01a2:
            if (r17 != 0) goto L_0x01a6
            r17 = r7
        L_0x01a6:
            r8 = r2
            r25 = r3
            r26 = r4
            r2 = 0
            r3 = 0
            r4 = 0
            r23 = 0
            r24 = 0
            r27 = 0
        L_0x01b4:
            java.lang.String r13 = "."
            r14 = 2
            if (r2 >= r14) goto L_0x0369
            if (r2 != 0) goto L_0x01c1
            r14 = r8
            r28 = r15
            r15 = r22
            goto L_0x01c7
        L_0x01c1:
            r28 = r15
            r15 = r17
            r14 = r25
        L_0x01c7:
            if (r14 != 0) goto L_0x01ce
            r29 = r0
            r30 = r8
            goto L_0x01e1
        L_0x01ce:
            r29 = r0
            if (r25 == 0) goto L_0x01d7
            r30 = r8
            r0 = r25
            goto L_0x01da
        L_0x01d7:
            r0 = r8
            r30 = r0
        L_0x01da:
            r8 = 0
            java.lang.String r0 = r14.getKey(r1, r0, r8)
            if (r0 != 0) goto L_0x01eb
        L_0x01e1:
            r31 = r6
            r32 = r11
            r33 = r12
        L_0x01e7:
            r8 = r30
            goto L_0x0359
        L_0x01eb:
            r31 = r6
            if (r25 == 0) goto L_0x01f2
            r8 = r25
            goto L_0x01f4
        L_0x01f2:
            r8 = r30
        L_0x01f4:
            r6 = 1
            java.lang.String r8 = r14.getKey(r1, r8, r6)
            java.lang.String r6 = r14.path
            if (r6 == 0) goto L_0x021b
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            r6.append(r8)
            r6.append(r13)
            java.lang.String r8 = r14.path
            java.lang.String r8 = getHttpUrlExtension(r8, r9)
            r6.append(r8)
            java.lang.String r8 = r6.toString()
            r32 = r11
            r33 = r12
            goto L_0x0340
        L_0x021b:
            org.telegram.tgnet.TLRPC$PhotoSize r6 = r14.photoSize
            r32 = r11
            boolean r11 = r6 instanceof org.telegram.tgnet.TLRPC$TL_photoStrippedSize
            if (r11 != 0) goto L_0x032c
            boolean r6 = r6 instanceof org.telegram.tgnet.TLRPC$TL_photoPathSize
            if (r6 == 0) goto L_0x0229
            goto L_0x032c
        L_0x0229:
            org.telegram.tgnet.TLRPC$TL_fileLocationToBeDeprecated r6 = r14.location
            if (r6 == 0) goto L_0x0261
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            r6.append(r8)
            r6.append(r13)
            r6.append(r15)
            java.lang.String r8 = r6.toString()
            java.lang.String r6 = r37.getExt()
            if (r6 != 0) goto L_0x025b
            org.telegram.tgnet.TLRPC$TL_fileLocationToBeDeprecated r6 = r14.location
            byte[] r11 = r6.key
            if (r11 != 0) goto L_0x025b
            r33 = r12
            long r11 = r6.volume_id
            r34 = -2147483648(0xfffffffvar_, double:NaN)
            int r13 = (r11 > r34 ? 1 : (r11 == r34 ? 0 : -1))
            if (r13 != 0) goto L_0x0340
            int r6 = r6.local_id
            if (r6 >= 0) goto L_0x0340
            goto L_0x025d
        L_0x025b:
            r33 = r12
        L_0x025d:
            r27 = 1
            goto L_0x0340
        L_0x0261:
            r33 = r12
            org.telegram.messenger.WebFile r6 = r14.webFile
            if (r6 == 0) goto L_0x0289
            java.lang.String r6 = r6.mime_type
            java.lang.String r6 = org.telegram.messenger.FileLoader.getMimeTypePart(r6)
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            r11.append(r8)
            r11.append(r13)
            org.telegram.messenger.WebFile r8 = r14.webFile
            java.lang.String r8 = r8.url
            java.lang.String r6 = getHttpUrlExtension(r8, r6)
            r11.append(r6)
            java.lang.String r8 = r11.toString()
            goto L_0x0340
        L_0x0289:
            org.telegram.messenger.SecureDocument r6 = r14.secureDocument
            if (r6 == 0) goto L_0x02a1
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            r6.append(r8)
            r6.append(r13)
            r6.append(r15)
            java.lang.String r8 = r6.toString()
            goto L_0x0340
        L_0x02a1:
            org.telegram.tgnet.TLRPC$Document r6 = r14.document
            if (r6 == 0) goto L_0x0340
            if (r2 != 0) goto L_0x02ba
            if (r10 == 0) goto L_0x02ba
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r11 = "q_"
            r6.append(r11)
            r6.append(r0)
            java.lang.String r0 = r6.toString()
        L_0x02ba:
            org.telegram.tgnet.TLRPC$Document r6 = r14.document
            java.lang.String r6 = org.telegram.messenger.FileLoader.getDocumentFileName(r6)
            r11 = 46
            int r11 = r6.lastIndexOf(r11)
            r12 = -1
            java.lang.String r13 = ""
            if (r11 != r12) goto L_0x02cd
            r6 = r13
            goto L_0x02d1
        L_0x02cd:
            java.lang.String r6 = r6.substring(r11)
        L_0x02d1:
            int r11 = r6.length()
            r12 = 1
            if (r11 > r12) goto L_0x02f6
            org.telegram.tgnet.TLRPC$Document r6 = r14.document
            java.lang.String r6 = r6.mime_type
            java.lang.String r11 = "video/mp4"
            boolean r6 = r11.equals(r6)
            if (r6 == 0) goto L_0x02e7
            java.lang.String r13 = ".mp4"
            goto L_0x02f7
        L_0x02e7:
            org.telegram.tgnet.TLRPC$Document r6 = r14.document
            java.lang.String r6 = r6.mime_type
            java.lang.String r11 = "video/x-matroska"
            boolean r6 = r11.equals(r6)
            if (r6 == 0) goto L_0x02f7
            java.lang.String r13 = ".mkv"
            goto L_0x02f7
        L_0x02f6:
            r13 = r6
        L_0x02f7:
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            r6.append(r8)
            r6.append(r13)
            java.lang.String r8 = r6.toString()
            org.telegram.tgnet.TLRPC$Document r6 = r14.document
            boolean r6 = org.telegram.messenger.MessageObject.isVideoDocument(r6)
            if (r6 != 0) goto L_0x0328
            org.telegram.tgnet.TLRPC$Document r6 = r14.document
            boolean r6 = org.telegram.messenger.MessageObject.isGifDocument((org.telegram.tgnet.TLRPC$Document) r6)
            if (r6 != 0) goto L_0x0328
            org.telegram.tgnet.TLRPC$Document r6 = r14.document
            boolean r6 = org.telegram.messenger.MessageObject.isRoundVideoDocument(r6)
            if (r6 != 0) goto L_0x0328
            org.telegram.tgnet.TLRPC$Document r6 = r14.document
            boolean r6 = org.telegram.messenger.MessageObject.canPreviewDocument(r6)
            if (r6 != 0) goto L_0x0328
            r6 = 1
            goto L_0x0329
        L_0x0328:
            r6 = 0
        L_0x0329:
            r27 = r6
            goto L_0x0340
        L_0x032c:
            r33 = r12
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            r6.append(r8)
            r6.append(r13)
            r6.append(r15)
            java.lang.String r8 = r6.toString()
        L_0x0340:
            if (r2 != 0) goto L_0x0346
            r4 = r0
            r23 = r8
            goto L_0x0349
        L_0x0346:
            r3 = r0
            r24 = r8
        L_0x0349:
            if (r14 != r5) goto L_0x01e7
            if (r2 != 0) goto L_0x0352
            r4 = 0
            r8 = 0
            r23 = 0
            goto L_0x0359
        L_0x0352:
            r8 = r30
            r3 = 0
            r24 = 0
            r25 = 0
        L_0x0359:
            int r2 = r2 + 1
            r14 = r37
            r15 = r28
            r0 = r29
            r6 = r31
            r11 = r32
            r12 = r33
            goto L_0x01b4
        L_0x0369:
            r29 = r0
            r31 = r6
            r30 = r8
            r32 = r11
            r33 = r12
            r28 = r15
            if (r5 == 0) goto L_0x03e2
            org.telegram.messenger.ImageLocation r0 = r37.getStrippedLocation()
            if (r0 != 0) goto L_0x0383
            if (r25 == 0) goto L_0x0381
            r26 = r25
        L_0x0381:
            r0 = r26
        L_0x0383:
            r2 = 0
            java.lang.String r2 = r5.getKey(r1, r0, r2)
            r6 = 1
            java.lang.String r0 = r5.getKey(r1, r0, r6)
            java.lang.String r1 = r5.path
            if (r1 == 0) goto L_0x03ad
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r1.append(r0)
            r1.append(r13)
            java.lang.String r0 = r5.path
            java.lang.String r0 = getHttpUrlExtension(r0, r9)
            r1.append(r0)
            java.lang.String r0 = r1.toString()
        L_0x03a9:
            r18 = r0
            r9 = r2
            goto L_0x03e6
        L_0x03ad:
            org.telegram.tgnet.TLRPC$PhotoSize r1 = r5.photoSize
            boolean r8 = r1 instanceof org.telegram.tgnet.TLRPC$TL_photoStrippedSize
            if (r8 != 0) goto L_0x03cf
            boolean r1 = r1 instanceof org.telegram.tgnet.TLRPC$TL_photoPathSize
            if (r1 == 0) goto L_0x03b8
            goto L_0x03cf
        L_0x03b8:
            org.telegram.tgnet.TLRPC$TL_fileLocationToBeDeprecated r1 = r5.location
            if (r1 == 0) goto L_0x03a9
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r1.append(r0)
            r1.append(r13)
            r1.append(r7)
            java.lang.String r0 = r1.toString()
            goto L_0x03a9
        L_0x03cf:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r1.append(r0)
            r1.append(r13)
            r1.append(r7)
            java.lang.String r0 = r1.toString()
            goto L_0x03a9
        L_0x03e2:
            r6 = 1
            r9 = 0
            r18 = 0
        L_0x03e6:
            java.lang.String r0 = "@"
            if (r3 == 0) goto L_0x0402
            if (r33 == 0) goto L_0x0402
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r1.append(r3)
            r1.append(r0)
            r12 = r33
            r1.append(r12)
            java.lang.String r1 = r1.toString()
            r13 = r1
            goto L_0x0405
        L_0x0402:
            r12 = r33
            r13 = r3
        L_0x0405:
            if (r4 == 0) goto L_0x041e
            if (r32 == 0) goto L_0x041e
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r1.append(r4)
            r1.append(r0)
            r11 = r32
            r1.append(r11)
            java.lang.String r4 = r1.toString()
            goto L_0x0420
        L_0x041e:
            r11 = r32
        L_0x0420:
            if (r9 == 0) goto L_0x043a
            if (r31 == 0) goto L_0x043a
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r1.append(r9)
            r1.append(r0)
            r8 = r31
            r1.append(r8)
            java.lang.String r0 = r1.toString()
            r2 = r0
            goto L_0x043d
        L_0x043a:
            r8 = r31
            r2 = r9
        L_0x043d:
            java.lang.String r0 = r37.getUniqKeyPrefix()
            if (r0 == 0) goto L_0x0458
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = r37.getUniqKeyPrefix()
            r0.append(r1)
            r0.append(r4)
            java.lang.String r0 = r0.toString()
            r14 = r0
            goto L_0x0459
        L_0x0458:
            r14 = r4
        L_0x0459:
            if (r30 == 0) goto L_0x0499
            r9 = r30
            java.lang.String r0 = r9.path
            if (r0 == 0) goto L_0x0495
            r12 = 0
            r10 = 1
            r15 = 1
            if (r29 == 0) goto L_0x046a
            r21 = 2
            goto L_0x046c
        L_0x046a:
            r21 = 1
        L_0x046c:
            r0 = r36
            r1 = r37
            r3 = r18
            r4 = r7
            r6 = r8
            r19 = r9
            r7 = r12
            r9 = r10
            r10 = r15
            r15 = r11
            r11 = r21
            r12 = r28
            r0.createLoadOperationForImageReceiver(r1, r2, r3, r4, r5, r6, r7, r9, r10, r11, r12)
            long r7 = r37.getSize()
            r9 = 1
            r10 = 0
            r11 = 0
            r2 = r14
            r3 = r23
            r4 = r22
            r5 = r19
            r6 = r15
            r0.createLoadOperationForImageReceiver(r1, r2, r3, r4, r5, r6, r7, r9, r10, r11, r12)
            goto L_0x0543
        L_0x0495:
            r19 = r9
            r15 = r11
            goto L_0x049c
        L_0x0499:
            r15 = r11
            r19 = r30
        L_0x049c:
            if (r25 == 0) goto L_0x0506
            int r0 = r37.getCacheType()
            r20 = 1
            if (r0 != 0) goto L_0x04ab
            if (r27 == 0) goto L_0x04ab
            r21 = 1
            goto L_0x04ad
        L_0x04ab:
            r21 = r0
        L_0x04ad:
            if (r21 != 0) goto L_0x04b1
            r9 = 1
            goto L_0x04b3
        L_0x04b1:
            r9 = r21
        L_0x04b3:
            if (r29 != 0) goto L_0x04d0
            r10 = 0
            r26 = 1
            r27 = 1
            r0 = r36
            r1 = r37
            r3 = r18
            r4 = r7
            r6 = r8
            r7 = r10
            r10 = r26
            r11 = r27
            r18 = r12
            r12 = r28
            r0.createLoadOperationForImageReceiver(r1, r2, r3, r4, r5, r6, r7, r9, r10, r11, r12)
            goto L_0x04d2
        L_0x04d0:
            r18 = r12
        L_0x04d2:
            if (r16 != 0) goto L_0x04eb
            r7 = 0
            r10 = 0
            r11 = 0
            r0 = r36
            r1 = r37
            r2 = r14
            r3 = r23
            r4 = r22
            r5 = r19
            r6 = r15
            r9 = r20
            r12 = r28
            r0.createLoadOperationForImageReceiver(r1, r2, r3, r4, r5, r6, r7, r9, r10, r11, r12)
        L_0x04eb:
            long r7 = r37.getSize()
            r10 = 3
            r11 = 0
            r0 = r36
            r1 = r37
            r2 = r13
            r3 = r24
            r4 = r17
            r5 = r25
            r6 = r18
            r9 = r21
            r12 = r28
            r0.createLoadOperationForImageReceiver(r1, r2, r3, r4, r5, r6, r7, r9, r10, r11, r12)
            goto L_0x0543
        L_0x0506:
            int r0 = r37.getCacheType()
            if (r0 != 0) goto L_0x0510
            if (r27 == 0) goto L_0x0510
            r13 = 1
            goto L_0x0511
        L_0x0510:
            r13 = r0
        L_0x0511:
            if (r13 != 0) goto L_0x0515
            r9 = 1
            goto L_0x0516
        L_0x0515:
            r9 = r13
        L_0x0516:
            r10 = 0
            r12 = 1
            if (r29 == 0) goto L_0x051e
            r21 = 2
            goto L_0x0520
        L_0x051e:
            r21 = 1
        L_0x0520:
            r0 = r36
            r1 = r37
            r3 = r18
            r4 = r7
            r6 = r8
            r7 = r10
            r10 = r12
            r11 = r21
            r12 = r28
            r0.createLoadOperationForImageReceiver(r1, r2, r3, r4, r5, r6, r7, r9, r10, r11, r12)
            long r7 = r37.getSize()
            r10 = 0
            r11 = 0
            r2 = r14
            r3 = r23
            r4 = r22
            r5 = r19
            r6 = r15
            r9 = r13
            r0.createLoadOperationForImageReceiver(r1, r2, r3, r4, r5, r6, r7, r9, r10, r11, r12)
        L_0x0543:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.telegram.messenger.ImageLoader.loadImageForImageReceiver(org.telegram.messenger.ImageReceiver):void");
    }

    /* access modifiers changed from: private */
    public BitmapDrawable getFromLottieCache(String str) {
        BitmapDrawable bitmapDrawable = this.lottieMemCache.get(str);
        if (!(bitmapDrawable instanceof AnimatedFileDrawable) || !((AnimatedFileDrawable) bitmapDrawable).isRecycled()) {
            return bitmapDrawable;
        }
        this.lottieMemCache.remove(str);
        return null;
    }

    private boolean useLottieMemCache(ImageLocation imageLocation, String str) {
        return (imageLocation != null && (MessageObject.isAnimatedStickerDocument(imageLocation.document, true) || imageLocation.imageType == 1 || MessageObject.isVideoSticker(imageLocation.document))) || isAnimatedAvatar(str);
    }

    /* access modifiers changed from: private */
    public void httpFileLoadError(String str) {
        this.imageLoadQueue.postRunnable(new ImageLoader$$ExternalSyntheticLambda6(this, str));
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$httpFileLoadError$8(String str) {
        CacheImage cacheImage = this.imageLoadingByUrl.get(str);
        if (cacheImage != null) {
            HttpImageTask httpImageTask = cacheImage.httpTask;
            if (httpImageTask != null) {
                HttpImageTask httpImageTask2 = new HttpImageTask(httpImageTask.cacheImage, httpImageTask.imageSize);
                cacheImage.httpTask = httpImageTask2;
                this.httpTasks.add(httpImageTask2);
            }
            runHttpTasks(false);
        }
    }

    /* access modifiers changed from: private */
    public void artworkLoadError(String str) {
        this.imageLoadQueue.postRunnable(new ImageLoader$$ExternalSyntheticLambda5(this, str));
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$artworkLoadError$9(String str) {
        CacheImage cacheImage = this.imageLoadingByUrl.get(str);
        if (cacheImage != null) {
            ArtworkLoadTask artworkLoadTask = cacheImage.artworkTask;
            if (artworkLoadTask != null) {
                ArtworkLoadTask artworkLoadTask2 = new ArtworkLoadTask(artworkLoadTask.cacheImage);
                cacheImage.artworkTask = artworkLoadTask2;
                this.artworkTasks.add(artworkLoadTask2);
            }
            runArtworkTasks(false);
        }
    }

    /* access modifiers changed from: private */
    public void fileDidLoaded(String str, File file, int i) {
        this.imageLoadQueue.postRunnable(new ImageLoader$$ExternalSyntheticLambda8(this, str, i, file));
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$fileDidLoaded$10(String str, int i, File file) {
        ThumbGenerateInfo thumbGenerateInfo = this.waitingForQualityThumb.get(str);
        if (!(thumbGenerateInfo == null || thumbGenerateInfo.parentDocument == null)) {
            generateThumb(i, file, thumbGenerateInfo);
            this.waitingForQualityThumb.remove(str);
        }
        CacheImage cacheImage = this.imageLoadingByUrl.get(str);
        if (cacheImage != null) {
            this.imageLoadingByUrl.remove(str);
            ArrayList arrayList = new ArrayList();
            for (int i2 = 0; i2 < cacheImage.imageReceiverArray.size(); i2++) {
                String str2 = cacheImage.keys.get(i2);
                String str3 = cacheImage.filters.get(i2);
                int intValue = cacheImage.types.get(i2).intValue();
                ImageReceiver imageReceiver = cacheImage.imageReceiverArray.get(i2);
                int intValue2 = cacheImage.imageReceiverGuidsArray.get(i2).intValue();
                CacheImage cacheImage2 = this.imageLoadingByKeys.get(str2);
                if (cacheImage2 == null) {
                    cacheImage2 = new CacheImage();
                    cacheImage2.secureDocument = cacheImage.secureDocument;
                    cacheImage2.currentAccount = cacheImage.currentAccount;
                    cacheImage2.finalFilePath = file;
                    cacheImage2.parentObject = cacheImage.parentObject;
                    cacheImage2.key = str2;
                    cacheImage2.imageLocation = cacheImage.imageLocation;
                    cacheImage2.type = intValue;
                    cacheImage2.ext = cacheImage.ext;
                    cacheImage2.encryptionKeyPath = cacheImage.encryptionKeyPath;
                    cacheImage2.cacheTask = new CacheOutTask(cacheImage2);
                    cacheImage2.filter = str3;
                    cacheImage2.imageType = cacheImage.imageType;
                    this.imageLoadingByKeys.put(str2, cacheImage2);
                    arrayList.add(cacheImage2.cacheTask);
                }
                cacheImage2.addImageReceiver(imageReceiver, str2, str3, intValue, intValue2);
            }
            for (int i3 = 0; i3 < arrayList.size(); i3++) {
                CacheOutTask cacheOutTask = (CacheOutTask) arrayList.get(i3);
                if (cacheOutTask.cacheImage.type == 1) {
                    this.cacheThumbOutQueue.postRunnable(cacheOutTask);
                } else {
                    this.cacheOutQueue.postRunnable(cacheOutTask);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void fileDidFailedLoad(String str, int i) {
        if (i != 1) {
            this.imageLoadQueue.postRunnable(new ImageLoader$$ExternalSyntheticLambda3(this, str));
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$fileDidFailedLoad$11(String str) {
        CacheImage cacheImage = this.imageLoadingByUrl.get(str);
        if (cacheImage != null) {
            cacheImage.setImageAndClear((Drawable) null, (String) null);
        }
    }

    /* access modifiers changed from: private */
    public void runHttpTasks(boolean z) {
        if (z) {
            this.currentHttpTasksCount--;
        }
        while (this.currentHttpTasksCount < 4 && !this.httpTasks.isEmpty()) {
            HttpImageTask poll = this.httpTasks.poll();
            if (poll != null) {
                poll.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[]{null, null, null});
                this.currentHttpTasksCount++;
            }
        }
    }

    /* access modifiers changed from: private */
    public void runArtworkTasks(boolean z) {
        if (z) {
            this.currentArtworkTasksCount--;
        }
        while (this.currentArtworkTasksCount < 4 && !this.artworkTasks.isEmpty()) {
            try {
                this.artworkTasks.poll().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[]{null, null, null});
                this.currentArtworkTasksCount++;
            } catch (Throwable unused) {
                runArtworkTasks(false);
            }
        }
    }

    public boolean isLoadingHttpFile(String str) {
        return this.httpFileLoadTasksByKeys.containsKey(str);
    }

    public static String getHttpFileName(String str) {
        return Utilities.MD5(str);
    }

    public static File getHttpFilePath(String str, String str2) {
        String httpUrlExtension = getHttpUrlExtension(str, str2);
        File directory = FileLoader.getDirectory(4);
        return new File(directory, Utilities.MD5(str) + "." + httpUrlExtension);
    }

    public void loadHttpFile(String str, String str2, int i) {
        if (str != null && str.length() != 0 && !this.httpFileLoadTasksByKeys.containsKey(str)) {
            String httpUrlExtension = getHttpUrlExtension(str, str2);
            File directory = FileLoader.getDirectory(4);
            File file = new File(directory, Utilities.MD5(str) + "_temp." + httpUrlExtension);
            file.delete();
            HttpFileTask httpFileTask = new HttpFileTask(str, file, httpUrlExtension, i);
            this.httpFileLoadTasks.add(httpFileTask);
            this.httpFileLoadTasksByKeys.put(str, httpFileTask);
            runHttpFileLoadTasks((HttpFileTask) null, 0);
        }
    }

    public void cancelLoadHttpFile(String str) {
        HttpFileTask httpFileTask = this.httpFileLoadTasksByKeys.get(str);
        if (httpFileTask != null) {
            httpFileTask.cancel(true);
            this.httpFileLoadTasksByKeys.remove(str);
            this.httpFileLoadTasks.remove(httpFileTask);
        }
        Runnable runnable = this.retryHttpsTasks.get(str);
        if (runnable != null) {
            AndroidUtilities.cancelRunOnUIThread(runnable);
        }
        runHttpFileLoadTasks((HttpFileTask) null, 0);
    }

    /* access modifiers changed from: private */
    public void runHttpFileLoadTasks(HttpFileTask httpFileTask, int i) {
        AndroidUtilities.runOnUIThread(new ImageLoader$$ExternalSyntheticLambda11(this, httpFileTask, i));
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$runHttpFileLoadTasks$13(HttpFileTask httpFileTask, int i) {
        if (httpFileTask != null) {
            this.currentHttpFileLoadTasksCount--;
        }
        if (httpFileTask != null) {
            if (i == 1) {
                if (httpFileTask.canRetry) {
                    ImageLoader$$ExternalSyntheticLambda10 imageLoader$$ExternalSyntheticLambda10 = new ImageLoader$$ExternalSyntheticLambda10(this, new HttpFileTask(httpFileTask.url, httpFileTask.tempFile, httpFileTask.ext, httpFileTask.currentAccount));
                    this.retryHttpsTasks.put(httpFileTask.url, imageLoader$$ExternalSyntheticLambda10);
                    AndroidUtilities.runOnUIThread(imageLoader$$ExternalSyntheticLambda10, 1000);
                } else {
                    this.httpFileLoadTasksByKeys.remove(httpFileTask.url);
                    NotificationCenter.getInstance(httpFileTask.currentAccount).postNotificationName(NotificationCenter.httpFileDidFailedLoad, httpFileTask.url, 0);
                }
            } else if (i == 2) {
                this.httpFileLoadTasksByKeys.remove(httpFileTask.url);
                File file = new File(FileLoader.getDirectory(4), Utilities.MD5(httpFileTask.url) + "." + httpFileTask.ext);
                if (!httpFileTask.tempFile.renameTo(file)) {
                    file = httpFileTask.tempFile;
                }
                NotificationCenter.getInstance(httpFileTask.currentAccount).postNotificationName(NotificationCenter.httpFileDidLoad, httpFileTask.url, file.toString());
            }
        }
        while (this.currentHttpFileLoadTasksCount < 2 && !this.httpFileLoadTasks.isEmpty()) {
            this.httpFileLoadTasks.poll().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[]{null, null, null});
            this.currentHttpFileLoadTasksCount++;
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$runHttpFileLoadTasks$12(HttpFileTask httpFileTask) {
        this.httpFileLoadTasks.add(httpFileTask);
        runHttpFileLoadTasks((HttpFileTask) null, 0);
    }

    public static boolean shouldSendImageAsDocument(String str, Uri uri) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        if (!(str != null || uri == null || uri.getScheme() == null)) {
            if (uri.getScheme().contains("file")) {
                str = uri.getPath();
            } else {
                try {
                    str = AndroidUtilities.getPath(uri);
                } catch (Throwable th) {
                    FileLog.e(th);
                }
            }
        }
        if (str != null) {
            BitmapFactory.decodeFile(str, options);
        } else if (uri != null) {
            try {
                InputStream openInputStream = ApplicationLoader.applicationContext.getContentResolver().openInputStream(uri);
                BitmapFactory.decodeStream(openInputStream, (Rect) null, options);
                openInputStream.close();
            } catch (Throwable th2) {
                FileLog.e(th2);
                return false;
            }
        }
        float f = (float) options.outWidth;
        float f2 = (float) options.outHeight;
        if (f / f2 > 10.0f || f2 / f > 10.0f) {
            return true;
        }
        return false;
    }

    /* JADX WARNING: Missing exception handler attribute for start block: B:60:0x00d2 */
    /* JADX WARNING: Removed duplicated region for block: B:120:0x0177  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0072  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0077  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0081  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x008d  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x009e  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00a0  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00a7 A[SYNTHETIC, Splitter:B:46:0x00a7] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x00b1  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x00d6  */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x00f5 A[SYNTHETIC, Splitter:B:81:0x00f5] */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x0109  */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x0116 A[SYNTHETIC, Splitter:B:93:0x0116] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.graphics.Bitmap loadBitmap(java.lang.String r11, android.net.Uri r12, float r13, float r14, boolean r15) {
        /*
            android.graphics.BitmapFactory$Options r0 = new android.graphics.BitmapFactory$Options
            r0.<init>()
            r1 = 1
            r0.inJustDecodeBounds = r1
            if (r11 != 0) goto L_0x003e
            if (r12 == 0) goto L_0x003e
            java.lang.String r2 = r12.getScheme()
            if (r2 == 0) goto L_0x003e
            java.lang.String r2 = r12.getScheme()
            java.lang.String r3 = "file"
            boolean r2 = r2.contains(r3)
            if (r2 == 0) goto L_0x0023
            java.lang.String r11 = r12.getPath()
            goto L_0x003e
        L_0x0023:
            int r2 = android.os.Build.VERSION.SDK_INT
            r3 = 30
            if (r2 < r3) goto L_0x0035
            java.lang.String r2 = r12.getScheme()
            java.lang.String r3 = "content"
            boolean r2 = r3.equals(r2)
            if (r2 != 0) goto L_0x003e
        L_0x0035:
            java.lang.String r11 = org.telegram.messenger.AndroidUtilities.getPath(r12)     // Catch:{ all -> 0x003a }
            goto L_0x003e
        L_0x003a:
            r2 = move-exception
            org.telegram.messenger.FileLog.e((java.lang.Throwable) r2)
        L_0x003e:
            r2 = 0
            if (r11 == 0) goto L_0x0045
            android.graphics.BitmapFactory.decodeFile(r11, r0)
            goto L_0x0067
        L_0x0045:
            if (r12 == 0) goto L_0x0067
            android.content.Context r3 = org.telegram.messenger.ApplicationLoader.applicationContext     // Catch:{ all -> 0x0062 }
            android.content.ContentResolver r3 = r3.getContentResolver()     // Catch:{ all -> 0x0062 }
            java.io.InputStream r3 = r3.openInputStream(r12)     // Catch:{ all -> 0x0062 }
            android.graphics.BitmapFactory.decodeStream(r3, r2, r0)     // Catch:{ all -> 0x0062 }
            r3.close()     // Catch:{ all -> 0x0062 }
            android.content.Context r3 = org.telegram.messenger.ApplicationLoader.applicationContext     // Catch:{ all -> 0x0062 }
            android.content.ContentResolver r3 = r3.getContentResolver()     // Catch:{ all -> 0x0062 }
            java.io.InputStream r3 = r3.openInputStream(r12)     // Catch:{ all -> 0x0062 }
            goto L_0x0068
        L_0x0062:
            r11 = move-exception
            org.telegram.messenger.FileLog.e((java.lang.Throwable) r11)
            return r2
        L_0x0067:
            r3 = r2
        L_0x0068:
            int r4 = r0.outWidth
            float r4 = (float) r4
            int r5 = r0.outHeight
            float r5 = (float) r5
            float r4 = r4 / r13
            float r5 = r5 / r14
            if (r15 == 0) goto L_0x0077
            float r13 = java.lang.Math.max(r4, r5)
            goto L_0x007b
        L_0x0077:
            float r13 = java.lang.Math.min(r4, r5)
        L_0x007b:
            r14 = 1065353216(0x3var_, float:1.0)
            int r15 = (r13 > r14 ? 1 : (r13 == r14 ? 0 : -1))
            if (r15 >= 0) goto L_0x0083
            r13 = 1065353216(0x3var_, float:1.0)
        L_0x0083:
            r15 = 0
            r0.inJustDecodeBounds = r15
            int r4 = (int) r13
            r0.inSampleSize = r4
            int r4 = r4 % 2
            if (r4 == 0) goto L_0x0098
            r4 = 1
        L_0x008e:
            int r5 = r4 * 2
            int r6 = r0.inSampleSize
            if (r5 >= r6) goto L_0x0096
            r4 = r5
            goto L_0x008e
        L_0x0096:
            r0.inSampleSize = r4
        L_0x0098:
            int r4 = android.os.Build.VERSION.SDK_INT
            r5 = 21
            if (r4 >= r5) goto L_0x00a0
            r4 = 1
            goto L_0x00a1
        L_0x00a0:
            r4 = 0
        L_0x00a1:
            r0.inPurgeable = r4
            java.lang.String r4 = "Orientation"
            if (r11 == 0) goto L_0x00b1
            androidx.exifinterface.media.ExifInterface r15 = new androidx.exifinterface.media.ExifInterface     // Catch:{ all -> 0x00dd }
            r15.<init>((java.lang.String) r11)     // Catch:{ all -> 0x00dd }
            int r15 = r15.getAttributeInt(r4, r1)     // Catch:{ all -> 0x00dd }
            goto L_0x00d3
        L_0x00b1:
            if (r12 == 0) goto L_0x00d3
            android.content.Context r5 = org.telegram.messenger.ApplicationLoader.applicationContext     // Catch:{ all -> 0x00d3 }
            android.content.ContentResolver r5 = r5.getContentResolver()     // Catch:{ all -> 0x00d3 }
            java.io.InputStream r5 = r5.openInputStream(r12)     // Catch:{ all -> 0x00d3 }
            androidx.exifinterface.media.ExifInterface r6 = new androidx.exifinterface.media.ExifInterface     // Catch:{ all -> 0x00cc }
            r6.<init>((java.io.InputStream) r5)     // Catch:{ all -> 0x00cc }
            int r15 = r6.getAttributeInt(r4, r1)     // Catch:{ all -> 0x00cc }
            if (r5 == 0) goto L_0x00d3
            r5.close()     // Catch:{ all -> 0x00d3 }
            goto L_0x00d3
        L_0x00cc:
            r1 = move-exception
            if (r5 == 0) goto L_0x00d2
            r5.close()     // Catch:{ all -> 0x00d2 }
        L_0x00d2:
            throw r1     // Catch:{ all -> 0x00d3 }
        L_0x00d3:
            r1 = 3
            if (r15 == r1) goto L_0x00f5
            r1 = 6
            if (r15 == r1) goto L_0x00ea
            r1 = 8
            if (r15 == r1) goto L_0x00df
        L_0x00dd:
            r15 = r2
            goto L_0x0101
        L_0x00df:
            android.graphics.Matrix r15 = new android.graphics.Matrix     // Catch:{ all -> 0x00dd }
            r15.<init>()     // Catch:{ all -> 0x00dd }
            r1 = 1132920832(0x43870000, float:270.0)
            r15.postRotate(r1)     // Catch:{ all -> 0x0100 }
            goto L_0x0101
        L_0x00ea:
            android.graphics.Matrix r15 = new android.graphics.Matrix     // Catch:{ all -> 0x00dd }
            r15.<init>()     // Catch:{ all -> 0x00dd }
            r1 = 1119092736(0x42b40000, float:90.0)
            r15.postRotate(r1)     // Catch:{ all -> 0x0100 }
            goto L_0x0101
        L_0x00f5:
            android.graphics.Matrix r15 = new android.graphics.Matrix     // Catch:{ all -> 0x00dd }
            r15.<init>()     // Catch:{ all -> 0x00dd }
            r1 = 1127481344(0x43340000, float:180.0)
            r15.postRotate(r1)     // Catch:{ all -> 0x0100 }
            goto L_0x0101
        L_0x0100:
        L_0x0101:
            int r1 = r0.inSampleSize
            float r1 = (float) r1
            float r13 = r13 / r1
            int r1 = (r13 > r14 ? 1 : (r13 == r14 ? 0 : -1))
            if (r1 <= 0) goto L_0x0114
            if (r15 != 0) goto L_0x0110
            android.graphics.Matrix r15 = new android.graphics.Matrix
            r15.<init>()
        L_0x0110:
            float r14 = r14 / r13
            r15.postScale(r14, r14)
        L_0x0114:
            if (r11 == 0) goto L_0x0177
            android.graphics.Bitmap r2 = android.graphics.BitmapFactory.decodeFile(r11, r0)     // Catch:{ all -> 0x013c }
            if (r2 == 0) goto L_0x01bd
            boolean r12 = r0.inPurgeable     // Catch:{ all -> 0x013c }
            if (r12 == 0) goto L_0x0123
            org.telegram.messenger.Utilities.pinBitmap(r2)     // Catch:{ all -> 0x013c }
        L_0x0123:
            r5 = 0
            r6 = 0
            int r7 = r2.getWidth()     // Catch:{ all -> 0x013c }
            int r8 = r2.getHeight()     // Catch:{ all -> 0x013c }
            r10 = 1
            r4 = r2
            r9 = r15
            android.graphics.Bitmap r12 = org.telegram.messenger.Bitmaps.createBitmap(r4, r5, r6, r7, r8, r9, r10)     // Catch:{ all -> 0x013c }
            if (r12 == r2) goto L_0x01bd
            r2.recycle()     // Catch:{ all -> 0x013c }
            r2 = r12
            goto L_0x01bd
        L_0x013c:
            r12 = move-exception
            org.telegram.messenger.FileLog.e((java.lang.Throwable) r12)
            org.telegram.messenger.ImageLoader r12 = getInstance()
            r12.clearMemory()
            if (r2 != 0) goto L_0x0159
            android.graphics.Bitmap r2 = android.graphics.BitmapFactory.decodeFile(r11, r0)     // Catch:{ all -> 0x0157 }
            if (r2 == 0) goto L_0x0159
            boolean r11 = r0.inPurgeable     // Catch:{ all -> 0x0157 }
            if (r11 == 0) goto L_0x0159
            org.telegram.messenger.Utilities.pinBitmap(r2)     // Catch:{ all -> 0x0157 }
            goto L_0x0159
        L_0x0157:
            r11 = move-exception
            goto L_0x0173
        L_0x0159:
            if (r2 == 0) goto L_0x01bd
            r5 = 0
            r6 = 0
            int r7 = r2.getWidth()     // Catch:{ all -> 0x0157 }
            int r8 = r2.getHeight()     // Catch:{ all -> 0x0157 }
            r10 = 1
            r4 = r2
            r9 = r15
            android.graphics.Bitmap r11 = org.telegram.messenger.Bitmaps.createBitmap(r4, r5, r6, r7, r8, r9, r10)     // Catch:{ all -> 0x0157 }
            if (r11 == r2) goto L_0x01bd
            r2.recycle()     // Catch:{ all -> 0x0157 }
            r2 = r11
            goto L_0x01bd
        L_0x0173:
            org.telegram.messenger.FileLog.e((java.lang.Throwable) r11)
            goto L_0x01bd
        L_0x0177:
            if (r12 == 0) goto L_0x01bd
            android.graphics.Bitmap r11 = android.graphics.BitmapFactory.decodeStream(r3, r2, r0)     // Catch:{ all -> 0x01ab }
            if (r11 == 0) goto L_0x01a1
            boolean r12 = r0.inPurgeable     // Catch:{ all -> 0x019e }
            if (r12 == 0) goto L_0x0186
            org.telegram.messenger.Utilities.pinBitmap(r11)     // Catch:{ all -> 0x019e }
        L_0x0186:
            r5 = 0
            r6 = 0
            int r7 = r11.getWidth()     // Catch:{ all -> 0x019e }
            int r8 = r11.getHeight()     // Catch:{ all -> 0x019e }
            r10 = 1
            r4 = r11
            r9 = r15
            android.graphics.Bitmap r12 = org.telegram.messenger.Bitmaps.createBitmap(r4, r5, r6, r7, r8, r9, r10)     // Catch:{ all -> 0x019e }
            if (r12 == r11) goto L_0x01a1
            r11.recycle()     // Catch:{ all -> 0x019e }
            r2 = r12
            goto L_0x01a2
        L_0x019e:
            r12 = move-exception
            r2 = r11
            goto L_0x01ac
        L_0x01a1:
            r2 = r11
        L_0x01a2:
            r3.close()     // Catch:{ all -> 0x01a6 }
            goto L_0x01bd
        L_0x01a6:
            r11 = move-exception
            org.telegram.messenger.FileLog.e((java.lang.Throwable) r11)
            goto L_0x01bd
        L_0x01ab:
            r12 = move-exception
        L_0x01ac:
            org.telegram.messenger.FileLog.e((java.lang.Throwable) r12)     // Catch:{ all -> 0x01b3 }
            r3.close()     // Catch:{ all -> 0x01a6 }
            goto L_0x01bd
        L_0x01b3:
            r11 = move-exception
            r3.close()     // Catch:{ all -> 0x01b8 }
            goto L_0x01bc
        L_0x01b8:
            r12 = move-exception
            org.telegram.messenger.FileLog.e((java.lang.Throwable) r12)
        L_0x01bc:
            throw r11
        L_0x01bd:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: org.telegram.messenger.ImageLoader.loadBitmap(java.lang.String, android.net.Uri, float, float, boolean):android.graphics.Bitmap");
    }

    public static void fillPhotoSizeWithBytes(TLRPC$PhotoSize tLRPC$PhotoSize) {
        if (tLRPC$PhotoSize != null) {
            byte[] bArr = tLRPC$PhotoSize.bytes;
            if (bArr == null || bArr.length == 0) {
                try {
                    RandomAccessFile randomAccessFile = new RandomAccessFile(FileLoader.getInstance(UserConfig.selectedAccount).getPathToAttach(tLRPC$PhotoSize, true), "r");
                    if (((int) randomAccessFile.length()) < 20000) {
                        byte[] bArr2 = new byte[((int) randomAccessFile.length())];
                        tLRPC$PhotoSize.bytes = bArr2;
                        randomAccessFile.readFully(bArr2, 0, bArr2.length);
                    }
                } catch (Throwable th) {
                    FileLog.e(th);
                }
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x009a  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x009f  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00bd  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00cd  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00e3  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static org.telegram.tgnet.TLRPC$PhotoSize scaleAndSaveImageInternal(org.telegram.tgnet.TLRPC$PhotoSize r2, android.graphics.Bitmap r3, android.graphics.Bitmap.CompressFormat r4, boolean r5, int r6, int r7, float r8, float r9, float r10, int r11, boolean r12, boolean r13, boolean r14) throws java.lang.Exception {
        /*
            r5 = 1065353216(0x3var_, float:1.0)
            int r5 = (r10 > r5 ? 1 : (r10 == r5 ? 0 : -1))
            if (r5 > 0) goto L_0x000b
            if (r13 == 0) goto L_0x0009
            goto L_0x000b
        L_0x0009:
            r5 = r3
            goto L_0x0010
        L_0x000b:
            r5 = 1
            android.graphics.Bitmap r5 = org.telegram.messenger.Bitmaps.createScaledBitmap(r3, r6, r7, r5)
        L_0x0010:
            r6 = 0
            r7 = -2147483648(0xfffffffvar_, double:NaN)
            if (r2 == 0) goto L_0x0020
            org.telegram.tgnet.TLRPC$FileLocation r9 = r2.location
            boolean r10 = r9 instanceof org.telegram.tgnet.TLRPC$TL_fileLocationToBeDeprecated
            if (r10 != 0) goto L_0x001d
            goto L_0x0020
        L_0x001d:
            org.telegram.tgnet.TLRPC$TL_fileLocationToBeDeprecated r9 = (org.telegram.tgnet.TLRPC$TL_fileLocationToBeDeprecated) r9
            goto L_0x007a
        L_0x0020:
            org.telegram.tgnet.TLRPC$TL_fileLocationToBeDeprecated r9 = new org.telegram.tgnet.TLRPC$TL_fileLocationToBeDeprecated
            r9.<init>()
            r9.volume_id = r7
            r2 = -2147483648(0xfffffffvar_, float:-0.0)
            r9.dc_id = r2
            int r2 = org.telegram.messenger.SharedConfig.getLastLocalId()
            r9.local_id = r2
            byte[] r2 = new byte[r6]
            r9.file_reference = r2
            org.telegram.tgnet.TLRPC$TL_photoSize_layer127 r2 = new org.telegram.tgnet.TLRPC$TL_photoSize_layer127
            r2.<init>()
            r2.location = r9
            int r10 = r5.getWidth()
            r2.w = r10
            int r10 = r5.getHeight()
            r2.h = r10
            int r13 = r2.w
            r0 = 100
            if (r13 > r0) goto L_0x0055
            if (r10 > r0) goto L_0x0055
            java.lang.String r10 = "s"
            r2.type = r10
            goto L_0x007a
        L_0x0055:
            r0 = 320(0x140, float:4.48E-43)
            if (r13 > r0) goto L_0x0060
            if (r10 > r0) goto L_0x0060
            java.lang.String r10 = "m"
            r2.type = r10
            goto L_0x007a
        L_0x0060:
            r0 = 800(0x320, float:1.121E-42)
            if (r13 > r0) goto L_0x006b
            if (r10 > r0) goto L_0x006b
            java.lang.String r10 = "x"
            r2.type = r10
            goto L_0x007a
        L_0x006b:
            r0 = 1280(0x500, float:1.794E-42)
            if (r13 > r0) goto L_0x0076
            if (r10 > r0) goto L_0x0076
            java.lang.String r10 = "y"
            r2.type = r10
            goto L_0x007a
        L_0x0076:
            java.lang.String r10 = "w"
            r2.type = r10
        L_0x007a:
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            long r0 = r9.volume_id
            r10.append(r0)
            java.lang.String r13 = "_"
            r10.append(r13)
            int r13 = r9.local_id
            r10.append(r13)
            java.lang.String r13 = ".jpg"
            r10.append(r13)
            java.lang.String r10 = r10.toString()
            r13 = 4
            if (r14 == 0) goto L_0x009f
            java.io.File r6 = org.telegram.messenger.FileLoader.getDirectory(r13)
            goto L_0x00ae
        L_0x009f:
            long r0 = r9.volume_id
            int r9 = (r0 > r7 ? 1 : (r0 == r7 ? 0 : -1))
            if (r9 == 0) goto L_0x00aa
            java.io.File r6 = org.telegram.messenger.FileLoader.getDirectory(r6)
            goto L_0x00ae
        L_0x00aa:
            java.io.File r6 = org.telegram.messenger.FileLoader.getDirectory(r13)
        L_0x00ae:
            java.io.File r7 = new java.io.File
            r7.<init>(r6, r10)
            java.io.FileOutputStream r6 = new java.io.FileOutputStream
            r6.<init>(r7)
            r5.compress(r4, r11, r6)
            if (r12 != 0) goto L_0x00c8
            java.nio.channels.FileChannel r7 = r6.getChannel()
            long r7 = r7.size()
            int r8 = (int) r7
            r2.size = r8
        L_0x00c8:
            r6.close()
            if (r12 == 0) goto L_0x00e1
            java.io.ByteArrayOutputStream r6 = new java.io.ByteArrayOutputStream
            r6.<init>()
            r5.compress(r4, r11, r6)
            byte[] r4 = r6.toByteArray()
            r2.bytes = r4
            int r4 = r4.length
            r2.size = r4
            r6.close()
        L_0x00e1:
            if (r5 == r3) goto L_0x00e6
            r5.recycle()
        L_0x00e6:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: org.telegram.messenger.ImageLoader.scaleAndSaveImageInternal(org.telegram.tgnet.TLRPC$PhotoSize, android.graphics.Bitmap, android.graphics.Bitmap$CompressFormat, boolean, int, int, float, float, float, int, boolean, boolean, boolean):org.telegram.tgnet.TLRPC$PhotoSize");
    }

    public static TLRPC$PhotoSize scaleAndSaveImage(Bitmap bitmap, float f, float f2, int i, boolean z) {
        return scaleAndSaveImage((TLRPC$PhotoSize) null, bitmap, Bitmap.CompressFormat.JPEG, false, f, f2, i, z, 0, 0, false);
    }

    public static TLRPC$PhotoSize scaleAndSaveImage(TLRPC$PhotoSize tLRPC$PhotoSize, Bitmap bitmap, float f, float f2, int i, boolean z, boolean z2) {
        return scaleAndSaveImage(tLRPC$PhotoSize, bitmap, Bitmap.CompressFormat.JPEG, false, f, f2, i, z, 0, 0, z2);
    }

    public static TLRPC$PhotoSize scaleAndSaveImage(Bitmap bitmap, float f, float f2, int i, boolean z, int i2, int i3) {
        return scaleAndSaveImage((TLRPC$PhotoSize) null, bitmap, Bitmap.CompressFormat.JPEG, false, f, f2, i, z, i2, i3, false);
    }

    public static TLRPC$PhotoSize scaleAndSaveImage(Bitmap bitmap, float f, float f2, boolean z, int i, boolean z2, int i2, int i3) {
        return scaleAndSaveImage((TLRPC$PhotoSize) null, bitmap, Bitmap.CompressFormat.JPEG, z, f, f2, i, z2, i2, i3, false);
    }

    public static TLRPC$PhotoSize scaleAndSaveImage(Bitmap bitmap, Bitmap.CompressFormat compressFormat, float f, float f2, int i, boolean z, int i2, int i3) {
        return scaleAndSaveImage((TLRPC$PhotoSize) null, bitmap, compressFormat, false, f, f2, i, z, i2, i3, false);
    }

    public static TLRPC$PhotoSize scaleAndSaveImage(TLRPC$PhotoSize tLRPC$PhotoSize, Bitmap bitmap, Bitmap.CompressFormat compressFormat, boolean z, float f, float f2, int i, boolean z2, int i2, int i3, boolean z3) {
        boolean z4;
        float f3;
        int i4;
        int i5;
        float f4;
        int i6 = i2;
        int i7 = i3;
        if (bitmap == null) {
            return null;
        }
        float width = (float) bitmap.getWidth();
        float height = (float) bitmap.getHeight();
        if (!(width == 0.0f || height == 0.0f)) {
            float max = Math.max(width / f, height / f2);
            if (!(i6 == 0 || i7 == 0)) {
                float f5 = (float) i6;
                if (width < f5 || height < ((float) i7)) {
                    if (width >= f5 || height <= ((float) i7)) {
                        if (width > f5) {
                            float f6 = (float) i7;
                            if (height < f6) {
                                f4 = height / f6;
                            }
                        }
                        f4 = Math.max(width / f5, height / ((float) i7));
                    } else {
                        f4 = width / f5;
                    }
                    f3 = f4;
                    z4 = true;
                    i4 = (int) (width / f3);
                    i5 = (int) (height / f3);
                    if (!(i5 == 0 || i4 == 0)) {
                        int i8 = i5;
                        int i9 = i4;
                        float f7 = height;
                        float f8 = width;
                        return scaleAndSaveImageInternal(tLRPC$PhotoSize, bitmap, compressFormat, z, i4, i5, width, height, f3, i, z2, z4, z3);
                    }
                }
            }
            f3 = max;
            z4 = false;
            i4 = (int) (width / f3);
            i5 = (int) (height / f3);
            int i82 = i5;
            int i92 = i4;
            float var_ = height;
            float var_ = width;
            try {
                return scaleAndSaveImageInternal(tLRPC$PhotoSize, bitmap, compressFormat, z, i4, i5, width, height, f3, i, z2, z4, z3);
            } catch (Throwable th) {
                FileLog.e(th);
            }
        }
        return null;
    }

    public static String getHttpUrlExtension(String str, String str2) {
        String lastPathSegment = Uri.parse(str).getLastPathSegment();
        if (!TextUtils.isEmpty(lastPathSegment) && lastPathSegment.length() > 1) {
            str = lastPathSegment;
        }
        int lastIndexOf = str.lastIndexOf(46);
        String substring = lastIndexOf != -1 ? str.substring(lastIndexOf + 1) : null;
        return (substring == null || substring.length() == 0 || substring.length() > 4) ? str2 : substring;
    }

    public static void saveMessageThumbs(TLRPC$Message tLRPC$Message) {
        TLRPC$PhotoSize findPhotoCachedSize;
        byte[] bArr;
        if (tLRPC$Message.media != null && (findPhotoCachedSize = findPhotoCachedSize(tLRPC$Message)) != null && (bArr = findPhotoCachedSize.bytes) != null && bArr.length != 0) {
            TLRPC$FileLocation tLRPC$FileLocation = findPhotoCachedSize.location;
            if (tLRPC$FileLocation == null || (tLRPC$FileLocation instanceof TLRPC$TL_fileLocationUnavailable)) {
                TLRPC$TL_fileLocationToBeDeprecated tLRPC$TL_fileLocationToBeDeprecated = new TLRPC$TL_fileLocationToBeDeprecated();
                findPhotoCachedSize.location = tLRPC$TL_fileLocationToBeDeprecated;
                tLRPC$TL_fileLocationToBeDeprecated.volume_id = -2147483648L;
                tLRPC$TL_fileLocationToBeDeprecated.local_id = SharedConfig.getLastLocalId();
            }
            boolean z = true;
            File pathToAttach = FileLoader.getInstance(UserConfig.selectedAccount).getPathToAttach(findPhotoCachedSize, true);
            int i = 0;
            if (MessageObject.shouldEncryptPhotoOrVideo(tLRPC$Message)) {
                pathToAttach = new File(pathToAttach.getAbsolutePath() + ".enc");
            } else {
                z = false;
            }
            if (!pathToAttach.exists()) {
                if (z) {
                    try {
                        File internalCacheDir = FileLoader.getInternalCacheDir();
                        RandomAccessFile randomAccessFile = new RandomAccessFile(new File(internalCacheDir, pathToAttach.getName() + ".key"), "rws");
                        long length = randomAccessFile.length();
                        byte[] bArr2 = new byte[32];
                        byte[] bArr3 = new byte[16];
                        if (length <= 0 || length % 48 != 0) {
                            Utilities.random.nextBytes(bArr2);
                            Utilities.random.nextBytes(bArr3);
                            randomAccessFile.write(bArr2);
                            randomAccessFile.write(bArr3);
                        } else {
                            randomAccessFile.read(bArr2, 0, 32);
                            randomAccessFile.read(bArr3, 0, 16);
                        }
                        randomAccessFile.close();
                        byte[] bArr4 = findPhotoCachedSize.bytes;
                        Utilities.aesCtrDecryptionByteArray(bArr4, bArr2, bArr3, 0, (long) bArr4.length, 0);
                    } catch (Exception e) {
                        FileLog.e((Throwable) e);
                    }
                }
                RandomAccessFile randomAccessFile2 = new RandomAccessFile(pathToAttach, "rws");
                randomAccessFile2.write(findPhotoCachedSize.bytes);
                randomAccessFile2.close();
            }
            TLRPC$TL_photoSize_layer127 tLRPC$TL_photoSize_layer127 = new TLRPC$TL_photoSize_layer127();
            tLRPC$TL_photoSize_layer127.w = findPhotoCachedSize.w;
            tLRPC$TL_photoSize_layer127.h = findPhotoCachedSize.h;
            tLRPC$TL_photoSize_layer127.location = findPhotoCachedSize.location;
            tLRPC$TL_photoSize_layer127.size = findPhotoCachedSize.size;
            tLRPC$TL_photoSize_layer127.type = findPhotoCachedSize.type;
            TLRPC$MessageMedia tLRPC$MessageMedia = tLRPC$Message.media;
            if (tLRPC$MessageMedia instanceof TLRPC$TL_messageMediaPhoto) {
                int size = tLRPC$MessageMedia.photo.sizes.size();
                while (i < size) {
                    if (tLRPC$Message.media.photo.sizes.get(i) instanceof TLRPC$TL_photoCachedSize) {
                        tLRPC$Message.media.photo.sizes.set(i, tLRPC$TL_photoSize_layer127);
                        return;
                    }
                    i++;
                }
            } else if (tLRPC$MessageMedia instanceof TLRPC$TL_messageMediaDocument) {
                int size2 = tLRPC$MessageMedia.document.thumbs.size();
                while (i < size2) {
                    if (tLRPC$Message.media.document.thumbs.get(i) instanceof TLRPC$TL_photoCachedSize) {
                        tLRPC$Message.media.document.thumbs.set(i, tLRPC$TL_photoSize_layer127);
                        return;
                    }
                    i++;
                }
            } else if (tLRPC$MessageMedia instanceof TLRPC$TL_messageMediaWebPage) {
                int size3 = tLRPC$MessageMedia.webpage.photo.sizes.size();
                while (i < size3) {
                    if (tLRPC$Message.media.webpage.photo.sizes.get(i) instanceof TLRPC$TL_photoCachedSize) {
                        tLRPC$Message.media.webpage.photo.sizes.set(i, tLRPC$TL_photoSize_layer127);
                        return;
                    }
                    i++;
                }
            }
        }
    }

    private static TLRPC$PhotoSize findPhotoCachedSize(TLRPC$Message tLRPC$Message) {
        TLRPC$PhotoSize tLRPC$PhotoSize;
        TLRPC$Photo tLRPC$Photo;
        TLRPC$MessageMedia tLRPC$MessageMedia = tLRPC$Message.media;
        int i = 0;
        if (tLRPC$MessageMedia instanceof TLRPC$TL_messageMediaPhoto) {
            int size = tLRPC$MessageMedia.photo.sizes.size();
            while (i < size) {
                tLRPC$PhotoSize = tLRPC$Message.media.photo.sizes.get(i);
                if (!(tLRPC$PhotoSize instanceof TLRPC$TL_photoCachedSize)) {
                    i++;
                }
            }
            return null;
        } else if (tLRPC$MessageMedia instanceof TLRPC$TL_messageMediaDocument) {
            int size2 = tLRPC$MessageMedia.document.thumbs.size();
            while (i < size2) {
                tLRPC$PhotoSize = tLRPC$Message.media.document.thumbs.get(i);
                if (!(tLRPC$PhotoSize instanceof TLRPC$TL_photoCachedSize)) {
                    i++;
                }
            }
            return null;
        } else if (!(tLRPC$MessageMedia instanceof TLRPC$TL_messageMediaWebPage) || (tLRPC$Photo = tLRPC$MessageMedia.webpage.photo) == null) {
            return null;
        } else {
            int size3 = tLRPC$Photo.sizes.size();
            while (i < size3) {
                tLRPC$PhotoSize = tLRPC$Message.media.webpage.photo.sizes.get(i);
                if (!(tLRPC$PhotoSize instanceof TLRPC$TL_photoCachedSize)) {
                    i++;
                }
            }
            return null;
        }
        return tLRPC$PhotoSize;
    }

    public static void saveMessagesThumbs(ArrayList<TLRPC$Message> arrayList) {
        if (arrayList != null && !arrayList.isEmpty()) {
            for (int i = 0; i < arrayList.size(); i++) {
                saveMessageThumbs(arrayList.get(i));
            }
        }
    }

    public static MessageThumb generateMessageThumb(TLRPC$Message tLRPC$Message) {
        int i;
        int i2;
        Bitmap strippedPhotoBitmap;
        byte[] bArr;
        TLRPC$Message tLRPC$Message2 = tLRPC$Message;
        TLRPC$PhotoSize findPhotoCachedSize = findPhotoCachedSize(tLRPC$Message);
        if (findPhotoCachedSize == null || (bArr = findPhotoCachedSize.bytes) == null || bArr.length == 0) {
            TLRPC$MessageMedia tLRPC$MessageMedia = tLRPC$Message2.media;
            if (tLRPC$MessageMedia instanceof TLRPC$TL_messageMediaDocument) {
                int size = tLRPC$MessageMedia.document.thumbs.size();
                for (int i3 = 0; i3 < size; i3++) {
                    TLRPC$PhotoSize tLRPC$PhotoSize = tLRPC$Message2.media.document.thumbs.get(i3);
                    if (tLRPC$PhotoSize instanceof TLRPC$TL_photoStrippedSize) {
                        TLRPC$PhotoSize closestPhotoSizeWithSize = FileLoader.getClosestPhotoSizeWithSize(tLRPC$Message2.media.document.thumbs, 320);
                        if (closestPhotoSizeWithSize == null) {
                            int i4 = 0;
                            while (true) {
                                if (i4 >= tLRPC$Message2.media.document.attributes.size()) {
                                    i2 = 0;
                                    i = 0;
                                    break;
                                } else if (tLRPC$Message2.media.document.attributes.get(i4) instanceof TLRPC$TL_documentAttributeVideo) {
                                    TLRPC$TL_documentAttributeVideo tLRPC$TL_documentAttributeVideo = (TLRPC$TL_documentAttributeVideo) tLRPC$Message2.media.document.attributes.get(i4);
                                    i = tLRPC$TL_documentAttributeVideo.h;
                                    i2 = tLRPC$TL_documentAttributeVideo.w;
                                    break;
                                } else {
                                    i4++;
                                }
                            }
                        } else {
                            i = closestPhotoSizeWithSize.h;
                            i2 = closestPhotoSizeWithSize.w;
                        }
                        org.telegram.ui.Components.Point messageSize = ChatMessageCell.getMessageSize(i2, i);
                        String format = String.format(Locale.US, "%s_false@%d_%d_b", new Object[]{ImageLocation.getStrippedKey(tLRPC$Message2, tLRPC$Message2, tLRPC$PhotoSize), Integer.valueOf((int) (messageSize.x / AndroidUtilities.density)), Integer.valueOf((int) (messageSize.y / AndroidUtilities.density))});
                        if (!getInstance().isInMemCache(format, false) && (strippedPhotoBitmap = getStrippedPhotoBitmap(tLRPC$PhotoSize.bytes, (String) null)) != null) {
                            Utilities.blurBitmap(strippedPhotoBitmap, 3, 1, strippedPhotoBitmap.getWidth(), strippedPhotoBitmap.getHeight(), strippedPhotoBitmap.getRowBytes());
                            float f = messageSize.x;
                            float f2 = AndroidUtilities.density;
                            Bitmap createScaledBitmap = Bitmaps.createScaledBitmap(strippedPhotoBitmap, (int) (f / f2), (int) (messageSize.y / f2), true);
                            if (createScaledBitmap != strippedPhotoBitmap) {
                                strippedPhotoBitmap.recycle();
                                strippedPhotoBitmap = createScaledBitmap;
                            }
                            return new MessageThumb(format, new BitmapDrawable(strippedPhotoBitmap));
                        }
                    }
                }
            }
        } else {
            File pathToAttach = FileLoader.getInstance(UserConfig.selectedAccount).getPathToAttach(findPhotoCachedSize, true);
            TLRPC$TL_photoSize_layer127 tLRPC$TL_photoSize_layer127 = new TLRPC$TL_photoSize_layer127();
            tLRPC$TL_photoSize_layer127.w = findPhotoCachedSize.w;
            tLRPC$TL_photoSize_layer127.h = findPhotoCachedSize.h;
            tLRPC$TL_photoSize_layer127.location = findPhotoCachedSize.location;
            tLRPC$TL_photoSize_layer127.size = findPhotoCachedSize.size;
            tLRPC$TL_photoSize_layer127.type = findPhotoCachedSize.type;
            if (pathToAttach.exists() && tLRPC$Message2.grouped_id == 0) {
                org.telegram.ui.Components.Point messageSize2 = ChatMessageCell.getMessageSize(findPhotoCachedSize.w, findPhotoCachedSize.h);
                String format2 = String.format(Locale.US, "%d_%d@%d_%d_b", new Object[]{Long.valueOf(findPhotoCachedSize.location.volume_id), Integer.valueOf(findPhotoCachedSize.location.local_id), Integer.valueOf((int) (messageSize2.x / AndroidUtilities.density)), Integer.valueOf((int) (messageSize2.y / AndroidUtilities.density))});
                if (!getInstance().isInMemCache(format2, false)) {
                    String path = pathToAttach.getPath();
                    float f3 = messageSize2.x;
                    float f4 = AndroidUtilities.density;
                    Bitmap loadBitmap = loadBitmap(path, (Uri) null, (float) ((int) (f3 / f4)), (float) ((int) (messageSize2.y / f4)), false);
                    if (loadBitmap != null) {
                        Utilities.blurBitmap(loadBitmap, 3, 1, loadBitmap.getWidth(), loadBitmap.getHeight(), loadBitmap.getRowBytes());
                        float f5 = messageSize2.x;
                        float f6 = AndroidUtilities.density;
                        Bitmap createScaledBitmap2 = Bitmaps.createScaledBitmap(loadBitmap, (int) (f5 / f6), (int) (messageSize2.y / f6), true);
                        if (createScaledBitmap2 != loadBitmap) {
                            loadBitmap.recycle();
                            loadBitmap = createScaledBitmap2;
                        }
                        return new MessageThumb(format2, new BitmapDrawable(loadBitmap));
                    }
                }
            }
        }
        return null;
    }

    public void onFragmentStackChanged() {
        for (int i = 0; i < this.cachedAnimatedFileDrawables.size(); i++) {
            this.cachedAnimatedFileDrawables.get(i).repeatCount = 0;
        }
    }

    public DispatchQueue getCacheOutQueue() {
        return this.cacheOutQueue;
    }

    public static class MessageThumb {
        BitmapDrawable drawable;
        String key;

        public MessageThumb(String str, BitmapDrawable bitmapDrawable) {
            this.key = str;
            this.drawable = bitmapDrawable;
        }
    }
}
