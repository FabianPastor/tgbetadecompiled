package org.telegram.messenger;

public final /* synthetic */ class UserConfig$$ExternalSyntheticLambda1 implements Runnable {
    public final /* synthetic */ UserConfig f$0;
    public final /* synthetic */ boolean f$1;

    public /* synthetic */ UserConfig$$ExternalSyntheticLambda1(UserConfig userConfig, boolean z) {
        this.f$0 = userConfig;
        this.f$1 = z;
    }

    public final void run() {
        this.f$0.lambda$saveConfig$0(this.f$1);
    }
}
