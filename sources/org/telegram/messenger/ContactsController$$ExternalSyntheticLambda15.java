package org.telegram.messenger;

public final /* synthetic */ class ContactsController$$ExternalSyntheticLambda15 implements Runnable {
    public final /* synthetic */ ContactsController f$0;
    public final /* synthetic */ Long f$1;

    public /* synthetic */ ContactsController$$ExternalSyntheticLambda15(ContactsController contactsController, Long l) {
        this.f$0 = contactsController;
        this.f$1 = l;
    }

    public final void run() {
        this.f$0.lambda$applyContactsUpdates$46(this.f$1);
    }
}
