package org.telegram.messenger;

public final /* synthetic */ class MediaDataController$$ExternalSyntheticLambda31 implements Runnable {
    public final /* synthetic */ MediaDataController f$0;
    public final /* synthetic */ long f$1;

    public /* synthetic */ MediaDataController$$ExternalSyntheticLambda31(MediaDataController mediaDataController, long j) {
        this.f$0 = mediaDataController;
        this.f$1 = j;
    }

    public final void run() {
        this.f$0.lambda$markFaturedStickersByIdAsRead$49(this.f$1);
    }
}
