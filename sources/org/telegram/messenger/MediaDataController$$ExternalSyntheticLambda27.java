package org.telegram.messenger;

public final /* synthetic */ class MediaDataController$$ExternalSyntheticLambda27 implements Runnable {
    public final /* synthetic */ MediaDataController f$0;
    public final /* synthetic */ int f$1;
    public final /* synthetic */ boolean f$2;

    public /* synthetic */ MediaDataController$$ExternalSyntheticLambda27(MediaDataController mediaDataController, int i, boolean z) {
        this.f$0 = mediaDataController;
        this.f$1 = i;
        this.f$2 = z;
    }

    public final void run() {
        this.f$0.lambda$loadStickers$65(this.f$1, this.f$2);
    }
}
