package org.telegram.messenger.camera;

public final /* synthetic */ class CameraController$$ExternalSyntheticLambda12 implements Runnable {
    public final /* synthetic */ CameraController f$0;
    public final /* synthetic */ boolean f$1;
    public final /* synthetic */ Exception f$2;
    public final /* synthetic */ Runnable f$3;

    public /* synthetic */ CameraController$$ExternalSyntheticLambda12(CameraController cameraController, boolean z, Exception exc, Runnable runnable) {
        this.f$0 = cameraController;
        this.f$1 = z;
        this.f$2 = exc;
        this.f$3 = runnable;
    }

    public final void run() {
        this.f$0.lambda$initCamera$3(this.f$1, this.f$2, this.f$3);
    }
}
