package org.telegram.messenger.camera;

public final /* synthetic */ class CameraController$$ExternalSyntheticLambda13 implements Runnable {
    public final /* synthetic */ CameraController f$0;
    public final /* synthetic */ boolean f$1;
    public final /* synthetic */ Runnable f$2;

    public /* synthetic */ CameraController$$ExternalSyntheticLambda13(CameraController cameraController, boolean z, Runnable runnable) {
        this.f$0 = cameraController;
        this.f$1 = z;
        this.f$2 = runnable;
    }

    public final void run() {
        this.f$0.lambda$initCamera$4(this.f$1, this.f$2);
    }
}
