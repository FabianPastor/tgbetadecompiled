package org.telegram.messenger.camera;

public final /* synthetic */ class CameraController$$ExternalSyntheticLambda3 implements Runnable {
    public final /* synthetic */ CameraController f$0;

    public /* synthetic */ CameraController$$ExternalSyntheticLambda3(CameraController cameraController) {
        this.f$0 = cameraController;
    }

    public final void run() {
        this.f$0.finishRecordingVideo();
    }
}
