package org.telegram.messenger.camera;

public final /* synthetic */ class CameraController$$ExternalSyntheticLambda8 implements Runnable {
    public final /* synthetic */ CameraController f$0;
    public final /* synthetic */ Runnable f$1;

    public /* synthetic */ CameraController$$ExternalSyntheticLambda8(CameraController cameraController, Runnable runnable) {
        this.f$0 = cameraController;
        this.f$1 = runnable;
    }

    public final void run() {
        this.f$0.lambda$initCamera$2(this.f$1);
    }
}
