package org.telegram.messenger.camera;

import java.util.Comparator;

public final /* synthetic */ class CameraController$$ExternalSyntheticLambda17 implements Comparator {
    public static final /* synthetic */ CameraController$$ExternalSyntheticLambda17 INSTANCE = new CameraController$$ExternalSyntheticLambda17();

    private /* synthetic */ CameraController$$ExternalSyntheticLambda17() {
    }

    public final int compare(Object obj, Object obj2) {
        return CameraController.lambda$initCamera$0((Size) obj, (Size) obj2);
    }
}
