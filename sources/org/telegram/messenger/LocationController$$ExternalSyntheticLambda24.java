package org.telegram.messenger;

public final /* synthetic */ class LocationController$$ExternalSyntheticLambda24 implements Runnable {
    public final /* synthetic */ LocationController f$0;
    public final /* synthetic */ boolean f$1;

    public /* synthetic */ LocationController$$ExternalSyntheticLambda24(LocationController locationController, boolean z) {
        this.f$0 = locationController;
        this.f$1 = z;
    }

    public final void run() {
        this.f$0.lambda$startLocationLookupForPeopleNearby$26(this.f$1);
    }
}
