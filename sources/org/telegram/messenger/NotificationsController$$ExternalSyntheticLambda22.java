package org.telegram.messenger;

public final /* synthetic */ class NotificationsController$$ExternalSyntheticLambda22 implements Runnable {
    public final /* synthetic */ NotificationsController f$0;
    public final /* synthetic */ long f$1;

    public /* synthetic */ NotificationsController$$ExternalSyntheticLambda22(NotificationsController notificationsController, long j) {
        this.f$0 = notificationsController;
        this.f$1 = j;
    }

    public final void run() {
        this.f$0.lambda$setOpenedDialogId$2(this.f$1);
    }
}
