package org.telegram.messenger;

public final /* synthetic */ class LocationController$$ExternalSyntheticLambda12 implements Runnable {
    public final /* synthetic */ LocationController f$0;
    public final /* synthetic */ long f$1;

    public /* synthetic */ LocationController$$ExternalSyntheticLambda12(LocationController locationController, long j) {
        this.f$0 = locationController;
        this.f$1 = j;
    }

    public final void run() {
        this.f$0.lambda$removeSharingLocation$22(this.f$1);
    }
}
