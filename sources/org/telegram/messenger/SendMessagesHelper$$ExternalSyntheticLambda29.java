package org.telegram.messenger;

public final /* synthetic */ class SendMessagesHelper$$ExternalSyntheticLambda29 implements Runnable {
    public final /* synthetic */ SendMessagesHelper f$0;
    public final /* synthetic */ String f$1;
    public final /* synthetic */ Runnable f$2;

    public /* synthetic */ SendMessagesHelper$$ExternalSyntheticLambda29(SendMessagesHelper sendMessagesHelper, String str, Runnable runnable) {
        this.f$0 = sendMessagesHelper;
        this.f$1 = str;
        this.f$2 = runnable;
    }

    public final void run() {
        this.f$0.lambda$sendVote$20(this.f$1, this.f$2);
    }
}
