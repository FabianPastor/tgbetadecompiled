package org.webrtc;

import java.util.concurrent.Callable;

public final /* synthetic */ class TextureBufferImpl$$ExternalSyntheticLambda2 implements Callable {
    public final /* synthetic */ TextureBufferImpl f$0;

    public /* synthetic */ TextureBufferImpl$$ExternalSyntheticLambda2(TextureBufferImpl textureBufferImpl) {
        this.f$0 = textureBufferImpl;
    }

    public final Object call() {
        return this.f$0.lambda$toI420$1();
    }
}
