package org.webrtc;

import org.webrtc.TextureBufferImpl;

public final /* synthetic */ class TextureBufferImpl$$ExternalSyntheticLambda1 implements Runnable {
    public final /* synthetic */ TextureBufferImpl f$0;
    public final /* synthetic */ TextureBufferImpl.RefCountMonitor f$1;

    public /* synthetic */ TextureBufferImpl$$ExternalSyntheticLambda1(TextureBufferImpl textureBufferImpl, TextureBufferImpl.RefCountMonitor refCountMonitor) {
        this.f$0 = textureBufferImpl;
        this.f$1 = refCountMonitor;
    }

    public final void run() {
        this.f$0.lambda$new$0(this.f$1);
    }
}
