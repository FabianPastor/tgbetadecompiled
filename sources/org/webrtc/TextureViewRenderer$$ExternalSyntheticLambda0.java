package org.webrtc;

public final /* synthetic */ class TextureViewRenderer$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ TextureViewRenderer f$0;
    public final /* synthetic */ int f$1;
    public final /* synthetic */ int f$2;

    public /* synthetic */ TextureViewRenderer$$ExternalSyntheticLambda0(TextureViewRenderer textureViewRenderer, int i, int i2) {
        this.f$0 = textureViewRenderer;
        this.f$1 = i;
        this.f$2 = i2;
    }

    public final void run() {
        this.f$0.lambda$updateVideoSizes$1(this.f$1, this.f$2);
    }
}
