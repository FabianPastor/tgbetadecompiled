package org.webrtc;

public final /* synthetic */ class EglRenderer$$ExternalSyntheticLambda1 implements Runnable {
    public final /* synthetic */ EglRenderer f$0;
    public final /* synthetic */ float f$1;
    public final /* synthetic */ float f$2;
    public final /* synthetic */ float f$3;
    public final /* synthetic */ float f$4;

    public /* synthetic */ EglRenderer$$ExternalSyntheticLambda1(EglRenderer eglRenderer, float f, float f2, float f3, float f4) {
        this.f$0 = eglRenderer;
        this.f$1 = f;
        this.f$2 = f2;
        this.f$3 = f3;
        this.f$4 = f4;
    }

    public final void run() {
        this.f$0.lambda$clearImage$6(this.f$1, this.f$2, this.f$3, this.f$4);
    }
}
