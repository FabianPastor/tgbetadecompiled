package org.webrtc;

public final /* synthetic */ class SurfaceTextureHelper$$ExternalSyntheticLambda6 implements Runnable {
    public final /* synthetic */ SurfaceTextureHelper f$0;
    public final /* synthetic */ int f$1;
    public final /* synthetic */ int f$2;

    public /* synthetic */ SurfaceTextureHelper$$ExternalSyntheticLambda6(SurfaceTextureHelper surfaceTextureHelper, int i, int i2) {
        this.f$0 = surfaceTextureHelper;
        this.f$1 = i;
        this.f$2 = i2;
    }

    public final void run() {
        this.f$0.lambda$setTextureSize$2(this.f$1, this.f$2);
    }
}
