package org.webrtc;

public final /* synthetic */ class SurfaceTextureHelper$$ExternalSyntheticLambda5 implements Runnable {
    public final /* synthetic */ SurfaceTextureHelper f$0;
    public final /* synthetic */ int f$1;

    public /* synthetic */ SurfaceTextureHelper$$ExternalSyntheticLambda5(SurfaceTextureHelper surfaceTextureHelper, int i) {
        this.f$0 = surfaceTextureHelper;
        this.f$1 = i;
    }

    public final void run() {
        this.f$0.lambda$setFrameRotation$4(this.f$1);
    }
}
