package j$.util.function;

public interface ToIntFunction<T> {
    int applyAsInt(T t);
}
