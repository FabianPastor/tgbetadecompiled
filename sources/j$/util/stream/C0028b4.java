package j$.util.stream;

import java.util.Map;

/* renamed from: j$.util.stream.b4  reason: case insensitive filesystem */
class CLASSNAMEb4 {
    final Map a;

    CLASSNAMEb4(Map map) {
        this.a = map;
    }

    /* access modifiers changed from: package-private */
    public CLASSNAMEb4 a(CLASSNAMEc4 c4Var) {
        this.a.put(c4Var, 2);
        return this;
    }

    /* access modifiers changed from: package-private */
    public CLASSNAMEb4 b(CLASSNAMEc4 c4Var) {
        this.a.put(c4Var, 1);
        return this;
    }

    /* access modifiers changed from: package-private */
    public CLASSNAMEb4 c(CLASSNAMEc4 c4Var) {
        this.a.put(c4Var, 3);
        return this;
    }
}
