package j$.util.stream;

/* renamed from: j$.util.stream.k1  reason: case insensitive filesystem */
enum CLASSNAMEk1 {
    ANY(true, true),
    ALL(false, false),
    NONE(true, false);
    
    /* access modifiers changed from: private */
    public final boolean a;
    /* access modifiers changed from: private */
    public final boolean b;

    private CLASSNAMEk1(boolean z, boolean z2) {
        this.a = z;
        this.b = z2;
    }
}
