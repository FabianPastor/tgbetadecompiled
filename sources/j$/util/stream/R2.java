package j$.util.stream;

import j$.util.CLASSNAMEl;
import j$.util.function.Consumer;
import j$.util.function.o;
import j$.util.function.p;
import j$.util.function.q;

class R2 implements S2, CLASSNAMEl3 {
    private boolean a;
    private long b;
    final /* synthetic */ o c;

    R2(o oVar) {
        this.c = oVar;
    }

    public /* synthetic */ void accept(double d) {
        CLASSNAMEo1.f(this);
        throw null;
    }

    public /* synthetic */ void accept(int i) {
        CLASSNAMEo1.d(this);
        throw null;
    }

    public void accept(long j) {
        if (this.a) {
            this.a = false;
        } else {
            j = this.c.applyAsLong(this.b, j);
        }
        this.b = j;
    }

    public /* synthetic */ Consumer andThen(Consumer consumer) {
        return Consumer.CC.$default$andThen(this, consumer);
    }

    /* renamed from: b */
    public /* synthetic */ void accept(Long l) {
        CLASSNAMEo1.c(this, l);
    }

    public q f(q qVar) {
        qVar.getClass();
        return new p(this, qVar);
    }

    public Object get() {
        return this.a ? CLASSNAMEl.a() : CLASSNAMEl.d(this.b);
    }

    public void h(S2 s2) {
        R2 r2 = (R2) s2;
        if (!r2.a) {
            accept(r2.b);
        }
    }

    public /* synthetic */ void m() {
    }

    public void n(long j) {
        this.a = true;
        this.b = 0;
    }

    public /* synthetic */ boolean o() {
        return false;
    }
}
