package j$.util.stream;

import j$.util.function.Consumer;
import j$.util.function.k;
import j$.util.function.l;

/* renamed from: j$.util.stream.p4  reason: case insensitive filesystem */
public final /* synthetic */ class CLASSNAMEp4 implements CLASSNAMEk3 {
    public final /* synthetic */ int a = 0;
    public final /* synthetic */ Object b;

    public /* synthetic */ CLASSNAMEp4(l lVar) {
        this.b = lVar;
    }

    public /* synthetic */ void accept(double d) {
        switch (this.a) {
            case 0:
                CLASSNAMEo1.f(this);
                throw null;
            default:
                CLASSNAMEo1.f(this);
                throw null;
        }
    }

    public /* synthetic */ Consumer andThen(Consumer consumer) {
        switch (this.a) {
            case 0:
                return Consumer.CC.$default$andThen(this, consumer);
            default:
                return Consumer.CC.$default$andThen(this, consumer);
        }
    }

    public /* synthetic */ void b(Integer num) {
        switch (this.a) {
            case 0:
                CLASSNAMEo1.b(this, num);
                return;
            default:
                CLASSNAMEo1.b(this, num);
                return;
        }
    }

    public l l(l lVar) {
        switch (this.a) {
            case 0:
                lVar.getClass();
                return new k(this, lVar);
            default:
                lVar.getClass();
                return new k(this, lVar);
        }
    }

    public /* synthetic */ void m() {
    }

    public /* synthetic */ void n(long j) {
    }

    public /* synthetic */ boolean o() {
        return false;
    }

    public /* synthetic */ CLASSNAMEp4(W3 w3) {
        this.b = w3;
    }

    public final void accept(int i) {
        switch (this.a) {
            case 0:
                ((l) this.b).accept(i);
                return;
            default:
                ((W3) this.b).accept(i);
                return;
        }
    }

    public /* synthetic */ void accept(long j) {
        switch (this.a) {
            case 0:
                CLASSNAMEo1.e(this);
                throw null;
            default:
                CLASSNAMEo1.e(this);
                throw null;
        }
    }

    public /* bridge */ /* synthetic */ void accept(Object obj) {
        switch (this.a) {
            case 0:
                b((Integer) obj);
                return;
            default:
                b((Integer) obj);
                return;
        }
    }
}
